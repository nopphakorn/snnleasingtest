<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="";

$sql_ctff="SELECT * FROM tb_content WHERE c_name='car'";
$Re_ctff=$mysqli->query($sql_ctff);
$row_Re_ctff=$Re_ctff->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="en">
<head><?php include 'include/inc_header.php';?></head>
<body>
	<div class="container-fluid"><?php include 's_header.php';?></div>

  <div class="container-fluid bg_top">
      <div class="container pt-4 mb-4">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="my-4 tx_blue"><img src="images/icon/icon_staff.png" alt=""> Staff only</h3>
            <hr class="hr_yellow">

            <div class="card border-0 bg-transparent">
              <div class="card-body">
									<div class="form_box_login">
										<form name="form_clogin" id="form_clogin" method="post" enctype="multipart/form-data">
											<div class="form-row"> 
												<div class="col-md-12 text-center mb-2">
													กรอกชื่อผู้ใช้งาน และรหัสผ่านเพื่อเข้าสู่ระบบ 
												</div>
												<div class="form-group col-md-12">
													<label for="c_username"><i class="fas fa-user"></i> Username</label>
													<input type="text" class="form-control" name="c_username" id="c_username" placeholder="กรอก Username">
													<div id="c_username_validate"></div>
												</div>
												<div class="form-group col-md-12">
													<label for="c_password"><i class="fas fa-key"></i> Password</label>
													<input type="password" class="form-control" name="c_password" id="c_password" placeholder="กรอก Password">
													<div id="c_password_validate"></div>
												</div>
												<div class="form-check mb-2">
													&nbsp;&nbsp;<input class="form-check-input" type="checkbox" value="" id="c_chk" name="c_chk">
													<label class="form-check-label" for="c_chk">
													<a href="#" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
													ฉันได้อ่านและยอมรับตามข้อตกลงและเงื่อนไข</a>
													</label>
													<span id="c_chk_validate"></span>
												</div>
											</div>												
											<button type="submit" id="submit" class="btn btn-primary">เข้าสู่ระบบ</button>
											<button type="reset" value="Reset" class="btn btn-secondary">ยกเลิก</button>
										</form>
									</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 mb-3">
								<div class="collapse" id="collapseExample">
									<div class="card card-body">
										<?php
										if(!empty($row_Re_ctff['c_detail'])){
											echo $row_Re_ctff['c_detail']; 
										}else{
											echo "ไม่มี ข้อตกลง & เงื่อนไข ในขณะนี้";
										}
										?>
									</div>
								</div>
							</div>
						</div>

          </div>
        </div>
      </div>
  </div>

  <?php include 's_footer.php';?>
  <?php include 'include/inc_script.php';?>
	<script type="text/javascript" src="library/validation/js/jquery.validate.js"></script>
  <script type="text/javascript" src="library/validation/js/additional-methods.js"></script>
  <script type="text/javascript" src="js/ajax_car.js"></script>
</body>
</html>
