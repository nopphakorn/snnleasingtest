<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="";

$sql_ctff="SELECT * FROM tb_content WHERE c_name='TFF'";
$Re_ctff=$mysqli->query($sql_ctff);
$row_Re_ctff=$Re_ctff->fetch_assoc();
?>      

<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'include/inc_header.php';?>
  <style>
		body {
			background:url(images/background/bg-tff.jpg) no-repeat;
			background-size:cover;
			background-attachment:fixed;
		}
	</style>
</head>
<body>  
  <div class="container-fluid m-0 p-0"><?php include 's_header.php';?></div>
  <div class="container mt-4 pt-4 pb-4">
    <div class="row">
      <div class="col-12">
        <h3 style="text-align:center">ข้อตกลงและเงื่อนไข</h3>
		<p style="text-align:center">สมาชิก SNN LEASING MEMBERSHIP CLUB</p>
        <hr>
        <div class="form_box">
          	<?php
			if(!empty($row_Re_ctff['c_detail'])){
				echo $row_Re_ctff['c_detail']; 
			}else{
				echo "ไม่มี ข้อตกลง & เงื่อนไข ในขณะนี้";
			}
			?>
        </div>
      </div> 
     </div> 
  </div>
	<?php include 's_footer.php';?>
  <?php include 'include/inc_script.php';?>
</body>
</html>
