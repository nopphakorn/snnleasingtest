<?php 
include 'tff_check.php';
include 'Connections/con_db.php';
include 'function/function.php';

$sql_tffa="SELECT * FROM tb_tff_article WHERE a_id='".$_GET['id']."'";
$Re_tffa=$mysqli->query($sql_tffa);
$row_Re_tffa=$Re_tffa->fetch_assoc();
$total_Re_tffa=$Re_tffa->num_rows;

$a_id = $row_Re_tffa['a_id'];
$counter = $row_Re_tffa['a_counter'];
$a_counter = $counter+1;

$sql_ins="UPDATE tb_tff_article SET a_counter='$a_counter' WHERE a_id='$a_id' ";
$Re_ins=$mysqli->query($sql_ins);
?>      

<!DOCTYPE html>
<html lang="en">
<head>
  	<?php include 'include/inc_header.php';?>
  	<meta property="og:locale" 			content="th_TH" />
	<meta property="og:url"           	content="http://www.snnleasing.com/tff_article_detail.php?id=<?php echo $row_Re_tffa['a_id']; ?>" />
	<meta property="og:type"          	content="article" />
	<meta property="og:title"         	content="<?php echo $row_Re_tffa['a_name']; ?>" />
	<meta property="og:description"   	content="<?php echo strip_tags($row_Re_tffa['a_detail']);?>" />
	<meta property="og:image"         	content="http://www.snnleasing.com/images/tff_article/<?php echo $row_Re_tffa['a_photo']; ?>" />
	<meta property="fb:app_id"         	content="106896906786738" />
</head>
<body>  
  	<div class="container-fluid m-0 p-0"><?php include 's_header.php';?></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-sm-3"><?php include 'tff_menu.php';?></div>
				<div class="col-sm-9 mt-3 mb-4">
					<div class="col-12">
						<h3 class="my-4 tx_blue"></h3>บทความดีๆ</h3>
						<hr class="hr_yellow">
						<p><a href="tff_article.php">บทความดีๆ</a> > <?php echo $row_Re_tffa['a_name']; ?></p>
						
						<?php if($row_Re_tffa['a_photo']!=""){?>
						<img class="img_fluid w-100" src="images/tff_article/<?php echo $row_Re_tffa['a_photo']; ?>" alt="">
						<?php } ?>
						<div class="form_box m-0 w-100">						
							<?php
							if(!empty($row_Re_tffa['a_detail'])){
								echo $row_Re_tffa['a_detail']; 
							}
							?>
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
	<?php include 's_footer.php';?>
  	<?php include 'include/inc_script.php';?>
</body>
</html>
