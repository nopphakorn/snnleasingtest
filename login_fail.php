<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="";
?>

<!DOCTYPE html>
<html lang="en">
<head><?php include 'include/inc_header.php';?></head>
<body>
  <div class="container-fluid m-0 p-0"><?php include 's_header.php';?></div>  

  <div class="container-fluid m-0 p-0 ">
      <div class="container p-3">
        
        <div class="row">
          <div class="col-lg-12">
            <h3><img class="rounded" src="images/logo/m_cont.png" width='40px'> 
            ล็อกอินเข้าสู่ระบบจัดการเว็บไซต์</h3>
            <hr class="ed_hr">

            <div class="alert alert-danger text-center" role="alert">
				<i class="fa fa-exclamation-triangle fa-5x" aria-hidden="true"></i><br><br>
				  ไม่สามารถเข้าสู่ระบบได้ กรุณาตรวจสอบ Username หรือ Password อีกครั้ง<br>
				  <a href="login.php" class="alert-link">Login</a> : ใหม่อีกครั้ง
            </div>
          </div>
        </div>
      </div>
  </div>

  <div class="container-fluid m-0 p-0 bg-dark"><?php include 's_footer.php';?></div>
  <?php include 'include/inc_script.php';?>
</body>
</html>
