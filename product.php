<?php 
include 'Connections/con_db.php';
include 'product_check.php';
include 'function/function.php';
$page_name="product";

$maxRows_Re_p = 12;
$pageNum_Re_p = 0;
if (isset($_GET['pageNum_Re_p'])) {
  $pageNum_Re_p = $_GET['pageNum_Re_p'];
}
$startRow_Re_p = (($pageNum_Re_p-1) * $maxRows_Re_p);
if($startRow_Re_p<0){$startRow_Re_p=0;}

$query_Re_p = "SELECT a.*, (SELECT pro_p_photo FROM tb_product_photo b WHERE b.pro_id=a.pro_id LIMIT 1 ) AS pro_p_photo ";
$query_Re_p.= "FROM tb_product a WHERE pro_status='1' ";

if(isset($_GET['brand_search']) AND $_GET['brand_search']!=""){
	$brand_search = mysqli_real_escape_string($mysqli, $_GET['brand_search']);
	$query_Re_p.= "AND pro_brand= '$brand_search' ";
}

$query_Re_p.= "ORDER BY a.pro_id DESC ";
$query_limit_Re_p = sprintf("%s LIMIT %d, %d", $query_Re_p, $startRow_Re_p, $maxRows_Re_p);
$Re_p=$mysqli->query($query_limit_Re_p);

if (isset($_GET['totalRows_Re_p'])) {
  $totalRows_Re_p = $_GET['totalRows_Re_p'];
} else {
  $all_Re_p=$mysqli->query($query_Re_p);
  $totalRows_Re_p=$all_Re_p->num_rows;
}
$totalPages_Re_p = ceil($totalRows_Re_p/$maxRows_Re_p);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
if(isset($_GET['brand_search']) AND $_GET['brand_search']!=""){
	$currentSent = "&brand_search=".$_GET['brand_search'];
}else{
	$currentSent = "";
}

if($pageNum_Re_p==0){$page=1;}
else if($pageNum_Re_p>0){$page=$pageNum_Re_p;}

?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
	<div class="container-fluid"><?php include 's_header.php';?></div>
	  
	 <div class="container-fluid bg_top">
      	<div class="container pt-4 mb-4">
        	<div class="row">
          		<div class="col-lg-12">
            		<h3 class="my-4 tx_blue">ทรัพย์สินรอขาย</h3>
            		<hr class="hr_yellow">

					<div class="row">
						<div class="col-sm-12 mb-3">
							<form class="form-inline" id="form_bch" name="form_bch" method="GET" action="product.php">
								<input class="form-control" type="search" id="brand_search" name="brand_search" placeholder="้ค้นหายี่ห้อรถ" aria-label="Search" value="<?php if(isset($_GET['brand_search']) AND $_GET['brand_search']!=""){echo $_GET['brand_search'];} ?>">
								<button type="submit" class="btn btn-warning ml-2 mr-2">ค้นหา</button>
								<button type="button" class="btn btn-warning ml-2 mr-2" onclick="self.location.href=('product.php')">รายการทั้งหมด</button>
							</form>
						</div>
					</div>

					<div class="row">
						<?php while ($row_Re_p = $Re_p->fetch_assoc()) { ?>
							<div class="col-md-4 p-1">
								<div class="card h-100">
									<div class="card-body">
										<div class="thumbnail mb-2">
											<a href="product_detail.php?id=<?php echo $row_Re_p['pro_id'];?>">
											<div class="pro_img_list">
												<img class="card-img-top" src="images/product/<?php echo $row_Re_p['pro_p_photo'];?>">
											</div>
											</a>
										</div>
										<div class="card-text">
											<?php 
											echo "รถปี ".$row_Re_p['pro_year']."<br>"; 
											echo $row_Re_p['pro_brand']."  ".$row_Re_p['pro_model']; 
											?>
										</div>
										<div class="alert alert-secondary text-right tx_blue_content mb-0" role="alert">
											<?php echo number_format($row_Re_p['pro_price'])." บาท"; ?>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<br>

					<div class="row mt-1">
						<div class="col-12 pr-2">
							<ul class="pagination justify-content-end">
							<li class="page-item <?php if($page==1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_p=%d%s", $currentPage, 0, $currentSent); ?>" aria-label="Previous">
								<i class="fa fa-angle-double-left" aria-hidden="true"></i>
								</a>
							</li>

							<li class="page-item <?php if($page==1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_p=%d%s", $currentPage, $page - 1, $currentSent); ?>" aria-label="Previous">
								<i class="fa fa-angle-left" aria-hidden="true"></i>
								</a>
							</li>

							<li class="page-item disabled">
								<span class="page-link">
								<?php 
								if($totalPages_Re_p>0){
									echo $page."/".$totalPages_Re_p;
								}else{
									echo "-";
								}
								?>
								</span>
							</li>

							<li class="page-item <?php if($page==$totalPages_Re_p OR $totalPages_Re_p<1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_p=%d%s", $currentPage, $page + 1, $currentSent); ?>" aria-label="Next">
								<i class="fa fa-angle-right" aria-hidden="true"></i>
								</a>
							</li>
									
							<li class="page-item <?php if($page==$totalPages_Re_p OR $totalPages_Re_p<1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_p=%d%s", $currentPage, $totalPages_Re_p, $currentSent); ?>" aria-label="Next">
								<i class="fa fa-angle-double-right" aria-hidden="true"></i>
								</a>
							</li>
							</ul>
						</div>
					</div>
          		</div>
			</div>
      	</div>
  	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
