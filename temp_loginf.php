<!DOCTYPE html>
<html>
<head>
<title>Facebook Login JavaScript Example</title>
<meta charset="UTF-8">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
</head>
<body>
<script>

function fb_login(){
		FB.login(function(response) {
		   testAPI() 
		}, {scope: 'public_profile,email'});
 }
  
  function checkLoginState() {               // Called when a person is finished with the Login Button.
    FB.getLoginStatus(function(response) {  // See the onlogin handler
      statusChangeCallback(response);
    });
  }


  window.fbAsyncInit = function() {
    FB.init({
      appId      : '106896906786738',
	  status  	: true,
      cookie     : true,                     // Enable cookies to allow the server to access the session.
      xfbml      : true,                     // Parse social plugins on this webpage.
      version    : 'v2.12'           // Use this Graph API version for this call.
    });


    FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
      statusChangeCallback(response);        // Returns the login status.
    });
  };

  
  (function(d, s, id) {                      // Load the SDK asynchronously
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

 
  function testAPI() {                      // Testing Graph API after login.  See statusChangeCallback() for when this call is made.
   
    FB.api('/me', function(response) {
      console.log('Successful login for: ' + response.status);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.status + '!';
    });
  }

</script>


//  The JS SDK Login Button 

<!--<fb:login-button scope="public_profile,email" onlogin="checkLoginState();">-->
</fb:login-button>
<a href="#" onclick="fb_login();"  class="btn btn-lg btn-block omb_btn-facebook tx_fb"><i class="fab fa-facebook-f"></i> เข้าสู่ระบบด้วย Facebook</a>

<div id="status">
</div>

</body>
</html>