<div class="container-fluid m-0 p-0 bg_gray_footer">
    <div class="container-fluid m-0 p-0 border_line_b">
        <div class="container">
            <div class="row p-0 m-0">
                <div class="col-sm-4 py-3 border_line_r">
                    <div class="f_menu_social tx_blue">
                        <ul>
                            <li><b>ติดตามเราได้ที่</b></li>
                            <li class="">
                                <a href="https://www.facebook.com/snnleasing2544/" target="_blank">
                                    <img src="images/logo/logo_fb.png" alt="Facebook"></a>
                                <a href="https://www.youtube.com/channel/UCl07rnPoGqxnECSWYkhU0JA" target="_blank">
                                    <img src="images/logo/logo_youtube.png" alt="Youtube"></a>
                                <a href="https://line.me/R/ti/p/uEc2JFx5yF" target="_blank">
                                    <img src="images/logo/logo_line.png" alt="Facebook"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-8 py-3">
                    <div class="row ">
                        <div class="col-md-6 pl-md-5 footer_menu_l tx_blue_content">
                            สมัครรับข่าวสารและโปรโมชั่น<br>
                            ที่คุณจะได้รู้เป็นคนแรกทาง SMS
                        </div>
                        <div class="col-md-6 footer_menu_r">
                            <form name="form_tel" id="form_tel" method="post" enctype="multipart/form-data" class="form-inline my-2 my-lg-0 w-100 justify-content-center float-lg-left" >
                                <input class="form-control" type="text" id="t_tel" name="t_tel" style="width:calc(100% - 100px);" placeholder="กรุณากรอกเบอร์โทรศัพท์มือถือ">&nbsp;
                                <button class="btn btn-warning bg_yellow my-2 my-sm-0 text-white" type="submit">สมัคร</button>
                                <div id="t_tel_validate"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row p-0 m-0 pt-4">
            <div class="col-lg-4 ">
                <div class="footer_logo">
                    <img src="images/logo/logo_snn_f.png" class="img-fluid p-3">
                    <p class="text-center tx_blue_content">
                        บริษัท เอส เอ็น เอ็น ลีสซิ่ง จำกัด<br>
                        SNN Leasing Company Limited
                    </p>
                </div>
            </div>
            <div class="col-lg-8 ">
                <div class="row p-0 m-0">
                    <div class="col-sm-4 p-0 footer_menu_l">
                        <b>เกี่ยวกับ เอส เอ็น เอ็น</b><br>
                        <a href="about.php#v-ab" >ประวัติความเป็นมา</a><br>
                        <a href="about.php#v-vs">วิสัยทัศน์ พันธกิจ ค่านิยมองค์กร</a><br>
                        <a href="about.php#v-og">โครงสร้างองค์กร</a><br>
                        <a href="about.php#v-bd">โครงสร้างคณะกรรมการ</a><br>
                        <a href="about.php#v-cp">การรับเรื่องร้องเรียน</a><br>
                        <a href="about.php#v-sc">ความรับผิดชอบต่อสังคม</a><br>
                        <a href="about.php#v-cn">บริษัทในเครือ</a><br>
                        <a href="contact.php">ติดต่อเรา</a>
                    </div>
                    <div class="col-sm-4 p-0 footer_menu_l">
                        <div class="col-sm-12 mb-4">
                            <a href="news.php">ข่าวสารและกิจกรรม</a><br>
                            <a href="answer.php">คำถามที่พบบ่อย</a><br>
                            <a href="payment.php">ช่องทางการชำระเงิน</a>
                        </div>
                    </div>
                    <div class="col-sm-4 p-0 footer_menu_l">
                        <div class="col-sm-12 mb-4">
                            <a href="investor.php?type=1">นักลงทุนสัมพันธ์</a><br>
                            <a href="product.php">ทรัพย์สินรอขาย</a><br>
                            <a href="policy.php">นโยบายความเป็นส่วนตัว</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row pb-1 m-0">
        <div class="container-fluid">
            <a href="car.php"><img class="float-right" src="images/logo/logo_staff2.png" alt=""></a>
        </div>
    </div>

    <div class="row p-0 m-0">
        <div class="col-sm-12 p-3 bg_yellow">
            <div class="text-center tx_blue d-none d-sm-block">
                <img class="img-fluid" src="images/icon/icon_telf_f.png" alt="">&nbsp;
                <a href="tel:+66888908990"><img class="img-fluid" src="images/icon/icon_telf.png" alt=""></a>
            </div>

            <div class="text-center tx_blue d-block d-sm-none">
                <img class="img-fluid" src="images/icon/icon_telf_f.png" alt=""><br>
                <a href="tel:+66888908990"><img class="img-fluid" src="images/icon/icon_telf.png" alt=""></a>
            </div>
        </div>
    </div>
</div>
<div class="footer_bottom"></div>
<button onclick="topFunction()" id="btn_top" title="Go to top"><i class="fas fa-angle-double-up"></i></button>
<script type="text/javascript" src="js/jquery-3.4.1.min.js"  ></script>
<script type="text/javascript" src="js/ajax_tel.js"></script>