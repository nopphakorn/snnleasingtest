<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="";

$sql_ctff="SELECT * FROM tb_content WHERE c_name='TFF'";
$Re_ctff=$mysqli->query($sql_ctff);
$row_Re_ctff=$Re_ctff->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/inc_header.php';?>
	<style>
		body {
			background:url(images/background/bg-tff.jpg) no-repeat;
			background-size:cover;
			background-attachment:fixed;
		}
	</style>
</head>
<body>
	<script>
  	window.fbAsyncInit = function() {
    	FB.init({
			appId      	: '106896906786738',
			status  	: true,
			cookie     	: true,
			xfbml      	: true,
			version    	: 'v2.12'
    	});
    	FB.AppEvents.logPageView();   
  	};

  	(function(d, s, id){
     	var js, fjs = d.getElementsByTagName(s)[0];
     	if (d.getElementById(id)) {return;}
     	js = d.createElement(s); js.id = id;
     	js.src = "//connect.facebook.net/en_US/sdk.js";
     	fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

	function fb_login(){
		
		FB.login(function(response) {
			if (response.status == 'connected') {
				FB.api('/me',{fields: 'id,name,first_name,last_name,email'}, function (response) {
					var fbid=response.id;
					var fbname=response.name;
					var fbfirst_name=response.first_name;
					var fblast_name=response.last_name;
					var fbemail=response.email;
					console.log(fbid);
					$.ajax({
						type: 	"POST",
						url:  	"tff_login_fb_check.php",
						data: {fbid: fbid, fbname: fbname, fbfirst_name:fbfirst_name, fblast_name:fblast_name, fbemail: fbemail},
						dataType: "html",
						success: function(data) {
							if (data == 'login') {
								location.href = 'tff.php';
							} else if (data == 'regis'){
								location.href = 'tff_login_fb_register.php?action=reg&fb='+ fbid;
							} else if (data == 'error'){
								location.href = 'tff_login.php';
							}
						}
					});
				});
			}else{
				 $.alert({
                     theme: 'light',
                    type: 'red',
                    title: 'ผิดพลาด',
                    content: 'ไม่ได้ Login เข้า Facebook',
                });
			}	  
		}, { scope: 'email' });
	}
function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}


</script>

  	<div class="container-fluid m-0 p-0"><?php include 's_header.php';?></div>
	<div class="container mt-4 pt-4 pb-4">
		<h3 style="text-align:center">ยินดีต้อนรับสู่ SNN TFF</h3>
		<div class="tff_login mt-4 pt-4">
			<p><img class="img-fluid mx-auto d-block" src="images/logo/logo_snn_tff.png" alt=""></p>
			<div class="row omb_socialButtons mt-4 mr-0 ml-0">
				<div class="col-sm-12">
					<a href="#" onclick="fb_login();" class="btn btn-lg btn-block omb_btn-facebook tx_fb"><i class="fab fa-facebook-f"></i> เข้าสู่ระบบด้วย Facebook</a>
				</div>
			</div>

			<div class="row tff_loginOr mr-0 ml-0">
				<div class="col-sm-12">
					<hr class="tff_hrOr">
					<span class="tff_spanOr tx_blue_content">หรือ</span>
				</div>
			</div>

			<div class="row mr-0 ml-0">
				<div class="col-sm-12">	
					<form id="tff_loginForm" method="POST">
						<input type="text" class="form-control mb-1" id="email" name="email" placeholder="email address">
						<div id="email_validate"></div>
						<input  type="password" class="form-control mb-1" id="password" name="password" placeholder="Password">
						<div id="password_validate"></div>
						<p class="text-left"><a href="tff_login_forgot.php">ลืมรหัสผ่าน</a></p>
						<button class="btn btn-lg btn-warning btn-block" type="submit" id="submit">Login</button>
					</form>
				</div>
			</div>

			<div class="row mr-0 ml-0">
				<div class="col-sm-12 pt-2">
					<p><a href="tff_register.php">สมัครสมาชิก</a></p>
				</div>
			</div>	    	
		</div>
	</div>

	<div class="container mt-5 tx_blue_content">
		<h3 style="text-align:center">สิทธิประโยชน์พิเศษมากมายได้ทุกวัน</h3>
		<div class="row">
			<div class="col-sm-4 text-center mt-3 mb-3">
				<img src="images/tff/tffmenu-1.jpg" alt="..." class="rounded-circle"></a>
				<p>
					<h4>วางแผนการเงิน</h4>
					<p>ไม่ว่าแผนชีวิตของคุณเป็นยังไง เอส เอ็น เอ็นก็ช่วยคุณวางแผนได้ เพื่อเป้าหมายในฝันของคุณ</p>
				</p>
			</div>
			<div class="col-sm-4 text-center mt-3 mb-3">
				<img src="images/tff/tffmenu-2.jpg" alt="..." class="rounded-circle"></a>
				<p>
					<h4>บทความดีๆ</h4>
					<p>เพราะมีอีกหลายเรื่องราวในชีวิตที่อยากให้คุณรู้ พบกับบทความดีๆ ในแบบส่งตรงถึงคุณ เพราะมีเพียงสมาชิก SNN TFF เท่านั้นที่มีสิทธิ์</p>
				</p>
			</div>
			<div class="col-sm-4 text-center mt-3 mb-3">
				<img src="images/tff/tffmenu-3.jpg" alt="..." class="rounded-circle"></a>
				<p>
					<h4>อนาคตพิษณุโลก สู่สังคมเมือง</h4>
					<p>พิษณุโลกของเรากำลังจะกลายเป็นสังคมเมืองขนาดใหญ่ในอนาคตอันใกล้นี้ ให้คุณสังเกตแนวเส้นทางการเติบโตที่แอททรีนำมาฝากกันให้ดีๆ</p>
				</p>
			</div>
		</div>
	</div>

	<div class="container mt-5 tx_blue_content">
		<div class="row">
			<div class="col-sm-12 mt-3 mb-3">
				<p class="text-center">
					<button class="btn btn-lg btn-secondary w-50" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
						ข้อตกลง & เงื่อนไข
					</button>
				</p>
				<div class="collapse" id="collapseExample">
					<div class="card card-body">
						<?php
						if(!empty($row_Re_ctff['c_detail'])){
							echo $row_Re_ctff['c_detail']; 
						}else{
							echo "ไม่มี ข้อตกลง & เงื่อนไข ในขณะนี้";
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
  	<?php include 'include/inc_script.php';?>
	<script type="text/javascript" src="library/validation/js/jquery.validate.js"></script>
  	<script type="text/javascript" src="library/validation/js/additional-methods.js"></script>
  	<script type="text/javascript" src="js/ajax_tff.js"></script>
	<script>
	
</body>
</html>