<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="";
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-12">
					<h3 class="my-4 tx_blue">เอกสารสมัครสินเชื่อ</h3>
					<ul class="nav nav-pills la_nav_pills" id="pills-tab" role="tablist">
						<li class="nav-item col-md-6 p-0 text-center">
							<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">สำหรับบุคคลธรรมดา</a>
						</li>
						<li class="nav-item col-md-6 p-0 text-center">
							<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">สำหรับเจ้าของกิจการ/นิติบุคคล</a>
						</li>
					</ul>
					<div class="tab-content bg-white m-0 p-4 tx_blue_content" id="pills-tabContent">
						<div class="tab-pane fade show active p-4" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
							<b>คุณสมบัติผู้สมัคร</b><br>
							ผู้ขอสมัคร หรือ ผู้เช่าซื้อรถยนต์ จะต้องมีอายุ 20-65 ปี ณ วันทำสัญญา และมีรถยนต์เป็นของตนเอง<br>
							ผู้ขอสมัคร หรือ ผู้เช่าซื้อรถยนต์ จะต้องมีถิ่นพำนักในประเทศไทย<br><br><br>

							<b>เอกสารประกอบการสมัคร</b><br>
							<div class="row">
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/namecard.png"></div>
										<div class="col-sm-8">บัตรประชาชนตัวจริง พร้อมสำเนาจำนวน 2 ฉบับ</div>
									</div>
								</div>
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/home.png"></div>
										<div class="col-sm-8">ทะเบียนบ้านฉบับจริง พร้อมสำเนาจำนวน 2 ฉบับ</div>
									</div>
								</div>
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/bank.png"></div>
										<div class="col-sm-8">สำเนาบัญชีเงินฝากออมทรัพย์/ประจำ/กระแสรายวันย้อนหลัง 3 เดือน</div>
									</div>
								</div>
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/Salary.png"></div>
										<div class="col-sm-8">สลิปเงินเดือนหรือหนังสือรับรองรายได้ฉบับล่าสุด หรือย้อนหลังไม่เกิน 2 เดือน</div>
									</div>
								</div>
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/map.png"></div>
										<div class="col-sm-8">แผนที่ที่ตั้งที่อยู่อาศัยและที่ทำงาน</div>
									</div>
								</div>
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/bank.png"></div>
										<div class="col-sm-8">สำเนาสมุดบัญชีหน้าแรกของธนาคารที่ต้องโอนเงินเข้า</div>
									</div>
								</div>
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/car.png"></div>
										<div class="col-sm-8">เล่มทะเบียนรถยนต์ตัวจริง</div>
									</div>
								</div>
							</div>
							<div class="row mt-4">
								<div class="col-sm-12 p-3">
									<b>เอกสารประกอบการสมัคร</b><br>
									1. บัตรประชาชนตัวจริง พร้อมสำเนาจำนวน 2 ฉบับ<br>
									2. ทะเบียนบ้านฉบับจริง พร้อมสำเนาจำนวน 2 ฉบับ<br>
									3. สลิปเงินเดือนหรือหนังสือรับรองรายได้ฉบับล่าสุด หรือย้อนหลังไม่เกิน 2 เดือน<br>
									4. สำเนาบัญชีเงินฝากออมทรัพย์/ประจำ/กระแสรายวันย้อนหลัง 3 เดือน<br>
									5. แผนที่ที่ตั้งที่อยู่อาศัยและที่ทำงาน<br>
									6. สำเนาสมุดบัญชีหน้าแรกของธนาคารที่ต้องโอนเงินเข้า<br>
									7. เล่มทะเบียนรถยนต์ตัวจริง<br>
								</div>
							</div>
						</div>
						<div class="tab-pane fade p-4 tx_blue_content" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
							<b>คุณสมบัติผู้สมัคร</b><br>
							ผู้ขอสมัคร หรือ ผู้เช่าซื้อรถยนต์ จะต้องมีอายุ 20-65 ปี ณ วันทำสัญญา และมีรถยนต์เป็นของตนเอง<br>
							ผู้ขอสมัคร หรือ ผู้เช่าซื้อรถยนต์ จะต้องมีถิ่นพำนักในประเทศไทย<br><br>

							<b>เอกสารประกอบการสมัคร</b><br>
							<div class="row">
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/dbd.png"></div>
										<div class="col-sm-8">หนังสือรับรองบริษัท (ฉบับปัจจุบัน) จำนวน 2 ชุด</div>
									</div>
								</div>
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/pp20.png"></div>
										<div class="col-sm-8">ภพ.20</div>
									</div>
								</div>
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/home-name.png"></div>
										<div class="col-sm-8">สำเนาบัตรประชาชน และทะเบียนบ้านของกรรมการทุกรายที่มีอำนาจลงนามของบริษัท</div>
									</div>
								</div>
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/home-name.png"></div>
										<div class="col-sm-8">สำเนาบัตรประชาชน และทะเบียนบ้านของผู้ถือหุ้นตั้งแต่ 25% ขึ้นไป ของบริษัทจำกัด</div>
									</div>
								</div>
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/home-name.png"></div>
										<div class="col-sm-8">สำเนาบัตรประชาชน และทะเบียนบ้านของผู้ค้ำประกัน</div>
									</div>
								</div>
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/bank.png"></div>
										<div class="col-sm-8">งบการเงินหรือสำเนาเงินฝากออมทรัพย์/ประจำ/กระแสรายวัน ย้อนหลัง 3 เดือน</div>
									</div>
								</div>
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/Salary.png"></div>
										<div class="col-sm-8">รายงานการประชุม "แจ้งความประสงค์ในการขอสินเชื่อ" (ต้นฉบับลงนาม โดยกรรมการบริษัท/หุ้นส่วนของห้างหุ้นส่วนฯ)</div>
									</div>
								</div>
								<div class="col-sm-6 p-3">
									<div class="row">
										<div class="col-sm-4"><img class="img-fluid" src="images/credit/Salary.png"></div>
										<div class="col-sm-8">บัญชีรายชื่อผู้ถือหุ้น (บอจ. 5) ยกเว้น บริษัทจำกัด (มหาชน) ที่จดทะเบียนในตลาดหลักทรัพย์</div>
									</div>
								</div>
							</div>
							<div class="row mt-4">
								<div class="col-sm-12 p-3">
									<b>เอกสารประกอบการสมัคร</b><br>
									1. หนังสือรับรองบริษัท (ฉบับปัจจุบัน) จำนวน 2 ชุด<br>
									2. ภพ.20<br>
									3. สำเนาบัตรประชาชน และทะเบียนบ้านของกรรมการทุกรายที่มีอำนาจลงนามของบริษัท<br>
									4. สำเนาบัตรประชาชน และทะเบียนบ้านของผู้ถือหุ้นตั้งแต่ 25% ขึ้นไป ของบริษัทจำกัด<br>
									5. สำเนาบัตรประชาชน และทะเบียนบ้านของผู้ค้ำประกัน<br>
									6. งบการเงินหรือสำเนาเงินฝากออมทรัพย์/ประจำ/กระแสรายวัน ย้อนหลัง 3 เดือน<br>
									7. รายงานการประชุม "แจ้งความประสงค์ในการขอสินเชื่อ" (ต้นฉบับลงนาม โดยกรรมการบริษัท/หุ้นส่วนของห้างหุ้นส่วนฯ)<br>
									8. บัญชีรายชื่อผู้ถือหุ้น (บอจ. 5) ยกเว้น บริษัทจำกัด (มหาชน) ที่จดทะเบียนในตลาดหลักทรัพย์
								</div>
							</div>
						</div>
					</div>

				</div> 
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
