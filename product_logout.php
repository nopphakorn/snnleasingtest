<?php
if (!isset($_SESSION)) {session_start();}
$_SESSION['PU_pro_u_id']= NULL;
$_SESSION['PU_pro_u_user']= NULL;
$_SESSION['PU_pro_u_name']= NULL;
$_SESSION["PU_pro_u_access"]= NULL;

unset($_SESSION['PU_pro_u_id']);
unset($_SESSION['PU_pro_u_user']);
unset($_SESSION['PU_pro_u_name']);
unset($_SESSION["PU_pro_u_access"]);
header("Location: product_login.php"); 
exit;
?>