<?php 
include 'Connections/con_db.php';
include 'car_check.php';
include 'function/function.php';
$page_name="";

$query_Re_cb = "SELECT * FROM tb_car_brand ORDER BY cb_name ASC ";
$Re_cb=$mysqli->query($query_Re_cb);

if((isset($_GET['cb_id'])) AND (isset($_GET['ct_id']))){
	if(!empty($_GET['cb_id'])){
	$query_Re_cb2 = "SELECT * FROM tb_car_brand WHERE cb_id = ".$_GET['cb_id']." ";
	$Re_cb2=$mysqli->query($query_Re_cb2);
	$row_Re_cb2=$Re_cb2->fetch_assoc();

	$query_Re_ct = "SELECT * FROM tb_car_type WHERE ct_id IN(".$row_Re_cb2['ct_id'].") ORDER BY ct_id ASC ";
	$Re_ct=$mysqli->query($query_Re_ct);
	}
}

if((isset($_GET['cb_id'])) AND (isset($_GET['ct_id'])) AND (isset($_GET['cg_id']))){
	if(!empty($_GET['cb_id'])){
	$query_Re_cb2 = "SELECT * FROM tb_car_brand WHERE cb_id = ".$_GET['cb_id']." ";
	$Re_cb2=$mysqli->query($query_Re_cb2);
	$row_Re_cb2=$Re_cb2->fetch_assoc();

	$query_Re_ct = "SELECT * FROM tb_car_type WHERE ct_id IN(".$row_Re_cb2['ct_id'].") ORDER BY ct_id ASC ";
	$Re_ct=$mysqli->query($query_Re_ct);

	$query_Re_cg = "SELECT * FROM tb_car_group WHERE cb_id='".$cb_id."' AND ct_id='".$ct_id."'  ORDER BY cg_name ASC ";
	$Re_cg=$mysqli->query($query_Re_cg);
	}
}

if((isset($_GET['cb_id'])) AND (isset($_GET['cg_id'])) AND (isset($_GET['cm_id']))){
$query_Re_cm = "SELECT * FROM tb_car_model WHERE cb_id='".$_GET['cb_id']."' AND cg_id='".$_GET['cg_id']."' ORDER BY cm_name ASC ";
$Re_cm=$mysqli->query($query_Re_cm);
}
//----------------------------
$maxRows_Re_c = 50;
$pageNum_Re_c = 0;
if (isset($_GET['pageNum_Re_c'])) {
  $pageNum_Re_c = $_GET['pageNum_Re_c'];
}
$startRow_Re_c = (($pageNum_Re_c-1) * $maxRows_Re_c);
if($startRow_Re_c<0){$startRow_Re_c=0;}

$query_Re_c = "SELECT a.*, b.ct_name, c.cb_name, d.cg_name, e.cm_name, e.ty_car FROM tb_car a ";
$query_Re_c.= "LEFT JOIN tb_car_type b ON(a.ct_id=b.ct_id) ";
$query_Re_c.= "LEFT JOIN tb_car_brand c ON(a.cb_id=c.cb_id) ";
$query_Re_c.= "LEFT JOIN tb_car_group d ON(a.cg_id=d.cg_id) ";
$query_Re_c.= "LEFT JOIN tb_car_model e ON(a.cm_id=e.cm_id) ";
$query_Re_c.= "WHERE a.c_status='1' AND a.c_id!='' ";
if(!isset($_GET['ct_id']) AND !isset($_GET['cb_id']) AND !isset($_GET['cg_id']) AND !isset($_GET['cm_id']) AND !isset($_GET['c_year'])){
    $query_Re_c.= "AND a.c_id=-1 ";
}
if(isset($_GET['ct_id']) AND $_GET['ct_id']!=""){
    $ct_id_chk=mysqli_real_escape_string($mysqli , $_GET["ct_id"]);
    $query_Re_c.= "AND a.ct_id='$ct_id_chk' ";
}
if(isset($_GET['cb_id']) AND $_GET['cb_id']!=""){
    $cb_id_chk=mysqli_real_escape_string($mysqli , $_GET["cb_id"]);
    $query_Re_c.= "AND a.cb_id='$cb_id_chk' ";
}
if(isset($_GET['cg_id']) AND $_GET['cg_id']!=""){
    $cg_id_chk=mysqli_real_escape_string($mysqli , $_GET["cg_id"]);
    $query_Re_c.= "AND a.cg_id='$cg_id_chk' ";
}
if(isset($_GET['cm_id']) AND $_GET['cm_id']!=""){
    $cm_id_chk=mysqli_real_escape_string($mysqli , $_GET["cm_id"]);
    $query_Re_c.= "AND a.cm_id='$cm_id_chk' ";
}
if(isset($_GET['c_year']) AND $_GET['c_year']!=""){
    $c_year_chk=mysqli_real_escape_string($mysqli , $_GET["c_year"]);
    $query_Re_c.= "AND a.c_year='$c_year_chk' ";
}
if(isset($_GET['c_gear']) AND $_GET['c_gear']!=""){
    $c_gear_chk=mysqli_real_escape_string($mysqli , $_GET["c_gear"]);
    $query_Re_c.= "AND a.c_gear='$c_gear_chk' ";
}
$query_Re_c.= "ORDER BY c.cb_name ASC, b.ct_name ASC, d.cg_name ASC, e.cm_name ASC, a.c_year ASC ";
$query_limit_Re_c = sprintf("%s LIMIT %d, %d", $query_Re_c, $startRow_Re_c, $maxRows_Re_c);
$Re_c=$mysqli->query($query_limit_Re_c);

if (isset($_GET['totalRows_Re_c'])) {
  $totalRows_Re_c = $_GET['totalRows_Re_c'];
} else {
  $all_Re_c=$mysqli->query($query_Re_c);
  $totalRows_Re_c=$all_Re_c->num_rows;
}
$totalPages_Re_c = ceil($totalRows_Re_c/$maxRows_Re_c);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";
if(isset($_GET['ct_id']) AND $_GET['ct_id']!=""){$currentSent.= "&ct_id=".$_GET['ct_id'];}
if(isset($_GET['cb_id']) AND $_GET['cb_id']!=""){$currentSent.= "&cb_id=".$_GET['cb_id'];}
if(isset($_GET['cg_id']) AND $_GET['cg_id']!=""){$currentSent.= "&cg_id=".$_GET['cg_id'];}
if(isset($_GET['cm_id']) AND $_GET['cm_id']!=""){$currentSent.= "&cm_id=".$_GET['cm_id'];}
if(isset($_GET['c_year'])){$currentSent.= "&c_year=".$_GET['c_year'];}
if(isset($_GET['c_gear'])){$currentSent.= "&c_gear=".$_GET['c_gear'];}

if($pageNum_Re_c==0){$page=1;}
else if($pageNum_Re_c>0){$page=$pageNum_Re_c;}

if (!$Re_c) {printf("Error: %s\n", $mysqli->error);}
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
	<script type="text/javascript" src="library/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
	<script type="text/javascript" src="library/fancybox/jquery.fancybox.pack.js"></script>
	<link rel="stylesheet" type="text/css" href="library/fancybox/jquery.fancybox.css" media="screen" />	
	<script type="text/javascript">
        $(document).ready(function() {
            $('.btn_view').fancybox({
				'type' : 'iframe',
				'width' : '670',
				'height' : '420',
				'autoScale' : false,
				'fitToView' : false,
				'autoSize' : false,
				'afterClose' : function() {parent.location.reload(true); },
            });
        });
	</script>
</head>
<body>
	<div class="container-fluid"><?php include 's_header.php';?></div>
	  
	<div class="container-fluid bg_top">
      <div class="container pt-4 mb-4">
        	<div class="row">
          		<div class="col-lg-12">
            		<h3 class="my-4 tx_blue"><img src="images/icon/icon_staff.png" alt=""> Staff only</h3>
					คุณ <?php echo $row_Re_emchk['em_name']; ?><br>
					เข้าใช้งานครั้งล่าสุดเมื่อ : <i class="far fa-calendar-alt"></i> <?php echo datethai_time($row_Re_emchk['em_date']); ?><br>
					<a class="btn btn-secondary" href="car_logout.php"><i class="fas fa-sign-out-alt"></i> ออกจากระบบ</a>
            		<br><br>

            		<div class="card mb-3">
              			<div class="card-body">
							<div class="form_box">
								<form name="form_csearch" id="form_csearch" method="GET" enctype="multipart/form-data" action="car.php">
									<div class="tx_car">ค้นหาราคากลางรถยนต์ใช้แล้ว</div>
									<hr class="hr_yellow">
									<div class="form-row">  
										<div class="form-group col-md-6">
											<label for="cb_id">เลือกยี่ห้อรถ</label>
											<select name="cb_id" id="cb_id" class="form-control g_search" onchange="list_ca('ct_id',form_csearch.cb_id.value,'','')">
												<option value="">เลือก</option>
												<?php while($row_Re_cb=$Re_cb->fetch_assoc()){?>
												<option value="<?php echo $row_Re_cb['cb_id'];?>" <?php if(isset($_GET['cb_id'])){if($row_Re_cb['cb_id']==$_GET['cb_id']){echo "selected=\"selected\"";}} ?>>
												<?php echo $row_Re_cb['cb_name'];?></option>
												<?php } ?>
											</select>
										</div>
										<div class="form-group col-md-6">
											<label for="ct_id">เลือกประเภท</label>
											<select name="ct_id" id="ct_id" class="form-control g_search" onchange="list_ca('cg_id', form_csearch.cb_id.value, form_csearch.ct_id.value,'')">
												<option value="">เลือก</option>
												<?php 
												if((isset($_GET['cb_id'])) AND (!empty($_GET['cb_id']))){ 
													while($row_Re_ct=$Re_ct->fetch_assoc()){?>
												<option value="<?php echo $row_Re_ct['ct_id'];?>" <?php if(isset($_GET['ct_id'])){if($row_Re_ct['ct_id']==$_GET['ct_id']){echo "selected=\"selected\"";}} ?>>
												<?php echo $row_Re_ct['ct_name'];?></option>
												<?php }}?>
											</select>
										</div>
									</div>

									<div class="form-row">  
										<div class="form-group col-md-6">
											<label for="cg_id">กลุ่มรุ่นรถ</label>
											<select name="cg_id" id="cg_id" class="form-control g_search" onchange="list_ca('cm_id', form_csearch.cb_id.value, form_csearch.ct_id.value, form_csearch.cg_id.value)">
												<option value="">เลือก</option>
												<?php 
												if((isset($_GET['cb_id']) AND !empty($_GET['cb_id'])) AND (isset($_GET['ct_id']) AND !empty($_GET['ct_id']))){ 
													while($row_Re_cg=$Re_cg->fetch_assoc()){
												?>
												<option value="<?php echo $row_Re_cg['cg_id'];?>" <?php if(isset($_GET['cg_id'])){if($row_Re_cg['cg_id']==$_GET['cg_id']){echo "selected=\"selected\"";}} ?>><?php echo $row_Re_cg['cg_name'];?></option>
												<?php }} ?>
											</select>
										</div>
										<div class="form-group col-md-6">
											<label for="cm_id">ชื่อรุ่นรถ</label>
											<select name="cm_id" id="cm_id" class="form-control g_search">
												<option value="">รุ่นรถ</option>
												<?php 
												if((isset($_GET['cm_id'])) AND (isset($_GET['cm_id']))){ 
													while($row_Re_cm=$Re_cm->fetch_assoc()){
												?>
												<option value="<?php echo $row_Re_cm['cm_id'];?>" <?php if(isset($_GET['cm_id'])){if($row_Re_cm['cm_id']==$_GET['cm_id']){echo "selected=\"selected\"";}} ?>><?php echo $row_Re_cm['cm_name'];?></option>
												<?php }} ?>
											</select>
										</div>
									</div>	

									<div class="form-row">  
										<div class="form-group col-md-6">
											<label for="cg_id">ปีรถ</label>
											<input type="text" id="c_year" name="c_year" class="form-control g_search" placeholder="ปีรถ" value="<?php if(isset($_GET['c_year'])){echo $_GET['c_year'];} ?>">
										</div>
										<div class="form-group col-md-6">
											<label for="c_gear">ระบบเกียร์</label>
											<select name="c_gear" id="c_gear" class="form-control g_search">
												<option value="" <?php if(isset($_GET['c_gear'])){if($_GET['c_gear']==""){echo "selected=\"selected\"";}} ?>>เลือก</option>
												<option value="AT" <?php if(isset($_GET['c_gear'])){if($_GET['c_gear']=="AT"){echo "selected=\"selected\"";}} ?>>ออโต้</option>
												<option value="MT" <?php if(isset($_GET['c_gear'])){if($_GET['c_gear']=="MT"){echo "selected=\"selected\"";}} ?>>ธรรมดา</option>
											</select>
										</div>
									</div>								
									<button type="submit" id="submit" class="btn btn-primary">ค้นหาราคา</button>
									<button type="button" id="submit" class="btn btn-secondary" onclick="location.href='car.php';">ยกเลิก</button>
								</form>
							</div>
              			</div>
            		</div>

					<div class="card">
              			<div class="card-body ">
						  	<div class="table-responsive-lg">
								<table class="table table-striped table-bordered tb_head_blue">
									<thead>
									<tr>
										<th width="40">No.</th>
										
										<th width="100">ยี่ห้อรถ</th>
										<th width="100">ประเภท</th>
										<th width="180">กลุ่มรุ่นรถ</th>
										<th width="">รุ่นรถ</th>
										<th width="150">เกียร์</th>
										<th width="80">รถปี</th>
										<th width="100">ประเภท รย.</th>
										<th width="80"><div align="center">ดูราคา</div></th>
									</tr>
									</thead>
									<?php 
									if($totalRows_Re_c>0){
										$number=0;
										while($row_Re_c=$Re_c->fetch_assoc()){
									?>
									<tr>
										<td><?php echo $number+=1; ?></td> 
										
										<td><?php echo $row_Re_c['cb_name']; ?></td>
										<td><?php echo $row_Re_c['ct_name']; ?></td>
										<td><?php echo $row_Re_c['cg_name']; ?></td>
										<td><?php echo $row_Re_c['cm_name']; ?></td>
										<td>
											<?php if($row_Re_c['c_gear']=='AT'){echo "ออโต้";}else if($row_Re_c['c_gear']=='MT'){echo "เกียร์ธรรมดา";}  ?>
										</td>
										<td><?php echo $row_Re_c['c_year']; ?></td>
										<td><?php echo $row_Re_c['ty_car']; ?></td>
										<td><div align="center"><a class="btn btn-warning btn-sm btn_view text-white" href="car_view.php?c_id=<?php echo $row_Re_c['c_id']; ?>" role="button"><i class="fas fa-search"></i></a></div></td>
									</tr>
									<?php }}else{  ?>
									<tr>
										<td colspan="9">
											<div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการค้นหาในขณะนี้</div>
										</td>
									</tr>
									<?php } ?>
								</table>
							</div>
							<div class="row mt-1">
								<div class="col-12 pr-2">
									<ul class="pagination justify-content-end">
									<li class="page-item <?php if($page==1){echo "disabled";}?>">
										<a class="page-link" href="<?php printf("%s?pageNum_Re_c=%d%s", $currentPage, 0, $currentSent); ?>" aria-label="Previous">
										<i class="fa fa-angle-double-left" aria-hidden="true"></i>
										</a>
									</li>

									<li class="page-item <?php if($page==1){echo "disabled";}?>">
										<a class="page-link" href="<?php printf("%s?pageNum_Re_c=%d%s", $currentPage, $page - 1, $currentSent); ?>" aria-label="Previous">
										<i class="fa fa-angle-left" aria-hidden="true"></i>
										</a>
									</li>

									<li class="page-item disabled">
										<span class="page-link">
										<?php 
										if($totalPages_Re_c>0){
											echo "หน้า ".$page."/".$totalPages_Re_c."  จำนวน ".$totalRows_Re_c." รายการ" ;
										}else{
											echo "-";
										}
										?>
										</span>
									</li>

									<li class="page-item <?php if($page==$totalPages_Re_c OR $totalPages_Re_c<1){echo "disabled";}?>">
										<a class="page-link" href="<?php printf("%s?pageNum_Re_c=%d%s", $currentPage, $page + 1, $currentSent); ?>" aria-label="Next">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
										</a>
									</li>
											
									<li class="page-item <?php if($page==$totalPages_Re_c OR $totalPages_Re_c<1){echo "disabled";}?>">
										<a class="page-link" href="<?php printf("%s?pageNum_Re_c=%d%s", $currentPage, $totalPages_Re_c, $currentSent); ?>" aria-label="Next">
										<i class="fa fa-angle-double-right" aria-hidden="true"></i>
										</a>
									</li>
									</ul>
								</div>
							</div>
						</div>
					</div>

          		</div>
        	</div>
      	</div>
  	</div>

	<div id="modal_view" title="" style="display:none;">
		<div id="modal_view_content"></div>
	</div>

  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
	<script type="text/javascript" src="library/validation/js/jquery.validate.js"></script>
  	<script type="text/javascript" src="library/validation/js/additional-methods.js"></script>
  	<script type="text/javascript" src="js/ajax_car.js"></script>
</body>
</html>
