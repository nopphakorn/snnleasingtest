<?php
$page_name="";
if (!isset($_SESSION)) {session_start();}
if (
	!isset($_SESSION['SS_tff_id']) 
	OR !isset($_SESSION['SS_tff_email']) 
  	OR !isset($_SESSION['SS_tff_oauth']) 
	OR !isset($_SESSION['SS_tff_status'])
	OR !isset($_SESSION['SS_tff_access'])
	OR $_SESSION['SS_tff_status']!='1'
	OR $_SESSION['SS_tff_access']!='true'
	) {
		$_SESSION['SS_tff_id']= NULL;
		$_SESSION['SS_tff_email']= NULL;
	  	$_SESSION['SS_tff_oauth']= NULL;
		$_SESSION["SS_tff_status"]= NULL;
		$_SESSION["SS_tff_access"]= NULL;
	
		unset($_SESSION['SS_tff_id']);
	  	unset($_SESSION['SS_tff_email']);
	  	unset($_SESSION['SS_tff_oauth']);
    	unset($_SESSION["SS_tff_status"]);
    	unset($_SESSION["SS_tff_access"]);
    	header("Location: tff_login.php"); 
   	 	exit;
	}
?>