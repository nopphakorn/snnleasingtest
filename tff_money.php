<?php 
include 'tff_check.php';
include 'Connections/con_db.php';
include 'function/function.php';

$sql_ctff="SELECT * FROM tb_content WHERE c_name='TFF_money'";
$Re_ctff=$mysqli->query($sql_ctff);
$row_Re_ctff=$Re_ctff->fetch_assoc();
?>      

<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'include/inc_header.php';?>
</head>
<body>  
  	<div class="container-fluid m-0 p-0"><?php include 's_header.php';?></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-sm-3"><?php include 'tff_menu.php';?></div>
				<div class="col-sm-9 mt-3 mb-4">
					<div class="row">
								<div class="col-12">
									<h3 class="my-4 tx_blue"><?php echo $row_Re_ctff['c_name_th']; ?></h3>
									<hr class="hr_yellow">
									<div class="form_box m-0 w-100 tx_blue_content">
											<?php
											if(!empty($row_Re_ctff['c_detail'])){
												echo $row_Re_ctff['c_detail']; 
											}
											?>
									</div>
								</div> 
						</div> 
				</div>
			</div>
		</div>
	</div>
	<?php include 's_footer.php';?>
  	<?php include 'include/inc_script.php';?>
</body>
</html>
