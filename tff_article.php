<?php 
include 'tff_check.php';
include 'Connections/con_db.php';
include 'function/function.php';

$maxRows_Re_tffa = 20;
$pageNum_Re_tffa = 0;
if (isset($_GET['pageNum_Re_tffa'])) {
  $pageNum_Re_tffa = $_GET['pageNum_Re_tffa'];
}

$startRow_Re_tffa = (($pageNum_Re_tffa-1) * $maxRows_Re_tffa);
if($startRow_Re_tffa<0){$startRow_Re_tffa=0;}

$query_Re_tffa = "SELECT * FROM tb_tff_article WHERE a_status = '1'  ORDER BY a_id DESC ";
$query_limit_Re_tffa = sprintf("%s LIMIT %d, %d", $query_Re_tffa, $startRow_Re_tffa, $maxRows_Re_tffa);
$Re_tffa=$mysqli->query($query_limit_Re_tffa);

if (isset($_GET['totalRows_Re_tffa'])) {
  $totalRows_Re_tffa = $_GET['totalRows_Re_tffa'];
} else {

	$all_Re_tffa = $mysqli->query($query_Re_tffa);
	$totalRows_Re_tffa = $all_Re_tffa->num_rows;
	
}
$totalPages_Re_tffa = ceil($totalRows_Re_tffa/$maxRows_Re_tffa);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_tffa==0){$page=1;}
else if($pageNum_Re_tffa>0){$page=$pageNum_Re_tffa;}
?>      

<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'include/inc_header.php';?>
</head>
<body>  
  	<div class="container-fluid m-0 p-0"><?php include 's_header.php';?></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-sm-3"><?php include 'tff_menu.php';?></div>
				<div class="col-sm-9 mt-3 mb-4">
					<h3 class="my-4 tx_blue">บทความดีๆ</h3>
					<hr class="hr_yellow">
					<div class="row">
						<div class="card-deck">
							<?php 
								if($totalRows_Re_tffa>0){ 
									while($row_Re_tffa=$Re_tffa->fetch_assoc()){
							?>
							<div class="col-sm-6 p-3">
								<div class="card">
									<a href="tff_article_detail.php?id=<?php echo $row_Re_tffa['a_id'];?>">
									<img class="card-img-top" src="images/tff_article/<?php echo $row_Re_tffa['a_photo'];?>" alt="">
									</a>
									<div class="card-body">
										<h5 class="card-title"><?php echo $row_Re_tffa['a_name'];?></h5>
										<p class="card-text"><?php echo $row_Re_tffa['a_title'];?></p>
									</div>
									<div class="card-footer">
										<div class="row">
											<div class="col-sm-6">
												<?php echo dateTH_m($row_Re_tffa['a_date']);?>
											</div>
											<div class="col-sm-6 text-md-right">
												<div class="fb-share-button" 
													data-href="http://www.snnleasing.com/tff_article_detail.php?id=<?php echo $row_Re_tffa['a_id']; ?>" 
													data-layout="button_count" 
													data-size="small" 
													data-mobile-iframe="true">
													<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http://www.snnleasing.com/tff_article_detail.php?id=<?php echo $row_Re_tffa['a_id']; ?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">แชร์</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php }} ?>
						</div>
					</div>

					<br>
					<div class="row mt-1">
						<div class="col-12 pr-2">
							<ul class="pagination justify-content-end">
								<li class="page-item <?php if($page==1){echo "disabled";}?>">
									<a class="page-link" href="<?php printf("%s?pageNum_Re_tffa=%d%s", $currentPage, 0, $currentSent); ?>" aria-label="Previous">
										<i class="fa fa-angle-double-left" aria-hidden="true"></i>
									</a>
								</li>

								<li class="page-item <?php if($page==1){echo "disabled";}?>">
									<a class="page-link" href="<?php printf("%s?pageNum_Re_tffa=%d%s", $currentPage, $page - 1, $currentSent); ?>" aria-label="Previous">
										<i class="fa fa-angle-left" aria-hidden="true"></i>
									</a>
								</li>

								<li class="page-item disabled">
									<span class="page-link">
										<?php 
										if($totalPages_Re_tffa>0){
												echo $page."/".$totalPages_Re_tffa;
										}else{
												echo "-";
										}
										?>
									</span>
								</li>

								<li class="page-item <?php if($page==$totalPages_Re_tffa OR $totalPages_Re_tffa<1){echo "disabled";}?>">
									<a class="page-link" href="<?php printf("%s?pageNum_Re_tffa=%d%s", $currentPage, $page + 1, $currentSent); ?>" aria-label="Next">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
									</a>
								</li>
											
								<li class="page-item <?php if($page==$totalPages_Re_tffa OR $totalPages_Re_tffa<1){echo "disabled";}?>">
									<a class="page-link" href="<?php printf("%s?pageNum_Re_tffa=%d%s", $currentPage, $totalPages_Re_tffa, $currentSent); ?>" aria-label="Next">
										<i class="fa fa-angle-double-right" aria-hidden="true"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				
				</div>
			</div>
		</div>
	</div>

	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.12&appId=106896906786738&autoLogAppEvents=1';
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>
