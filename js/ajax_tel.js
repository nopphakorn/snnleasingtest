$(document).ready(function() {
    $("#form_tel").validate({
        rules: {
            t_tel: {
                required: true,
                number: true
            },
        },
        messages: {
            t_tel: {
                required: "ระบุหมายเลขโทรศัพท์",
                number: 'กรอกตัวเลขเท่านั้น'
            },
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },

        submitHandler: function(form) {
            $.ajax({
                url: "tel_save.php",
                type: 'POST',
                data: $('#form_tel').serialize(),
                success: function(data) {
                    if (data == 'ture') {
                        $('#form_tel')[0].reset();
                        $.alert({
                            theme: 'light',
                            type: 'green',
                            title: 'เรียบร้อย',
                            content: 'สมัครรับข่าวสาร SMS เรียบร้อย',
                        });
                    } else if (data == 'false') {
                        $.alert({
                            theme: 'light',
                            type: 'red',
                            title: 'ผิดพลาด',
                            content: 'เกิดข้อผิดพลาดในการ สมัครรับข่าวสาร SMS ร้องเรียน',
                        });
                    }
                }
            });
        },
    });
    $("#form_search").validate({
        rules: {
            websearch: { required: true, },
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },
    });
})