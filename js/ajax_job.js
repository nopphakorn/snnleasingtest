$(document).ready(function() {
    $("#form_jmail").validate({
        rules: {
            j_email_sent: {
                required: true,
                email: true
            },
            j_name_sent: { required: true },
            j_email_to: {
                required: true,
                email: true
            },
            j_title_to: { required: true },
            j_detail_to: { required: true },
        },
        messages: {
            j_email_sent: {
                required: "กรอก email ผู้ส่ง",
                email: 'รูปแบบ Email ไม่ถูกต้อง'
            },
            j_name_sent: "กรอกชื่อผู้ส่ง",
            j_email_to: {
                required: "กรอก email ผู้รับ",
                email: 'รูปแบบ Email ไม่ถูกต้อง'
            },
            j_title_to: "กรอก ตำแหน่งงาน",
            j_detail_to: "กรอก รายละเอียด",
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },

        submitHandler: function(form) {
            $.ajax({
                url: "job_list_detail_share.php",
                type: 'POST',
                data: $('#form_tffreg').serialize(),
                success: function(data) {
                    if (data == 'ture') {
                        $('#form_tffreg')[0].reset();
                        $.alert({
                            theme: 'light',
                            type: 'green',
                            title: 'เรียบร้อย',
                            content: 'ส่งให้เพื่อนเรียบร้อย',
                        });
                    } else {
                        $.alert({
                            theme: 'light',
                            type: 'red',
                            title: 'ผิดพลาด',
                            content: 'เกิดข้อผิดพลาดในการส่ง',
                        });
                    }
                }
            });
        },
    });
})