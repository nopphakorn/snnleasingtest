$(document).ready(function() {
    $("#form_clogin").validate({
        rules: {
            c_username: { required: true },
            c_password: { required: true },
            c_chk: { required: true },
        },
        messages: {
            c_username: "กรอก Username",
            c_password: "กรอก Password",
            c_chk: "กรุณาทำเครื่องหมายยอมรับเงื่อนไข",
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },

        submitHandler: function(form) {
            $.ajax({
                url: "car_login_check.php",
                type: 'POST',
                data: $('#form_clogin').serialize(),
                success: function(data) {
                    if (data == 'ture') {
                        window.location = 'car.php'
                    } else {
                        $.alert({
                            theme: 'light',
                            type: 'red',
                            title: 'ผิดพลาด',
                            content: 'กรุณาตรวจเช็ค Username หรีอ Password ไม่ถูกต้อง',
                        });
                    }
                }
            });
        },
    });

    $("#form_csearch").validate({
        rules: {
            ct_id: { require_from_group: [1, ".g_search"] },
            cb_id: { require_from_group: [1, ".g_search"] },
            cg_id: { require_from_group: [1, ".g_search"] },
            cm_id: { require_from_group: [1, ".g_search"] },
            cg_id: { require_from_group: [1, ".g_search"] },
            c_gear: { require_from_group: [1, ".g_search"], },
            c_year: {
                require_from_group: [1, ".g_search"],
                number: true,
            },
        },
        messages: {
            ct_id: { require_from_group: "กรุณาเลือกรายการค้นหา" },
            cb_id: { require_from_group: "กรุณาเลือกรายการค้นหา" },
            cg_id: { require_from_group: "กรุณาเลือกรายการค้นหา" },
            cm_id: { require_from_group: "กรุณาเลือกรายการค้นหา" },
            cg_id: { require_from_group: "กรุณาเลือกรายการค้นหา" },
            c_gear: { require_from_group: "กรุณาเลือกรายการค้นหา" },
            c_year: {
                require_from_group: "กรุณาเลือกรายการค้นหา",
                number: "ระบุเป็นตัวเลข พ.ศ. เท่านั้น"
            },
        },
    });
})


function uzXmlHttp() {
    var xmlhttp = false;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
            xmlhttp = false;
        }
    }

    if (!xmlhttp && document.createElement) {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function list_ca(action, cb_id, ct_id, cg_id) {
    var url = 'car_search.php?action=' + action + '&cb_id=' + cb_id + '&ct_id=' + ct_id + '&cg_id=' + cg_id;
    xmlhttp = uzXmlHttp();
    xmlhttp.open("GET", url, false);
    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8"); // set Header
    xmlhttp.send(null);
    document.getElementById(action).innerHTML = xmlhttp.responseText;
}