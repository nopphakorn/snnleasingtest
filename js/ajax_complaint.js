$(document).ready(function() {
    $("#form_cp").validate({
        rules: {
            ab_cp_title: { required: true },
            ab_cp_name: { required: true },
            ab_cp_last: { required: true },
            ab_cp_email: {
                required: true,
                email: true
            },
            ab_cp_tel: { required: true },
            ab_cp_detail: { required: true },
            ab_cp_file: { extension: "JPG|PDF" },
        },
        messages: {
            ab_cp_title: "เลือกคำนำชื่อ",
            ab_cp_name: "กรอกชื่อ",
            ab_cp_last: "กรอกนามสกุล",
            ab_cp_email: {
                required: "กรอก Email",
                email: 'รูปแบบ Email ไม่ถูกต้อง'
            },
            ab_cp_tel: "กรอกหมายเลขโทรศัพท์",
            ab_cp_detail: "ระบุข้อความ",
            ab_cp_file: { extension: 'ไฟล์รูปภาพเฉพาะ JPG หรือ PDF เท่านั้น' },
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },

        submitHandler: function(form) {
            $.ajax({
                url: "about_complaint_sent.php",
                type: 'POST',
                data: $('#form_cp').serialize(),
                success: function(data) {
                    if (data == 'ture') {
                        $('#form_cp')[0].reset();
                        $.alert({
                            theme: 'light',
                            type: 'green',
                            title: 'เรียบร้อย',
                            content: 'ส่งเรื่องร้องเรียนเรียบร้อย',
                        });
                    } else if (data == 'false') {
                        $.alert({
                            theme: 'light',
                            type: 'red',
                            title: 'ผิดพลาด',
                            content: 'เกิดข้อผิดพลาดในการส่งเรื่องร้องเรียน',
                        });
                    }
                }
            });
        },
    });
})