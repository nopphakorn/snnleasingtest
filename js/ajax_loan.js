$(document).ready(function() {

    $("#form_lg").validate({
        rules: {
            l_type: { required: true },
            l_name: { required: true },
            l_province: { required: true },
            l_tel: { required: true },
        },
        messages: {
            l_type: "เลือประเภทสมัครขอสินเชื่อ",
            l_name: "กรอกชื่อ-นามสกุล",
            l_province: "กรอกจังหวัดที่อยู่ปัจจุบัน",
            l_tel: "กรอกเบอร์โทรศัพท์",
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },

        submitHandler: function(form) {
            $.ajax({
                url: "loan_save.php",
                type: 'POST',
                data: $('#form_lg').serialize(),
                success: function(data) {
					
                    if (data == 'ture') {
                        $('#form_lg')[0].reset();
                        $.alert({
                            theme: 'light',
                            type: 'green',
                            title: 'เรียบร้อย',
                            content: 'SNN ได้รับข้อมูลของท่านแล้ว<br>เราจะติดต่อกลับภายใน 1 วันทำการค่ะ',
                        });
                    } else {
                        $.alert({
                            theme: 'light',
                            type: 'red',
                            title: 'ผิดพลาด',
                            content: 'เกิดข้อผิดพลาดในการสมัคร',
                        });
                    }
                }
            });
        },
    });
})