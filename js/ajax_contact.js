$(document).ready(function() {
    $("#form_ct").validate({
        rules: {
            q1: { required: true },
            q2: { required: true },
            q3: { required: true },
            q4: {
                required: true,
                email: true
            },
            q5: { required: true },
        },
        messages: {
            q1: "เลือกหัวข้อติดต่อ",
            q2: "เลือกประเภทลูกค้า",
            q3: "กรอกซื้อ-นามสกุลผู้ติดต่อ",
            q4: {
                required: "กรอก Email",
                email: 'รูปแบบ Email ไม่ถูกต้อง'
            },
            q5: "กรอกหมายเลขโทรศัพท์",
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },

        submitHandler: function(form) {
            $.ajax({
                url: "contact_sent.php",
                type: 'POST',
                data: $('#form_ct').serialize(),
                success: function(data) {
                    if (data == 'ture') {
                        $('#form_ct')[0].reset();
                        $.alert({
                            theme: 'light',
                            type: 'green',
                            title: 'เรียบร้อย',
                            content: 'ส่ง ข้อความ เรียบร้อย',
                        });
                    } else if (data == 'false') {
                        $.alert({
                            theme: 'light',
                            type: 'red',
                            title: 'ผิดพลาด',
                            content: 'เกิดข้อผิดพลาดในการส่งข้อความ',
                        });
                    }
                }
            });
        },
    });
})