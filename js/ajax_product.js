$(document).ready(function() {
    $("#form_plogin").validate({
        rules: {
            p_username: { required: true },
            p_password: { required: true },
        },
        messages: {
            p_username: "กรอก Username",
            p_password: "กรอก Password",
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },

        submitHandler: function(form) {
            $.ajax({
                url: "product_login_check.php",
                type: 'POST',
                data: $('#form_plogin').serialize(),
                success: function(data) {
                    if (data == 'ture') {
                        window.location = 'product.php'
                    } else {
                        $.alert({
                            theme: 'light',
                            type: 'red',
                            title: 'ผิดพลาด',
                            content: 'กรุณาตรวจเช็ค Username หรีอ Password ไม่ถูกต้อง',
                        });
                    }
                }
            });
        },
    });
})