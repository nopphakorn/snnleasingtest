$(document).ready(function() {
    $("#tff_loginForm").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: { required: true },
        },
        messages: {
            email: {
                required: "กรอก Email",
                email: 'รูปแบบ Email ไม่ถูกต้อง'
            },
            password: "กรอก Password",
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },

        submitHandler: function(form) {
            $.ajax({
                url: "tff_login_check.php",
                type: 'POST',
                data: $('#tff_loginForm').serialize(),
                success: function(data) {
                    if (data == 'ture') {
                        window.location = 'tff.php'
                    } else {
                        $.alert({
                            theme: 'light',
                            type: 'red',
                            title: 'ผิดพลาด',
                            content: 'กรุณาตรวจเช็ค Email หรีอ Password ไม่ถูกต้อง',
                        });
                    }
                }
            });
        },
    });

    $("#form_tffreg").validate({
        rules: {
            tff_title: { required: true },
            tff_name: { required: true },
            tff_last: { required: true },
            tff_display: { required: true },
            tff_email: {
                required: true,
                email: true
            },
            tff_bd: { required: true },
            tff_bm: { required: true },
            tff_by: { required: true },
            tff_tel: { required: true },
            tff_pass: { required: true },
            tff_pass_confirm: {
                required: true,
                equalTo: "#tff_pass"
            },
            tff_no: { required: true },
            tff_job: { required: true },

            tff_ans1: { required: true },
            tff_ans2: { required: true },
            tff_ans3_1: { require_from_group: [1, ".a3"] },
            tff_ans3_2: { require_from_group: [1, ".a3"] },
            tff_ans3_3: { require_from_group: [1, ".a3"] },
            tff_ans3_4: { require_from_group: [1, ".a3"] },
            tff_ans3_5: { require_from_group: [1, ".a3"] },
            tff_ans3_6: { require_from_group: [1, ".a3"] },
            tff_ans3_7: { require_from_group: [1, ".a3"] },
            tff_ans3_8: { require_from_group: [1, ".a3"] },

            tff_ans4_1: { require_from_group: [1, ".a4"] },
            tff_ans4_2: { require_from_group: [1, ".a4"] },
            tff_ans4_3: { require_from_group: [1, ".a4"] },
            tff_ans4_4: { require_from_group: [1, ".a4"] },
            tff_ans4_5: { require_from_group: [1, ".a4"] },
            tff_ans4_6: { require_from_group: [1, ".a4"] },
            tff_ans4_7: { require_from_group: [1, ".a4"] },
            tff_ans4_8: { require_from_group: [1, ".a4"] },
            tff_ans4_9: { require_from_group: [1, ".a4"] },
            tff_ans4_10: { require_from_group: [1, ".a4"] },

            tff_check: { required: true },
        },
        messages: {
            tff_title: "เลือกคำนำชื่อ",
            tff_name: "กรอกชื่อ",
            tff_last: "กรอกนามสกุล",
            tff_display: "กรอก Display Name",
            tff_email: {
                required: "กรอก Email",
                email: 'รูปแบบ Email ไม่ถูกต้อง'
            },
            tff_bd: "กรอกวันที่",
            tff_bm: "เลือกเดือน",
            tff_by: "กรอก พ.ศ.",
            tff_tel: "กรอกหมายเลขโทรศัพท์",
            tff_pass: "กรอก Password",
            tff_pass_confirm: {
                required: "กรอกยืนยัน Password",
                equalTo: 'Password ไม่ตรงกัน'
            },
            tff_no: "กรอกหมายเลข",
            tff_job: "กรอกอาชีพ",

            tff_ans1: "กรุณาเลือกคำตอบ",
            tff_ans2: "กรุณาเลือกคำตอบ",
            tff_ans3_1: "กรุณาเลือกผลิตภัณฑ์ / บริการ",
            tff_ans4_1: "กรุณาเลือกคุณรู้จักเอส เอ็น เอ็น จากช่องทางใด",
            tff_check: "กรุณาทำเครื่องหมายว่าอ่านแล้ว ข้อตกลงและเงื่อนไข"

        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },

        submitHandler: function(form) {
            $.ajax({
                url: "tff_save.php",
                type: 'POST',
                data: $('#form_tffreg').serialize(),
                success: function(data) {
                    if (data == 'ture') {
                        $('#form_tffreg')[0].reset();
                        $.alert({
                            theme: 'light',
                            type: 'green',
                            title: 'เรียบร้อย',
                            content: 'สมัครเป็นสมาชิกเรียบร้อย',
                            buttons: {
                                Login: function() {
                                    location.href = 'tff_login.php';
                                }
                            }
                        });
                    } else if (data == 'false') {
                        $.alert({
                            theme: 'light',
                            type: 'red',
                            title: 'ผิดพลาด',
                            content: 'เกิดข้อผิดพลาดในการสมัคร',
                        });
                    } else if (data == 'double') {
                        $("#tff_email").focus();
                        $.alert({
                            theme: 'light',
                            type: 'orange',
                            title: 'ผิดพลาด',
                            content: 'Email ถูกใช้งานแล้ว',
                        });

                    }
                }
            });
        },
    });

    $("#form_tffps").validate({
        rules: {
            password: {
                required: true
            },
            password_new: {
                required: true
            },
            password_new_c: {
                required: true,
                equalTo: "#password_new"
            }
        },
        messages: {
            password: "กรอก รหัสผ่านปัจจุบัน",
            password_new: "กรอก รหัสผ่านใหม่",
            password_new_c: {
                required: "กรอก ยืนยันรหัสผ่านใหม่",
                equalTo: 'รหัสผ่าน ไม่ตรงกัน'
            },
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },

        submitHandler: function(form) {
            $.ajax({
                url: "tff_save.php",
                type: 'POST',
                data: $('#form_tffps').serialize(),
                success: function(data) {
                    if (data == 'ture') {
                        $('#form_tffps')[0].reset();
                        $.alert({
                            theme: 'light',
                            type: 'green',
                            title: 'เรียบร้อย',
                            content: 'แก้ไขรหัสผ่านเรียบร้อย',
                        });
                    } else if (data == 'false-old') {
                        $("#password").focus();
                        $.alert({
                            theme: 'light',
                            type: 'orange',
                            title: 'ผิดพลาด',
                            content: 'รหัสผ่านปัจจุบันกรอกไม่ถูกต้อง',
                        });
                    } else {
                        $.alert({
                            theme: 'light',
                            type: 'red',
                            title: 'ผิดพลาด',
                            content: 'เกิดข้อผิดพลาดไม่สามารถแก้ไขรหัสผ่านได้',
                        });
                    }
                }
            });
        },
    });

    $("#form_tffedit").validate({
        rules: {
            tff_title: { required: true },
            tff_name: { required: true },
            tff_last: { required: true },
            tff_display: { required: true },
            tff_bd: { required: true },
            tff_bm: { required: true },
            tff_by: { required: true },
            tff_tel: { required: true },
            tff_no: { required: true },
            tff_job: { required: true },

        },
        messages: {
            tff_title: "เลือกคำนำชื่อ",
            tff_name: "กรอกชื่อ",
            tff_last: "กรอกนามสกุล",
            tff_display: "กรอก Display Name",
            tff_bd: "กรอกวันที่",
            tff_bm: "เลือกเดือน",
            tff_by: "กรอก พ.ศ.",
            tff_tel: "กรอกหมายเลขโทรศัพท์",
            tff_no: "กรอกหมายเลข",
            tff_job: "กรอกอาชีพ",
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },

        submitHandler: function(form) {
            $.ajax({
                url: "tff_save.php",
                type: 'POST',
                data: $('#form_tffedit').serialize(),
                success: function(data) {
                    if (data == 'ture') {
                        $.alert({
                            theme: 'light',
                            type: 'green',
                            title: 'เรียบร้อย',
                            content: 'บันทึกแก้ไขข้อมูลส่วนตัวเรียบร้อย',
                            buttons: {
                                OK: function() {
                                    window.location.reload();
                                }
                            }
                        });
                    } else {
                        $.alert({
                            theme: 'light',
                            type: 'red',
                            title: 'ผิดพลาด',
                            content: 'เกิดข้อผิดพลาดไม่สามารถแก้ไขข้อมูลได้',
                        });
                    }
                }
            });
        },
    });

    $("#form_tff_fbreg").validate({
        rules: {
            tff_title: { required: true },
            tff_name: { required: true },
            tff_last: { required: true },
            tff_display: { required: true },
            tff_email: {
                required: true,
                email: true
            },
            tff_bd: { required: true },
            tff_bm: { required: true },
            tff_by: { required: true },
            tff_tel: { required: true },
            tff_pass: { required: true },
            tff_pass_confirm: {
                required: true,
                equalTo: "#tff_pass"
            },
            tff_no: { required: true },
            tff_job: { required: true },

            tff_ans1: { required: true },
            tff_ans2: { required: true },
            tff_ans3_1: { require_from_group: [1, ".a3"] },
            tff_ans3_2: { require_from_group: [1, ".a3"] },
            tff_ans3_3: { require_from_group: [1, ".a3"] },
            tff_ans3_4: { require_from_group: [1, ".a3"] },
            tff_ans3_5: { require_from_group: [1, ".a3"] },
            tff_ans3_6: { require_from_group: [1, ".a3"] },
            tff_ans3_7: { require_from_group: [1, ".a3"] },
            tff_ans3_8: { require_from_group: [1, ".a3"] },

            tff_ans4_1: { require_from_group: [1, ".a4"] },
            tff_ans4_2: { require_from_group: [1, ".a4"] },
            tff_ans4_3: { require_from_group: [1, ".a4"] },
            tff_ans4_4: { require_from_group: [1, ".a4"] },
            tff_ans4_5: { require_from_group: [1, ".a4"] },
            tff_ans4_6: { require_from_group: [1, ".a4"] },
            tff_ans4_7: { require_from_group: [1, ".a4"] },
            tff_ans4_8: { require_from_group: [1, ".a4"] },
            tff_ans4_9: { require_from_group: [1, ".a4"] },
            tff_ans4_10: { require_from_group: [1, ".a4"] },

            tff_check: { required: true },
        },
        messages: {
            tff_title: "เลือกคำนำชื่อ",
            tff_name: "กรอกชื่อ",
            tff_last: "กรอกนามสกุล",
            tff_display: "กรอก Display Name",
            tff_email: {
                required: "กรอก Email",
                email: 'รูปแบบ Email ไม่ถูกต้อง'
            },
            tff_bd: "กรอกวันที่",
            tff_bm: "เลือกเดือน",
            tff_by: "กรอก พ.ศ.",
            tff_tel: "กรอกหมายเลขโทรศัพท์",
            tff_pass: "กรอก Password",
            tff_pass_confirm: {
                required: "กรอกยืนยัน Password",
                equalTo: 'Password ไม่ตรงกัน'
            },
            tff_no: "กรอกหมายเลข",
            tff_job: "กรอกอาชีพ",

            tff_ans1: "กรุณาเลือกคำตอบ",
            tff_ans2: "กรุณาเลือกคำตอบ",
            tff_ans3_1: "กรุณาเลือกผลิตภัณฑ์ / บริการ",
            tff_ans4_1: "กรุณาเลือกคุณรู้จักเอส เอ็น เอ็น จากช่องทางใด",
            tff_check: "กรุณาทำเครื่องหมายว่าอ่านแล้ว ข้อตกลงและเงื่อนไข"

        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },

        submitHandler: function(form) {
            $.ajax({
                url: "tff_save.php",
                type: 'POST',
                data: $('#form_tff_fbreg').serialize(),
                success: function(data) {
                    if (data == 'ture') {
                        $('#form_tff_fbreg')[0].reset();
                        $.alert({
                            theme: 'light',
                            type: 'green',
                            title: 'เรียบร้อย',
                            content: 'สมัครเป็นสมาชิกเรียบร้อย',
                            buttons: {
                                Login: function() {
                                    location.href = 'tff_login.php';
                                }
                            }
                        });
                    } else {
                        $.alert({
                            theme: 'light',
                            type: 'red',
                            title: 'ผิดพลาด',
                            content: 'เกิดข้อผิดพลาดในการสมัคร',
                        });
                    }
                }
            });
        },
    });

    $("#tff_forgetForm").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
        },
        messages: {
            email: {
                required: "กรอก Email",
                email: 'รูปแบบ Email ไม่ถูกต้อง'
            },
        },
        errorPlacement: function(error, element) {
            var name = $(element).attr("name");
            error.appendTo($("#" + name + "_validate"));
        },

        submitHandler: function(form) {
            $.ajax({
                type: "POST",
                url: 'tff_forget_check.php',
                data: $('#tff_forgetForm').serialize(),
                success: function(data) {
                    if (data == 'ture') {
                        $('#tff_forgetForm')[0].reset();
                        $.alert({
                            theme: 'light',
                            type: 'green',
                            title: 'เรียบร้อย',
                            content: 'ระบบส่ง Password ใหม่ไปที่ Email เรียบร้อย',
                        });
                    } else {
                        $.alert({
                            theme: 'light',
                            type: 'red',
                            title: 'ผิดพลาด',
                            content: 'ไม่มี Email ที่สมัครสมาชิก TFF',
                        });
                    }
                }
            });
        },
    });
})