$(document).ready(function() {
    $(document).on('click', '.btn_youtube', function() {
        var id = $(this).attr("id");
        $.ajax({
            url: "youtube_show.php",
            method: "POST",
            data: { id: id },
            success: function(data) {
                if (data != 'false') {
                    $('#youtube_show').html(data);
                }
            }
        });

    });
})