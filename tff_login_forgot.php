<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'include/inc_header.php';?>
	<style>
		body {
			background:url(images/background/bg-tff.jpg) no-repeat;
			background-size:cover;
			background-attachment:fixed;
		}
	</style>
</head>
<body>	

  	<div class="container-fluid m-0 p-0"><?php include 's_header.php';?></div>
	<div class="container mt-4 pt-4 pb-4">
		<h3 style="text-align:center">ยินดีต้อนรับสมาชิก SNN LEASING MEMBERSHIP CLUB</h3>
		<div class="tff_login mt-4">
			<p><b>SNN LEASING MEMBERSHIP</b></p>
			<p>กรุณากรอกอีเมลของท่าน<br>เพื่อรับรหัสผ่านใหม่</p>
			<div class="row mr-0 ml-0 mb-3">
				<div class="col-sm-12">	
					<form id="tff_forgetForm" name="tff_forgetForm" method="POST">
						<input type="text" class="form-control mb-1" id="email" name="email" placeholder="email address">
						<div id="email_validate"></div>
						<button class="btn btn-lg btn-danger btn-block" type="submit" id="submit">ส่งขอรหัสใหม่</button>
					</form>
				</div>
			</div>

			<div class="row mr-0 ml-0">
				<div class="col-sm-12 pt-2">
					<p><a href="tff_register.php">สมัครสมาชิก</a></p>
					<p><a href="tff_login.php">เข้าสู่ระบบ TFF</a></p>
				</div>
			</div>	
		</div>
	</div>

  	<?php include 's_footer.php';?>
  	<?php include 'include/inc_script.php';?>
	<script type="text/javascript" src="library/validation/js/jquery.validate.js"></script>
  	<script type="text/javascript" src="library/validation/js/additional-methods.js"></script>
  	<script type="text/javascript" src="js/ajax_tff.js"></script>
</body>
</html>