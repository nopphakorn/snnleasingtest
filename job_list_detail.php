<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="job_detail";

if (isset($_GET['id'])) {$j_id = mysqli_real_escape_string($mysqli,$_GET['id']);}

$query_Re_j = "SELECT * FROM tb_job WHERE j_id = '".$j_id."' ";
$Re_j=$mysqli->query($query_Re_j);
$row_Re_j=$Re_j->fetch_assoc();
?>

<!DOCTYPE html>
<head>
	<?php include 'include/inc_header.php';?>
	<link href="library/lightbox2/css/lightbox.css" rel="stylesheet">
	<meta property="og:locale" 			content="th_TH" />
	<meta property="og:url"           	content="http://www.snnleasing.com/job_list_detail.php?id=<?php echo $row_Re_j['j_id'];?>" />
	<meta property="og:type"          	content="article" />
	<meta property="og:title"         	content="<?php echo $row_Re_j['j_title'];?>" />
	<meta property="og:description"   	content="<?php echo strip_tags($row_Re_j['j_detail']);?>" />
	<meta property="og:image"         	content="http://www.snnleasing.com/images/icon/career.png" />
	<meta property="fb:app_id"         	content="106896906786738" />

	<link href="library/kindeditor/themes/default/default.css" rel="stylesheet" />
	<script src="library/kindeditor/kindeditor-all-min.js"></script>
	<script type="text/javascript">
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('#j_detail_to', {
				readonlyMode : true,
                langType : 'en',
                items: ['justifyleft', 'justifycenter', 'justifyright', 'justifyfull',  '|', 'fullscreen'],
			});
        });
    </script>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-sm-12">
					<div class="row">
						<div class="col-md-12 px-5 tx_blue_content">
							<h3 class="my-4 tx_blue">ร่วมงานกับเรา</h3>
							<a href="job.php">ร่วมงานกับเรา</a> > <a href="job_list.php">ตำแหน่งงาน</a> > <?php echo $row_Re_j['j_title'];?>
							<hr class="hr_yellow">
							
							<h5><?php echo $row_Re_j['j_title'];?></h5>
							<br>
							<?php echo "<p><b>รายละเอียดของงาน :</b><br>".$row_Re_j['j_detail']."</p>";?>
							<?php echo "<p><b>คุณสมบัติ :</b><br>".$row_Re_j['j_property']."</p>";?>
							<?php echo "<p><b>สถานที่ทำงาน :</b><br>".$row_Re_j['j_location']."</p>";?>
							<?php echo "<p><b>สอบถามเพิ่มเติม :</b><br>".$row_Re_j['j_contact']."</p>";?>
							
						</div> 
					</div> 
					
					<div class="row justify-content-center">
						<div class="col-md-3">
							<button type="button" onclick="window.location.href='https://www.origami.life:8088/origami/login.php'" 
								class="btn btn-primary mx-auto d-block mb-2 btn-block"><i class="fas fa-pen-square"></i> สมัครงาน</button>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-md-3 text-md-right text-sm-center text-xs-center ">
							<button type="button" class="btn btn-primary mb-2 btn-block" 
								 data-toggle="collapse" data-target="#j_email" aria-expanded="false" aria-controls="j_email">
								 <i class="far fa-envelope"></i> ส่งให้เพื่อน
							</button>
						</div>
						<div class="col-md-3 text-md-left text-sm-center text-xs-center ">
							<button type="button" class="btn btn-primary mb-2 btn-block" 
								data-toggle="collapse" data-target="#j_Share" aria-expanded="false" aria-controls="j_Share">
								<i class="far fa-comments"></i> แชร์
							</button>
							<div class="collapse pt-2" id="j_Share">
								<a class="btn btn-lg btn_fb fb-xfbml-parse-ignore" href="https://www.facebook.com/sharer/sharer.php?u=http://www.snnleasing.com/job_list_detail.php?id=<?php echo $row_Re_j['j_id']; ?>&amp;src=sdkpreparse" rel="nofollow" target="_blank">
										<i class="fab fa-facebook-f"></i>
									</a>
							
								<a class="btn btn-lg btn_gl" href="https://plus.google.com/share?url=http://www.snnleasing.com/job_list_detail.php?id=<?php echo $row_Re_j['j_id']; ?>" rel="nofollow" target="_blank">
									<i class="fab fa-google-plus-g"></i>
								</a>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12 mb-3">
							<div class="collapse mt-4" id="j_email">
								<div class="card card-body">
									<form name="form_jmail" id="form_jmail" method="post" enctype="multipart/form-data" novalidate>
										<h5>ส่งให้เพื่อน</h5>
										<hr class="hr_yellow">
																		
										<div class="form-row mt-3">  
											<div class="form-group col-md-6">
												<label for="j_email_sent" class="col-form-label"><i class="far fa-envelope"></i> From Email</label>
												<input type="email" class="form-control" name="j_email_sent" id="j_email_sent" placeholder="Email ผู้ส่ง">
												<div id="j_email_sent_validate"></div>
											</div>
											<div class="form-group col-md-6">
												<label for="j_name_sent" class="col-form-label"><i class="fa fa-user" aria-hidden="true"></i> From Name</label>
												<input type="text" class="form-control" name="j_name_sent" id="j_name_sent" placeholder="ชื่อผู้ส่ง">
												<div id="j_name_sent_validate"></div>
											</div>
										</div>

										<div class="form-row">
											<div class="form-group col-md-6">
												<label for="j_email_to" class="col-form-label"><i class="far fa-envelope"></i> To Email</label>
												<input type="email" class="form-control" name="j_email_to" id="j_email_to" placeholder="email ผู้รับ">
												<div id="j_email_to_validate"></div>
											</div>
											<div class="form-group col-md-6">
												<label for="j_title_to" class="col-form-label"><i class="fa fa-user" aria-hidden="true"></i> ตำแหน่งงาน</label>
												<input type="text" class="form-control" name="j_title_to" id="j_title_to" placeholder="ชื่อผู้ส่ง" readonly value="<?php echo $row_Re_j['j_title'];?>">
												<div id="j_name_sent_validate"></div>
											</div>
										</div>

										<div class="form-row">
											<div class="form-group col-md-12">
												<label for="j_detail_to" class="col-form-label"><i class="far fa-file-alt"></i> รายละเอียด</label>
												<textarea class="form-control" name="j_detail_to" id="j_detail_to" style="height: 150px; width: 100%;"><?php echo $row_Re_j['j_detail'];?></textarea>
											</div>
										</div>
												
										<br>
										<input type="hidden" id="action" name="action" value="register">
										<button type="submit" id="submit" class="btn btn-primary">ส่งให้เพื่อน</button>
										<a class="btn btn-secondary" href="job_list_detail.php">ยกเลิกส่ง</a>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
	<script type="text/javascript" src="library/validation/js/jquery.validate.js"></script>
  	<script type="text/javascript" src="library/validation/js/additional-methods.js"></script>
	  <script type="text/javascript" src="js/ajax_job.js"></script>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.12&appId=106896906786738&autoLogAppEvents=1';
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>
