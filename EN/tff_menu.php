<?php
$sql_tff_chk="SELECT * FROM tb_tff WHERE tff_id = '".$_SESSION['SS_tff_id']."' AND tff_email = '".$_SESSION['SS_tff_email']."' AND tff_status = '1' ";
$Re_tff_chk=$mysqli->query($sql_tff_chk);
$row_Re_tff_chk=$Re_tff_chk->fetch_assoc();
?>
<div class="text-center">
	<img src="../images/logo/logo_tff.png" alt=""><br>
	<h5 class="tx_blue_content">SNN TRUE FRIEND FOREVER</h5>
</div>

<div class="tff_nav_side_menu">
	<div class="menu-list">
		<ul id="menu-content" class="menu-content collapse out">
			<li class="active">
				<a href="tff.php"><i class="fa fa-home" aria-hidden="true"></i> หน้าหลัก SNN TFF</a>
			</li>
			<li  data-toggle="collapse" data-target="#sub-1" class="collapsed ">
				<a href="#"><i class="fa fa-user" aria-hidden="true"></i> ข้อมูลของคุณ <span class="arrow"></span></a>
			</li>
				<ul class="sub-menu collapse logout" id="sub-1">
					<li><a href="tff_edit.php">แก้ไขข้อมูลส่วนตัว</a></li>
					<li><a href="tff_password.php">เปลี่ยนรหัสผ่าน</a></li>
					<li><a class="btn_link" data-title="ยกเลิกสมาชิก SNN TFF" href="tff_save.php?action=dele&id=<?php echo $row_Re_tff_chk['tff_id'];?>">ยกเลิกสมาชิก SNN TFF</a></li>
					<li id="btn_logout" class="logout">ออกจากระบบ</li>
				</ul>
			<li>
				<a href="tff_money.php"><i class="far fa-credit-card"></i> วางแผนการเงิน</a>
			</li>
			<li>
				<a href="tff_article.php"><i class="fa fa-book" aria-hidden="true"></i> บทความดีๆ</a>
			</li>
			<li>
				<a href="tff_phl.php"><i class="fa fa-university" aria-hidden="true"></i> อนาคตพิษณุโลก สู่สังคมเมือง</a>
			</li>
		</ul>
	</div>
</div>
<script>
$('#btn_logout').on('click', function () {
    $.confirm({
		theme: 'light',
        type: 'red',
        title: 'Logout',
        content: 'คุณต้องการออกจากระบบหรือไม่',
        buttons: {
            ออกจากระบบ: function () {
                window.location.href="tff_logout.php";
        	},
			ยกเลิก: function () {},
        }
    });
});

$('a.btn_link').confirm({
	theme: 'light',
    type: 'red',
    buttons: {
        ยืนยัน: function(){
            location.href = this.$target.attr('href');
        },
		ยกเลิก: function () {},
    }
});
</script>
