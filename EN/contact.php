<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="contact";

$sql_c="SELECT * FROM tb_content WHERE c_name='Contact' ";
$Re_c=$mysqli->query($sql_c);
$row_Re_c=$Re_c->fetch_assoc();
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
	<script type="text/javascript">
		$(document).ready(function () {
			$("#q_check").click(function () {
				if ($(this).is(':checked')) {
					var qq3=$('#q3').val();
					$('#q5').val(qq3);
				} else {
					$('#q5').val('');
				}
			});
		});
  </script>
  
 
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid"></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-lg-12">
					<h3 class="my-4 tx_blue">Contact SNN</h3>
					<hr class="hr_yellow mb-4">
					<div class="form_box bg-transparent"><?php echo $row_Re_c['c_detail_en'];?></div>
					
					<div class="form_box">
						<div class="form_box_content">
							<form name="form_ct" id="form_ct" method="post" enctype="multipart/form-data">
								<div class="form-group row mx-0">
									<label class="col-lg-3 col-form-label" for="q1">* หัวข้อ</label>
									<div class="col-lg-9">
										<select id="q1" name="q1" class="form-control">
											<option value="">โปรดระบุ</option>
											<option value="ร้องเรียนการให้บริการ">ร้องเรียนการให้บริการ</option>
											<option value="สมัครใช้บริการ">สมัครใช้บริการ</option>
											<option value="สอบถามข้อมูลและบริการ">สอบถามข้อมูลและบริการ</option>
										</select>
										<div id="q1_validate"></div>
									</div>
								</div>

								<div class="form-group row mx-0">
									<label class="col-lg-3 col-form-label" for="q2">* ประเภทลูกค้า</label>
									<div class="col-lg-9">
										<select id="q2" name="q2" class="form-control">
											<option value="">โปรดระบุ</option>
											<option value="ยังไม่เป็นลูกค้า">ยังไม่เป็นลูกค้า</option>
											<option value="ลูกค้าปัจจุบัน">ลูกค้าปัจจุบัน</option>
											<option value="ลูกค้าใหม่รอการพิจารณา">ลูกค้าใหม่รอการพิจารณา</option>
										</select>
										<div id="q2_validate"></div>
									</div>
								</div>

								<div class="form-group row mx-0">
									<label class="col-lg-3 col-form-label" for="q3">* ชื่อ-นามสกุลผู้ติดต่อ</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="q3" id="q3" placeholder="โปรดระบุ">
										<div id="q3_validate"></div>
									</div>
								</div>

								<div class="form-group row mx-0">
									<label class="col-lg-3 col-form-label" for="q4">* Email</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="q4" id="q4" placeholder="โปรดระบุ">
										<div id="q4_validate"></div>
									</div>
								</div>

								<div class="form-group row mx-0">
									<label class="col-lg-3 col-form-label" for="q5">* หมายเลขโทรศัพท์</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="q5" id="q5" placeholder="โปรดระบุ">
										<div id="q5_validate"></div>
									</div>
								</div>

								<div class="form-group row mx-0">
									<label class="col-lg-3 col-form-label" for="q6">เลขที่สัญญา</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="q6" id="q6" placeholder="โปรดระบุ">
										<div id="q6_validate"></div>
									</div>
								</div>

								<div class="form-group row mx-0">
									<label class="col-lg-3 col-form-label" for="q7">หมายเลขทะเบียนรถ</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="q7" id="q7" placeholder="โปรดระบุ">
										<div id="q7_validate"></div>
									</div>
								</div>


								<div class="form-row mx-0">
									<div class="form-group col-lg-12">
										<label for="q8" class="col-form-label">ข้อความ</label>
										<textarea class="form-control" name="q8" id="q8" rows="5"></textarea>
									</div>
								</div>
			
								<br>
								<div class="text-center">
									<input type="hidden" id="action" name="action" value="register">
									<button type="submit" id="submit" class="btn btn-primary">ส่งข้อความ</button>
								</div>
							</form>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
	<script type="text/javascript" src="library/validation/js/jquery.validate.js"></script>
  	<script type="text/javascript" src="library/validation/js/additional-methods.js"></script>
  	<script type="text/javascript" src="js/ajax_contact.js"></script>
</body>
</html>
