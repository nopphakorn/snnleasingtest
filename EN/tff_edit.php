<?php 
include 'tff_check.php';
include '../Connections/con_db.php';
include '../function/function.php';

$sql_tff="SELECT * FROM tb_tff WHERE tff_id = '".$_SESSION['SS_tff_id']."' AND tff_email = '".$_SESSION['SS_tff_email']."' AND tff_status = '1' ";
$Re_tff=$mysqli->query($sql_tff);
$row_Re_tff=$Re_tff->fetch_assoc();
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
	<script type="text/javascript">
    $(document).ready(function() {
		$("#tff_title_ohter").hide()
		if(t_chk=='นาย' || t_chk=='นาง' || t_chk=='นางสาว'){
			$("#tff_title_ohter").hide()
		}else{
			$("#tff_title_ohter").show()
		}
    });
    function get_tff_title(elem) {
      elem.checked && elem.value == "oth" ? $("#tff_title_ohter").show() : $("#tff_title_ohter").hide();
    };
  </script>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid"></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-sm-3"><?php include 'tff_menu.php';?></div>
				<div class="col-sm-9 mt-3 mb-4">
					<div class="form_box m-0 px-3 py-4">
						<form name="form_tffedit" id="form_tffedit" method="post" enctype="multipart/form-data">
							<h5>Edit TFF Profile : <?php echo "คุณ".$row_Re_tff['tff_name']." ".$row_Re_tff['tff_last'];?></h5>
							<hr class="hr_yellow">
							<div class="form-row">
								<div class="custom-control-inline">
									<label for="tff_title1">
									<input type="radio" name="tff_title" id="tff_title1" value="นาย" onClick="get_tff_title(this)" <?php if($row_Re_tff['tff_title']=="นาย"){echo "checked";}?>>
									นาย</label>
								</div>
								<div class="custom-control-inline">
									<label for="tff_title2">
									<input type="radio" name="tff_title" id="tff_title2" value="นาง" onClick="get_tff_title(this)" <?php if($row_Re_tff['tff_title']=="นาง"){echo "checked";}?>>
									นาง</label>
								</div>
								<div class="custom-control-inline">
									<label for="tff_title3">
									<input type="radio" name="tff_title" id="tff_title3" value="นางสาว" onClick="get_tff_title(this)" <?php if($row_Re_tff['tff_title']=="นางสาว"){echo "checked";}?>>
									นางสาว</label>
								</div>
								<div class="custom-control-inline">
									<label for="tff_title4">
									<input type="radio" name="tff_title" id="tff_title4" value="oth" onClick="get_tff_title(this)" 
									<?php if($row_Re_tff['tff_title']!="นาย" AND $row_Re_tff['tff_title']!="นาง" AND $row_Re_tff['tff_title']!="นางสาว"){echo "checked";}?>>
									อื่นๆ</label>
								</div>
								<div class="col-md-4">
									<input type="text" id="tff_title_ohter" name="tff_title_ohter" class="form-control" placeholder="ระบุคำนำหน้าอื่นๆ" value="<?php if($row_Re_tff['tff_title']!="นาย" AND $row_Re_tff['tff_title']!="นาง" AND $row_Re_tff['tff_title']!="นางสาว"){echo $row_Re_tff['tff_title'];}?>">
								</div>
							</div>
							<div id="tff_title_validate"></div>
							

							<div class="form-row mt-3">  
							<div class="form-group col-md-6">
								<label for="tff_name"><i class="fa fa-user-o" aria-hidden="true"></i> ชื่อ</label>
								<input type="text" class="form-control" name="tff_name" id="tff_name" placeholder="กรอก ชื่อ" value="<?php echo $row_Re_tff['tff_name'];?>">
								<div id="tff_name_validate"></div>
							</div>
							<div class="form-group col-md-6">
								<label for="tff_last"><i class="fa fa-user-o" aria-hidden="true"></i> นามสกุล</label>
								<input type="text" class="form-control" name="tff_last" id="tff_last" placeholder="กรอก นามสกุล" value="<?php echo $row_Re_tff['tff_last'];?>">
								<div id="tff_last_validate"></div>
							</div>
							</div>

							<div class="form-row">
							<div class="form-group col-md-6">
								<label for="tff_display" class="col-form-label"><i class="fa fa-user" aria-hidden="true"></i> Display Name</label>
								<input type="text" class="form-control" name="tff_display" id="tff_display" placeholder="กรอก Display Name" value="<?php echo $row_Re_tff['tff_display'];?>">
								<div id="tff_display_validate"></div>
							</div>
							</div>

							<div class="form-row">
							<div class="form-group col-md-6">
								<label for=""><i class="fa fa-calendar" aria-hidden="true"></i> วัน / เดือน / ปีเกิด</label>
								<div class="col-md-12 p-0">
								<div class="form-row pt-1">
									<?php
									$b_dd=substr($row_Re_tff['tff_bday'],8,2);
									$b_mm=substr($row_Re_tff['tff_bday'],5,2);
									$b_yy=(substr($row_Re_tff['tff_bday'],0,4))+543;
									?>
									<div class="form-group col-md-3">
									<label class="sr-only" for="tff_bd">วันที่</label>
									<input type="text" class="form-control" id="tff_bd" name="tff_bd" placeholder="วันที่" maxlength="2" value="<?php echo $b_dd;?>">
									<div id="tff_bd_validate"></div>
									</div>
									<div class="form-group col-md-6">
									<label class="sr-only" for="tff_bm">State</label>
									<select id="tff_bm" name="tff_bm" class="form-control">
										<option value="" <?php if($b_mm==""){echo "selected=\"selected\"";}?>>เดือน</option>
										<option value="01" <?php if($b_mm=="01"){echo "selected=\"selected\"";}?>>มกราคม</option>
										<option value="02" <?php if($b_mm=="02"){echo "selected=\"selected\"";}?>>กุมภาพันธ์</option>
										<option value="03" <?php if($b_mm=="03"){echo "selected=\"selected\"";}?>>มีนาคม</option>
										<option value="04" <?php if($b_mm=="04"){echo "selected=\"selected\"";}?>>เมษายน</option>
										<option value="05" <?php if($b_mm=="05"){echo "selected=\"selected\"";}?>>พฤษภาคม</option>
										<option value="06" <?php if($b_mm=="06"){echo "selected=\"selected\"";}?>>มิถุนายน</option>
										<option value="07" <?php if($b_mm=="07"){echo "selected=\"selected\"";}?>>กรกฎาคม</option>
										<option value="08" <?php if($b_mm=="08"){echo "selected=\"selected\"";}?>>สิงหาคม</option>
										<option value="09" <?php if($b_mm=="09"){echo "selected=\"selected\"";}?>>กันยายน</option>
										<option value="10" <?php if($b_mm=="10"){echo "selected=\"selected\"";}?>>ตุลาคม</option>
										<option value="11" <?php if($b_mm=="11"){echo "selected=\"selected\"";}?>>พฤศจิกายน</option>
										<option value="12" <?php if($b_mm=="12"){echo "selected=\"selected\"";}?>>ธันวาคม</option>
									</select>
									<div id="tff_bm_validate"></div>
									</div>
									<div class="form-group col-md-3">
									<label class="sr-only" for="tff_by">พ.ศ.เกิด</label>
									<input type="text" class="form-control" id="tff_by" name="tff_by" placeholder="พ.ศ.เกิด" maxlength="4" value="<?php echo $b_yy;?>">
									<div id="tff_by_validate"></div>
									</div>
								</div>
								</div>
							</div>
							<div class="form-group col-md-6">
								<label for="tff_tel" class="col-form-label"><i class="fa fa-phone" aria-hidden="true"></i> โทรศัพท์มือถือ</label>
								<input type="text" class="form-control" name="tff_tel" id="tff_tel" placeholder="กรอกโทรศัพท์มือถือ" value="<?php echo $row_Re_tff['tff_tel'];?>">
								<div id="tff_tel_validate"></div>
							</div>
							</div>

							<div class="form-row">
							<div class="form-group col-md-6">
								<label for="tff_no" class="col-form-label"><i class="fa fa-id-card-o" aria-hidden="true"></i> หมายเลขบัตรประชาชน/เลขที่หนังสือเดินทาง</label>
								<input type="text" class="form-control" name="tff_no" id="tff_no" placeholder="กรอก หมายเลข" value="<?php echo $row_Re_tff['tff_no'];?>">
								<div id="tff_no_validate"></div>
							</div>
							<div class="form-group col-md-6">
								<label for="tff_job" class="col-form-label"><i class="fa fa-id-badge" aria-hidden="true"></i> อาชีพของคุณ</label>
								<input type="text" class="form-control" name="tff_job" id="tff_job"placeholder="กรอก อาชีพของคุณ" value="<?php echo $row_Re_tff['tff_job'];?>">
								<div id="tff_job_validate"></div>
							</div>
							</div>

							<br>
							<input type="hidden" id="action" name="action" value="edit">
							<input type="hidden" id="tff_id" name="tff_id" value="<?php echo $row_Re_tff['tff_id'];?>">
							<button type="submit" id="submit" class="btn btn-primary">Edit</button>
							<a class="btn btn-secondary" href="tff.php">Cancel</a>
						</form>
						</div>
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
	<script type="text/javascript" src="../library/validation/js/jquery.validate.js"></script>
  <script type="text/javascript" src="../library/validation/js/additional-methods.js"></script>
  <script type="text/javascript" src="../js/ajax_tff.js"></script>
</body>
</html>
