<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="promotion";

$maxRows_Re_pm = 6;
$pageNum_Re_pm = 0;
if (isset($_GET['pageNum_Re_pm'])) {
  $pageNum_Re_pm = $_GET['pageNum_Re_pm'];
}
$startRow_Re_pm = (($pageNum_Re_pm-1) * $maxRows_Re_pm);
if($startRow_Re_pm<0){$startRow_Re_pm=0;}

$query_Re_pm = "SELECT * FROM tb_promotion ORDER BY pm_id DESC ";
$query_limit_Re_pm = sprintf("%s LIMIT %d, %d", $query_Re_pm, $startRow_Re_pm, $maxRows_Re_pm);
$Re_pm=$mysqli->query($query_limit_Re_pm);

if (isset($_GET['totalRows_Re_pm'])) {
  $totalRows_Re_pm = $_GET['totalRows_Re_pm'];
} else {
  $all_Re_pm=$mysqli->query($query_Re_pm);
  $totalRows_Re_pm=$all_Re_pm->num_rows;
}
$totalPages_Re_pm = ceil($totalRows_Re_pm/$maxRows_Re_pm);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_pm==0){$page=1;}
else if($pageNum_Re_pm>0){$page=$pageNum_Re_pm;}
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid"></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-md-12">
					<h3 class="my-4 tx_blue">Promotions</h3>
					<a href="promotion.php">Promotions</a> > Promotions List<br>
					<hr class="hr_yellow">
				</div>
			</div>

			<div class="row mb-3">
				<div class="col-md-12 col-lg-3">
					<a class="pm_menu" href="promotion.php">Promotions</a>
				</div>
				<div class="col-md-12 col-lg-9">
					<div class="row">
						<?php while ($row_Re_pm = $Re_pm->fetch_assoc()) { ?>
							<div class="col-md-6 p-1">
								<div class="card h-100">
									<div class="card-body">
										<div class="thumbnail mb-2">
											<a href="promotion_detail.php?id=<?php echo $row_Re_pm['pm_id'];?>">
											<div class="pm_img_list"><img class="card-img-top" src="../images/promotion/<?php echo $row_Re_pm['pm_photo'];?>"/></div></a>
										</div>
										<p class="card-text">
											<?php echo $row_Re_pm['pm_title']; ?>

											<div class="fb-share-button" 
												data-href="http://www.kamolchaibangkaew.com/gallery_detail.php?id=<?php echo $row_Re_g['g_id']; ?>" 
												data-layout="button_count">
											</div>
										</p>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="row mb-3">
						<div class="col-lg-12">
							<br>
							<div class="row mt-1">
								<div class="col-12 pr-2">
									<ul class="pagination justify-content-end">
									<li class="page-item <?php if($page==1){echo "disabled";}?>">
										<a class="page-link" href="<?php printf("%s?pageNum_Re_pm=%d%s", $currentPage, 0, $currentSent); ?>" aria-label="Previous">
										<i class="fa fa-angle-double-left" aria-hidden="true"></i>
										</a>
									</li>

									<li class="page-item <?php if($page==1){echo "disabled";}?>">
										<a class="page-link" href="<?php printf("%s?pageNum_Re_pm=%d%s", $currentPage, $page - 1, $currentSent); ?>" aria-label="Previous">
										<i class="fa fa-angle-left" aria-hidden="true"></i>
										</a>
									</li>

									<li class="page-item disabled">
										<span class="page-link">
										<?php 
										if($totalPages_Re_pm>0){
											echo $page."/".$totalPages_Re_pm;
										}else{
											echo "-";
										}
										?>
										</span>
									</li>

									<li class="page-item <?php if($page==$totalPages_Re_pm OR $totalPages_Re_pm<1){echo "disabled";}?>">
										<a class="page-link" href="<?php printf("%s?pageNum_Re_pm=%d%s", $currentPage, $page + 1, $currentSent); ?>" aria-label="Next">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
										</a>
									</li>
											
									<li class="page-item <?php if($page==$totalPages_Re_pm OR $totalPages_Re_pm<1){echo "disabled";}?>">
										<a class="page-link" href="<?php printf("%s?pageNum_Re_pm=%d%s", $currentPage, $totalPages_Re_pm, $currentSent); ?>" aria-label="Next">
										<i class="fa fa-angle-double-right" aria-hidden="true"></i>
										</a>
									</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
