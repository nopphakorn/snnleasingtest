<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="answer";

$maxRows_Re_a = 10;
$pageNum_Re_a = 0;
if (isset($_GET['pageNum_Re_a'])) {
  $pageNum_Re_a = $_GET['pageNum_Re_a'];
}
$startRow_Re_a = (($pageNum_Re_a-1) * $maxRows_Re_a);
if($startRow_Re_a<0){$startRow_Re_a=0;}

$query_Re_a = "SELECT * FROM tb_answer ORDER BY a_id ASC ";
$query_limit_Re_a = sprintf("%s LIMIT %d, %d", $query_Re_a, $startRow_Re_a, $maxRows_Re_a);
$Re_a=$mysqli->query($query_limit_Re_a);

if (isset($_GET['totalRows_Re_a'])) {
  $totalRows_Re_a = $_GET['totalRows_Re_a'];
} else {
  $all_Re_a=$mysqli->query($query_Re_a);
  $totalRows_Re_a=$all_Re_a->num_rows;
}
$totalPages_Re_a = ceil($totalRows_Re_a/$maxRows_Re_a);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_a==0){$page=1;}
else if($pageNum_Re_a>0){$page=$pageNum_Re_a;}

$sql_btel="SELECT * FROM tb_banner_tel WHERE ban_id = '1'";
$Re_btel=$mysqli->query($sql_btel);
$row_Re_btel=$Re_btel->fetch_assoc();
$totalRows_Re_btel=$Re_btel->num_rows;
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid"></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-md-12">
					<h3 class="my-4">Popular Questions</h3>
					<hr class="hr_yellow">
				</div>
			</div>

			<div class="row mb-3 p-4">
				<div class="col-md-12">
					<div class="row">
						<?php while ($row_Re_a = $Re_a->fetch_assoc()) { ?>
							<div class="row p-2">
								<div class="a_box_l"><img src="../images/icon/a_que.png" alt=""></div>
								<div class="a_box_r">
									<div class="a_que_box tx_blue"><?php echo $row_Re_a['a_que'];?></div>
								</div>
							</div>
							<div class="row w-100 p-2">
								<div class="a_box_l"><img src="../images/icon/a_anw.png" alt=""></div>
								<div class="a_box_r tx_blue"><?php echo $row_Re_a['a_anw'];?></div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="row mt-1">
				<div class="col-12">
					
					<ul class="pagination justify-content-center">
					<li class="page-item <?php if($page==1){echo "disabled";}?>">
						<a class="page-link" href="<?php printf("%s?pageNum_Re_a=%d%s", $currentPage, 0, $currentSent); ?>" aria-label="Previous">
						<i class="fa fa-angle-double-left" aria-hidden="true"></i>
						</a>
					</li>

					<li class="page-item <?php if($page==1){echo "disabled";}?>">
						<a class="page-link" href="<?php printf("%s?pageNum_Re_a=%d%s", $currentPage, $page - 1, $currentSent); ?>" aria-label="Previous">
						<i class="fa fa-angle-left" aria-hidden="true"></i>
						</a>
					</li>

					<li class="page-item disabled">
						<span class="page-link">
						<?php 
						if($totalPages_Re_a>0){
							echo $page."/".$totalPages_Re_a;
						}else{
							echo "-";
						}
						?>
						</span>
					</li>

					<li class="page-item <?php if($page==$totalPages_Re_a OR $totalPages_Re_a<1){echo "disabled";}?>">
						<a class="page-link" href="<?php printf("%s?pageNum_Re_a=%d%s", $currentPage, $page + 1, $currentSent); ?>" aria-label="Next">
						<i class="fa fa-angle-right" aria-hidden="true"></i>
						</a>
					</li>
							
					<li class="page-item <?php if($page==$totalPages_Re_a OR $totalPages_Re_a<1){echo "disabled";}?>">
						<a class="page-link" href="<?php printf("%s?pageNum_Re_a=%d%s", $currentPage, $totalPages_Re_a, $currentSent); ?>" aria-label="Next">
						<i class="fa fa-angle-double-right" aria-hidden="true"></i>
						</a>
					</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12"><img class="img-fluid mx-auto d-block" src="../images/background/bg_ans.png" alt=""></div>
			</div>
			<div class="container pt-4 pb-4">
			<div class="row p-0 m-0 mb-2">
				<?php if($totalRows_Re_btel>0){?>
				<div class="col-lg-12 text-center">
				<a href="<?php echo $row_Re_btel['ban_link'];?>">
				<img class="img-fluid" src="../images/banner_tel/<?php echo $row_Re_btel['ban_photo'];?>"/></a>
				</div>
				<?php } ?>
			</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
