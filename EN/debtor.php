<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="debtor";

$sql_c="SELECT * FROM tb_content WHERE c_name='Debtor' ";
$Re_c=$mysqli->query($sql_c);
$row_Re_c=$Re_c->fetch_assoc();

?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid"></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-md-12">
					<h3 class="my-4 tx_blue"><?php echo $row_Re_c['c_name_en'];?></h3>
					<hr class="hr_yellow mb-4">
					<div class="row">
						<div class="col-lg-12"><img class="float-right" src="../images/logo/logo_snn_t.png" alt=""></div>
					</div>
					<?php echo $row_Re_c['c_detail_en'];?>
				</div>
			</div>
		</div>
	</div>	
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
