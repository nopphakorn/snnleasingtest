<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="job_list";

$maxRows_Re_j = 50;
$pageNum_Re_j = 0;
if (isset($_GET['pageNum_Re_j'])) {
  $pageNum_Re_j = $_GET['pageNum_Re_j'];
}
$startRow_Re_j = (($pageNum_Re_j-1) * $maxRows_Re_j);
if($startRow_Re_j<0){$startRow_Re_j=0;}

$query_Re_j = "SELECT * FROM tb_job WHERE j_status = '1'  ORDER BY j_function ASC, j_id DESC ";
$query_limit_Re_j = sprintf("%s LIMIT %d, %d", $query_Re_j, $startRow_Re_j, $maxRows_Re_j);
$Re_j=$mysqli->query($query_limit_Re_j);

if (isset($_GET['totalRows_Re_j'])) {
  $totalRows_Re_j = $_GET['totalRows_Re_j'];
} else {
	$all_Re_j = $mysqli->query($query_Re_j);
	$totalRows_Re_j = $all_Re_j->num_rows;
}
$totalPages_Re_j = ceil($totalRows_Re_j/$maxRows_Re_j);
$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_j==0){$page=1;}
else if($pageNum_Re_j>0){$page=$pageNum_Re_j;}
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-sm-12 mt-3 mb-4">
					<div class="row">
						<div class="col-12">
							<h3 class="my-4 tx_blue">Jobs with SNN</h3>
							<a href="job.php">Career</a> > Jobs
							<div class="table-responsive-md mt-3">
								<table class="table table-bordered w-100 tb_head_blue">
									<thead>
										<tr>
											<th>JOB TITLE</th>
											<th>JOB FUNCTION</th>
										</tr>
									</thead>
									<?php 
									if($totalRows_Re_j>0){ 
										while($row_Re_j=$Re_j->fetch_assoc()){
									?>
									<tr>
										<td>
											<?php if($row_Re_j['j_urgent']=='URGENT'){echo "<span class=\"text-secondary\">".$row_Re_j['j_urgent']." | </span>";}?>
											<a href="job_list_detail.php?id=<?php echo $row_Re_j['j_id'];?>">
												<?php echo $row_Re_j['en_j_title'];?>
											</a>
										</td>
										<td class="text-center"><?php echo $row_Re_j['en_j_function'];?></td>
									</tr>
									<?php }} ?>
								</table>
								<p>
									<button type="button" onclick="window.location.href='https://www.origami.life:8088/origami/login.php'" class="btn btn-secondary mx-auto d-block"><i class="fas fa-envelope"></i> Resume</button>
								</p>
							</div>
							<div class="row mt-1">
								<div class="col-12 pr-2">
									<ul class="pagination justify-content-end">
									<li class="page-item <?php if($page==1){echo "disabled";}?>">
										<a class="page-link" href="<?php printf("%s?pageNum_Re_j=%d%s", $currentPage, 0, $currentSent); ?>" aria-label="Previous">
										<i class="fa fa-angle-double-left" aria-hidden="true"></i>
										</a>
									</li>

									<li class="page-item <?php if($page==1){echo "disabled";}?>">
										<a class="page-link" href="<?php printf("%s?pageNum_Re_j=%d%s", $currentPage, $page - 1, $currentSent); ?>" aria-label="Previous">
										<i class="fa fa-angle-left" aria-hidden="true"></i>
										</a>
									</li>

									<li class="page-item disabled">
										<span class="page-link">
										<?php 
										if($totalPages_Re_j>0){
											echo $page."/".$totalPages_Re_j;
										}else{
											echo "-";
										}
										?>
										</span>
									</li>

									<li class="page-item <?php if($page==$totalPages_Re_j OR $totalPages_Re_j<1){echo "disabled";}?>">
										<a class="page-link" href="<?php printf("%s?pageNum_Re_j=%d%s", $currentPage, $page + 1, $currentSent); ?>" aria-label="Next">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
										</a>
									</li>
											
									<li class="page-item <?php if($page==$totalPages_Re_j OR $totalPages_Re_j<1){echo "disabled";}?>">
										<a class="page-link" href="<?php printf("%s?pageNum_Re_j=%d%s", $currentPage, $totalPages_Re_j, $currentSent); ?>" aria-label="Next">
										<i class="fa fa-angle-double-right" aria-hidden="true"></i>
										</a>
									</li>
									</ul>
								</div>
							</div>

						</div> 
					</div> 
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
