<?php 
include 's_header_menu.php';

if($page_name!='service'){
$sql_banPage = "SELECT * FROM tb_banner_page WHERE ban_status='1' AND ban_page='".$page_name."'";
$Re_banPage=$mysqli->query($sql_banPage);
$row_Re_banPage=$Re_banPage->fetch_assoc();
$totalRows_Re_banPage=$Re_banPage->num_rows;
?>

<?php if($totalRows_Re_banPage>0){?>
<div class="row">
  <div class="col-12 p-0">
    <picture>
      <source srcset="../images/banner_page/<?php echo $row_Re_banPage['ban_photo']; ?>" media="(min-width: 768px)" class="w-100 img-fluid">
      <source srcset="../images/banner_page/<?php echo $row_Re_banPage['ban_photo_md']; ?>" media="(min-width: 576px)" class="w-100 img-fluid">
      <img srcset="../images/banner_page/<?php echo $row_Re_banPage['ban_photo_sm']; ?>" class="d-block w-100 img-fluid">
    </picture>
  </div>
</div>
<?php }} ?>
