<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="branch";

$bch_search ="";
if(isset($_POST['action'])){$action = mysqli_real_escape_string($mysqli,$_POST['action']);}
if(isset($_POST['bch_search'])){$bch_search = mysqli_real_escape_string($mysqli,$_POST['bch_search']);}
if(isset($_GET['bch_search'])){$bch_search = mysqli_real_escape_string($mysqli,$_GET['bch_search']);}
if(isset($_GET['id'])){$bch_id = mysqli_real_escape_string($mysqli,$_GET['id']);}

$sql_b ="SELECT * FROM tb_branch WHERE bch_id!='' ";
if(isset($action) AND $action="search"){
    if(isset($bch_search) AND $bch_search!=""){
        $sql_b.="AND bch_tag LIKE '%$bch_search%' ";
    }else if(isset($bch_id) AND $bch_id!=""){
        $sql_b.="AND bch_id = $bch_id ";
    }
}
$sql_b.="ORDER BY bch_no ASC";
$Re_b=$mysqli->query($sql_b);
$total_Re_b=$Re_b->num_rows;

$sql_bg ="SELECT * FROM tb_branch WHERE bch_id!='' ";
    if(isset($action) AND $action="search"){
    if(isset($bch_search) AND $bch_search!=""){
        $sql_bg.="AND bch_tag LIKE '%$bch_search%' ";
    }else if(isset($bch_id) AND $bch_id!=""){
        $sql_bg.="AND bch_id = $bch_id ";
    }
}
$sql_bg.="ORDER BY bch_no ASC";
$Re_bg=$mysqli->query($sql_bg);
$total_Re_bg=$Re_bg->num_rows;


if($total_Re_bg>0){
    while( $row_Re_bg = $Re_bg->fetch_assoc() ){
        $name = $row_Re_bg['bch_name'];
        $add = $row_Re_bg['bch_add'];
        $tel = $row_Re_bg['bch_tel'];
        $fax = $row_Re_bg['bch_fax'];
        $open = $row_Re_bg['bch_open'];
        $time = $row_Re_bg['bch_time'];
        $latitude = $row_Re_bg['bch_lat'];
	    $longitude = $row_Re_bg['bch_lng'];                              
        $link="http://maps.google.com/maps?daddr=".$latitude.",".$longitude."";
	    $locations[]=array( 'name'=>$name, 'add'=>$add, 'tel'=>$tel, 'fax'=>$fax, 'open'=>$open, 'time'=>$time, 'lat'=>$latitude, 'lng'=>$longitude, 'lnk'=>$link );
    }
}else{
    $latitude = '16.816635';
    $longitude = '100.317976';                              
    $locations[]=array( 'lat'=>$latitude, 'lng'=>$longitude);
}
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>

	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCVyX8iDXZT05ujiZltpnTSAxZi5BlWikA"></script> 
    <script type="text/javascript">
        var map;
        var Markers = {};
        var infowindow;
        var locations = [
            <?php for($i=0;$i<sizeof($locations);$i++){ $j=$i+1;?>
            [
                'SNN Leasing',
                '<div class="googlemap_d">'+
                '<ul><li><?php echo "<h5>SNN ".$locations[$i]['name']."</h5>";?></li></ul>'+
                '<ul><li class="st">ที่ตั้ง :</li><li><?php echo $locations[$i]['add'];?></li></ul>'+
                '<ul><li class="st">โทรศัพท์ :</li><li><?php echo $locations[$i]['tel'];?></li></ul>'+
                '<ul><li class="st">แฟ็กซ์ :</li><li><?php echo $locations[$i]['fax'];?></li></ul>'+
                '<ul><li class="st">เวลาทำการ :</li><li><?php echo $locations[$i]['open'];?></li></ul>'+
                '<ul><li class="st"></li><li><?php echo $locations[$i]['time'];?></li></ul>'+
                '<ul><li><a href="<?php echo $locations[$i]['lnk'];?>" target ="_blank"><i class="fas fa-map-marker-alt"></i> เส้นทางไปสาขา</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo $locations[$i]['lnk'];?>" target ="_blank"><i class="fas fa-map"></i> เปิด Google Map</a></p></li></ul>'+
                '</div>',
                <?php echo $locations[$i]['lat'];?>,
                <?php echo $locations[$i]['lng'];?>,
                0
            ]<?php if($j!=sizeof($locations))echo ","; }?>
        ];
        var origin = new google.maps.LatLng(locations[0][2], locations[0][3]);
        function initialize() {
        var mapOptions = {
            zoom: <?php if(!isset($bch_id) AND $bch_search==""){echo "6";}
            else if(!isset($bch_id) AND $bch_search=="ทั้งหมด"){echo "6";}
            else if(!isset($bch_id) AND $bch_search!=""){echo "9";}
            else if(isset($bch_id)){echo "18";}?>,
            center: origin
        };
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
            
            infowindow = new google.maps.InfoWindow();
            for(i=0; i<locations.length; i++) {
                var position = new google.maps.LatLng(locations[i][2], locations[i][3]);
                var marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    icon: "../images/icon/makemap3.png",
                });
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i][1]);
                        infowindow.setOptions({maxWidth: 500});
                        infowindow.open(map, marker);
                    }
                }) (marker, i));
            }
            locate(0);
        }
        function locate(marker_id) {
            var myMarker = Markers[marker_id];
            var markerPosition = myMarker.getPosition();
            map.setCenter(markerPosition);
            google.maps.event.trigger(myMarker, 'click');
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
    <div class="container-fluid bg_top">
        <div class="container pt-4 mb-4">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="my-4 tx_blue">Find a Branch</h3>
                            <hr class="hr_yellow">
                            <div class="col-sm-12 my-4">
                                <form class="form-inline justify-content-center" id="form_bch" name="form_bch" method="POST" action="branch.php">
                                    <input class="form-control w-50" type="search" 
                                    id="bch_search" name="bch_search" placeholder="ค้นหาจากจังหวัด, อำเภอ, ชื่อสาขา" aria-label="Search"
                                    value="<?php if(isset($_POST['bch_search'])){echo $_POST['bch_search'];} ?>";>
                                    <button type="submit" class="btn btn-primary ml-2 mr-2">Search</button>
                                    <button type="button" onclick="location.href='branch.php?action=search';" class="btn btn-warning ml-1 mr-0">ทั้งหมด</button>
                                </form>
                            </div>

                            <div id="map-canvas"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 mt-4 p-2 p-md-4 p-lg-4">
                            <div class="text-center">
                                <h4>เอส เอ็น เอ็น</h5>
                                <p class="tx_branch">
                                เปิดทำการวันจันทร์-ศุกร์ และทุกเสาร์ต้นเดือนและเสาร์สิ้นเดือน<br>
                                เวลา 08.00-17.00 น.
                                </p>
                                <p class="tx_branch">ผลการค้นหาสาขา " <?php echo $bch_search ;?> " พบทั้งหมด <?php echo $total_Re_b; ?> รายการ</p>
                            </div>

                            <div class="table-responsive-lg">
                                <table class="table table-bordered tb_head_blue tx_blue_content">
                                    <thead>
                                    <tr>
                                        <th width="70">จังหวัด</th>
                                        <th width="150">สาขา</th>
                                        <th width="">ที่ตั้งสาขา</th>
                                        <th width="200">จุดสังเกตุ</th>
                                        <th width="150">เบอร์ติดต่อ</th>
                                        <th width="150">แฟ็กซ์</th>
                                    </tr>
                                    </thead>
                                    <?php while($row_Re_b=$Re_b->fetch_assoc()){?>
                                    <tr>
                                        <td><?php echo $row_Re_b['bch_province'];?></td>
                                        <td>
                                            <a href="branch.php?id=<?php echo $row_Re_b['bch_id'];?>">
                                            <?php echo $row_Re_b['bch_name'];?></a>
                                        </td>
                                        <td><?php echo $row_Re_b['bch_add'];?></td>
                                        <td><?php echo $row_Re_b['bch_landmark'];?></td>
                                        <td><?php echo $row_Re_b['bch_tel'];?></td>
                                        <td><?php echo $row_Re_b['bch_fax'];?></td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
