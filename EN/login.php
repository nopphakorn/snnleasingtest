<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="";
?>

<!DOCTYPE html>
<html lang="en">
<head><?php include 'include/inc_header.php';?></head>
<body>
  <div class="container-fluid"><?php include 's_header.php';?></div>  

	  <div class="container-fluid bg_top">
      	<div class="container pt-4 mb-4">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="my-4 tx_blue"><i class="far fa-user"></i> Administator Login to Backoffice.</h3>
						Enter Username and Password to login.
            <hr class="ed_hr">

            <div class="card">
              <div class="card-body">
									<div class="form_box_login">
										<form name="form1" id="form1" method="post" enctype="multipart/form-data" ACTION="../backoffice/admin_backoffice_login_chk.php">
											<div class="form-row">  
												<div class="form-group col-md-12">
													<label for="username"><i class="fas fa-user"></i> Username</label>
													<input type="text" class="form-control" name="username" id="username" placeholder="Username">
													<div id="username_validate"></div>
												</div>
												<div class="form-group col-md-12">
													<label for="password"><i class="fas fa-key"></i> Password</label>
													<input type="password" class="form-control" name="password" id="password" placeholder="Password">
													<div id="password_validate"></div>
												</div>
											</div>												
											<button type="submit" id="submit" class="btn btn-primary">Login</button>
											<button type="button" id="submit" class="btn btn-secondary" onclick="location.href='index.php';">Cancel</button>
										</form>
									</div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
  <div class="container-fluid m-0 p-0 bg-dark"><?php include 's_footer.php';?></div>
  <?php include 'include/inc_script.php';?>
</body>
</html>
