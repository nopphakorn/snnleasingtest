<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="";
?>      

<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'include/inc_header.php';?>
  <script type="text/javascript">
    $(document).ready(function() {
      $("#tff_title_ohter").hide()
    });
    function get_tff_title(elem) {
      elem.checked && elem.value == "oth" ? $("#tff_title_ohter").show() : $("#tff_title_ohter").hide();
    };
  </script>
  <style>
		body {
			background:url(images/background/bg-tff.jpg) no-repeat;
			background-size:cover;
			background-attachment:fixed;
		}
	</style>
</head>
<body>  
  <div class="container-fluid m-0 p-0"><?php include 's_header.php';?></div>
  <div class="container mt-4 pt-4 pb-4">
    <div class="row px-2">
      <div class="col-12">
        <p style="text-align:center"><img class="img-fluid" src="images/icon/tx_snn_tff.png" alt=""></p>
        <hr>
        <div class="form_box tx_blue_content">
          <form name="form_tffreg" id="form_tffreg" method="post" action="tff_save.php" enctype="multipart/form-data" novalidate>
            <h5>สมาชิก SNN LEASING MEMBERSHIP CLUB</h5>
            <hr class="hr_yellow">
            <div class="form-row">
              <div class="custom-control-inline">
                <label for="tff_title1">
                <input type="radio" name="tff_title" id="tff_title1" value="นาย" onClick="get_tff_title(this)">
                นาย</label>
              </div>
              <div class="custom-control-inline">
                <label for="tff_title2">
                <input type="radio" name="tff_title" id="tff_title2" value="นาง" onClick="get_tff_title(this)">
                นาง</label>
              </div>
              <div class="custom-control-inline">
                <label for="tff_title3">
                <input type="radio" name="tff_title" id="tff_title3" value="นางสาว" onClick="get_tff_title(this)">
                นางสาว</label>
              </div>
              <div class="custom-control-inline">
                <label for="tff_title4">
                <input type="radio" name="tff_title" id="tff_title4" value="oth" onClick="get_tff_title(this)">
                อื่นๆ</label>
              </div>
              <div class="col-md-4">
                <input type="text" id="tff_title_ohter" name="tff_title_ohter" class="form-control" placeholder="ระบุคำนำหน้าอื่นๆ">
              </div>
            </div>
            <div id="tff_title_validate"></div>
            

            <div class="form-row mt-3">  
              <div class="form-group col-md-6">
                <label for="tff_name"><i class="fa fa-user-o" aria-hidden="true"></i> ชื่อ</label>
                <input type="text" class="form-control" name="tff_name" id="tff_name" placeholder="กรอก ชื่อ">
                <div id="tff_name_validate"></div>
              </div>
              <div class="form-group col-md-6">
                <label for="tff_last"><i class="fa fa-user-o" aria-hidden="true"></i> นามสกุล</label>
                <input type="text" class="form-control" name="tff_last" id="tff_last" placeholder="กรอก นามสกุล">
                <div id="tff_last_validate"></div>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="tff_display" class="col-form-label"><i class="fa fa-user" aria-hidden="true"></i> Display Name</label>
                <input type="text" class="form-control" name="tff_display" id="tff_display" placeholder="กรอก Display Name">
                <div id="tff_display_validate"></div>
              </div>
              <div class="form-group col-md-6">
                <label for="tff_email"  class="col-form-label"><i class="fa fa-envelope-o" aria-hidden="true"></i> Email address</label>
                <input type="email" class="form-control" name="tff_email" id="tff_email" aria-describedby="emailHelp" placeholder="กรอก email">
                <div id="tff_email_validate"></div>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for=""><i class="fa fa-calendar" aria-hidden="true"></i> วัน / เดือน / ปีเกิด</label>
                <div class="col-md-12 p-0">
                  <!--  -->
                  <div class="form-row pt-1">
                    <div class="form-group col-md-3">
                      <label class="sr-only" for="tff_bd">วันที่</label>
                      <input type="text" class="form-control" id="tff_bd" name="tff_bd" placeholder="วันที่" maxlength="2">
                      <div id="tff_bd_validate"></div>
                    </div>
                    <div class="form-group col-md-6">
                      <label class="sr-only" for="tff_bm">State</label>
                      <select id="tff_bm" name="tff_bm" class="form-control">
                        <option value="">เดือน</option>
                        <option value="01">มกราคม</option>
                        <option value="02">กุมภาพันธ์</option>
                        <option value="03">มีนาคม</option>
                        <option value="04">เมษายน</option>
                        <option value="05">พฤษภาคม</option>
                        <option value="06">มิถุนายน</option>
                        <option value="07">กรกฎาคม</option>
                        <option value="08">สิงหาคม</option>
                        <option value="09">กันยายน</option>
                        <option value="10">ตุลาคม</option>
                        <option value="11">พฤศจิกายน</option>
                        <option value="12">ธันวาคม</option>
                      </select>
                      <div id="tff_bm_validate"></div>
                    </div>
                    <div class="form-group col-md-3">
                      <label class="sr-only" for="tff_by">พ.ศ.เกิด</label>
                      <input type="text" class="form-control" id="tff_by" name="tff_by" placeholder="พ.ศ.เกิด" maxlength="4">
                      <div id="tff_by_validate"></div>
                    </div>
                  </div>
                  <!--  -->
                </div>
              </div>
              <div class="form-group col-md-6">
                <label for="tff_tel" class="col-form-label"><i class="fa fa-phone" aria-hidden="true"></i> โทรศัพท์มือถือ</label>
                <input type="text" class="form-control" name="tff_tel" id="tff_tel" placeholder="กรอกโทรศัพท์มือถือ">
                <div id="tff_tel_validate"></div>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="tff_pass" class="col-form-label"><i class="fa fa-key" aria-hidden="true"></i> รหัสผ่าน</label>
                <input type="password" class="form-control" name="tff_pass" id="tff_pass" placeholder="กรอก ยืนยันรหัสผ่าน">
                <div id="tff_pass_validate"></div>
              </div>
              <div class="form-group col-md-6">
                <label for="tff_pass_confirm" class="col-form-label"><i class="fa fa-key" aria-hidden="true"></i> ยืนยันรหัสผ่าน</label>
                <input type="password" class="form-control" name="tff_pass_confirm" id="tff_pass_confirm" placeholder="กรอก ยืนยันรหัสผ่าน">
                <div id="tff_pass_confirm_validate"></div>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="tff_no" class="col-form-label"><i class="fa fa-id-card-o" aria-hidden="true"></i> หมายเลขบัตรประชาชน/เลขที่หนังสือเดินทาง</label>
                <input type="text" class="form-control" name="tff_no" id="tff_no" placeholder="กรอก หมายเลข">
                <div id="tff_no_validate"></div>
              </div>
              <div class="form-group col-md-6">
                <label for="tff_job" class="col-form-label"><i class="fa fa-id-badge" aria-hidden="true"></i> อาชีพของคุณ</label>
                <select id="tff_job" name="tff_job" class="form-control">
                  <option value="">เลือกอาชีพ</option>
                  <option value="พนักงานเงินเดือนประจำ">พนักงานเงินเดือนประจำ</option>
                  <option value="ทหาร/ตำรวจ">ทหาร/ตำรวจ</option>
                  <option value="รับราชการ">รับราชการ</option>
                  <option value="พนักงานรัฐวิสาหกิจ">พนักงานรัฐวิสาหกิจ</option>
                  <option value="ลูกจ้างชั่วคราว">ลูกจ้างชั่วคราว</option>
                  <option value="อาชีพอิสระ">อาชีพอิสระ</option>
                  <option value="เจ้าของกิจการ มีหน้าร้านถาวร">เจ้าของกิจการ มีหน้าร้านถาวร</option>
                  <option value="เจ้าของกิจการ ไม่มีหน้าร้านถาวร">เจ้าของกิจการ ไม่มีหน้าร้านถาวร</option>
                </select>
                <div id="tff_job_validate"></div>
              </div>
            </div>

            <hr class="hr_yellow">

            <div class="form-group row">
              <div class="col-md-4"><i class="fa fa-question-circle-o" aria-hidden="true"></i> คุณเคยใช้บริการกับเอส เอ็น เอ็นหรือไม่</div>
              <div class="custom-control-inline col-md-1">
                <label for="tff_ans1_1">
                <input type="radio" name="tff_ans1" id="tff_ans1_1" value="เคย">
                เคย</label>
              </div>
              <div class="custom-control-inline col-md-2">
                <label for="tff_ans1_2">
                <input type="radio" name="tff_ans1" id="tff_ans1_2" value="ไม่เคย">
                ไม่เคย</label>
              </div>
              <span id="tff_ans1_validate"></span>
            </div>

            <div class="form-group row">
              <div class="col-md-4"><i class="fa fa-question-circle-o" aria-hidden="true"></i> คุณมีรถส่วนตัวหรือไม่</div>
              <div class="custom-control-inline col-md-1">
                <label for="tff_ans2_1">
                <input type="radio" name="tff_ans2" id="tff_ans2_1" value="มี">
                มี</label>
              </div>
              <div class="custom-control-inline col-md-1">
                <label for="tff_ans2_2">
                <input type="radio" name="tff_ans2" id="tff_ans2_2" value="ไม่มี">
                ไม่มี</label>
              </div>
              <span id="tff_ans2_validate"></span>
            </div>

            <div class="col-md-12 pl-0"><i class="fa fa-question-circle-o" aria-hidden="true"></i> ผลิตภัณฑ์ / บริการเอส เอ็น เอ็น ที่คุณสนใจ (ตอบได้มากกว่า 1 ข้อ)</div>
            <div class="form-row mb-2">
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans3_1" id="tff_ans3_1" class="a3" value="สินเชื่อรถยนต์">
                <label for="">สินเชื่อรถยนต์</label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans3_2" id="tff_ans3_2" class="a3" value="สินเชื่อรถเพื่อการเกษตร"> 
                <label for="">สินเชื่อรถเพื่อการเกษตร</label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans3_3" id="tff_ans3_3" class="a3" value="สินเชื่อบ้านที่ดิน"> 
                <label for="">สินเชื่อบ้านที่ดิน</label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans3_4" id="tff_ans3_4" class="a3" value="ประกันภัยรถยนต์/พ.ร.บ."> 
                <label for="">ประกันภัยรถยนต์</label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans3_5" id="tff_ans3_5" class="a3" value="ประกันภัยรถยนต์/พ.ร.บ."> 
                <label for="">พ.ร.บ.</label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans3_6" id="tff_ans3_6" class="a3" value="สินเชื่อเพื่อความงาม"> 
                <label for="">สินเชื่อเพื่อความงาม</label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans3_7" id="tff_ans3_7" class="a3" value="สินเชื่อเพื่อการท่องเที่ยว"> 
                <label for="">สินเชื่อเพื่อการท่องเที่ยว</label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans3_8" id="tff_ans3_8" class="a3" value="สินเชื่อเพื่อการศึกษา"> 
                <label for="">สินเชื่อเพื่อการศึกษา</label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="text" class="form-control" name="tff_ans3_9" class="a3" id="tff_ans3_9"placeholder="อื่นๆ โปรดระบุ">
              </div>
              <div id="tff_ans3_1_validate"></div>
            </div>

            <div class="col-md-12 pl-0"><i class="fa fa-question-circle-o" aria-hidden="true"></i> คุณรู้จักเอส เอ็น เอ็น จากช่องทางใดบ้าง (ตอบได้มากกว่า 1 ข้อ)</div>
            <div class="form-row mb-2">
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans4_1" id="tff_ans4_1" class="a4" value="Facebook"> 
                <label for="tff_ans4_1">Facebook</label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans4_2" id="tff_ans4_2" class="a4" value="Youtube"> 
                <label for="tff_ans4_2">Youtube</label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans4_3" id="tff_ans4_3" class="a4" value="ไลน์แอด"> 
                <label for="tff_ans4_3">ไลน์แอด</label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans4_4" id="tff_ans4_4" class="a4" value="เว็บไซด์"> 
                <label for="tff_ans4_4">เว็บไซด์ </label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans4_5" id="tff_ans4_5" class="a4" value="ป้ายโฆษณา"> 
                <label for="tff_ans4_5">ป้ายโฆษณา</label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans4_6" id="tff_ans4_6" class="a4" value="โบวชัวร์"> 
                <label for="tff_ans4_6">โบวชัวร์/ใบปลิว</label>
              </div>
              <div class="custom-control custom-checkbox col-md-3">
                <input type="checkbox" name="tff_ans4_7" id="tff_ans4_7" class="a4" value="การแนะนำ"> 
                <label for="tff_ans4_7">การแนะนำ</label>
              </div>
            </div>

            <div class="form-row">
              <div class="custom-control custom-checkbox col-md-2">
                <input type="checkbox" name="tff_ans4_8" id="tff_ans4_8" class="a4" value="วิทยุคลื่น"> 
                <label for="tff_ans4_8">วิทยุคลื่น</label>
              </div>
              <div class="custom-control custom-checkbox col-md-4">
                <input type="email" class="form-control" name="tff_ans4_8_detail" id="tff_ans4_8_detail"placeholder="โปรดระบุคลื่นวิทยุคลื่น">
              </div>
            </div>

            <div class="form-row">
              <div class="custom-control custom-checkbox col-md-2">
                <input type="checkbox" name="tff_ans4_10" id="tff_ans4_10" class="a4" value="อื่นๆ"> 
                <label for="tff_ans4_10">อื่นๆ โปรดระบุ</label>
              </div>
              <div class="custom-control custom-checkbox col-md-4">
                <input type="email" class="form-control" name="tff_ans4_10_detail" id="tff_ans4_10_detail"placeholder="อื่นๆ โปรดระบุ">
              </div>
            </div>
            <div id="tff_ans4_1_validate"></div>

            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="tff_no" class="col-form-label"><i class="fa fa-file-text-o" aria-hidden="true"></i> ข้อเสนอแนะ</label>
                <textarea class="form-control" name="tff_ans5" id="tff_ans5" rows="5"></textarea>
              </div>
            </div>
                      
            <br>
            <div class="form-group">
              <div class="form-check">
                <label for="gridCheck">
                  <input type="checkbox" id="tff_check" name="tff_check" >
                  ฉันได้อ่าน และยอมรับตาม <a href="tff_register_condition.php" >ข้อตกลงและเงื่อนไข</a>
                </label>
                <div id="tff_check_validate"></div>
              </div>
            </div>
            <input type="hidden" id="action" name="action" value="register">
            <button type="submit" id="submit" class="btn btn-primary">ยืนยันสมัครสมาชิก</button>
            <a class="btn btn-secondary" href="tff_login.php">ยกเลิกการสมัคร</a>
          </form>
        </div>
      </div> 
     </div> 
  </div>
	<div class="container-fluid m-0 p-0 bg-dark"><?php include 's_footer.php';?></div>
  <?php include 'include/inc_script.php';?>
  <script type="text/javascript" src="library/validation/js/jquery.validate.js"></script>
  <script type="text/javascript" src="library/validation/js/additional-methods.js"></script>
  <script type="text/javascript" src="js/ajax_tff.js"></script>
</head>
</body>
</html>
