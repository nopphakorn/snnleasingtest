<?php
if (!isset($_SESSION)) {session_start();}
$_SESSION['SS_tff_id']= NULL;
$_SESSION['SS_tff_email']= NULL;
$_SESSION['SS_tff_oauth']= NULL;
$_SESSION["SS_tff_status"]= NULL;
$_SESSION["SS_tff_access"]= NULL;

unset($_SESSION['SS_tff_id']);
unset($_SESSION['SS_tff_email']);
unset($_SESSION['SS_tff_oauth']);
unset($_SESSION["SS_tff_status"]);
unset($_SESSION["SS_tff_access"]);
header("Location: tff.php"); 
exit;
?>