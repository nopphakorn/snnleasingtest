<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="loan_register";

$sql_c="SELECT * FROM tb_content WHERE c_name='Loan' ";
$Re_c=$mysqli->query($sql_c);
$row_Re_c=$Re_c->fetch_assoc();

$sql_pv="SELECT * FROM tb_province ORDER BY p_name ASC ";
$Re_pv=$mysqli->query($sql_pv);
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid bg_top">
		<div class="container pt-0 pt-lg-4 pt-sm-4 mb-4">
			<div class="row">
					<div class="col-12">
						<h3 class="my-0 my-lg-4 my-sm-4 tx_blue">Loan Applying</h3>
						<hr class="hr_yellow">
						<div class="form_box">
							<div class="row p-0 p-lg-4 p-sm-4 m-0 justify-content-center">
								<div class="col-lg-10">
									<form name="form_lg" id="form_lg" class="pt-0 pt-lg-3 pt-sm-3" method="post" enctype="multipart/form-data">
										<div class="form-group row">
											<label for="l_type" class="col-md-4 col-form-label tx_form t-0">ประเภทสมัครขอสินเชื่อ* : </label>
											<div class="col-md-8">
											<select class="form-control" id="l_type" name="l_type">
												<option value="">เลือก</option>
												<option value="รถเก๋ง">รถส่วนบุคคล เก๋ง กระบะ รถตู้</option>
												<option value="รถกระบะ">รถเพื่อการเกษตร</option>
												<option value="รถตู้">บ้านที่ดิน(เฉพาะจังหวัดพิษณุโลก)</option>
												<option value="รถกระบะ">ประกันภัย</option>
												<option value="รถกระบะ">พ.ร.บ.</option>
											</select>
											<div id="l_type_validate"></div>
											</div>
										</div>
										<div class="form-group row">
											<label for="l_name" class="col-md-4 col-form-label tx_form t-0">ชื่อ-นามสกุล* : </label>
											<div class="col-md-8">
											<input type="text" class="form-control" id="l_name" name="l_name" placeholder="กรุณาใส่ ชื่อ-นามสกุล">
											<div id="l_name_validate"></div>
											</div>
										</div>
										<div class="form-group row">
											<label for="l_province" class="col-md-4 col-form-label tx_form pt-0">จังหวัดที่อยู่ปัจจุบัน* : </label>
											<div class="col-md-8">
											<select class="form-control" id="l_province" name="l_province">
												<option value="">กรุณาเลือกจังหวัดที่อยู่ปัจจุบัน</option>
												<?php while ($row_Re_pv=$Re_pv->fetch_assoc()) { ?>
													<option value="<?php echo $row_Re_pv['p_name'];?>"><?php echo $row_Re_pv['p_name'];?></option>
												<?php } ?>
											</select>
											<div id="l_province_validate"></div>
											</div>
										</div>
										<div class="form-group row">
											<label for="l_tel" class="col-md-4 col-form-label tx_form t-0">หมายเลขโทรศัพท์มือถือ* : </label>
											<div class="col-md-8">
											<input type="text" class="form-control"id="l_tel" name="l_tel" placeholder="กรุณาใส่หมายเลขโทรศัพท์มือถือ">
											<div id="l_tel_validate"></div>
											</div>
										</div>							
										
										<input type="hidden" id="action" name="action" value="register">
										<button type="submit" id="submit" class="btn btn-primary mx-auto d-block"><i class="fas fa-envelope"></i> ยืนยันสมัครขอสินเชื่อ</button>
									</form>
								</div>
							</div>
						</div>
					</div> 
				</div>

			<div class="row">
				<div class="col-12">
					<div class="form_box">
					<?php echo $row_Re_c['c_detail'];?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<div class="form_box tx_blue_content">
						<?php echo $row_Re_c['c_detail'];?>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<div class="form_box tx_blue_content">
						<div class="row text-center">
							<div class="col-12 col-lg-4">
								<a href="interest.php"><img class="img-fluid" src="../images/menu/01.png"></a>
							</div>
							<div class="col-12 col-lg-4">
								<a href="debtor.php"><img class="img-fluid" src="../images/menu/02.png"></a>
							</div>
							<div class="col-12 col-lg-4">
								<a href="answer.php"><img class="img-fluid" src="../images/menu/03.png"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<script type="text/javascript" src="../js/ajax_loan.js"></script>
</body>
</html>
