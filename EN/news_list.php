<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="";

$maxRows_Re_n = 30;
$pageNum_Re_n = 0;
if (isset($_GET['pageNum_Re_n'])) {
  $pageNum_Re_n = $_GET['pageNum_Re_n'];
}
$startRow_Re_n = (($pageNum_Re_n-1) * $maxRows_Re_n);
if($startRow_Re_n<0){$startRow_Re_n=0;}

$query_Re_n = "SELECT * FROM tb_news ORDER BY n_id DESC ";
$query_limit_Re_n = sprintf("%s LIMIT %d, %d", $query_Re_n, $startRow_Re_n, $maxRows_Re_n);
$Re_n=$mysqli->query($query_limit_Re_n);

if (isset($_GET['totalRows_Re_n'])) {
  $totalRows_Re_n = $_GET['totalRows_Re_n'];
} else {
  $all_Re_n=$mysqli->query($query_Re_n);
  $totalRows_Re_n=$all_Re_n->num_rows;
}
$totalPages_Re_n = ceil($totalRows_Re_n/$maxRows_Re_n);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_n==0){$page=1;}
else if($pageNum_Re_n>0){$page=$pageNum_Re_n;}
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
	<div class="container-fluid"><?php include 's_header.php';?></div>
	  
	 <div class="container-fluid bg_top">
      	<div class="container pt-4 mb-4">
        	<div class="row">
          		<div class="col-lg-12 ">
					<h3 class="my-4 tx_blue">News & Activities</h3>
					<a href="news.php">News & Activities</a> > List<br>
            		<hr class="hr_yellow">

					<div class="row">
						<?php while ($row_Re_n = $Re_n->fetch_assoc()) { ?>
							<div class="col-md-4 p-1">
								<div class="card h-100">
									<div class="card-body">
										<div class="thumbnail mb-2">
											<a href="news_list_detail.php?id=<?php echo $row_Re_n['n_id'];?>">
											<div class="n_img_list"><img class="card-img-top" src="../images/news/<?php echo $row_Re_n['n_photo'];?>"/></div></a>
										</div>
										<p class="card-text">
											<?php echo $row_Re_n['en_n_name']; ?>
										</p>
									</div>
									<div class="card-footer card-footer_new">
										<div class="row pt-2">
											<div class="col-6 text-left"><div class="ed_txt_date"><i class="far fa-calendar-alt"></i> <?php echo dateEng_m($row_Re_n['n_date']); ?></div></div>
											<div class="col-6 text-right">
												<div class="fb-share-button" 
													data-href="http://www.snnleasing.com/EN/news_list_detail.php?id=<?php echo $row_Re_n['n_id']; ?>" 
													data-layout="button_count" 
													data-size="small" 
													data-mobile-iframe="true">
													<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http://www.snnleasing.com/EN/news_list_detail.php?id=<?php echo $row_Re_n['n_id']; ?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">แชร์</a>
												</div>
												<div class="fb-like" data-href="http://www.snnleasing.com/EN/news_list_detail.php?id=<?php echo $row_Re_n['n_id']; ?>" 
												data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="row mt-1">
						<div class="col-12 pr-2">
							<ul class="pagination justify-content-end">
							<li class="page-item <?php if($page==1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_n=%d%s", $currentPage, 0, $currentSent); ?>" aria-label="Previous">
								<i class="fa fa-angle-double-left" aria-hidden="true"></i>
								</a>
							</li>

							<li class="page-item <?php if($page==1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_n=%d%s", $currentPage, $page - 1, $currentSent); ?>" aria-label="Previous">
								<i class="fa fa-angle-left" aria-hidden="true"></i>
								</a>
							</li>

							<li class="page-item disabled">
								<span class="page-link">
								<?php 
								if($totalPages_Re_n>0){
									echo $page."/".$totalPages_Re_n;
								}else{
									echo "-";
								}
								?>
								</span>
							</li>

							<li class="page-item <?php if($page==$totalPages_Re_n OR $totalPages_Re_n<1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_n=%d%s", $currentPage, $page + 1, $currentSent); ?>" aria-label="Next">
								<i class="fa fa-angle-right" aria-hidden="true"></i>
								</a>
							</li>
									
							<li class="page-item <?php if($page==$totalPages_Re_n OR $totalPages_Re_n<1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_n=%d%s", $currentPage, $totalPages_Re_n, $currentSent); ?>" aria-label="Next">
								<i class="fa fa-angle-double-right" aria-hidden="true"></i>
								</a>
							</li>
							</ul>
						</div>
					</div>
          		</div>
			</div>
      	</div>
  	</div>

  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.10';
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>
