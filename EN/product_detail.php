<?php 
include '../Connections/con_db.php';
include 'product_check.php';
include '../function/function.php';
$page_name="product";

$pro_id_chk = mysqli_real_escape_string($mysqli,$_GET['id']);
$query_Re_p = "SELECT * FROM tb_product WHERE pro_id='$pro_id_chk' ";
$Re_p=$mysqli->query($query_Re_p);
$row_Re_p=$Re_p->fetch_assoc();

$query_Re_pp = "SELECT * FROM tb_product_photo WHERE pro_id='$pro_id_chk' ORDER BY pro_p_id ASC";
$Re_pp=$mysqli->query($query_Re_pp);
$totalRows_Re_pp=$Re_pp->num_rows;

$query_Re_pp2 = "SELECT * FROM tb_product_photo WHERE pro_id='$pro_id_chk' ORDER BY pro_p_id ASC";
$Re_pp2=$mysqli->query($query_Re_pp);
$totalRows_Re_pp2=$Re_pp2->num_rows;
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
  	<link rel="stylesheet" href="../library/gallery/css/gallery.css">
</head>
<body>
	<div class="container-fluid"><?php include 's_header.php';?></div>
	  
	<div class="container-fluid bg_top">
      	<div class="container pt-4 mb-4">
        	<div class="row">
          		<div class="col-lg-12">
            		<h3 class="my-4">Car for sale</h3>
					<a href="product.php">Car for sale</a> > Detail<br>
            		<hr class="hr_yellow">

					<div class="row">
						<div class="col-sm-7 mb-3">
							<div>
								<p class="text-danger">
									<?php echo $row_Re_p['pro_year']." ";?>
									<?php echo $row_Re_p['pro_brand']." ";?>
									<?php echo $row_Re_p['pro_model']." ";?>
									<?php echo $row_Re_p['pro_model_detail'];?>
								</p>
								<div class="row">
								<div class="col-6 text-left"><?php echo "Update : ".$row_Re_p['pro_date']; ?></div>
								<div class="col-6 text-right"><h2><?php echo number_format($row_Re_p['pro_price'])." Baht"; ?></h2></div>
								</div>
							</div>
							<div id="mygallery" class="gallery">
								<div class="images">
									<?php 
									$check=0;
									while($row_Re_pp=$Re_pp->fetch_assoc()){ 
										$check+=1;
									?>
									<div class="<?php if($check==1){echo "active";} ?>" style="background-image: url(../images/product/<?php echo $row_Re_pp['pro_p_photo'];?>)"></div>
									<?php } ?>
									<span class="left"></span>
									<span class="right"></span>
								</div>

								<div class="thumbs">
									<?php
									$check2=0;
									while($row_Re_pp2=$Re_pp2->fetch_assoc()){ 
									$check2+=1;
									?>
									<div class="<?php if($check2==1){echo "active";} ?>" style="background-image: url(../images/product/<?php echo $row_Re_pp2['pro_p_photo'];?>)"></div>
									<?php } ?>
								</div>
							</div>
						</div>

						<div class="col-sm-5 mb-3">
							<h4>Car Detail</h4><br>
							<table class="table table-md">
								<tr p-1>
									<td><i class="fas fa-shopping-cart"></i> สถานะ</td>
									<td><div align="right">
										<?php 
										if($row_Re_p['pro_status']==0){echo "<font color='red'>ขายแล้ว</font>";}
										else if($row_Re_p['pro_status']==1){echo "ลงขาย";}
										?>
									</div></td>
								</tr>
								<tr p-1>
									<td><i class="fas fa-search"></i> ประเภทรถ</td>
									<td><div align="right">
										<?php 
										if($row_Re_p['pro_type']==0){echo "รถใหม่";}
										else if($row_Re_p['pro_type']==1){echo "รถมือสอง";}
										?>
									</div></td>
								</tr>
								<tr p-1>
									<td><i class="fas fa-car"></i> ยี่ห้อรถ</td>
									<td><div align="right"><?php echo $row_Re_p['pro_brand'];?></div></td>
								</tr>
								<tr p-1>
									<td><i class="fas fa-car"></i> รุ่นรถ</td>
									<td><div align="right"><?php echo $row_Re_p['pro_model'];?></div></td>
								</tr>
								<tr>
									<td><i class="fas fa-car"></i> โฉมรถยนต์</td>
									<td><div align="right"><?php echo $row_Re_p['pro_model_type'];?></div></td>
								</tr>
								<tr>
									<td><i class="fas fa-car"></i> รายละเอียดรุ่นรถ</td>
									<td><div align="right"><?php echo $row_Re_p['pro_model_detail'];?></div></td>
								</tr>
								<tr>
									<td><i class="far fa-calendar-alt"></i> ปี</td>
									<td><div align="right"><?php echo $row_Re_p['pro_year'];?></div></td>
								</tr>
								<tr>
									<td><i class="fas fa-sun"></i> ขนาดเครื่องยนต์</td>
									<td><div align="right"><?php echo $row_Re_p['pro_cc'];?></div></td>
								</tr>
								<tr>
									<td><i class="fas fa-cog"></i> ระบบเกียร์</td>
									<td><div align="right"><?php echo $row_Re_p['pro_gear'];?></div></td>
								</tr>
								<tr>
									<td><i class="fas fa-flag"></i> เลขไมล์(กม)</td>
									<td><div align="right"><?php echo $row_Re_p['pro_mile'];?></div></td>
								</tr>
								<tr>
									<td><i class="fas fa-pencil-alt"></i> สีรถ</td>
									<td><div align="right"><?php echo $row_Re_p['pro_color'];?></div></td>
								</tr>
								<tr>
									<td colspan="2"><?php echo $row_Re_p['pro_detail'];?></td>
								</tr>
							</table>
						</div>
					</div>

          		</div>
			</div>
      	</div>
  	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
	<script src="../library/gallery/bower_components/hammerjs/hammer.min.js"></script>
  	<script src="../library/gallery/js/gallery.min.js"></script>
 	<script src="../library/gallery/js/main.min.js"></script>
</body>
</html>
