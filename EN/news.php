<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="";

$query_Re_n = "SELECT * FROM tb_news WHERE n_status='1' ORDER BY n_id DESC LIMIT 0,6";
$Re_n=$mysqli->query($query_Re_n);
$totalRows_Re_n=$Re_n->num_rows;

$query_Re_v = "SELECT * FROM tb_video ORDER BY v_status DESC, v_id DESC LIMIT 0,6";
$Re_v=$mysqli->query($query_Re_v);
$totalRows_Re_v=$Re_v->num_rows;
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
	<div class="container-fluid"><?php include 's_header.php';?></div>
	 <div class="container-fluid bg_top">
      	<div class="container pt-4 mb-4">
        	<div class="row">
          		<div class="col-lg-12 ">
            		<h3 class="my-4 tx_blue">News & Activities</h3>
            		<hr class="hr_yellow">

					<div class="row">
						<?php while ($row_Re_n = $Re_n->fetch_assoc()) { ?>
							<div class="col-md-4 p-1">
								<div class="card h-100">
									<div class="card-body">
										<div class="thumbnail mb-2">
											<a href="news_list_detail.php?id=<?php echo $row_Re_n['n_id'];?>">
											<div class="n_img_list"><img class="card-img-top" src="../images/news/<?php echo $row_Re_n['n_photo'];?>"/></div></a>
										</div>
										<p class="card-text">
											<?php echo $row_Re_n['en_n_name']; ?>
										</p>
									</div>
									<div class="card-footer card-footer_new">
										<div class="row pt-2">
											<div class="col-6 text-left"><div class="ed_txt_date"><i class="far fa-calendar-alt"></i> <?php echo dateEng_m($row_Re_n['n_date']); ?></div></div>
											<div class="col-6 text-right">
												<div class="fb-share-button" 
													data-href="http://www.snnleasing.com/EN/news_list_detail.php?id=<?php echo $row_Re_n['n_id']; ?>" 
													data-layout="button_count" 
													data-size="small" 
													data-mobile-iframe="true">
													<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http://www.snnleasing.com/EN/news_list_detail.php?id=<?php echo $row_Re_n['n_id']; ?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">แชร์</a>
												</div>
												<div class="fb-like" data-href="http://www.snnleasing.com/EN/news_list_detail.php?id=<?php echo $row_Re_n['n_id']; ?>" 
												data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="row mt-2 mb-4">
						<div class="col-md-12 text-center">
							<hr>
							<a class="btn btn-secondary" href="news_list.php"><i class="fas fa-list"></i> Load more</a>
						</div>
					</div>
          		</div>
			</div>
      	</div>
  	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = 'https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.12&appId=106896906786738&autoLogAppEvents=1';
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>
