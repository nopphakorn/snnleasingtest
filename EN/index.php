<?php 
include './../Connections/con_db.php';
include '../function/function.php';

$sql_rv="SELECT * FROM tb_review WHERE rv_status='1' ORDER BY rv_no ASC";
$Re_rv=$mysqli->query($sql_rv);
$total_Re_rv=$Re_rv->num_rows;
if (!$Re_rv) {printf("Error: %s\n", $mysqli->error);}

$sql_v="SELECT * FROM tb_video WHERE v_status='1' ORDER BY v_id DESC LIMIT 1";
$Re_v=$mysqli->query($sql_v);
$row_Re_v=$Re_v->fetch_assoc();
$total_Re_v=$Re_v->num_rows;

$sql_vl="SELECT * FROM tb_video WHERE v_status='1' ORDER BY v_id DESC LIMIT 1,3";
$Re_vl=$mysqli->query($sql_vl);
$total_Re_vl=$Re_vl->num_rows;

$sql_btel="SELECT * FROM tb_banner_tel WHERE ban_id = '1'";
$Re_btel=$mysqli->query($sql_btel);
$row_Re_btel=$Re_btel->fetch_assoc();
$totalRows_Re_btel=$Re_btel->num_rows;
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <?php include 'include/inc_header.php';?>
   <script type="text/javascript" src="../js/ajax_youtube.js"></script>
</head>
<body>
  <div class="container-fluid"><?php include 's_header_idx.php';?></div>
  <div class="container-futid py-lg-4 py-md-4 py-sm-2 py-2 bg_yellow">
    <div class="row p-0 m-0">
      <div class="col-sm-12">
        <form class="form-inline justify-content-center" id="form_bch" name="form_bch" method="POST" action="branch.php">
          <input class="form-control w-40" type="search" id="bch_search" name="bch_search" placeholder="้Search your province, district or branch" aria-label="Search">
          <button type="submit" class="btn btn-primary ml-2 mr-2"><i class="fa fa-search"></i> Search</button>
          <h5 class="tx_gray mt-2 mb-0 mt-md-0 mt-lg-0">Branch Locations</h5>
        </form>
      </div>
    </div>
  </div>

  <div class="container-fluid bg_top">
    <div class="container pb-2 pt-2 pb-lg-2">
      <div class="row p-0 m-0 mt-5">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 col-6 mb-0 mb-lg-3">
          <a class="btn idx_menu d-block rounded-circle p-0" href="service.php#sv1" role="button">
            <img src="../images/icon/menu_car.png" class="w-100 mx-auto"></a>
          <div class="text-center mt-3 mb-3">
            <h5 class="tx_blue_bk tx_sm"><strong>Car Loan</strong></h5>
          </div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 col-6 mb-0 mb-lg-3">
          <a class="btn idx_menu d-block rounded-circle p-0" href="service.php#sv2" role="button">
            <img src="../images/icon/menu_agriculture.png" class="w-100 mx-auto"></a>
          <div class="text-center mt-3 mb-3">
            <h5 class="tx_blue_bk tx_sm"><strong>Tractor Loan</strong></h5>
          </div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 col-6 mb-0 mb-lg-3">
          <a class="btn idx_menu d-block rounded-circle p-0" href="service.php#sv3" role="button">
            <img src="../images/icon/menu_home.png" class=" img-fluid mx-auto"></a>
          <div class="text-center mt-3 mb-3">
            <h5 class="tx_blue_bk tx_sm"><strong>House & Land Loan</strong></h5>
          </div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 col-6 mb-0 mb-lg-3">
          <a class="btn idx_menu d-block rounded-circle p-0" href="service.php#sv4" role="button">
            <img src="../images/icon/menu_insure.png" class="w-100 mx-auto"></a>
          <div class="text-center mt-3 mb-3">
            <h5 class="tx_blue_bk tx_sm"><strong>Car Insurance</strong></h5>
          </div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 col-6 mb-0 mb-lg-3">
          <a class="btn idx_menu d-block rounded-circle p-0" href="service.php#sv5" role="button">
            <img src="../images/icon/menu_pp.png" class="w-100 mx-auto"></a>
          <div class="text-center mt-3 mb-3">
            <h5 class="tx_blue_bk tx_sm"><strong>Car Act</strong></h5>
          </div>
        </div>
      </div>
    </div>

    <div class="container pt-0 pb-0 pt-lg-4 pb-lg-4">
      <div class="row p-0 m-0 mb-2">
        <?php if($totalRows_Re_btel>0){?>
            <div class="col-lg-12 text-center">
              <a href="<?php echo $row_Re_btel['ban_link'];?>">
              <picture>
                <source srcset="../images/banner_tel/<?php echo $row_Re_btel['ban_photo']; ?>" media="(min-width: 768px)" class="w-100 img-fluid">
                <source srcset="../images/banner_tel/<?php echo $row_Re_btel['ban_photo_md']; ?>" media="(min-width: 576px)" class="w-100 img-fluid">
                <img srcset="../images/banner_tel/<?php echo $row_Re_btel['ban_photo_sm']; ?>" class="d-block w-100 img-fluid">
              </picture>
              </a>
            </div>
        <?php } ?>
      </div>
    </div>

    <div class="container pt-0 pt-lg-4 pb-4">
      <div class="row p-0 m-0 mb-2">
        <div class="title_rv">Why SNN</div>
      </div>
      <div class="row p-0 m-0 mb-2">
        <?php while($row_Re_rv=$Re_rv->fetch_assoc()){?>
        <div class="col-lg-6 p-3">
          <div class="rv_img_list"><img class="card-img-top" src="../images/review/<?php echo $row_Re_rv['rv_photo'];?>"/></div></a>
        </div>
        <?php } ?>
      </div>
    </div>

    <div class="container p-4 mt-4">
      <div class="row p-0 m-0 mb-2">
          <div class="col-lg-8">
            <div class="card flex-md-row mx-1 mb-4">
              <div class="card-body p-0">
                <div class="youtube_box">
                  <?php
                  $url = $row_Re_v['v_url'];
                  preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $matches);
                  $id = $matches[1];
                  ?>
                  <div id="youtube_show">
                      <object>
                          <param name="movie" value="http://www.youtube.com/v/<?php echo $id; ?>?version=3" />
                          <param name="allowFullScreen" value="true" />
                          <param name="allowScriptAccess" value="always" />
                          <embed src="http://www.youtube.com/v/<?php echo $id; ?>?version=3" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always"> </embed>
                      </object>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4">
            <?php while($row_Re_vl=$Re_vl->fetch_assoc()){?>
            <div class="card flex-md-row mx-1 btn_youtube p-0" id="<?php echo $row_Re_vl['v_id'];?>">
                <div class="col-lg-12 p-0 ">
                    <?php
                    $url = $row_Re_vl['v_url'];
                    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $matches);
                    $idl = $matches[1];
                    ?>
                    <div class="img_youtube_list"><img class="w-100" src="http://img.youtube.com/vi/<?php echo $idl; ?>/0.jpg"/> </div>              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include 's_footer.php';?>
  <?php include 'include/inc_script.php';?>
</body>
</html>
