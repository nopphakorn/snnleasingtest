<?php
include 's_header_menu.php';
$sql_ban = "SELECT * FROM tb_banner WHERE ban_status='1' ORDER BY ban_no ASC";
$Re_ban=$mysqli->query($sql_ban);
$totalRows_Re_ban=$Re_ban->num_rows;
?>
<div class="row">
  <div class="col-12 p-0">
    <div id="banner_top" class="carousel slide" style="data-interval" data-ride="carousel">
      <ol class="carousel-indicators">
        <?php
        for( $i = 0; $i <= ($totalRows_Re_ban-1); $i ++){ ?>
          <li data-target="#banner_top" data-slide-to="<?php echo $i;?>" <?php if($i==0){ echo "class=\"active\"";}?>></li>
        <?php } ?>
      </ol>
      <div class="carousel-inner">
        <?php
        $j = 1;
        while($row_Re_ban=$Re_ban->fetch_assoc()){ 
        ?>
          <div class='img-fluid carousel-item <?php if($j==1){ echo "active";}?>' >
              <picture>
                <source srcset="../images/banner/<?php echo $row_Re_ban['ban_photo']; ?>" media="(min-width: 768px)" class="w-100 img-fluid">
                <source srcset="../images/banner/<?php echo $row_Re_ban['ban_photo_md']; ?>" media="(min-width: 576px)" class="w-100 img-fluid">
                <img srcset="../images/banner/<?php echo $row_Re_ban['ban_photo_sm']; ?>" class="d-block w-100 img-fluid">
              </picture>
          </div>
        <?php $j++;
        } ?>
      </div>
      <a class="carousel-control-prev" href="#banner_top" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#banner_top" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
</div>

