<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="policy";

$sql_c="SELECT * FROM tb_content WHERE c_name='Policy' ";
$Re_c=$mysqli->query($sql_c);
$row_Re_c=$Re_c->fetch_assoc();
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid"></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-5">
			<div class="row px-3 px-lg-4">
				<div id="col-md-12 p-2 p-lg-4">
					<h3 class="my-4 tx_blue"><?php echo $row_Re_c['c_name_en'];?></h3>
					<hr class="hr_yellow w-100">
					<?php echo $row_Re_c['c_detail_en'];?>
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
