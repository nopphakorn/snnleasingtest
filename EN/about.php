<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="about";

$sql_ab="SELECT * FROM tb_content WHERE c_name='About' ";
$Re_ab=$mysqli->query($sql_ab);
$row_Re_ab=$Re_ab->fetch_assoc();

$sql_vs="SELECT * FROM tb_content WHERE c_name='Vision' ";
$Re_vs=$mysqli->query($sql_vs);
$row_Re_vs=$Re_vs->fetch_assoc();

$sql_og="SELECT * FROM tb_content WHERE c_name='Organize' ";
$Re_og=$mysqli->query($sql_og);
$row_Re_og=$Re_og->fetch_assoc();

$sql_bd="SELECT * FROM tb_content WHERE c_name='Board' ";
$Re_bd=$mysqli->query($sql_bd);
$row_Re_bd=$Re_bd->fetch_assoc();

$sql_cp="SELECT * FROM tb_content WHERE c_name='Complaint' ";
$Re_cp=$mysqli->query($sql_cp);
$row_Re_cp=$Re_cp->fetch_assoc();

$sql_sc="SELECT * FROM tb_content WHERE c_name='Social' ";
$Re_sc=$mysqli->query($sql_sc);
$row_Re_sc=$Re_sc->fetch_assoc();

$sql_cn="SELECT * FROM tb_content WHERE c_name='Company' ";
$Re_cn=$mysqli->query($sql_cn);
$row_Re_cn=$Re_cn->fetch_assoc();

$sql_cn2="SELECT * FROM tb_company ORDER BY cn_no ASC";
$Re_cn2=$mysqli->query($sql_cn2);
$total_Re_cn2=$Re_cn2->num_rows;
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid"></div>
	<div class="container-fluid bg_top pt-5">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-md-12 col-lg-3">
					<div class="nav flex-column nav-pills ab_nav_pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" id="v-ab-tab" data-toggle="pill" href="#v-ab" role="tab" aria-controls="v-ab" ><?php echo $row_Re_ab['c_name_en'];?></a>
						<a class="nav-link" id="v-vs-tab" data-toggle="pill" href="#v-vs" role="tab" aria-controls="v-vs" ><?php echo $row_Re_vs['c_name_en'];?></a>
						<a class="nav-link" id="v-og-tab" data-toggle="pill" href="#v-og" role="tab" aria-controls="v-og" ><?php echo $row_Re_og['c_name_en'];?></a>
						<a class="nav-link" id="v-bd-tab" data-toggle="pill" href="#v-bd" role="tab" aria-controls="v-bd" ><?php echo $row_Re_bd['c_name_en'];?></a>
						<a class="nav-link" id="v-cp-tab" data-toggle="pill" href="#v-cp" role="tab" aria-controls="v-cp" ><?php echo $row_Re_cp['c_name_en'];?></a>
						<a class="nav-link" id="v-sc-tab" data-toggle="pill" href="#v-sc" role="tab" aria-controls="v-sc" ><?php echo $row_Re_sc['c_name_en'];?></a>
						<a class="nav-link" id="v-cn-tab" data-toggle="pill" href="#v-cn" role="tab" aria-controls="v-cn" ><?php echo $row_Re_cn['c_name_en'];?></a>
						<a class="nav-link" href="contact.php">Contact SNN</a>
					</div>
				</div>
				<div class="col-md-12 col-lg-9">
					<div class="tab-content mt-1" id="v-pills-tabContent">
					<div class="tab-pane fade show active" id="v-ab" role="tabpanel" aria-labelledby="v-ab-tab">
						<h3 class="mb-4 tx_blue"><?php echo $row_Re_ab['c_name_en'];?></h3><hr class="hr_yellow">
						<?php echo $row_Re_ab['c_detail_en'];?>
					</div>
					<div class="tab-pane fade" id="v-vs" role="tabpanel" aria-labelledby="v-vs-tab">
						<h3 class="mb-4 tx_blue"><?php echo $row_Re_vs['c_name_en'];?></h3><hr class="hr_yellow">
						<?php echo $row_Re_vs['c_detail_en'];?>
					</div>
					<div class="tab-pane fade" id="v-og" role="tabpanel" aria-labelledby="v-og-tab">
						<h3 class="mb-4 tx_blue"><?php echo $row_Re_og['c_name_en'];?></h3><hr class="hr_yellow">
						<?php echo $row_Re_og['c_detail_en'];?>
					</div>
					<div class="tab-pane fade" id="v-bd" role="tabpanel" aria-labelledby="v-bd-tab">
						<h3 class="mb-4 tx_blue"><?php echo $row_Re_bd['c_name_en'];?></h3><hr class="hr_yellow">
						<?php echo $row_Re_bd['c_detail_en'];?>
					</div>
					<div class="tab-pane fade" id="v-cp" role="tabpanel" aria-labelledby="v-cp-tab">
						<h3 class="mb-4 tx_blue"><?php echo $row_Re_cp['c_name_en'];?></h3><hr class="hr_yellow">
						<?php echo $row_Re_cp['c_detail_en'];?>

						<div class="form_box m-0">
							<form name="form_cp" id="form_cp" method="post" enctype="multipart/form-data" novalidate>
								<div class="form-row">
									<div class="custom-control-inline">
										<label for="ab_cp_title1">
										<input type="radio" name="ab_cp_title" id="ab_cp_title1" value="นาย"> Mr.</label>
									</div>
									<div class="custom-control-inline">
										<label for="ab_cp_title2">
										<input type="radio" name="ab_cp_title" id="ab_cp_title2" value="นาง"> Mrs.</label>
									</div>
									<div class="custom-control-inline">
										<label for="ab_cp_title3">
										<input type="radio" name="ab_cp_title" id="ab_cp_title3" value="นางสาว"> Ms.</label>
									</div>
								</div>
								<div id="ab_cp_title_validate"></div>
								

								<div class="form-row mt-3">  
								<div class="form-group col-md-6">
									<label for="ab_cp_name"><i class="far fa-user"></i> Name</label>
									<input type="text" class="form-control" name="ab_cp_name" id="ab_cp_name" placeholder="Name">
									<div id="ab_cp_name_validate"></div>
								</div>
								<div class="form-group col-md-6">
									<label for="ab_cp_last"><i class="far fa-user"></i> Surname</label>
									<input type="text" class="form-control" name="ab_cp_last" id="ab_cp_last" placeholder="Surname">
									<div id="ab_cp_last_validate"></div>
								</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="ab_cp_email"  class="col-form-label"><i class="far fa-envelope"></i> Email address</label>
										<input type="email" class="form-control" name="ab_cp_email" id="ab_cp_email" aria-describedby="emailHelp" placeholder="Email address">
										<div id="ab_cp_email_validate"></div>
									</div>
									<div class="form-group col-md-6">
										<label for="ab_cp_tel" class="col-form-label"><i class="fa fa-phone" aria-hidden="true"></i> Mobile phone</label>
										<input type="text" class="form-control" name="ab_cp_tel" id="ab_cp_tel" placeholder="Mobile phone">
										<div id="ab_cp_tel_validate"></div>
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="ab_cp_tel2" class="col-form-label"><i class="fa fa-phone" aria-hidden="true"></i> Work / Home phone</label>
										<input type="text" class="form-control" name="ab_cp_tel2" id="ab_cp_tel2" placeholder="Work / Home phone">
										<div id="ab_cp_tel2_validate"></div>
									</div>
									<div class="form-group col-md-6">
										<label for="ab_cp_ext" class="col-form-label"><i class="fa fa-phone" aria-hidden="true"></i> Ext.</label>
										<input type="text" class="form-control" name="ab_cp_ext" id="ab_cp_ext" placeholder="">
										<div id="ab_cp_ext_validate"></div>
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="ab_cp_fax" class="col-form-label"><i class="fa fa-fax" aria-hidden="true"></i> Fax</label>
										<input type="text" class="form-control" name="ab_cp_fax" id="ab_cp_fax" placeholder="Fax">
										<div id="ab_cp_tel_validate"></div>
									</div>
									<div class="form-group col-md-6">
										<label for="ab_cp_fax" class="col-form-label"><i class="fas fa-paperclip"></i> File</label>
										<input type="file" class="form-control" name="ab_cp_file" id="ab_cp_file" >
										<div id="ab_cp_file_validate"></div>
									</div>
								</div>

								<div class="form-row">
									<div class="form-group col-md-12">
										<label for="ab_cp_detail" class="col-form-label"><i class="fa fa-file-text-o" aria-hidden="true"></i> Detail</label>
										<textarea class="form-control" name="ab_cp_detail" id="ab_cp_detail" rows="5"></textarea>
									</div>
								</div>
										
								<br>
								<div class="text-center"><button type="submit" id="submit" class="btn btn-primary">>Send Mail</button></div>
								
							</form>
						</div>
					</div>
					<div class="tab-pane fade" id="v-sc" role="tabpanel" aria-labelledby="v-sc-tab">
						<h3 class="mb-4 tx_blue"><?php echo $row_Re_sc['c_name_en'];?></h3><hr class="hr_yellow">
						<?php echo $row_Re_sc['c_detail_en'];?>
					</div>
					<div class="tab-pane fade mb-4" id="v-cn" role="tabpanel" aria-labelledby="v-cn-tab">
						<h3 class="mb-4 tx_blue"><?php echo $row_Re_cn['c_name_en'];?></h3><hr class="hr_yellow">
						<?php if($total_Re_cn2>0){?>
						<div class="row">
							<?php while($row_Re_cn2=$Re_cn2->fetch_assoc()){?>
							<div class="col-sm-6">
								<div class="card col-sm-12 mx-0 my-2 mx-lg-2 bg-white border h-100">
									<div class="card-body pb-0">
										<div class="row">
											<div class="col-md-12 mb-2">
												<img class="img-fluid" src="../images/company/<?php echo $row_Re_cn2['cn_logo'];?>" alt="">
											</div>
											<div class="col-md-12">
												<h5 class="p-0 m-0 mt-2"><?php echo $row_Re_cn2['en_cn_name'];?></h5>
												<?php 
												echo "<b>".$row_Re_cn2['en_cn_company']."</b><br>";
												echo "Business type : ".$row_Re_cn2['en_cn_type']."<br>";
												echo nl2br($row_Re_cn2['en_cn_address'])."<br><br>"; 
												?>
												<?php 
												echo "Tel : ".$row_Re_cn2['cn_tel'];
												if(!empty($row_Re_cn2['cn_fax'])){echo "<br>Fax : ".$row_Re_cn2['cn_fax']; }
												?>
											</div>
										</div>
									</div>
									<div class="card-footer px-1 m-0 bg-white">
										<?php if($row_Re_cn2['cn_web']!=""){ ?>
											<a href="<?php echo $row_Re_cn2['cn_web']; ?>"><img src="../images/icon/cn_www.png" alt="Website"></a>
										<?php } if($row_Re_cn2['cn_fb']!=""){ ?>
											<a href="<?php echo $row_Re_cn2['cn_fb']; ?>"><img src="../images/icon/cn_fb.png" alt="Facebook"></a>
										<?php } if($row_Re_cn2['cn_line']!=""){ ?>
											<a href="<?php echo $row_Re_cn2['cn_line']; ?>"><img src="../images/icon/cn_line.png" alt="Line"></a>
										<?php } if($row_Re_cn2['cn_youtube']!=""){ ?>
											<a href="<?php echo $row_Re_cn2['cn_youtube']; ?>"><img src="../images/icon/cn_youtube.png" alt="Youtube"></a>
										<?php } ?>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<?php } ?>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
  	<script type="text/javascript" src="../js/ajax_complaint.js"></script>

 	<script type="text/javascript">
		var url = document.location.toString();
		if (url.match('#')) {
			$('.nav-pills a[href="#' + url.split('#')[1] + '"]').tab('show');
		} 

		$('.nav-pills a').on('shown.bs.tab', function (e) {
			window.location.hash = e.target.hash;
		})
    </script>
</body>
</html>
