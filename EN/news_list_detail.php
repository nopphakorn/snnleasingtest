<?php 
include '../Connections/con_db.php';
include '../function/function.php';
$page_name="";

if (isset($_GET['id'])) {$n_id = mysqli_real_escape_string($mysqli,$_GET['id']);}

$query_Re_n = "SELECT * FROM tb_news WHERE n_id='".$n_id."'";
$Re_n=$mysqli->query($query_Re_n);
$row_Re_n=$Re_n->fetch_assoc();
$totalRows_Re_n=$Re_n->num_rows;

$query_Re_np = "SELECT * FROM tb_news_photo WHERE n_id='".$n_id."'";
$Re_np=$mysqli->query($query_Re_np);
$totalRows_Re_np=$Re_np->num_rows;

$counter=$row_Re_n['n_counter'];
$n_counter=$counter+1;
$sql_count="UPDATE tb_news SET n_counter='$n_counter' where n_id='$n_id' ";
$Re_count=$mysqli->query($sql_count);
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
	<link href="../library/lightbox2/css/lightbox.css" rel="stylesheet">
	<meta property="og:locale" 			content="th_TH" />
	<meta property="og:url"           	content="http://www.snnleasing.com/EN/news_list_detail.php?id=<?php echo $row_Re_n['n_id']; ?>" />
	<meta property="og:type"          	content="article" />
	<meta property="og:title"         	content="<?php echo $row_Re_n['en_n_name']; ?>" />
	<meta property="og:description"   	content="<?php echo strip_tags($row_Re_n['en_n_detail']);?>" />
	<meta property="og:image"         	content="http://www.snnleasing.com/images/news/<?php echo $row_Re_n['n_photo']; ?>" />
	<meta property="fb:app_id"         	content="106896906786738" />
</head>
<body>
	<div class="container-fluid"><?php include 's_header.php';?></div>
	  
	 <div class="container-fluid bg_top">
      	<div class="container pt-4 mb-4">
        	<div class="row">
          		<div class="col-lg-12 ">
					<h3 class="my-4 tx_blue">News & Activities</h3>
					<a href="news.php">News & Activities</a> > <a href="news_list.php">List</a> > Detail<br>
            		<hr class="hr_yellow">

					<div class="row">
						<div class="col-lg-12 p-2 mb-2">
							<div class="card p-3">
								<div class="col-md-12 mb-3">
									<i class="far fa-calendar-alt"></i> <?php echo dateEng_m($row_Re_n['n_date']); ?>
									<h4><?php echo $row_Re_n['en_n_name'];?></h4>
									<div class="fb-share-button" 
										data-href="http://www.snnleasing.com/EN/news_list_detail.php?id=<?php echo $row_Re_n['n_id']; ?>" 
										data-layout="button_count" 
										data-size="small" 
										data-mobile-iframe="true">
										<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http://www.snnleasing.com/EN/news_list_detail.php?id=<?php echo $row_Re_n['n_id']; ?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">แชร์</a>
									</div>
									<div class="fb-like" data-href="http://www.snnleasing.com/EN/news_list_detail.php?id=<?php echo $row_Re_n['n_id']; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
								
									<hr>
									<?php echo $row_Re_n['en_n_detail'];?>
								</div>
								<!--  -->
								<?php if($totalRows_Re_np>0){ ?>
								<div class="col-lg-12">
									<div class="row">
										<div class="col-lg-4 p-1">
											<div class="card p-2 h-100">
												<a href="../images/news/<?php echo $row_Re_n['n_photo'];?>" data-lightbox="roadtrip">
													<div class="n_img_list"><div><img class="card-img-top" src="../images/news/<?php echo $row_Re_n['n_photo'];?>"/></div></div>
												</a>
											</div>
										</div>

										<?php while($row_Re_np=$Re_np->fetch_assoc()){ ?>
											<div class="col-lg-4 p-1">
												<div class="card p-2 h-100">
													<a href="../images/news/<?php echo $row_Re_np['n_p_photo'];?>" data-lightbox="roadtrip">
														<div class="n_img_list"><div><img class="card-img-top" src="../images/news/<?php echo $row_Re_np['n_p_photo'];?>"/></div></div>
													</a>
												</div>
											</div>
										<?php } ?>
									</div>
								</div>
								<?php } ?>
								<!--  -->
							</div>
						</div>
					</div>
          		</div>
			</div>
      	</div>
  	</div>

  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
	<script src="../library/lightbox2/js/lightbox-plus-jquery.js"></script>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v2.10';
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>
