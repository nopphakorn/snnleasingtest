<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>

<script>
    $(document).ready(function(){
      $('[data-toggle="popover"]').popover();   
    });

    $(window).scroll(function(){
      var wscroll = $(this).scrollTop();
      if(wscroll > 50){
        $(".navbar").addClass("shrink-nav");
      }
      else{
        $(".navbar").removeClass("shrink-nav");
      }
    });
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("btn_top").style.display = "block";
        } else {
            document.getElementById("btn_top").style.display = "none";
        }
    }
    function topFunction() {
        document.body.scrollTop = 0; 
        document.documentElement.scrollTop = 0;
    }
  </script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118327918-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118327918-1');
</script>
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      xfbml            : true,
      version          : 'v3.2'
    });
  };

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/th_TH/sdk/xfbml.customerchat.js';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-customerchat"
  attribution=setup_tool
  page_id="529148813814910"
  logged_in_greeting="สวัสดีค่ะ SNN ยินดีให้บริการ อยากให้ SNN ช่วยเหลือเรื่องอะไรดีคะ สอบถามได้เลยค่ะ"
  logged_out_greeting="สวัสดีค่ะ SNN ยินดีให้บริการ อยากให้ SNN ช่วยเหลือเรื่องอะไรดีคะ สอบถามได้เลยค่ะ">
</div>

