<?php 
include 'tff_check.php';
include '../Connections/con_db.php';
include '../function/function.php';
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-sm-3"><?php include 'tff_menu.php';?></div>
				<div class="col-sm-9 mt-3 mb-4">
					<h5 class="tx_blue_content">Welcome : <?php echo "คุณ".$row_Re_tff_chk['tff_name']." ".$row_Re_tff_chk['tff_last'];?></h5>
					<img class="img-fluid w-100" src="../images/background/tff_member.png" alt="">
				</div>
			</div>

			<div class="row">
				<div class="col-sm-4 text-center mt-3 mb-3">
					<a href="tff_money.php">
					<img src="../images/tff/tffmenu-1.jpg" alt="..." class="rounded-circle"></a>
					<p>
						<h4>วางแผนการเงิน</h4>
					</p>
				</div>
				<div class="col-sm-4 text-center mt-3 mb-3">
					<a href="tff_article.php">
					<img src="../images/tff/tffmenu-2.jpg" alt="..." class="rounded-circle"></a>
					<p>
						<h4>บทความดีๆ</h4>
					</p>
				</div>
				<div class="col-sm-4 text-center mt-3 mb-3">
					<a href="tff_phl.php">
					<img src="../images/tff/tffmenu-3.jpg" alt="..." class="rounded-circle"></a>
					<p>
						<h4>อนาคตพิษณุโลก สู่สังคมเมือง</h4>
					</p>
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?> 
	<?php include 'include/inc_script.php';?>
</body>
</html>
