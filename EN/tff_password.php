<?php 
include 'tff_check.php';
include '../Connections/con_db.php';
include '../function/function.php';
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid"></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-sm-3"><?php include 'tff_menu.php';?></div>
				<div class="col-sm-9 mt-3 mb-4">
					<div class="form_box">
						<form name="form_tffps" id="form_tffps" method="post" enctype="multipart/form-data">
							<h5>Passwoed Edit : <?php echo "คุณ".$row_Re_tff_chk['tff_name']." ".$row_Re_tff_chk['tff_last'];?></h5>
							<hr class="hr_yellow">

							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="password"><i class="fa fa-key" aria-hidden="true"></i> Password</label>
									<input type="password" class="form-control" name="password" id="password" placeholder="Password">
									<div id="password_validate"></div>
								</div>
							</div>

							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="password_new" class="col-form-label"><i class="fa fa-key" aria-hidden="true"></i> New Password</label>
									<input type="password" class="form-control" name="password_new" id="password_new" placeholder="New Password">
									<div id="password_new_validate"></div>
								</div>
							</div>

							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="password_new_c"  class="col-form-label"><i class="fa fa-key" aria-hidden="true"></i> New Password again</label>
									<input type="password" class="form-control" name="password_new_c" id="password_new_c" placeholder="New Password again">
									<div id="password_new_c_validate"></div>
								</div>
							</div>

							<input type="hidden" id="action" name="action" value="password">
							<input type="hidden" id="tff_id" name="tff_id" value="<?php echo $row_Re_tff_chk['tff_id'];?>">
							<button type="submit" id="submit" class="btn btn-primary">Edit</button>
							<a class="btn btn-secondary" href="tff.php">Cancel</a>
						</form>
						</div>
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
	<script type="text/javascript" src="../library/validation/js/jquery.validate.js"></script>
  <script type="text/javascript" src="../library/validation/js/additional-methods.js"></script>
  <script type="text/javascript" src="../js/ajax_tff.js"></script>
</body>
</html>
