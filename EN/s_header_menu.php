<?php
$url = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'].( ! empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : '' );
$th = "../";
$en = "";
$url_cut = substr($url, 22);
$url_th = $th.$url_cut;
$url_en = $en.$url_cut;
?>

<div class="fixed-top">
    <div id="hd_top_bar" class="row hd_top">
      <div class="container">
        <div id="hd_top_bar_l">
          <div id="hd_top_bar_logo" >
            <a href="index.php"><img src="../images/logo/logo_snn_y.png" alt="Home"></a>
          </div>
        </div>

        <div id="hd_top_bar_r">
          <div id="hd_top_bar_lag">
            <div class="blag mb-1">
              <a class="btn_lag" href="<?php echo $url_th; ?>"><img src="../images/icon/icon_th.png" alt="">  TH</a>
              <a class="btn_lag" href="<?php echo $url_en; ?>"><img src="../images/icon/icon_en.png" alt="">  ENG</a>
            </div>
            <div class="bfrm">
              <form name="form_search" id="form_search" method="post" enctype="multipart/form-data" class="form-inline" action="search.php">
                <div class="input-group input-group-sm mb-3 mr-auto ml-auto">
                  <input type="text" class="form-control border-0" placeholder="" id="websearch" name="websearch">
                  <div class="input-group-append">
                    <button class="btn form-control border-0" type="submit"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div id="hd_top_bar_tel">
            <a href="tel:+66888908990"><img class="img-fluid" src="../images/icon/icon_tel.png" alt=""></a>
          </div>
        </div> 
      </div>
    </div>

    <div class="row p-0 ">
      <div id="hd_nav_top" class="col-sm-12 m-0 py-0">
        <nav class="navbar navbar-expand-lg navbar-light fixed-top">
          
          <button class="navbar-toggler ml-3 border-white text-white" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars fa-md"></i>
          </button>

          <div class="navbar-brand text-white">
            <a class="btn_lag_nav" href="<?php echo $url_th; ?>"><img src="../images/icon/icon_th.png" alt="">  TH</a>
            <a class="btn_lag_nav" href="<?php echo $url_en; ?>"><img src="../images/icon/icon_en.png" alt="">  ENG</a>
          </div>

          <div class="collapse navbar-collapse justify-content-center m-1" id="navbarNav">
            <ul class="navbar-nav m-auto">
              <li class="nav-item active"><a class="nav-link" href="index.php">Home</a></li>
              <li class="nav-item active"><a class="nav-link" href="loan_document.php">Loan Application</a></li>
              <li class="nav-item active"><a class="nav-link" href="loan_register.php">Loan Applying</a></li>
              <li class="nav-item active"><a class="nav-link" href="branch.php">Branch</a></li>
              <li class="nav-item active"><a class="nav-link" href="promotion.php">Promotions</a></li>
              <li class="nav-item active"><a class="nav-link" href="about.php#v-ab">About us</a></li>
              <li class="nav-item active"><a class="nav-link" href="job.php">Career</a></li>
              <li class="nav-item end active"><a class="nav-link" href="tff.php"><img src="../images/icon/icon_tff_top.png" alt=""> SNN TFF</a></li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
</div>