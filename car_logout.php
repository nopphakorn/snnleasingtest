<?php
if (!isset($_SESSION)) {session_start();}
$_SESSION['SC_em_id']= NULL;
$_SESSION['SC_em_user']= NULL;
$_SESSION['SC_em_name']= NULL;
$_SESSION["SC_em_access"]= NULL;

unset($_SESSION['SC_em_id']);
unset($_SESSION['SC_em_user']);
unset($_SESSION['SC_em_name']);
unset($_SESSION["SC_em_access"]);
header("Location: car_login.php"); 
exit;
?>