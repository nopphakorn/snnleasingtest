<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="job_trainee";

$sql_ctff="SELECT * FROM tb_content WHERE c_name='job_trainee'";
$Re_ctff=$mysqli->query($sql_ctff);
$row_Re_ctff=$Re_ctff->fetch_assoc();
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-lg-12 px-4 tx_blue_content">
					<h3 class="my-4 tx_blue"><?php echo $row_Re_ctff['c_name_th']; ?></h3>
					<a href="job.php">ร่วมงานกับเรา</a> > <?php echo $row_Re_ctff['c_name_th']; ?>
					<hr class="hr_yellow">
					<?php
					if(!empty($row_Re_ctff['c_detail'])){
						echo $row_Re_ctff['c_detail']; 
					}
					?>
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
