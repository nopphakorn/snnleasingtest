<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="job_home";

$sql_ctff="SELECT * FROM tb_content WHERE c_name='job_home'";
$Re_ctff=$mysqli->query($sql_ctff);
$row_Re_ctff=$Re_ctff->fetch_assoc();
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-sm-12">
					<div class="row">
						<div class="col-12 px-5 tx_blue_content">
							<h3 class="my-4 tx_blue"><?php echo $row_Re_ctff['c_name_th']; ?></h3>
							<hr class="hr_yellow">
							<?php
							if(!empty($row_Re_ctff['c_detail'])){
								echo $row_Re_ctff['c_detail']; 
							}
							?>
						</div> 
					</div> 

					<div class="row">
						<div class="col-12">
							<div class="jumbotron text-white bg_blue job_jumbotron">
								<div class="tx_job3">ร่วมงานกับ เอส เอ็น เอ็น</div>
								<p class="lead tx_job">ค้นหางานกับเอส เอ็น เอ็น โอกาสมาถึงคุณแล้ว</p>
								<p class="lead">
									<a class="btn btn-warning btn-lg tx_job" href="job_list.php" role="button"><i class="fa fa-search" aria-hidden="true"></i> ค้นหาตำแหน่งงานและฝากประวัติส่วนตัว</a>
								</p>
							</div>
						</div> 
					</div> 
					
					<div class="row mt-4">
						<div class="col-md-6 mb-3">
							<a class="btn btn-secondary btn-lg btn-block text-white" href="job_trainee.php">
								<div class="row p-3">
									<div class="col-md-3">
									<i class="fas fa-chart-line fa-3x"></i>
									</div>
									<div class="col-md-9 text-left">
										<div class="tx_yellow tx_job2">Manager Trainee Program</div>
										อ่านรายละเอียด
									</div>
								</div>
							</a>
						</div> 
						<div class="col-md-6 mb-3">
							<a class="btn btn-warning btn-lg btn-block text-white" href="job_proud.php">
								<div class="row p-3">
									<div class="col-md-3">
									<i class="fa fa-users fa-3x" aria-hidden="true"></i>
									</div>
									<div class="col-md-9 text-left">
										<div class="text-dark tx_job2">การพัฒนาบุคลากรของเรา</div>
										อ่านรายละเอียด
									</div>
								</div>
							</a>
						</div> 
					</div> 
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
