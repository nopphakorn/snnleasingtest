<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="interest";

$maxRows_Re_i = 10;
$pageNum_Re_i = 0;
if (isset($_GET['pageNum_Re_i'])) {
  $pageNum_Re_i = $_GET['pageNum_Re_i'];
}
$startRow_Re_i = (($pageNum_Re_i-1) * $maxRows_Re_i);
if($startRow_Re_i<0){$startRow_Re_i=0;}

$query_Re_i = "SELECT * FROM tb_interest ORDER BY i_id DESC ";
$query_limit_Re_i = sprintf("%s LIMIT %d, %d", $query_Re_i, $startRow_Re_i, $maxRows_Re_i);
$Re_i=$mysqli->query($query_limit_Re_i);

if (isset($_GET['totalRows_Re_i'])) {
  $totalRows_Re_i = $_GET['totalRows_Re_i'];
} else {
  $all_Re_i=$mysqli->query($query_Re_i);
  $totalRows_Re_i=$all_Re_i->num_rows;
}
$totalPages_Re_i = ceil($totalRows_Re_i/$maxRows_Re_i);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_i==0){$page=1;}
else if($pageNum_Re_i>0){$page=$pageNum_Re_i;}

?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid"></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row ">
				<div class="col-md-12">
					<h3 class="my-4 tx_blue">อัตราดอกเบี้ยและค่าธรรมเนียม</h3>
					<hr class="hr_yellow mb-4">
					<div class="row mb-3 p-2 p-md-4 p-lg-4">
						<div class="col-md-12">
							<div class="row">
								<?php while ($row_Re_i = $Re_i->fetch_assoc()) { ?>
									<div class="col-12 p-2 d-flex justify-content-center ">
										<div class="col-12 col-md-9 col-lg-9 p-2 p-md-4 p-lg-4 border">
											<div class="a_box_l text-center align-middle">
												<span class="fa-stack fa-2x">
													<i class="fas fa-square fa-stack-2x"></i>
													<?php 
													if($row_Re_i['i_type']=="PDF"){echo "<i class=\"far fa-file-pdf fa-stack-1x fa-inverse text-warning\"></i>";} 
													else if($row_Re_i['i_type']=="JPG"){echo "<i class=\"far fa-file-image fa-stack-1x fa-inverse\"></i>";} 
													?>
												</span>
											</div>
											<div class="a_box_r">
												<a class="tx_blue" href="images/interest/<?php echo $row_Re_i['i_file'];?>">
												<b><?php echo $row_Re_i['i_name'];?></b>
												<?php if(!empty($row_Re_i['i_title'])){echo "<br>".$row_Re_i['i_title'];}?>
												<br>ประกาศมีผลบังคับใช้ตั้งแต่วันที่ <?php echo datethai($row_Re_i['i_date']);?> เป็นต้นไป
												</a>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<div class="row mt-1">
						<div class="col-12">
							
							<ul class="pagination justify-content-center">
							<li class="page-item <?php if($page==1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_i=%d%s", $currentPage, 0, $currentSent); ?>" aria-label="Previous">
								<i class="fa fa-angle-double-left" aria-hidden="true"></i>
								</a>
							</li>

							<li class="page-item <?php if($page==1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_i=%d%s", $currentPage, $page - 1, $currentSent); ?>" aria-label="Previous">
								<i class="fa fa-angle-left" aria-hidden="true"></i>
								</a>
							</li>

							<li class="page-item disabled">
								<span class="page-link">
								<?php if($totalPages_Re_i>0){echo $page."/".$totalPages_Re_i;}else{echo "-";}?>
								</span>
							</li>

							<li class="page-item <?php if($page==$totalPages_Re_i OR $totalPages_Re_i<1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_i=%d%s", $currentPage, $page + 1, $currentSent); ?>" aria-label="Next">
								<i class="fa fa-angle-right" aria-hidden="true"></i>
								</a>
							</li>
									
							<li class="page-item <?php if($page==$totalPages_Re_i OR $totalPages_Re_i<1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_i=%d%s", $currentPage, $totalPages_Re_i, $currentSent); ?>" aria-label="Next">
								<i class="fa fa-angle-double-right" aria-hidden="true"></i>
								</a>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
