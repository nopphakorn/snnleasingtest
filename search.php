<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="";

if(isset($_POST['websearch'])){$websearch = mysqli_real_escape_string($mysqli,$_POST['websearch']);}else{$websearch="";}

$maxRows_Re_s = 50;
$pageNum_Re_s = 0;
if (isset($_GET['pageNum_Re_s'])) {
  $pageNum_Re_s = $_GET['pageNum_Re_s'];
}
$startRow_Re_s = (($pageNum_Re_s-1) * $maxRows_Re_s);
if($startRow_Re_s<0){$startRow_Re_s=0;}

$query_Re_s = "SELECT * FROM tb_search WHERE 1 ";
if($websearch!=""){
	$query_Re_s.= "AND s_tag LIKE '%".$websearch."%' ";
}else if($websearch==""){
	$query_Re_s.= "AND s_tag='-1' ";
}
$query_Re_s.= "ORDER BY s_id ASC ";
$query_limit_Re_s = sprintf("%s LIMIT %d, %d", $query_Re_s, $startRow_Re_s, $maxRows_Re_s);
$Re_s=$mysqli->query($query_limit_Re_s);

if (isset($_GET['totalRows_Re_s'])) {
  $totalRows_Re_s = $_GET['totalRows_Re_s'];
} else {
  $all_Re_s=$mysqli->query($query_Re_s);
  $totalRows_Re_s=$all_Re_s->num_rows;
}
$totalPages_Re_s = ceil($totalRows_Re_s/$maxRows_Re_s);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_s==0){$page=1;}
else if($pageNum_Re_s>0){$page=$pageNum_Re_s;}
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid"></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row mt-3">
				<div class="col-md-12">
					<h3 class="my-4 tx_blue">ค้นหา</h3>
					<a href="index.php">หน้าแรก</a> > รายการค้นหา<br>
					<hr class="hr_yellow">
				</div>
			</div>

			<div class="row mb-3">
				<div class="col-md-12">

					<div class="row">
						<div class="col-md-12"><?php echo "คำค้นหา : ".$websearch."<br><hr>"; ?></div>
						<?php 
						if($totalRows_Re_s>0){
						$number=0;
						while ($row_Re_s = $Re_s->fetch_assoc()) { 
						?>
							<div class="col-md-12">
								<p>
									<?php
										$number+=1;
										echo $number.". ".$row_Re_s['s_name']."<br>";
									?>
									<a href="<?php echo $row_Re_s['s_url'];?>"><?php echo $row_Re_s['s_url'];?></a>
									<hr>
								</p>
							</div>
						<?php }}else{ ?>
							<div class="alert alert-danger w-100" role="alert">
							<i class="fas fa-exclamation-triangle"></i> ไม่พบรายการที่ค้นหาในขณะนี้
							</div>
						<?php } ?>
					</div>
					<div class="row mt-1">
						<div class="col-12 pr-2">
							<ul class="pagination justify-content-end">
							<li class="page-item <?php if($page==1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_s=%d%s", $currentPage, 0, $currentSent); ?>" aria-label="Previous">
								<i class="fa fa-angle-double-left" aria-hidden="true"></i>
								</a>
							</li>

							<li class="page-item <?php if($page==1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_s=%d%s", $currentPage, $page - 1, $currentSent); ?>" aria-label="Previous">
								<i class="fa fa-angle-left" aria-hidden="true"></i>
								</a>
							</li>

							<li class="page-item disabled">
								<span class="page-link">
								<?php 
								if($totalPages_Re_s>0){
									echo $page."/".$totalPages_Re_s;
								}else{
									echo "-";
								}
								?>
								</span>
							</li>

							<li class="page-item <?php if($page==$totalPages_Re_s OR $totalPages_Re_s<1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_s=%d%s", $currentPage, $page + 1, $currentSent); ?>" aria-label="Next">
								<i class="fa fa-angle-right" aria-hidden="true"></i>
								</a>
							</li>
									
							<li class="page-item <?php if($page==$totalPages_Re_s OR $totalPages_Re_s<1){echo "disabled";}?>">
								<a class="page-link" href="<?php printf("%s?pageNum_Re_s=%d%s", $currentPage, $totalPages_Re_s, $currentSent); ?>" aria-label="Next">
								<i class="fa fa-angle-double-right" aria-hidden="true"></i>
								</a>
							</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
