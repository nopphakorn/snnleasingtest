<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="service";

$sql_1="SELECT * FROM tb_service WHERE sv_id='1' ";
$Re_sv1=$mysqli->query($sql_1);
$row_Re_sv1=$Re_sv1->fetch_assoc();

$sql_2="SELECT * FROM tb_service WHERE sv_id='2' ";
$Re_sv2=$mysqli->query($sql_2);
$row_Re_sv2=$Re_sv2->fetch_assoc();

$sql_3="SELECT * FROM tb_service WHERE sv_id='3' ";
$Re_sv3=$mysqli->query($sql_3);
$row_Re_sv3=$Re_sv3->fetch_assoc();

$sql_4="SELECT * FROM tb_service WHERE sv_id='4' ";
$Re_sv4=$mysqli->query($sql_4);
$row_Re_sv4=$Re_sv4->fetch_assoc();

$sql_5="SELECT * FROM tb_service WHERE sv_id='5' ";
$Re_sv5=$mysqli->query($sql_5);
$row_Re_sv5=$Re_sv5->fetch_assoc();

$sql_btel="SELECT * FROM tb_banner_tel WHERE ban_id = '1'";
$Re_btel=$mysqli->query($sql_btel);
$row_Re_btel=$Re_btel->fetch_assoc();
$totalRows_Re_btel=$Re_btel->num_rows;
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>

	<!-- banner -->
	<div class="container-fluid"></div>

	<!-- content -->
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row">
				<div class="col-md-12 col-lg-3">
					<div class="nav flex-column nav-pills ab_nav_pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<h5 class="mb-4 tx_blue">ผลิตภัณท์และบริการ</h5>
						<a class="nav-link active" id="sv1-tab" data-toggle="pill" href="#sv1" role="tab" aria-controls="sv1" >สินเชื่อรถยนต์</a>
						<a class="nav-link" id="sv2-tab" data-toggle="pill" href="#sv2" role="tab" aria-controls="sv2" >สินเชื่อรถเพื่อการเกษตร</a>
						<a class="nav-link" id="sv3-tab" data-toggle="pill" href="#sv3" role="tab" aria-controls="sv3" >สินเชื่อบ้านที่ดิน</a>
						<a class="nav-link" id="sv4-tab" data-toggle="pill" href="#sv4" role="tab" aria-controls="sv4" >ประกันภัยรถยนต์</a>
						<a class="nav-link" id="sv5-tab" data-toggle="pill" href="#sv5" role="tab" aria-controls="sv5" >พ.ร.บ.</a>
					</div>
				</div>
				<div class="col-md-12 col-lg-9">
					<div class="tab-content mt-1" id="v-pills-tabContent">
						<div class="tab-pane fade show active tx_blue_content" id="sv1" role="tabpanel" aria-labelledby="sv1-tab">
							<h3 class="mb-4 tx_blue"><?php echo $row_Re_sv1['sv_name'];?></h3><hr class="hr_yellow">
							<?php if($row_Re_sv1['sv_photo']!=""){?><img class="img-fluid mb-3" src="images/service/<?php echo $row_Re_sv1['sv_photo'];?>"><?php } ?>
							<br><br>
							<?php echo $row_Re_sv1['sv_title'];?>
							<?php if($totalRows_Re_btel>0){?>
								<a href="<?php echo $row_Re_btel['ban_link'];?>">
								<img class="img-fluid mx-auto d-block" src="images/banner_tel/<?php echo $row_Re_btel['ban_photo'];?>"/></a>
							<?php } ?>
							<?php echo $row_Re_sv1['sv_detail'];?>
							<div class="row justify-content-md-center mb-4">
								<div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 col-6 mb-4">
									<a class="btn sv_menu d-block rounded-circle p-0" href="interest.php" role="button">
									<img src="images/icon/menu_fee.png" class="w-100 mx-auto"></a>
									<div class="text-center mt-1 mb-2"><strong>อัตราค่าธรรมเนียม</strong></div>
								</div>
								<div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 col-6 mb-4">
									<a class="btn sv_menu d-block rounded-circle p-0" href="loan_register.php" role="button">
									<img src="images/icon/menu_car.png" class="w-100 mx-auto"></a>
									<div class="text-center mt-1 mb-2"><strong>สมัครขอสินเชื่อ</strong></div>
								</div>
								<div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 col-6 mb-4">
									<a class="btn sv_menu d-block rounded-circle p-0" href="contact.php" role="button">
									<img src="images/icon/menu_contact.png" class="w-100 mx-auto"></a>
									<div class="text-center mt-1 mb-2"><strong>ติดต่อเรา</strong></div>
								</div>
							</div>
						</div>

						<div class="tab-pane fade tx_blue_content" id="sv2" role="tabpanel" aria-labelledby="sv2-tab">
							<h3 class="mb-4 tx_blue"><?php echo $row_Re_sv2['sv_name'];?></h3><hr class="hr_yellow">
							<?php if($row_Re_sv2['sv_photo']!=""){?><img class="img-fluid mb-3" src="images/service/<?php echo $row_Re_sv2['sv_photo'];?>"><?php } ?>
							<br><br>
							<?php echo $row_Re_sv2['sv_title'];?>
							<?php if($totalRows_Re_btel>0){?>
								<a href="<?php echo $row_Re_btel['ban_link'];?>">
								<img class="img-fluid mx-auto d-block" src="images/banner_tel/<?php echo $row_Re_btel['ban_photo'];?>"/></a>
							<?php } ?>
							<?php echo $row_Re_sv2['sv_detail'];?>
							<div class="row justify-content-md-center mb-4">
								<div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 col-6 mb-4">
									<a class="btn sv_menu d-block rounded-circle p-0" href="loan_register.php" role="button">
									<img src="images/icon/menu_agriculture.png" class="w-100 mx-auto"></a>
									<div class="text-center mt-1 mb-2"><strong>สมัครขอสินเชื่อ</strong></div>
								</div>
								<div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 col-6 mb-4">
									<a class="btn sv_menu d-block rounded-circle p-0" href="contact.php" role="button">
									<img src="images/icon/menu_contact.png" class="w-100 mx-auto"></a>
									<div class="text-center mt-1 mb-2"><strong>ติดต่อเรา</strong></div>
								</div>
							</div>
						</div>

						<div class="tab-pane fade tx_blue_content" id="sv3" role="tabpanel" aria-labelledby="sv3-tab">
							<h3 class="mb-4 tx_blue"><?php echo $row_Re_sv3['sv_name'];?></h3><hr class="hr_yellow">
							<?php if($row_Re_sv3['sv_photo']!=""){?><img class="img-fluid mb-3" src="images/service/<?php echo $row_Re_sv3['sv_photo'];?>"><?php } ?>
							<br><br>
							<?php echo $row_Re_sv3['sv_title'];?>
							<?php if($totalRows_Re_btel>0){?>
								<a href="<?php echo $row_Re_btel['ban_link'];?>">
								<img class="img-fluid mx-auto d-block" src="images/banner_tel/<?php echo $row_Re_btel['ban_photo'];?>"/></a>
							<?php } ?>
							<?php echo $row_Re_sv3['sv_detail'];?>
							<div class="row justify-content-md-center mb-4">
								<div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 col-6 mb-4">
									<a class="btn sv_menu d-block rounded-circle p-0" href="loan_register.php" role="button">
									<img src="images/icon/menu_home.png" class="w-100 mx-auto"></a>
									<div class="text-center mt-1 mb-2"><strong>สมัครขอสินเชื่อ</strong></div>
								</div>
								<div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 col-6 mb-4">
									<a class="btn sv_menu d-block rounded-circle p-0" href="contact.php" role="button">
									<img src="images/icon/menu_contact.png" class="w-100 mx-auto"></a>
									<div class="text-center mt-1 mb-2"><strong>ติดต่อเรา</strong></div>
								</div>
							</div>
						</div>

						<div class="tab-pane fade tx_blue_content" id="sv4" role="tabpanel" aria-labelledby="sv4-tab">
							<h3 class="mb-4 tx_blue"><?php echo $row_Re_sv4['sv_name'];?></h3><hr class="hr_yellow">
							<?php if($row_Re_sv4['sv_photo']!=""){?><img class="img-fluid mb-3" src="images/service/<?php echo $row_Re_sv4['sv_photo'];?>"><?php } ?>
							<br><br>
							<?php echo $row_Re_sv4['sv_title'];?>
							<div class="row justify-content-md-center mb-4">
								<div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 col-6 mb-4">
									<a class="btn sv_menu d-block rounded-circle p-0" href="loan_register.php" role="button">
									<img src="images/icon/menu_insure.png" class="w-100 mx-auto"></a>
									<div class="text-center mt-1 mb-2"><strong>ทำประกันภัย</strong></div>
								</div>
								<div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 col-6 mb-4">
									<a class="btn sv_menu d-block rounded-circle p-0" href="contact.php" role="button">
									<img src="images/icon/menu_contact.png" class="w-100 mx-auto"></a>
									<div class="text-center mt-1 mb-2"><strong>ติดต่อเรา</strong></div>
								</div>
							</div>

							<?php echo $row_Re_sv4['sv_detail'];?>
						</div>

						<div class="tab-pane fade tx_blue_content" id="sv5" role="tabpanel" aria-labelledby="sv5-tab">
							<h3 class="mb-4 tx_blue"><?php echo $row_Re_sv5['sv_name'];?></h3><hr class="hr_yellow">
							<?php if($row_Re_sv5['sv_photo']!=""){?><img class="img-fluid mb-3" src="images/service/<?php echo $row_Re_sv5['sv_photo'];?>"><?php } ?>
							<br><br>
							<?php echo $row_Re_sv5['sv_title'];?>
							<div class="row justify-content-md-center mb-4">
								<div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 col-6 mb-4">
									<a class="btn sv_menu d-block rounded-circle p-0" href="loan_register.php" role="button">
									<img src="images/icon/menu_pp.png" class="w-100 mx-auto"></a>
									<div class="text-center mt-1 mb-2"><strong>ทำ พ.ร.บ.</strong></div>
								</div>
								<div class="col-lg-4 col-md-3 col-sm-6 col-xs-6 col-6 mb-4">
									<a class="btn sv_menu d-block rounded-circle p-0" href="contact.php" role="button">
									<img src="images/icon/menu_contact.png" class="w-100 mx-auto"></a>
									<div class="text-center mt-1 mb-2"><strong>ติดต่อเรา</strong></div>
								</div>
							</div>
							<?php echo $row_Re_sv5['sv_detail'];?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--end container -->
	<!-- ******************************************************************* -->


  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
	<script type="text/javascript" src="library/validation/js/jquery.validate.js"></script>
  	<script type="text/javascript" src="library/validation/js/additional-methods.js"></script>
  	<script type="text/javascript" src="js/ajax_complaint.js"></script>

	<script type="text/javascript">
		// Javascript to enable link to tab
		var hash = document.location.hash;
		var prefix = "tab_";
		if (hash) {
			$('.nav-pills a[href="'+hash.replace(prefix,"")+'"]').tab('show');
		} 

		// Change hash for page-reload
		$('.nav-pills a').on('shown', function (e) {
			window.location.hash = e.target.hash.replace("#", "#" + prefix);
		});
    </script>
</body>
</html>
