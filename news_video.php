<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="";

$query_Re_n = "SELECT * FROM tb_news WHERE n_status='1' ORDER BY n_id DESC LIMIT 0,6";
$Re_n=$mysqli->query($query_Re_n);
$totalRows_Re_n=$Re_n->num_rows;

$query_Re_v = "SELECT * FROM tb_video ORDER BY v_status DESC, v_id DESC LIMIT 0,6";
$Re_v=$mysqli->query($query_Re_v);
$totalRows_Re_v=$Re_v->num_rows;
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
	<div class="container-fluid"><?php include 's_header.php';?></div>
	  
	 <div class="container-fluid m-0 p-0 ">
      	<div class="container p-3">
			<div class="row">
          		<div class="col-lg-12  mb-4">
            		<h3>Video</h3>
            		<hr class="hr_yellow">

					<div class="row">
						<?php while ($row_Re_v = $Re_v->fetch_assoc()) { ?>
							<div class="col-md-4 p-1">
								<div class="card h-100">
									<div class="card-body">
										<div class="thumbnail mb-2">
											<div class="n_youtube_box">
												<?php
												$url = $row_Re_v['v_url'];
												preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $matches);
												$id = $matches[1];
												?>
												<object width="100%" height="240px">
													<param name="movie" value="http://www.youtube.com/v/<?php echo $id; ?>?version=3" />
													<param name="allowFullScreen" value="true" />
													<param name="allowScriptAccess" value="always" />
													<embed src="http://www.youtube.com/v/<?php echo $id; ?>?version=3" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="426px" height="240px"> </embed>
												</object>
											</div>
										</div>
										<p class="card-text">
											<?php echo $row_Re_v['v_name']; ?>
										</p>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="row mt-2 mb-4">
						<div class="col-md-12 text-center">
							<hr>
							<a class="btn btn-secondary" href="news_video.php"><i class="fas fa-list"></i> รายการ Video ทั้งหมด</a>
						</div>
					</div>
          		</div>
        	</div>
      	</div>
  	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
