<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="";
?>

<!DOCTYPE html>
<html lang="en">
<head><?php include 'include/inc_header.php';?></head>
<body>
  <div class="container-fluid m-0 p-0"><?php include 's_header.php';?></div>  

  <div class="container-fluid bg_top">
      <div class="container pt-4 mb-4">
        <div class="row">
          <div class="col-lg-12">
            <h3 class="my-4 tx_blue">ทรัพย์สินรอขาย</h3>
            <hr class="hr_yellow">

            <div class="card border-0 bg-transparent">
              <div class="card-body">
								<div class="form_box_login">
									<form name="form_plogin" id="form_plogin" method="post" enctype="multipart/form-data">
										<div class="form-row">
											<div class="col-md-12 text-center mb-2">
													กรอกชื่อผู้ใช้งาน และรหัสผ่านเพื่อเข้าสู่ระบบ 
												</div>
											<div class="form-group col-md-12">
												<label for="p_username"><i class="fas fa-user"></i> Username</label>
												<input type="text" class="form-control" name="p_username" id="p_username" placeholder="กรอก Username">
												<div id="p_username_validate"></div>
											</div>
											<div class="form-group col-md-12">
												<label for="p_password"><i class="fas fa-key"></i> Password</label>
												<input type="text" class="form-control" name="p_password" id="p_password" placeholder="กรอก Password">
												<div id="p_password_validate"></div>
											</div>
										</div>												
										<button type="submit" id="submit" class="btn btn-primary">เข้าสู่ระบบ</button>
										<button type="button" id="submit" class="btn btn-secondary" onclick="location.href='index.php';">ยกเลิก</button>
									</form>
								</div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>

  <?php include 's_footer.php';?>
  <?php include 'include/inc_script.php';?>
	<script type="text/javascript" src="library/validation/js/jquery.validate.js"></script>
  <script type="text/javascript" src="library/validation/js/additional-methods.js"></script>
  <script type="text/javascript" src="js/ajax_product.js"></script>
</body>
</html>
