<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="payment";

$sql_c="SELECT * FROM tb_content WHERE c_name='Payment' ";
$Re_c=$mysqli->query($sql_c);
$row_Re_c=$Re_c->fetch_assoc();
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid"></div>
	<div class="container-fluid bg_top">
		<div class="container pt-4 mb-4">
			<div class="row p-2 p-lg-4">
				<div id="col-md-12">
					<h3 class="mb-4 tx_blue"><img src="images/icon/icon_money.png" > <?php echo $row_Re_c['c_name_th'];?></h3>
					<hr class="hr_yellow w-100">
					<?php echo $row_Re_c['c_detail'];?>
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
