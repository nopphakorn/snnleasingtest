<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="investor";

$iv_type=mysqli_real_escape_string($mysqli,$_GET['type']);

if($iv_type==1){ 
	$maxRowsRe_iv1 = 50;
	$pageNumRe_iv1 = 0;
	if (isset($_GET['pageNumRe_iv1'])) {
	$pageNumRe_iv1 = $_GET['pageNumRe_iv1'];
	}
	$startRowRe_iv1 = (($pageNumRe_iv1-1) * $maxRowsRe_iv1);
	if($startRowRe_iv1<0){$startRowRe_iv1=0;}

	$queryRe_iv1 = "SELECT * FROM tb_investor WHERE iv_type='$iv_type' ORDER BY iv_id DESC ";
	$query_limitRe_iv1 = sprintf("%s LIMIT %d, %d", $queryRe_iv1, $startRowRe_iv1, $maxRowsRe_iv1);
	$Re_iv1=$mysqli->query($query_limitRe_iv1);

	if (isset($_GET['totalRowsRe_iv1'])) {
	$totalRowsRe_iv1 = $_GET['totalRowsRe_iv1'];
	} else {
		$allRe_iv1 = $mysqli->query($queryRe_iv1);
		$totalRowsRe_iv1 = $allRe_iv1->num_rows;
	}
	
	$totalPagesRe_iv1 = ceil($totalRowsRe_iv1/$maxRowsRe_iv1);
	$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
	$currentSent = "&type=".$iv_type;

	if($pageNumRe_iv1==0){$page=1;}
	else if($pageNumRe_iv1>0){$page=$pageNumRe_iv1;}
}else if($iv_type==2){
	$maxRowsRe_iv2 = 50;
	$pageNumRe_iv2 = 0;
	if (isset($_GET['pageNumRe_iv2'])) {
	$pageNumRe_iv2 = $_GET['pageNumRe_iv2'];
	}
	$startRowRe_iv2 = (($pageNumRe_iv2-1) * $maxRowsRe_iv2);
	if($startRowRe_iv2<0){$startRowRe_iv2=0;}

	$queryRe_iv2 = "SELECT * FROM tb_investor WHERE iv_type='$iv_type' ORDER BY iv_id DESC ";
	$query_limitRe_iv2 = sprintf("%s LIMIT %d, %d", $queryRe_iv2, $startRowRe_iv2, $maxRowsRe_iv2);
	$Re_iv2=$mysqli->query($query_limitRe_iv2);

	if (isset($_GET['totalRowsRe_iv2'])) {
	$totalRowsRe_iv2 = $_GET['totalRowsRe_iv2'];
	} else {
		$allRe_iv2 = $mysqli->query($queryRe_iv2);
		$totalRowsRe_iv2 = $allRe_iv2->num_rows;
	}
	$totalPagesRe_iv2 = ceil($totalRowsRe_iv2/$maxRowsRe_iv2);
	$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
	$currentSent = "&type=".$iv_type;

	if($pageNumRe_iv2==0){$page=1;}
	else if($pageNumRe_iv2>0){$page=$pageNumRe_iv2;}
}else if($iv_type==3){ 
	$maxRowsRe_iv3 = 50;
	$pageNumRe_iv3 = 0;
	if (isset($_GET['pageNumRe_iv3'])) {
	$pageNumRe_iv3 = $_GET['pageNumRe_iv3'];
	}
	$startRowRe_iv3 = (($pageNumRe_iv3-1) * $maxRowsRe_iv3);
	if($startRowRe_iv3<0){$startRowRe_iv3=0;}

	$queryRe_iv3 = "SELECT * FROM tb_investor WHERE iv_type='$iv_type' ORDER BY iv_id DESC ";
	$query_limitRe_iv3 = sprintf("%s LIMIT %d, %d", $queryRe_iv3, $startRowRe_iv3, $maxRowsRe_iv3);
	$Re_iv3=$mysqli->query($query_limitRe_iv3);

	if (isset($_GET['totalRowsRe_iv3'])) {
	$totalRowsRe_iv3 = $_GET['totalRowsRe_iv3'];
	} else {
		$allRe_iv3 = $mysqli->query($queryRe_iv3);
		$totalRowsRe_iv3 = $allRe_iv3->num_rows;
	}
	$totalPagesRe_iv3 = ceil($totalRowsRe_iv3/$maxRowsRe_iv3);
	$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
	$currentSent = "&type=".$iv_type;

	if($pageNumRe_iv3==0){$page=1;}
	else if($pageNumRe_iv3>0){$page=$pageNumRe_iv3;}
}

?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid bg_top">
		<div class="container pt-5 mb-4">
			<div class="row">
				<div class="col-md-12 col-lg-3">
					<div class="nav flex-column nav-pills ab_nav_pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<p class="btn_iv">ข้อมูลทั่วไป</p>
						<a class="nav-link <?php if($iv_type==1){ echo "active";}?>" id="iv-1-tab" href="investor.php?type=1">Corporate Profile Presentation</a>
						<p class="btn_iv">ข้อมูลและรายงานการเงิน</p>
						<a class="nav-link <?php if($iv_type==2){ echo "active";}?>" id="iv-2-tab" href="investor.php?type=2">รายงานประจำปี</a>
						<a class="nav-link <?php if($iv_type==3){ echo "active";}?>" id="iv-3-tab" href="investor.php?type=3">ข้อมูลนำเสนอ</a>
					</div>
				</div>
				<div class="col-md-12 col-lg-9 tx_blue_content">
					<div class="tab-content mt-1" id="v-pills-tabContent">
						<?php if($iv_type==1){ ?>
						<div class="tab-pane fade <?php if($iv_type==1){ echo "show active";}?>" id="iv-1" role="tabpanel" aria-labelledby="iv-1-tab">
							<h3>Corporate Profile Presentation</h3><hr class="hr_yellow">
							<table class="table table-striped">
								<?php while($row_Re_iv1=$Re_iv1->fetch_assoc()){?>
								<tr>
									<td width=""><?php echo $row_Re_iv1['iv_name'];?></td>
									<td width="120">
										<a href="file/investor/<?php echo $row_Re_iv1['iv_file'];?>"  target="_blank">
											<i class="fas fa-file-pdf fa-lg tx_red"></i>
										</a>
										<?php
										$filename = "file/investor/".$row_Re_iv1['iv_file'];
										$filesize=filesize($filename)/1024/1024;
										if($filesize>0){echo number_format($filesize,2) . ' MB.';}
										else{echo '- MB.';}
										?>
									</td>
								</tr>
								<?php } ?>
							</table>

						</div>
						<?php }else if($iv_type==2){ ?>
						<div class="tab-pane fade <?php if($iv_type==2){ echo "show active";}?>" id="iv-2" role="tabpanel" aria-labelledby="iv-2-tab">
							<h3>รายงานประจำปี</h3><hr class="hr_yellow">
							<table class="table table-striped">
								<?php while($row_Re_iv2=$Re_iv2->fetch_assoc()){?>
								<tr>
									<td width=""><?php echo $row_Re_iv2['iv_name'];?></td>
									<td width="120">
										<a href="file/investor/<?php echo $row_Re_iv2['iv_file'];?>"  target="_blank">
											<i class="fas fa-file-pdf fa-lg tx_red"></i>
										</a>
										<?php
										$filename = "file/investor/".$row_Re_iv2['iv_file'];
										$filesize=filesize($filename)/1024/1024;
										if($filesize>0){echo number_format($filesize,2) . ' MB.';}
										else{echo '- MB.';}
										?>
									</td>
								</tr>
								<?php } ?>
							</table>
						</div>
						<?php }else if($iv_type==3){ ?>
						<div class="tab-pane fade <?php if($iv_type==3){ echo "show active";}?>" id="iv-3" role="tabpanel" aria-labelledby="iv-3-tab">
							<h3>ข้อมูลนำเสนอ</h3><hr class="hr_yellow">
							<table class="table table-striped">
								<?php while($row_Re_iv3=$Re_iv3->fetch_assoc()){?>
								<tr>
									<td width=""><?php echo $row_Re_iv3['iv_name'];?></td>
									<td width="120">
										<a href="file/investor/<?php echo $row_Re_iv3['iv_file'];?>"  target="_blank">
											<i class="fas fa-file-pdf fa-lg tx_red"></i>
										</a>
										<?php
										$filename = "file/investor/".$row_Re_iv3['iv_file'];
										$filesize=filesize($filename)/1024/1024;
										if($filesize>0){echo number_format($filesize,2) . ' MB.';}
										else{echo '- MB.';}
										?>
									</td>
								</tr>
								<?php } ?>
							</table>
						</div>
						<?php } ?>

						<br>
						<div class="row mt-1">
							<div class="col-12 pr-2">
								<ul class="pagination justify-content-end">
								<li class="page-item <?php if($page==1){echo "disabled";}?>">
									<a class="page-link" href="<?php printf("%s?pageNum_Re_iv<?php echo $iv_type;?>=%d%s", $currentPage, 0, $currentSent); ?>" aria-label="Previous">
									<i class="fa fa-angle-double-left" aria-hidden="true"></i>
									</a>
								</li>

								<li class="page-item <?php if($page==1){echo "disabled";}?>">
									<a class="page-link" href="<?php printf("%s?pageNum_Re_iv<?php echo $iv_type;?>=%d%s", $currentPage, $page - 1, $currentSent); ?>" aria-label="Previous">
									<i class="fa fa-angle-left" aria-hidden="true"></i>
									</a>
								</li>

								<li class="page-item disabled">
									<span class="page-link">
									<?php 
									if($iv_type==1){$totalPagesRe_iv=$totalPagesRe_iv1;}
									else if($iv_type==2){$totalPagesRe_iv=$totalPagesRe_iv2;}
									else if($iv_type==3){$totalPagesRe_iv=$totalPagesRe_iv3;}

									if($totalPagesRe_iv>0){
										echo $page."/".$totalPagesRe_iv;
									}else{
										echo "-";
									}
									?>
									</span>
								</li>

								<li class="page-item <?php if($page==$totalPagesRe_iv OR $totalPagesRe_iv<1){echo "disabled";}?>">
									<a class="page-link" href="<?php printf("%s?pageNum_Re_iv<?php echo $iv_type;?>=%d%s", $currentPage, $page + 1, $currentSent); ?>" aria-label="Next">
									<i class="fa fa-angle-right" aria-hidden="true"></i>
									</a>
								</li>
										
								<li class="page-item <?php if($page==$totalPagesRe_iv OR $totalPagesRe_iv<1){echo "disabled";}?>">
									<a class="page-link" href="<?php printf("%s?pageNum_Re_iv<?php echo $iv_type;?>=%d%s", $currentPage, $totalPagesRe_iv1, $currentSent); ?>" aria-label="Next">
									<i class="fa fa-angle-double-right" aria-hidden="true"></i>
									</a>
								</li>
								</ul>
							</div>
						</div>
					</div>

					
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
