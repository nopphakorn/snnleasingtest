<?php 
include 'Connections/con_db.php';
include 'function/function.php';
$page_name="promotion";

$query_Re_pm = "SELECT * FROM tb_promotion WHERE pm_id='".$_GET['id']."' ";
$Re_pm=$mysqli->query($query_Re_pm);
$totalRows_Re_pm=$Re_pm->num_rows;
$row_Re_pm=$Re_pm->fetch_assoc();

$sql_btel="SELECT * FROM tb_banner_tel WHERE ban_id = '1'";
$Re_btel=$mysqli->query($sql_btel);
$row_Re_btel=$Re_btel->fetch_assoc();
$totalRows_Re_btel=$Re_btel->num_rows;
?>

<!DOCTYPE html>
<head>
  	<?php include 'include/inc_header.php';?>
</head>
<body>
  	<div class="container-fluid"><?php include 's_header.php';?></div>
	<div class="container-fluid"></div>
	<div class="container-fluid bg_top">
		<div class="container  pt-4 mb-4">
			<div class="row">
				<div class="col-md-12">
					<h3 class="my-4 tx_blue">โปรโมชั่น</h3>
					<a href="promotion.php">โปรโมชั่น</a> > รายละเอียดโปรโมชั่น<br>
					<hr class="hr_yellow">
				</div>
			</div>

			<div class="row mb-3">
				<div class="col-md-12 col-lg-3">
					<a class="pm_menu" href="promotion.php">โปรโมชั่น</a>
				</div>
				<div class="col-md-12 col-lg-9">
					<?php echo $row_Re_pm['pm_detail'];?>
					<div class="row p-0 m-0 mb-2">
						<?php if($totalRows_Re_btel>0){?>
						<div class="col-lg-12 text-center">
						<a href="<?php echo $row_Re_btel['ban_link'];?>">
						<img class="img-fluid" src="images/banner_tel/<?php echo $row_Re_btel['ban_photo'];?>"/></a>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
  	<?php include 's_footer.php';?>
	<?php include 'include/inc_script.php';?>
</body>
</html>
