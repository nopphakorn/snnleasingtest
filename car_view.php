<?php 
include 'Connections/con_db.php';
include 'car_check.php';
include 'function/function.php';

$query_Re_c = "SELECT a.*, b.ct_name, c.cb_name, d.cg_name, e.cm_name, e.ty_car FROM tb_car a ";
$query_Re_c.= "LEFT JOIN tb_car_type b ON(a.ct_id=b.ct_id) ";
$query_Re_c.= "LEFT JOIN tb_car_brand c ON(a.cb_id=c.cb_id) ";
$query_Re_c.= "LEFT JOIN tb_car_group d ON(a.cg_id=d.cg_id) ";
$query_Re_c.= "LEFT JOIN tb_car_model e ON(a.cm_id=e.cm_id) ";
$query_Re_c.= "WHERE a.c_status='1' AND a.c_id='".$_GET['c_id']."' ";
$Re_c=$mysqli->query($query_Re_c);
$row_Re_c=$Re_c->fetch_assoc();
$totalRows_Re_c=$Re_c->num_rows;
?>

<!doctype html>
<html>
<head>
	<?php include 'include/inc_header.php';?>
	<style>
	html {
    	margin-top: 0px;
	}
	</style>
</head>
<body>
	<div id="fancy_title"><i class="fa fa-list-alt"></i>&nbsp;ราคากลางรถ</div>
    <div id="fancy_box">
		<table width="100%" cellspacing="5" class="table table-sm table-hover">
			<tr>
				<td width="130px">ปรับปรุงราคา</td>
				<td><?php echo datethai_time($row_Re_c['c_date']);?></td>
			</tr>
			<tr>
				<td width="130px">ประเภท</td>
				<td><?php echo $row_Re_c['ct_name'];?></td>
			</tr>
			<tr>
				<td>ยี่ห้อ</td>
				<td><?php echo $row_Re_c['cb_name'];?></td>
			</tr>
			<tr>
				<td>กลุ่มรุ่นรถ</td>
				<td><?php echo $row_Re_c['cg_name'];?></td>
			</tr>
			<tr>
				<td>ชื่อรุ่นรถ</td>
				<td><?php echo $row_Re_c['cm_name'];?></td>
			</tr>
			<tr>
				<td>ระบบเกียร์</td>
				<td>
					<?php
					if($row_Re_c['c_gear']=='AT'){echo "ออโต้";}
					else if($row_Re_c['c_gear']=='MT'){echo "ธรรมดา";}
					?>
				</td>
			</tr>
			<tr>
				<td>ปีรถ</td>
				<td><?php echo $row_Re_c['c_year'];?></td>
			</tr>
			<tr>
				<td>ประเภท รย.</td>
				<td><?php echo $row_Re_c['ty_car'];?></td>
			</tr>
			<tr>
				<td><div class="tx_red">ราคากลาง</div></td>
				<td><div class="tx_red"><?php echo number_format($row_Re_c['c_price'])." บาท";?></div></td>
			</tr>
		</table>
		<?php if($row_Re_c['c_photo1']!="" OR $row_Re_c['c_photo2']!=""){ ?>
			<?php if($row_Re_c['c_photo1']!=""){ ?>
			<div class="carprice_box">
				<div class="carprice_img_view"><img src="images/model/<?php echo $row_Re_c['c_photo1']; ?>"/></div>
			</div>
			<?php } ?>
			<?php if($row_Re_c['c_photo2']!=""){ ?>
			<div class="carprice_box">
				<div class="carprice_img_view"><img src="images/model/<?php echo $row_Re_c['c_photo2']; ?>"/></div>
			</div>
			<?php } ?>
		<?php } ?>
	</div>
</body>
</html>
<?php $mysqli->close(); ?>