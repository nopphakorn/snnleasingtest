<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST["sv_id"])){$sv_id=mysqli_real_escape_string($mysqli, $_POST["sv_id"]);}
if(isset($_POST["sv_name"])){$sv_name=mysqli_real_escape_string($mysqli, $_POST["sv_name"]);}
if(isset($_POST["sv_title"])){$sv_title=mysqli_real_escape_string($mysqli, $_POST["sv_title"]);}
if(isset($_POST["sv_status"])){$sv_status=mysqli_real_escape_string($mysqli, $_POST["sv_status"]);}
if(isset($_POST["sv_detail"])){$sv_detail=mysqli_real_escape_string($mysqli, $_POST["sv_detail"]);}
if(isset($_POST["h_sv_photo"])){$h_sv_photo=mysqli_real_escape_string($mysqli, $_POST["h_sv_photo"]);}
if(isset($_POST["sv_name_en"])){$sv_name_en=mysqli_real_escape_string($mysqli, $_POST["sv_name_en"]);}
if(isset($_POST["sv_title_en"])){$sv_title_en=mysqli_real_escape_string($mysqli, $_POST["sv_title_en"]);}
if(isset($_POST["sv_detail_en"])){$sv_detail_en=mysqli_real_escape_string($mysqli, $_POST["sv_detail_en"]);}
	
if($action=="status"){
	$sv_id = $_GET['sv_id'];
	$sv_status = $_GET['sv_status'];

	$sql="UPDATE tb_service SET sv_status=$sv_status WHERE sv_id='$sv_id'";
	$Re_sql=$mysqli->query($sql);
	
	$GoTo = "admin_service_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
		}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="insert"){

	$sql="SELECT max(sv_no) AS sv_max FROM tb_service";
	$Re_sql=$mysqli->query($sql);
	$row_Re_sql=$Re_sql->fetch_assoc();
	if($row_Re_sql['sv_max']==0){$sv_no=1;}
	else{$sv_no=$row_Re_sql['sv_max']+1;}

	$photo=$_FILES["sv_photo"];
	$photo=$_FILES['sv_photo']['tmp_name'];
	$photo_name=$_FILES['sv_photo']['name'];

	if($photo != "") {
		$new_name ="SV".date('His').rand(1000,9999); 
		$path = "../images/service"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>1300){
			$width=1300;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic="";}
	
	$sql="INSERT INTO tb_service(sv_no, sv_name, sv_title, sv_status, sv_detail, sv_photo, sv_name_en, sv_title_en, sv_detail_en)
	values('$sv_no', '$sv_name', '$sv_title', '$sv_status','$sv_detail', '$pathPic', '$sv_name_en', '$sv_title_en', '$sv_detail_en' )";
	$Re_sql=$mysqli->query($sql);
	if (!$Re_sql) {printf("Error: %s\n", $mysqli->error);}
	
	$GoTo = "admin_service_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="edit"){
	
	$photo=$_FILES["sv_photo_edit"];
	$photo=$_FILES['sv_photo_edit']['tmp_name'];
	$photo_name=$_FILES['sv_photo_edit']['name'];

	if($photo != "") {
		$sql_chk = "SELECT * FROM tb_service WHERE sv_id='$sv_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['sv_photo'];
		if($file!=""){
			if (file_exists ("../images/service/$file")){unlink("../images/service/$file");}
		}

		$new_name ="SV".date('His').rand(1000,9999); 
		$path = "../images/service"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>1300){
			$width=1300;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic=$h_sv_photo;}
		
	$sql="UPDATE tb_service SET sv_name='$sv_name', sv_title='$sv_title', sv_status='$sv_status',sv_detail='$sv_detail',sv_photo='$pathPic', sv_name_en='$sv_name_en', sv_title_en='$sv_title_en' ,sv_detail_en='$sv_detail_en' WHERE sv_id='$sv_id'";
	$Re_sql=$mysqli->query($sql);
	
	$GoTo = "admin_service_edit.php?sv_id=$sv_id";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

	if($_GET['action']=="dele"){
		
		if ((isset($_GET['sv_id'])) && ($_GET['sv_id'] != "")) {
	
			$sv_id=$_GET['sv_id'];
			mysql_select_db($database_con_db, $con_db);
			$query_Re_dp = "SELECT * FROM tb_service WHERE sv_id = '$sv_id'";
			$Re_dp = mysql_query($query_Re_dp, $con_db) or die(mysql_error());
			$row_Re_dp = mysql_fetch_assoc($Re_dp);
		
			$file=$row_Re_dp['sv_photo'];
			if($file!=""){
			if (file_exists ("../images/service/$file")){
				unlink("../images/service/$file");
			}}
			$sql ="DELETE FROM tb_service WHERE sv_id='$sv_id'";
			  
			mysql_select_db($database_con_db, $con_db);
			$Result1 = mysql_query($sql, $con_db) or die(mysql_error());
	
		$GoTo = "admin_service_list.php";
		  if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		  }
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
		}
	}

?>
