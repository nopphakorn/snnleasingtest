<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST["ban_id"])){$ban_id=mysqli_real_escape_string($mysqli, $_POST["ban_id"]);}
if(isset($_POST["ban_status"])){$ban_status=mysqli_real_escape_string($mysqli, $_POST["ban_status"]);}
if(isset($_POST["ban_link"])){$ban_link=mysqli_real_escape_string($mysqli, $_POST["ban_link"]);}
if(isset($_POST["h_ban_photo"])){$h_photo=mysqli_real_escape_string($mysqli, $_POST["h_ban_photo"]);}
if(isset($_POST["h_ban_photo_md"])){$h_photo_md=mysqli_real_escape_string($mysqli, $_POST["h_ban_photo_md"]);}
if(isset($_POST["h_ban_photo_sm"])){$h_photo_sm=mysqli_real_escape_string($mysqli, $_POST["h_ban_photo_sm"]);}

if($_GET['action']=="status"){
	$ban_id = $_GET['ban_id'];
	$ban_status = $_GET['ban_status'];

	$sql="UPDATE tb_banner_tel SET ban_status='$ban_status' WHERE ban_id='$ban_id'";
	$Re_st=$mysqli->query($sql);
	
	$GoTo = "admin_banner_tel_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
		}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="edit"){	
	$photo=$_FILES["ban_photo_edit"];
	$photo=$_FILES['ban_photo_edit']['tmp_name'];
	$photo_name=$_FILES['ban_photo_edit']['name'];

	$photo_md=$_FILES["ban_photo_md_edit"];
	$photo_md=$_FILES['ban_photo_md_edit']['tmp_name'];
	$photo_md_name=$_FILES['ban_photo_md_edit']['name'];

	$photo_sm=$_FILES["ban_photo_sm_edit"];
	$photo_sm=$_FILES['ban_photo_sm_edit']['tmp_name'];
	$photo_sm_name=$_FILES['ban_photo_sm_edit']['name'];

	if($photo != ""){

		$sql_chk = "SELECT * FROM tb_banner_tel WHERE ban_id='$ban_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['ban_photo'];
		if($file!=""){
			if (file_exists ("../images/banner_tel/$file")){unlink("../images/banner_tel/$file");}
		}

		$new_name ="B".date('His').rand(1000,9999);
		$path = "../images/banner_tel"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>1300){
			$width=1300;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".png"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".PNG"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".png"){
				ImagePNG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".PNG"){
				ImagePNG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 					
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic=$h_photo;}

	if($photo_md != ""){

		$sql_chk = "SELECT * FROM tb_banner_tel WHERE ban_id='$ban_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['ban_photo_md'];
		if($file!=""){
			if (file_exists ("../images/banner_tel/$file")){unlink("../images/banner_tel/$file");}
		}

		$new_name ="BMD".date('His').rand(1000,9999);
		$path = "../images/banner_tel"; 
										
		$images_photo = $photo_md;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>768){
			$width=768;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_md_name,".")==".jpg" || strchr($photo_md_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_md_name,".")==".png"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}else if(strchr($photo_md_name,".")==".JPG" || strchr($photo_md_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_md_name,".")==".PNG"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_md_name,".")==".jpg" || strchr($photo_md_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_md_name,".")==".png"){
				ImagePNG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_md_name,".")==".JPG" || strchr($photo_md_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_md_name,".")==".PNG"){
				ImagePNG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 					
		}			
		$RenameFile=$new_name.strchr($photo_md_name,".");
		if (copy( $photo_md , "$path/$RenameFile" )){		
			unlink($photo_md);
			$pathPic_md="$RenameFile";	
		}
	}else{$pathPic_md=$h_photo_md;}

	if($photo_sm != ""){

		$sql_chk = "SELECT * FROM tb_banner_tel WHERE ban_id='$ban_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['ban_photo_sm'];
		if($file!=""){
			if (file_exists ("../images/banner_tel/$file")){unlink("../images/banner_tel/$file");}
		}

		$new_name ="BSM".date('His').rand(1000,9999);
		$path = "../images/banner_tel"; 
										
		$images_photo = $photo_sm;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>576){
			$width=576;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_sm_name,".")==".jpg" || strchr($photo_sm_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_sm_name,".")==".png"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}else if(strchr($photo_sm_name,".")==".JPG" || strchr($photo_sm_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_sm_name,".")==".PNG"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_sm_name,".")==".jpg" || strchr($photo_sm_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_sm_name,".")==".png"){
				ImagePNG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_sm_name,".")==".JPG" || strchr($photo_sm_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_sm_name,".")==".PNG"){
				ImagePNG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 					
		}			
		$RenameFile=$new_name.strchr($photo_sm_name,".");
		if (copy( $photo_sm , "$path/$RenameFile" )){		
			unlink($photo_sm);
			$pathPic_sm="$RenameFile";	
		}
	}else{$pathPic_sm=$h_photo_sm;}
	
	
	$sql_update="UPDATE tb_banner_tel SET ban_status='$ban_status', ban_link='$ban_link', ban_photo='$pathPic', ban_photo_md='$pathPic_md', ban_photo_sm='$pathPic_sm' WHERE ban_id='$ban_id'";
	$Re_update=$mysqli->query($sql_update);
	
	$GoTo = "admin_banner_tel_edit.php?ban_id=".$ban_id;
	if (isset($_SERVER['QUERY_STRING'])) {
	$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
	$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="dele"){
	if ((isset($_GET['ban_id'])) && ($_GET['ban_id'] != "")) {
		$ban_id=$_GET['ban_id'];
			
		$sql_chk= "SELECT * FROM tb_banner_tel WHERE ban_id='$ban_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['ban_photo'];
		if($file!=""){
			if (file_exists ("../images/banner_tel/$file")){unlink("../images/banner_tel/$file");}
		}

		$sql_update="UPDATE tb_banner_tel SET ban_photo='' WHERE ban_id='$ban_id'";
		$Re_update=$mysqli->query($sql_update);
			
		$GoTo = "admin_banner_tel_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}
$mysqli->close();
?>
