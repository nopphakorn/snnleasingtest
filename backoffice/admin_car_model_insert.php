<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');

$query_Re_cb = "SELECT * FROM tb_car_brand ORDER BY cb_name ASC ";
$Re_cb=$mysqli->query($query_Re_cb);

?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>
<body>
	<?php if(!isset($_GET['save']) OR $_GET['save']!='sucess'){ ?>
	<div id="fancy_title"><i class="fa fa-list-alt"></i>&nbsp;เพิ่มชื่อรุ่นรถ</div>
    <div id="fancy_box">
		<form action="admin_car_save.php?action=model-insert" method="post" enctype="multipart/form-data" name="form_cm" id="form_cm">

			<p>
				ประเภท รย.<br>
				 <select name="ty_car" id="" >
							<option value="" <?php if(@$row_Re_c['ty_car']==''){echo 'selected';}?> >		เลือก</option>
							<option value="รย.1 เก๋ง" <?php if(@$row_Re_c['ty_car']=='รย.1 เก๋ง'){echo 'selected';}?>>		รย.1 เก๋ง</option>
							<option value="รย.1 SUV/PPV" <?php if(@$row_Re_c['ty_car']=='รย.1 SUV/PPV'){echo 'selected';}?>>	รย.1 SUV/PPV</option>
							<option value="รย.1 4ประตู" <?php if(@$row_Re_c['ty_car']=='รย.1 4ประต'){echo 'selected';}?>>	รย.1 4ประตู</option>
							<option value="รย.2 รถตู้" <?php if(@$row_Re_c['ty_car']=='รย.2 รถต'){echo 'selected';}?>>		รย.2 รถตู้</option>
							<option value="รย.3 กระบะ" <?php if(@$row_Re_c['ty_car']=='รย.3 กระบะ'){echo 'selected';}?>>	รย.3 กระบะ</option>
                        </select>
			</p>
			<p>
				ยี่ห้อรถ<br>
				<select name="cb_id" id="cb_id" onchange="list_cg(this.value,'insert',form_cm.cm_name.value,form_cm.h_cm_name.value); check_cm(form_cm.cb_id.value, form_cm.cg_id.value, form_cm.cm_name.value, form_cm.h_cm_name.value);">
					<option value="">เลือกยี่ห้อรถ</option>
					<?php while($row_Re_cb=$Re_cb->fetch_assoc()){?>
					<option value="<?php echo $row_Re_cb['cb_id'];?>"><?php echo $row_Re_cb['cb_name'];?></option>
					<?php } ?>
				</select>
			</p>
			<p>
				กลุ่มรุ่นรถ<br>
				<select name="cg_id" id="cg_id" onchange="check_cm(form_cm.cb_id.value, form_cm.cg_id.value, form_cm.cm_name.value, form_cm.h_cm_name.value);">
					<option value="">เลือกกลุ่มรุ่นรถ</option>
				</select>
			</p>
			<p>
				ชื่อรุ่นรถ<br>
				<input type="text" id="cm_name" name="cm_name" style="width:100%" 
				onblur="check_cm(form_cm.cb_id.value, form_cm.cg_id.value, form_cm.cm_name.value, form_cm.h_cm_name.value)">
				<span id="msg" class="tx_error"></span>
				<input type="hidden" name="h_cm_name" id="h_cm_name" value="" />
			</p>
			<p><hr><input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/></p>
		</form>
	</div>
	<?php } ?>

	<?php if(isset($_GET['save']) AND $_GET['save']='sucess'){ ?>
	<div id="fancy_sucess">
		<br><br><br>
		<p><i class="fa fa-check-circle-o  fa-5x" aria-hidden="true"></i></p>
		<p>บันทึกรายการ รุ่นรถ เรียบร้อย</p>
	</div>
	<?php } ?>
</body>
</html>
<?php $mysqli->close(); ?>