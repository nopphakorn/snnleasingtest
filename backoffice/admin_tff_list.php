<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$maxRows_Re_tff = 100;
$pageNum_Re_tff = 0;
if (isset($_GET['pageNum_Re_tff'])) {
  $pageNum_Re_tff = $_GET['pageNum_Re_tff'];
}
$startRow_Re_tff = (($pageNum_Re_tff-1) * $maxRows_Re_tff);
if($startRow_Re_tff<0){$startRow_Re_tff=0;}

$query_Re_tff = "SELECT * FROM tb_tff ORDER BY tff_id ASC ";
$query_limit_Re_tff = sprintf("%s LIMIT %d, %d", $query_Re_tff, $startRow_Re_tff, $maxRows_Re_tff);
$Re_tff=$mysqli->query($query_limit_Re_tff);

if (isset($_GET['totalRows_Re_tff'])) {
  $totalRows_Re_tff = $_GET['totalRows_Re_tff'];
} else {
  $all_Re_tff=$mysqli->query($query_Re_tff);
  $totalRows_Re_tff=$all_Re_tff->num_rows;
}
$totalPages_Re_tff = ceil($totalRows_Re_tff/$maxRows_Re_tff);
$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_tff==0){$page=1;}
else if($pageNum_Re_tff>0){$page=$pageNum_Re_tff;}

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
	<script type="text/javascript">
        $(document).ready(function() {
            $('.btn_view').fancybox({
                    'type' : 'iframe',
                    'width' : '700',
                    'height' : '350',
                    'autoScale' : false,
                    'fitToView' : false,
                    'autoSize' : false,
            });
        });
	</script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_tff_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_tff_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo" รายการ".$title;?></div>
               <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_tff=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_tff=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_tff>0){
                                echo "Page : ".$page."/".$totalPages_Re_tff." Total : ".number_format($totalRows_Re_tff)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                        

                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_tff=%d%s", $currentPage, $totalPages_Re_tff, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_tff){printf("%s?pageNum_Re_tff=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" align="center">
                    <tr >
                        <th width="40" height="40"><div align="center">ลำดับ</div></th>
						<th width="110" height="40"><div align="center">บัตรประชาชน/<br>หนังสือเดินทาง</div></th>
                        <th width="" height="40"><div align="center">ชื่อ-นามสกุล</div></th>
						<th width="150" height="40"><div align="center">Email</div></th>
						<th width="120" height="40"><div align="center">เบอร์โทร</div></th>
                        <th width="80" height="40"><div align="center">สถิติการเข้า</div></th>
                        <th width="180" height="40"><div align="center">เข้าล่าสุด</div></th>
                        <th width="40" height="40"><div align="center">สถานะ</div></th>
                        <th width="40" height="40"><div align="center">ดู</div></th>
                        <th width="40" height="40"><div align="center">ลบ</div></th>
                    </tr>
                    <?php 
                        if($totalRows_Re_tff>0){
                            $number=$startRow_Re_tff; 
                            while($row_Re_tff=$Re_tff->fetch_assoc()){
                    ?>
                    <tr >
                        <td><div align="center"><?php echo $number+=1; ?></div></td>
						<td><div align="center"><?php echo $row_Re_tff['tff_no']; ?></div></td>
                        <td><div align="left">
								<?php 
								if($row_Re_tff['tff_title']=='orther'){
									echo $row_Re_tff['tff_title_n'];
								}else{
									echo $row_Re_tff['tff_title'];
								}
								echo $row_Re_tff['tff_name']." ".$row_Re_tff['tff_last']; 
								?>
							</div></td>
						<td><div align="left"><?php echo $row_Re_tff['tff_email']; ?></div></td>
						<td><div align="left"><?php echo $row_Re_tff['tff_tel']; ?></div></td>
                        <td>
                            <div align="center">
                                <?php 
                                if($row_Re_tff['tff_count']!=""){
                                    echo $row_Re_tff['tff_count'];
                                } else { echo "<font color='#FF0004'>-</font>";} 
                                ?>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <?php 
                                if($row_Re_tff['tff_count']!=""){
                                    echo datethai($row_Re_tff['tff_date']);
                                } else { echo "<font color='#FF0004'>ไม่เคยเข้าสู่ระบบ</font>";}
                                ?>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <?php if($row_Re_tff['tff_status']=="1"){ ?>
                                <a href="admin_tff_save.php?tff_id=<?php echo $row_Re_tff['tff_id']; ?>&action=status&tff_status=0"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
                                <?php }else{ ?>
                                <a href="admin_tff_save.php?tff_id=<?php echo $row_Re_tff['tff_id']; ?>&action=status&tff_status=1"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
                                <?php } ?>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a class="btn_view" href="admin_tff_detail.php?tff_id=<?php echo $row_Re_tff['tff_id']; ?>">
                                <img src="images/icon/icon_anw_view_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_tff_save.php?action=dele&tff_id=<?php echo $row_Re_tff['tff_id']; ?>" onclick = "return confirm('คุณต้องการลบ สมาชิก TFF หรือไม่')">
                                <img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a></div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="10">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>   
    </div>
</body>
</html>
<?php $mysqli->close(); ?>