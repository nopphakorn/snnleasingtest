<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$maxRows_Re_t = 50;
$pageNum_Re_t = 0;
if (isset($_GET['pageNum_Re_t'])) {
  $pageNum_Re_t = $_GET['pageNum_Re_t'];
}
$startRow_Re_t = (($pageNum_Re_t-1) * $maxRows_Re_t);
if($startRow_Re_t<0){$startRow_Re_t=0;}

$query_Re_t = "SELECT * FROM tb_tel ORDER BY t_id DESC ";
$query_limit_Re_t = sprintf("%s LIMIT %d, %d", $query_Re_t, $startRow_Re_t, $maxRows_Re_t);
$Re_t=$mysqli->query($query_limit_Re_t);

if (isset($_GET['totalRows_Re_t'])) {
  $totalRows_Re_t = $_GET['totalRows_Re_t'];
} else {
  $all_Re_t=$mysqli->query($query_Re_t);
  $totalRows_Re_t=$all_Re_t->num_rows;
}
$totalPages_Re_t = ceil($totalRows_Re_t/$maxRows_Re_t);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_t==0){$page=1;}
else if($pageNum_Re_t>0){$page=$pageNum_Re_t;}

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_tel_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_tel_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;".$title;?></div>
                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_t=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_t=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_t>0){
                                echo "Page : ".$page."/".$totalPages_Re_t." Total : ".number_format($totalRows_Re_t)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                        

                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_t=%d%s", $currentPage, $totalPages_Re_t, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_t){printf("%s?pageNum_Re_t=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" border="1" align="center">
                    <tr>
						<th width="35">No.</th>
                        <th width="">เบอร์โทรศัพท์</th>
                        <th width="">วันที่สมัคร</th>
                        <th width="40"><div align="center">ลบ</div></th>
                    </tr>
                    <?php 
                    if($totalRows_Re_t>0){
                        $number=$startRow_Re_t;
                        while($row_Re_t=$Re_t->fetch_assoc()){
                    ?>
                    <tr>
						<td valign="top"><div align="center"><?php echo $number+=1; ?></div></td>
						<td valign="top"><?php echo $row_Re_t['t_tel']; ?></div></td>
						<td valign="top"><?php echo datethai($row_Re_t['t_date']); ?></div></td>
                        <td valign="top"><div align="center">
                                <div align="center"> <a href="admin_tel_save.php?action=dele&t_id=<?php echo $row_Re_t['t_id']; ?>" onclick = "return confirm('คุณต้องการลบเบอร์โทรศัพท์หรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a></div>
                            </div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="6">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
           </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>