<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

if (isset($_GET['admin_id'])) {$admin_id_chk = mysqli_real_escape_string($mysqli, $_GET['admin_id']);}
$sql_admin="SELECT * FROM tb_admin WHERE admin_id = '".$admin_id_chk."'";
$Re_admin=$mysqli->query($sql_admin);
$row_Re_admin=$Re_admin->fetch_assoc();
?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_user_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_user_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-pencil fa-lg" style="color:#3097FF"></i><?php echo" แก้ไขรายการ".$title;?></div>
                <div class="box_form">
                    <form action="admin_user_save.php?action=edit" method="post" enctype="multipart/form-data" name="form_ad" id="form_ad">
                        <table width="100%" border="0">
                            <tr>
                                <td width="160">สถานะ</td>
                                <td>
                                    <select name="admin_status" id="admin_status">
                                        <option value="1" <?php if (!(strcmp(1, $row_Re_admin['admin_status']))) {echo "selected=\"selected\"";} ?>>อนุญาติ</option>
                                        <option value="0" <?php if (!(strcmp(0, $row_Re_admin['admin_status']))) {echo "selected=\"selected\"";} ?>>ไม่อนุญาติ</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="160"><div align="left">ชื่อ-นามสกุล</div></td>
                                <td><input name="admin_name" type="text" id="admin_name" value="<?php echo $row_Re_admin['admin_name']; ?>" size="30"/></td>
                            </tr>
                            <tr>
                                <td width="160">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="160"><div align="left">Username</div></td>
                                <td>
                                    <input name="admin_user" type="text" id="admin_user" value="<?php echo $row_Re_admin['admin_user']; ?>" onkeyup="check_user(form_ad.admin_user.value, form_ad.h_admin_user.value)"/>
                                    <span id="msg"></span>
                                    <input name="h_admin_user" type="hidden" id="h_admin_user" value="<?php echo  $row_Re_admin['admin_user']; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td width="160"><div align="left" >เปลี่ยน Password ใหม่</div></td>
                                <td>
                                    <input name="n_admin_pass" type="password" id="n_admin_pass"/>
                                    <input name="h_admin_pass" type="hidden" id="h_admin_pass" value="<?php echo $row_Re_admin['admin_pass']; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td width="160"><div align="left" >Confirm Password ใหม่</div></td>
                                <td><input name="c_admin_pass_edit" type="password" id="c_admin_pass_edit"/></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <hr>
                                    <input name="Submit" type="submit" id="Submit" value="แก้ไขผู้ดูแลระบบ" />
                                    <input name="admin_id" type="hidden" id="admin_id" value="<?php echo $row_Re_admin['admin_id']; ?>" />
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>