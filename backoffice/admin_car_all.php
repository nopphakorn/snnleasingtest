<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="all-list";

$query_Re_cb_s = "SELECT * FROM tb_car_brand ORDER BY cb_name ASC ";
$Re_cb_s=$mysqli->query($query_Re_cb_s);

if((isset($_GET['cb_id'])) AND (isset($_GET['ct_id']))){
	if(!empty($_GET['cb_id'])){
	$query_Re_cb_s2 = "SELECT * FROM tb_car_brand WHERE cb_id = ".$_GET['cb_id']." ";
	$Re_cb_s2=$mysqli->query($query_Re_cb_s2);
	$row_Re_cb_s2=$Re_cb_s2->fetch_assoc();

	$query_Re_ct_s = "SELECT * FROM tb_car_type WHERE ct_id IN(".$row_Re_cb_s2['ct_id'].") ORDER BY ct_id ASC ";
	$Re_ct_s=$mysqli->query($query_Re_ct_s);
	}
}

if((isset($_GET['cb_id'])) AND (isset($_GET['ct_id'])) AND (isset($_GET['cg_id']))){
	if(!empty($_GET['cb_id'])){
	$query_Re_cb_s2 = "SELECT * FROM tb_car_brand WHERE cb_id = ".$_GET['cb_id']." ";
	$Re_cb_s2=$mysqli->query($query_Re_cb_s2);
	$row_Re_cb_s2=$Re_cb_s2->fetch_assoc();

	$query_Re_ct_s = "SELECT * FROM tb_car_type WHERE ct_id IN(".$row_Re_cb_s2['ct_id'].") ORDER BY ct_id ASC ";
	$Re_ct_s=$mysqli->query($query_Re_ct_s);

	$query_Re_cg_s = "SELECT * FROM tb_car_group WHERE cb_id='".$cb_id."' AND ct_id='".$ct_id."'  ORDER BY cg_name ASC ";
	$Re_cg_s=$mysqli->query($query_Re_cg_s);
	}
}

if((isset($_GET['cb_id'])) AND (isset($_GET['cg_id'])) AND (isset($_GET['cm_id']))){
$query_Re_cm_s = "SELECT * FROM tb_car_model WHERE cb_id='".$_GET['cb_id']."' AND cg_id='".$_GET['cg_id']."' ORDER BY cm_name ASC ";
$Re_cm_s=$mysqli->query($query_Re_cm_s);
}

$maxRows_Re_ca = 50;
$pageNum_Re_ca = 0;
if (isset($_GET['pageNum_Re_ca'])) {
  $pageNum_Re_ca = $_GET['pageNum_Re_ca'];
}
$startRow_Re_ca = (($pageNum_Re_ca-1) * $maxRows_Re_ca);
if($startRow_Re_ca<0){$startRow_Re_ca=0;}

$query_Re_ca = "SELECT a.*, b.ct_name, c.cb_name, d.cg_name, e.cm_name, e.ty_car FROM tb_car a ";
$query_Re_ca.= "LEFT JOIN tb_car_type b ON(a.ct_id=b.ct_id) ";
$query_Re_ca.= "LEFT JOIN tb_car_brand c ON(a.cb_id=c.cb_id) ";
$query_Re_ca.= "LEFT JOIN tb_car_group d ON(a.cg_id=d.cg_id) ";
$query_Re_ca.= "LEFT JOIN tb_car_model e ON(a.cm_id=e.cm_id) ";
$query_Re_ca.= "WHERE a.c_id!='' ";
if(!isset($_GET['ct_id']) AND !isset($_GET['cb_id']) AND !isset($_GET['cg_id']) AND !isset($_GET['cm_id']) AND !isset($_GET['c_year'])){
    $query_Re_ca.= "AND a.c_id=-1 ";
}
if(isset($_GET['ct_id']) AND $_GET['ct_id']!=""){
    $ct_id_chk=mysqli_real_escape_string($mysqli, $_GET["ct_id"]);
    $query_Re_ca.= "AND a.ct_id='$ct_id_chk' ";
}
if(isset($_GET['cb_id']) AND $_GET['cb_id']!=""){
    $cb_id_chk=mysqli_real_escape_string($mysqli, $_GET["cb_id"]);
    $query_Re_ca.= "AND a.cb_id='$cb_id_chk' ";
}
if(isset($_GET['cg_id']) AND $_GET['cg_id']!=""){
    $cg_id_chk=mysqli_real_escape_string($mysqli, $_GET["cg_id"]);
    $query_Re_ca.= "AND a.cg_id='$cg_id_chk' ";
}
if(isset($_GET['cm_id']) AND $_GET['cm_id']!=""){
    $cm_id_chk=mysqli_real_escape_string($mysqli, $_GET["cm_id"]);
    $query_Re_ca.= "AND a.cm_id='$cm_id_chk' ";
}
if(isset($_GET['c_year']) AND $_GET['c_year']!=""){
    $c_year_chk=mysqli_real_escape_string($mysqli, $_GET["c_year"]);
    $query_Re_ca.= "AND a.c_year='$c_year_chk' ";
}
$query_Re_ca.= "ORDER BY c.cb_name ASC, b.ct_name ASC, d.cg_name ASC, e.cm_name ASC, a.c_year ASC ";
$query_limit_Re_ca = sprintf("%s LIMIT %d, %d", $query_Re_ca, $startRow_Re_ca, $maxRows_Re_ca);
$Re_ca=$mysqli->query($query_limit_Re_ca);

if (isset($_GET['totalRows_Re_ca'])) {
  $totalRows_Re_ca = $_GET['totalRows_Re_ca'];
} else {
  $all_Re_ca=$mysqli->query($query_Re_ca);
  $totalRows_Re_ca=$all_Re_ca->num_rows;
}
$totalPages_Re_ca = ceil($totalRows_Re_ca/$maxRows_Re_ca);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";
if(isset($_GET['ct_id']) AND $_GET['ct_id']!=""){$currentSent.= "&ct_id=".$_GET['ct_id'];}
if(isset($_GET['cb_id']) AND $_GET['cb_id']!=""){$currentSent.= "&cb_id=".$_GET['cb_id'];}
if(isset($_GET['cg_id']) AND $_GET['cg_id']!=""){$currentSent.= "&cg_id=".$_GET['cg_id'];}
if(isset($_GET['cm_id']) AND $_GET['cm_id']!=""){$currentSent.= "&cm_id=".$_GET['cm_id'];}


if($pageNum_Re_ca==0){$page=1;}
else if($pageNum_Re_ca>0){$page=$pageNum_Re_ca;}

if (!$Re_ca) {printf("Error: %s\n", $mysqli->error);}

?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
	<script type="text/javascript">
        $(document).ready(function() {
            $('.btn_insert').fancybox({
                    'type' : 'iframe',
                    'width' : '690',
                    'height' : '420',
                    'autoScale' : false,
                    'fitToView' : false,
                    'autoSize' : false,
                    'afterClose' : function() {parent.location.reload(true); },
            });
        });
	</script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_car_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_car_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i>&nbsp;รายการราคากลางรถ</div>

                <table width="100%">
                    <tr>
                        <td width="170px">
                            <span class="btn_green">
                                <a class="btn_insert" href="admin_car_all_insert.php">
                                <i class="fa fa-plus fa-lg" style="color:#3097FF"></i> เพิ่มราคากลางรถ</a>
                            </span>
                        </td>
                        <td>
                            <form action="admin_car_all.php" method="GET" enctype="multipart/form-data" name="form1" id="form1">
                                เลือกการค้นหา 
                                <select name="cb_id" id="cb_id" onchange="list_ca('ct_id',form1.cb_id.value,'','')">
                                    <option value="">ยี่ห้อรถ</option>
                                    <?php while($row_Re_cb_s=$Re_cb_s->fetch_assoc()){?>
                                    <option value="<?php echo $row_Re_cb_s['cb_id'];?>" <?php if(isset($_GET['cb_id'])){if($row_Re_cb_s['cb_id']==$_GET['cb_id']){echo "selected=\"selected\"";}} ?>><?php echo $row_Re_cb_s['cb_name'];?></option>
                                    <?php } ?>
                                </select>
                                &nbsp;

                                <select name="ct_id" id="ct_id" onchange="list_ca('cg_id', form1.cb_id.value, form1.ct_id.value,'')">
                                    <option value="">ประเภท</option>
                                    <?php 
                                    if((isset($_GET['cb_id'])) AND (!empty($_GET['cb_id']))){ 
                                    while($row_Re_ct_s=$Re_ct_s->fetch_assoc()){?>
                                    <option value="<?php echo $row_Re_ct_s['ct_id'];?>" <?php if(isset($_GET['ct_id'])){if($row_Re_ct_s['ct_id']==$_GET['ct_id']){echo "selected=\"selected\"";}} ?>><?php echo $row_Re_ct_s['ct_name'];?></option>
                                    <?php }} ?>
                                </select>
                                &nbsp;
                                
                                <select name="cg_id" id="cg_id" onchange="list_ca('cm_id', form1.cb_id.value, form1.ct_id.value, form1.cg_id.value)">
                                    <option value="">กลุ่มรุ่นรถ</option>
                                    <?php 
                                    if((isset($_GET['cb_id']) AND !empty($_GET['cb_id'])) AND (isset($_GET['ct_id']) AND !empty($_GET['ct_id']))){ 
                                        while($row_Re_cg_s=$Re_cg_s->fetch_assoc()){
                                    ?>
                                    <option value="<?php echo $row_Re_cg_s['cg_id'];?>" <?php if(isset($_GET['cg_id'])){if($row_Re_cg_s['cg_id']==$_GET['cg_id']){echo "selected=\"selected\"";}} ?>><?php echo $row_Re_cg_s['cg_name'];?></option>
                                    <?php }} ?>
                                </select>
                                &nbsp;
                                <select name="cm_id" id="cm_id">
                                    <option value="">รุ่นรถ</option>
                                    <?php 
                                    if((isset($_GET['cm_id'])) AND (isset($_GET['cm_id']))){ 
                                        while($row_Re_cm_s=$Re_cm_s->fetch_assoc()){
                                    ?>
                                    <option value="<?php echo $row_Re_cm_s['cm_id'];?>" <?php if(isset($_GET['cm_id'])){if($row_Re_cm_s['cm_id']==$_GET['cm_id']){echo "selected=\"selected\"";}} ?>><?php echo $row_Re_cm_s['cm_name'];?></option>
                                    <?php }} ?>
                                </select>
                                <input type="submit" value="ค้นหา">
                            </form>
                        </td>
                    </tr>
                </table>
				
                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_ca=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_ca=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_ca>0){
                                echo "Page : ".$page."/".$totalPages_Re_ca." Total : ".number_format($totalRows_Re_ca)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_ca=%d%s", $currentPage, $totalPages_Re_ca, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_ca){printf("%s?pageNum_Re_ca=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" border="1" align="center" cellpadding="5">
                    <tr>
                        <th width="35">No.</th>
                        <th width="">ID</th>
                        <th width="110">Update</th>
                        <th width="90">รูปภาพรถ</th>
                        <th width="90">ยี่ห้อรถ</th>
						<th width="70">ประเภท</th>
						<th width="70">ประเภท รย.</th>
                        <th width="120">กลุ่มรุ่นรถ</th>
                        <th width="">รุ่นรถ</th>
                        <th width="50">เกียร์</th>
                        <th width="50">รถปี</th>
                        <th width="70">ราคากลาง</th>
                        <th width="40">สถานะ</th>
                        <th width="40">แก้ไข</th>
                        <th width="40">ลบ</th>
                    </tr>
                    <?php 
                    if($totalRows_Re_ca>0){
                        $number=0;
                        while($row_Re_ca=$Re_ca->fetch_assoc()){
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $number+=1; ?></div></td> 
                        <td><div align="center"><?php echo $row_Re_ca['c_id']; ?></div></td> 
                        <td><div class="tx_red" align="right"><?php echo datethai_sm_time($row_Re_ca['c_date']); ?></div></td>
                        <td>
                            <div align="center">
                                <div class="carprice_img_list">
                                    <?php if($row_Re_ca['c_photo1']!=""){?>
                                    <img src="../images/model/<?php echo $row_Re_ca['c_photo1'];?>" />
                                    <?php }else{?>
                                    <img src="../images/model/nophoto.png" />
                                    <?php } ?>
                                </div>
                            </div>
                        </td>
                        <td><div align="left"><?php echo $row_Re_ca['cb_name']; ?></div></td>
						<td><div align="left"><?php echo $row_Re_ca['ct_name']; ?></div></td>
						<td><div align="left"><?php echo $row_Re_ca['ty_car']; ?></div></td>
						<td><div align="left"><?php echo $row_Re_ca['cg_name']; ?></div></td>
                        <td><div align="left"><?php echo $row_Re_ca['cm_name']; ?></div></td>
                        <td>
                            <div align="center">
                            <?php if($row_Re_ca['c_gear']=='AT'){echo "ออโต้";}else if($row_Re_ca['c_gear']=='MT'){echo "เกียร์ธรรมดา";}  ?>
                            </div>
                        </td>
                        <td><div align="center"><?php echo $row_Re_ca['c_year']; ?></div></td>
                        <td><div align="right"><?php echo number_format($row_Re_ca['c_price']); ?></div></td>
                        <td><div align="center">
                                <?php if($row_Re_ca['c_status']=="1"){ ?>
                                <a href="<?php printf("admin_car_save.php?action=all-status&c_status=0&c_id=%d%s", $row_Re_ca['c_id'], $currentSent); ?>"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
                                <?php }else{ ?>
                                <a href="<?php printf("admin_car_save.php?action=all-status&c_status=1&c_id=%d%s", $row_Re_ca['c_id'], $currentSent); ?>"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
                                <?php } ?>
                            </div></td>
                        <td>
                            <div align="center">
                                <a class="btn_insert" href="admin_car_all_edit.php?c_id=<?php echo $row_Re_ca['c_id']; ?>">
                                <img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td width="19">
                            <div align="center">
                                <a href="<?php printf("admin_car_save.php?action=all-dele&&c_id=%d%s", $row_Re_ca['c_id'], $currentSent); ?>" onclick = "return confirm('คุณต้องการลบ ราคากลางรถ หรือไม่')">
                                <img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a>
                            </div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="15">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>   
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>

