<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="brand-list";

$query_Re_cb = "SELECT * FROM tb_car_brand ORDER BY cb_name ASC ";
$Re_cb=$mysqli->query($query_Re_cb);
$totalRows_Re_cb=$Re_cb->num_rows;

?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
	
	<script type="text/javascript">
        $(document).ready(function() {
            $('.btn_insert').fancybox({
                    'type' : 'iframe',
                    'width' : '500',
                    'height' : '200',
                    'autoScale' : false,
                    'fitToView' : false,
                    'autoSize' : false,
                    'afterClose' : function() {parent.location.reload(true); },
            });
        });
	</script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_car_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_car_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i>&nbsp;รายการยี่ห้อรถ</div>

				<span class="btn_green">
					<a class="btn_insert" href="admin_car_brand_insert.php">
					<i class="fa fa-plus fa-lg" style="color:#3097FF"></i> เพิ่มยี่ห้อรถ</a>
				</span>

                <table class="tb1" width="100%" border="1" align="center" cellpadding="5">
                    <tr>
                        <th width="45"><div align="center">No.</div></th>
                        <th width="200"><div align="center">รายการยี่ห้อรถ</th>
                        <th width="">ประเภทรถ</th>
                        <th width="40">สถานะ</th>
                        <th width="40"><div align="center">แก้ไข</div></th>
                        <th width="40"><div align="center">ลบ</div></th>
                    </tr>
                    <?php 
                    if($totalRows_Re_cb>0){
                        $number=0;
                        while($row_Re_cb=$Re_cb->fetch_assoc()){
                            $query_Re_ct = "SELECT * FROM tb_car_type ORDER BY ct_id ASC ";
                            $Re_ct=$mysqli->query($query_Re_ct);
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $number+=1; ?></div></td>
                        <td>
                            <div align="left">
                                <?php echo $row_Re_cb['cb_name']; ?>
                            </div>
                        </td>
                        <td>
                            <div align="left">
                                <?php echo $row_Re_cb['ct_name']; ?>
                            </div>
                        </td>
                        <td><div align="center">
                                <?php if($row_Re_cb['cb_status']=="1"){ ?>
                                <a href="admin_car_save.php?action=brand-status&cb_id=<?php echo $row_Re_cb['cb_id']; ?>&cb_status=0"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
                                <?php }else{ ?>
                                <a href="admin_car_save.php?action=brand-status&cb_id=<?php echo $row_Re_cb['cb_id']; ?>&cb_status=1"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
                                <?php } ?>
                            </div></td>
                        <td>
                            <div align="center">
                                <a class="btn_insert" href="admin_car_brand_edit.php?cb_id=<?php echo $row_Re_cb['cb_id']; ?>">
                                <img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td width="19" scope="col"><div align="center">
                                <div align="center"> <a href="admin_car_save.php?action=brand-dele&cb_id=<?php echo $row_Re_cb['cb_id']; ?>" onclick = "return confirm('คุณต้องการลบยี่ห้อรถหรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a></div>
                            </div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="5">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>   
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>

