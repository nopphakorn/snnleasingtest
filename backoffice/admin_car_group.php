<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="group-list";

$query_Re_cb = "SELECT * FROM tb_car_brand ORDER BY cb_name ASC ";
$Re_cb=$mysqli->query($query_Re_cb);
$totalRows_Re_cb=$Re_cb->num_rows;

$query_Re_cb2 = "SELECT * FROM tb_car_brand ORDER BY cb_name ASC ";
$Re_cb2=$mysqli->query($query_Re_cb2);

$maxRows_Re_cg = 50;
$pageNum_Re_cg = 0;
if (isset($_GET['pageNum_Re_cg'])) {
  $pageNum_Re_cg = $_GET['pageNum_Re_cg'];
}
$startRow_Re_cg = (($pageNum_Re_cg-1) * $maxRows_Re_cg);
if($startRow_Re_cg<0){$startRow_Re_cg=0;}

if(isset($_GET['cb_id']) AND $_GET['cb_id']!="all"){$cb_id_chk=mysqli_real_escape_string($mysqli, $_GET["cb_id"]);}
else if(isset($_GET['cb_id']) AND $_GET['cb_id']=="all"){$cb_id_chk=-1;}
else{$cb_id_chk=-1;}

$query_Re_cg = "SELECT a.*, b.cb_name, c.ct_name FROM tb_car_group a ";
$query_Re_cg.= "LEFT JOIN tb_car_brand b ON(a.cb_id=b.cb_id) ";
$query_Re_cg.= "LEFT JOIN tb_car_type c ON(a.ct_id=c.ct_id) ";
if($cb_id_chk!=-1){$query_Re_cg.= "WHERE a.cb_id='$cb_id_chk' ";}
$query_Re_cg.= "ORDER BY b.cb_name ASC, c.ct_id ASC, a.cg_name ASC ";
$query_limit_Re_cg = sprintf("%s LIMIT %d, %d", $query_Re_cg, $startRow_Re_cg, $maxRows_Re_cg);
$Re_cg=$mysqli->query($query_limit_Re_cg);

if (isset($_GET['totalRows_Re_cg'])) {
  $totalRows_Re_cg = $_GET['totalRows_Re_cg'];
} else {
  $all_Re_cg=$mysqli->query($query_Re_cg);
  $totalRows_Re_cg=$all_Re_cg->num_rows;
}
$totalPages_Re_cg = ceil($totalRows_Re_cg/$maxRows_Re_cg);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";
if(isset($_GET['cb_id']) AND $_GET['cb_id']!="all"){
$currentSent.= "&cb_id=".$_GET['cb_id'];
}

if($pageNum_Re_cg==0){$page=1;}
else if($pageNum_Re_cg>0){$page=$pageNum_Re_cg;}

?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
	
	<script type="text/javascript">
        $(document).ready(function() {
            $('.btn_insert').fancybox({
                    'type' : 'iframe',
                    'width' : '500',
                    'height' : '350',
                    'autoScale' : false,
                    'fitToView' : false,
                    'autoSize' : false,
                    'afterClose' : function() {parent.location.reload(true); },
            });
        });
	</script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_car_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_car_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i>&nbsp;รายการกลุ่มรุ่นรถ</div>

                <table width="100%">
                    <tr>
                        <td width="150px">
                            <span class="btn_green">
                                <a class="btn_insert" href="admin_car_group_insert.php">
                                <i class="fa fa-plus fa-lg" style="color:#3097FF"></i> เพิ่มกลุ่มรุ่นรถ</a>
                            </span>
                        </td>
                        <td>
                            <form action="admin_car_group.php" method="GET" enctype="multipart/form-data" name="form1" id="form1">
                                เลือกยี่ห้อรถ 
                                <select name="cb_id" id="cb_id">
                                    <option value="">เลือกยี่ห้อรถ</option>
                                    <option value="all">ทุกยี่ห้อ</option>
                                    <?php while($row_Re_cb2=$Re_cb2->fetch_assoc()){?>
                                    <option value="<?php echo $row_Re_cb2['cb_id'];?>"><?php echo $row_Re_cb2['cb_name'];?></option>
                                    <?php } ?>
                                </select>
                                <input type="submit" value="ค้นหา">
                            </form>
                        </td>
                    </tr>
                </table>

                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_cg=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_cg=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_cg>0){
                                echo "Page : ".$page."/".$totalPages_Re_cg." Total : ".number_format($totalRows_Re_cg)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_cg=%d%s", $currentPage, $totalPages_Re_cg, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_cg){printf("%s?pageNum_Re_cg=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" border="1" align="center" cellpadding="5">
                    <tr>
                        <th width="45">No.</th>
                        <th width="200">ยี่ห้อรถ</th>
                        <th width="100">ประเภทรถ</th>
                        <th width="">รายการกลุ่มรุ่นรถ</th>
                        <th width="40">สถานะ</th>
                        <th width="40">แก้ไข</th>
                        <th width="40">ลบ</th>
                    </tr>
                    <?php 
                    if($totalRows_Re_cg>0){
                        $number=0;
                        while($row_Re_cg=$Re_cg->fetch_assoc()){
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $number+=1; ?></div></td> 
                        <td><div align="left"><?php echo $row_Re_cg['cb_name']; ?></div></td>
                        <td><div align="left"><?php echo $row_Re_cg['ct_name']; ?></div></td>
						<td><div align="left"><?php echo $row_Re_cg['cg_name']; ?></div></td>
                        <td><div align="center">
                                <?php if($row_Re_cg['cg_status']=="1"){ ?>
                                <a href="admin_car_save.php?action=group-status&cg_id=<?php echo $row_Re_cg['cg_id']; ?>&cg_status=0"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
                                <?php }else{ ?>
                                <a href="admin_car_save.php?action=group-status&cg_id=<?php echo $row_Re_cg['cg_id']; ?>&cg_status=1"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
                                <?php } ?>
                            </div></td>
                        <td>
                            <div align="center">
                                <a class="btn_insert" href="admin_car_group_edit.php?cg_id=<?php echo $row_Re_cg['cg_id']; ?>">
                                <img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td width="19" scope="col"><div align="center">
                                <div align="center"> <a href="admin_car_save.php?action=group-dele&cg_id=<?php echo $row_Re_cg['cg_id']; ?>" onclick = "return confirm('คุณต้องการลบ กลุ่มรุ่นรถ หรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a></div>
                            </div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="6">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>   
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>

