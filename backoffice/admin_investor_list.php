<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$iv_type=mysqli_real_escape_string($mysqli, $_GET['type']);

$maxRows_Re_iv = 50;
$pageNum_Re_iv = 0;
if (isset($_GET['pageNum_Re_iv'])) {
  $pageNum_Re_iv = $_GET['pageNum_Re_iv'];
}
$startRow_Re_iv = (($pageNum_Re_iv-1) * $maxRows_Re_iv);
if($startRow_Re_iv<0){$startRow_Re_iv=0;}

$query_Re_iv = "SELECT * FROM tb_investor WHERE iv_type='$iv_type' ORDER BY iv_id DESC ";
$query_limit_Re_iv = sprintf("%s LIMIT %d, %d", $query_Re_iv, $startRow_Re_iv, $maxRows_Re_iv);
$Re_iv=$mysqli->query($query_limit_Re_iv);

if (isset($_GET['totalRows_Re_iv'])) {
  $totalRows_Re_iv = $_GET['totalRows_Re_iv'];
} else {
  $all_Re_iv=$mysqli->query($query_Re_iv);
  $totalRows_Re_iv=$all_Re_iv->num_rows;
}
$totalPages_Re_iv = ceil($totalRows_Re_iv/$maxRows_Re_iv);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_iv==0){$page=1;}
else if($pageNum_Re_iv>0){$page=$pageNum_Re_iv;}

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_investor_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_investor_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo" รายการ".$title;?></div>

                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_iv=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_iv=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_iv>0){
                                echo "Page : ".$page."/".$totalPages_Re_iv." Total : ".number_format($totalRows_Re_iv)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                        

                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_iv=%d%s", $currentPage, $totalPages_Re_iv, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_iv){printf("%s?pageNum_Re_iv=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" align="center">
                    <tr>
                        <th width="35">ลำดับ</th>
                        <th>หัวข้อ</th>
                        <th width="150">ไฟล์ PDF</th>
                        <th width="35">แก้ไข</th>
                        <th width="35">ลบ</th>
                    </tr>
                    <?php
                    if($totalRows_Re_iv>0){
						$number=$startRow_Re_iv; 
                        while($row_Re_iv=$Re_iv->fetch_assoc()){
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $number+=1; ?></div></td>
                        <td>
                            <?php echo $row_Re_iv['iv_name']; ?>
                            <?php if($row_Re_iv['en_iv_name']!=""){echo "<br><br>".$row_Re_iv['en_iv_name'];} ?>
                        </td>
                        <td>
                            <div align="center">
								<?php if($row_Re_iv['iv_file']!=""){?>
								<i class="fas fa-file-pdf tx_red fa-lg"></i> <a href="../file/investor/<?php echo $row_Re_iv['iv_file']; ?>"  target="_blank"><?php echo $row_Re_iv['iv_file']; ?></a>
								<?php }else{?>
								<i class="fas fa-file-pdf tx_none"></i>
								<?php } ?>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_investor_edit.php?type=<?php echo $row_Re_iv['iv_type']; ?>&iv_id=<?php echo $row_Re_iv['iv_id']; ?>"><img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_investor_save.php?action=dele&type=<?php echo $row_Re_iv['iv_type']; ?>&iv_id=<?php echo $row_Re_iv['iv_id']; ?>" onclick = "return confirm('คุณต้องการลบรูปภาพนี้หรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a>
                            </div>
                        </td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="6">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
   
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>