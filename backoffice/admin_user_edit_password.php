<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

if (isset($_GET['admin_id'])) {
	$admin_id_chk = mysqli_real_escape_string($mysqli, $_GET['admin_id']);
}
$sql_admin="SELECT * FROM tb_admin WHERE admin_id='".$admin_id_chk."'";
$Re_admin=$mysqli->query($sql_admin);
$row_Re_admin=$Re_admin->fetch_assoc();
$totalRows_Re_admin=$Re_admin->num_rows;
?>
<!doctype html>
<html>
<head>
	<?php include 's_inc_header.php';?>
</head>

<body>
	<div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_user_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

	<div id="containner"> 
		<div id="main">
			<div id="main_content">
				<div class="main_content_title"><i class="fa fa-pencil fa-lg" style="color:#3097FF"></i><?php echo" แก้ไข".$title;?></div>
				<form action="admin_user_save.php?action=password&admin_id=<?php echo $row_Re_admin['admin_id']; ?>" method="post" enctype="multipart/form-data" name="form_ad" id="form_ad">
					<table width="100%" border="0" align="center">
						<tr>
							<td width="153" height="20" scope="col"><div align="left">ชื่อ-นามสกุล</div></td>
							<td scope="col"><?php echo $row_Re_admin['admin_name']; ?></td>
						</tr>
						<tr>
							<td scope="col">&nbsp;</td>
							<td scope="col">&nbsp;</td>
						</tr>
						<tr>
							<td scope="col"><div align="left">Password เก่า</div></td>
							<td scope="col"><input type="password" name="o_admin_pass" id="o_admin_pass" onkeyup="check_edit_password(form1.o_admin_pass.value)"/>
								<span id="msg"></span><span id="msg2"></span>
								<input name="h_admin_pass" type="hidden" id="h_admin_pass" value="<?php echo $row_Re_admin['admin_pass']; ?>" /></td>
						</tr>
						<tr>
							<td scope="col"><div align="left">Password ใหม่</div></td>
							<td scope="col"><input name="n_admin_pass" type="password" id="n_admin_pass"/></td>
						</tr>
						<tr>
							<td scope="col"><div align="left">Confirm Password ใหม่</div></td>
							<td scope="col"><input name="c_admin_pass_edit" type="password" id="c_admin_pass_edit"/></td>
						</tr>
						<tr>
							<td colspan="2"><hr />
								<p>
									<input name="Submit" type="submit" id="Submit" value="แก้ไขรหัสผ่าน" disabled="disabled" />
									<input name="admin_idid" type="hidden" id="admin_id" value="<?php echo $row_Re_admin['admin_id']; ?>" />
								</p></td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</body>
</html>