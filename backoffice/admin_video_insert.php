<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="insert";
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_video_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_video_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;เพิ่ม".$title;?></div>
                <div class="box_form">
                    <form action="admin_video_save.php?action=insert" method="POST" enctype="multipart/form-data" name="form_v" id="form_v" >
                        <table width="100%" border="0" align="center">
                            <tr>
                                <td width="150px" valign="top">เลือกสถานะ</td>
                                <td width="" valign="top">
                                    <select name="v_status" id="v_status">
                                        <option value="">เลือกสถานะ</option>
                                        <option value="1">แสดง</option>
                                        <option value="0">ไม่แสดง</option>
                                    </select>
                                </td>
							</tr>
                            <tr>
                                <td valign="top">ชื่อหัวข้อ</td>
                                <td valign="top"><input type="text" name="v_name" id="v_name" style="width: 400px;" ></td>
                            </tr>
                            <tr>
                                <td valign="top">Youtube</td>
                                <td valign="top"><input type="text" name="v_url" id="v_url" style="width: 400px;" ></td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr /></td>
                            </tr>
                            <tr>
                                <td colspan="2" ><input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>