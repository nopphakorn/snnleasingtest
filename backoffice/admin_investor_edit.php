<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

$iv_id=mysqli_real_escape_string($mysqli, $_GET['iv_id']);
$iv_type=mysqli_real_escape_string($mysqli, $_GET['type']);

$query_Re_iv = "SELECT * FROM tb_investor WHERE iv_type='$iv_type' AND iv_id='$iv_id' ";
$Re_iv=$mysqli->query($query_Re_iv);
$row_Re_iv = mysqli_fetch_assoc($Re_iv);
$totalRows_Re_iv=$Re_iv->num_rows;

if (!$Re_iv) {printf("Error: %s\n", $mysqli->error);}

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_investor_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_investor_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo " แก้ไข ".$title;?></div>
                <div class="box_form">
                    <form action="admin_investor_save.php?action=edit" method="POST" enctype="multipart/form-data" name="form_iv" id="form_iv" >
                        <table width="100%">
							<tr>
                                <td width="120" valign="top">หัวข้อ</td>
                                <td><input type="text" id="iv_name" name="iv_name" style="width:600px" value="<?php echo $row_Re_iv['iv_name'];?>"></td>
                            </tr>
                            <tr>
                                <td width="120" valign="top">Name</td>
                                <td><input type="text" id="en_iv_name" name="en_iv_name" style="width:600px" value="<?php echo $row_Re_iv['en_iv_name'];?>"></td>
                            </tr>
                            <tr>
                                <td width="120" valign="top">เปลี่ยนไฟล์ PDF</td>
                                <td>
									<?php if($row_Re_iv['iv_file']!=""){?>
									<p>
									<i class="fas fa-file-pdf tx_red fa-lg"></i> <a href="../file/investor/<?php echo $row_Re_iv['iv_file']; ?>"  target="_blank"><?php echo $row_Re_iv['iv_file']; ?></a><br>
									</p>
									<?php } ?>
                                    <input type="file" name="iv_file_edit" id="iv_file_edit" />
                                    เฉพาะไฟล์ PDF เท่านั้น
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr>
								<input type="hidden" id="iv_id" name="iv_id" value="<?php echo $row_Re_iv['iv_id'];?>">
								<input type="hidden" id="iv_type" name="iv_type" value="<?php echo $row_Re_iv['iv_type'];?>">
								<input type="hidden" id="h_iv_file" name="h_iv_file" value="<?php echo $row_Re_iv['iv_file'];?>">
								<input name="button" type="submit" id="button" value="บันทึกแก้ไข" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>