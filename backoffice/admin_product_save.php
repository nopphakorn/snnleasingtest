<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST["pro_id"])){$pro_id=mysqli_real_escape_string($mysqli, $_POST["pro_id"]);}
if(isset($_POST["pro_status"])){$pro_status=mysqli_real_escape_string($mysqli, $_POST["pro_status"]);}
if(isset($_POST["pro_type"])){$pro_type=mysqli_real_escape_string($mysqli, $_POST["pro_type"]);}
if(isset($_POST["pro_brand"])){$pro_brand=mysqli_real_escape_string($mysqli, $_POST["pro_brand"]);}
if(isset($_POST["pro_model"])){$pro_model=mysqli_real_escape_string($mysqli, $_POST["pro_model"]);}
if(isset($_POST["pro_model_type"])){$pro_model_type=mysqli_real_escape_string($mysqli, $_POST["pro_model_type"]);}
if(isset($_POST["pro_model_detail"])){$pro_model_detail=mysqli_real_escape_string($mysqli, $_POST["pro_model_detail"]);}
if(isset($_POST["pro_year"])){$pro_year=mysqli_real_escape_string($mysqli, $_POST["pro_year"]);}
if(isset($_POST["pro_cc"])){$pro_cc=mysqli_real_escape_string($mysqli, $_POST["pro_cc"]);}
if(isset($_POST["pro_gear"])){$pro_gear=mysqli_real_escape_string($mysqli, $_POST["pro_gear"]);}
if(isset($_POST["pro_mile"])){$pro_mile=mysqli_real_escape_string($mysqli, $_POST["pro_mile"]);}
if(isset($_POST["pro_color"])){$pro_color=mysqli_real_escape_string($mysqli, $_POST["pro_color"]);}
if(isset($_POST["pro_detail"])){$pro_detail=mysqli_real_escape_string($mysqli, $_POST["pro_detail"]);}
if(isset($_POST["pro_price"])){$pro_price=mysqli_real_escape_string($mysqli, $_POST["pro_price"]);}

if($_GET['action']=="insert"){
	$pro_date=date("Y-m-d H:i:s");
	$sql="insert into tb_product (pro_status, pro_type, pro_brand, pro_model, pro_model_type, pro_model_detail, pro_year, pro_cc, pro_gear, pro_mile, pro_color, pro_detail, pro_price, pro_date) 
	values('$pro_status', '$pro_type', '$pro_brand', '$pro_model', '$pro_model_type', '$pro_model_detail', '$pro_year', '$pro_cc', '$pro_gear', '$pro_mile', '$pro_color', '$pro_detail', '$pro_price' , '$pro_date') ";
	$Re_insert=$mysqli->query($sql);
	$id_update = $mysqli->insert_id;
	
	for($i=0;$i<count($_FILES["pro_p_photo"]["name"]);$i++){
		if($_FILES["pro_p_photo"]["name"][$i] != ""){
			$photo_name = $_FILES["pro_p_photo"]["name"][$i];
			$pro_p_photo_name = $_FILES["pro_p_photo"]["tmp_name"][$i];
	
			$newfile = "PD".date('His').rand(1000,9999); 
			$path = "../images/product/"; 
										
			$images_photo = $pro_p_photo_name;
			$size=GetimageSize($images_photo);
			$size_w=$size[0];
			$size_h=$size[1];
			if($size_w>800){
				$width=800;					
				$height=round(($width/$size_w) * $size_h);
				
				if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
					$images_photo_orig = ImageCreateFromJPEG($images_photo);
				}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
					$images_photo_orig = ImageCreateFromJPEG($images_photo);
				}
			
				$images_photo_new = ImageCreateTrueColor($width, $height); 
				ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
			
				if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
					ImageJPEG($images_photo_new,$images_photo,100); 
				}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
					ImageJPEG($images_photo_new,$images_photo,100); 
				}			
				ImageDestroy($images_photo_orig);
				ImageDestroy($images_photo_new); 					
			}			
			$RenameFile=$newfile.strchr($photo_name,".");
			if (copy( $images_photo , "$path/$RenameFile" )){		
				unlink($images_photo);
				$pathPic="$RenameFile";	
			}

			$sql2="INSERT INTO tb_product_photo (pro_p_photo, pro_id) VALUES ('$pathPic','$id_update')";
			$Re_sql2=$mysqli->query($sql2);
		}
	}

	$GoTo = "admin_product_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="edit"){
	$pro_date=date("Y-m-d H:i:s");
	$sql="UPDATE tb_product SET pro_status='$pro_status', pro_type='$pro_type', pro_brand='$pro_brand', pro_model='$pro_model', pro_model_type='$pro_model_type', pro_model_detail='$pro_model_detail', pro_year='$pro_year', pro_cc='$pro_cc', pro_gear='$pro_gear', pro_mile='$pro_mile', pro_color='$pro_color', pro_detail='$pro_detail', pro_price='$pro_price', pro_date='$pro_date' WHERE pro_id='$pro_id'";
	$Re_update=$mysqli->query($sql);
	$id_update=$pro_id;
	
	for($i=0;$i<count($_FILES["pro_p_photo"]["name"]);$i++){
		if($_FILES["pro_p_photo"]["name"][$i] != ""){
			$photo_name = $_FILES["pro_p_photo"]["name"][$i];
			$pro_p_photo_name = $_FILES["pro_p_photo"]["tmp_name"][$i];
	
			$newfile = "PD".date('His').rand(1000,9999); 
			$path = "../images/product/"; 
										
			$images_photo = $pro_p_photo_name;
			$size=GetimageSize($images_photo);
			$size_w=$size[0];
			$size_h=$size[1];
			if($size_w>800){
				$width=800;					
				$height=round(($width/$size_w) * $size_h);
				
				if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
					$images_photo_orig = ImageCreateFromJPEG($images_photo);
				}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
					$images_photo_orig = ImageCreateFromJPEG($images_photo);
				}
			
				$images_photo_new = ImageCreateTrueColor($width, $height); 
				ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
			
				if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
					ImageJPEG($images_photo_new,$images_photo,100); 
				}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
					ImageJPEG($images_photo_new,$images_photo,100); 
				}			
				ImageDestroy($images_photo_orig);
				ImageDestroy($images_photo_new); 					
			}			
			$RenameFile=$newfile.strchr($photo_name,".");
			if (copy( $images_photo , "$path/$RenameFile" )){		
				unlink($images_photo);
				$pathPic="$RenameFile";	
			}

			$sql2="INSERT INTO tb_product_photo (pro_p_photo, pro_id) VALUES ('$pathPic','$id_update')";
			$Re_sql2=$mysqli->query($sql2);
		}
	}

	$GoTo = "admin_product_edit.php?pro_id=$pro_id";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="dele"){
	if ((isset($_GET['pro_id'])) && ($_GET['pro_id'] != "")) {
	
		$pro_id_chk=$_GET['pro_id'];
		$sql_chk = "SELECT * FROM tb_product WHERE pro_id = '$pro_id_chk'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
		
		$file=$row_Re_chk['pro_photo'];
		if($file!=""){
			if (file_exists ("../images/product/$file")){
				unlink("../images/product/$file");
			}
		}
		$sql ="DELETE FROM tb_product WHERE pro_id='$pro_id'";
		$Re_dele=$mysqli->query($sql);

		$GoTo = "admin_product_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}

if($_GET['action']=="dele_p"){
	$pro_p_id=$_GET['pro_p_id'];
	$sql = "SELECT * FROM tb_product_photo WHERE pro_p_id='$pro_p_id'";
	$Re_chk=$mysqli->query($sql);
	$row_Re_chk=$Re_chk->fetch_assoc();
	
	$file=$row_Re_chk['pro_p_photo'];
	if($file!=""){
		if (file_exists ("../images/product/$file")){
			unlink("../images/product/$file");
		}
	}
	$sql_dele ="DELETE FROM tb_product_photo WHERE pro_p_id='$pro_p_id'";
	$Re_dele=$mysqli->query($sql_dele);
	
	$GoTo = "admin_product_edit.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}
?>