<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

$i_id=mysqli_real_escape_string($mysqli, $_GET['i_id']);

$query_Re_i = "SELECT * FROM tb_interest WHERE i_id='$i_id' ";
$Re_i=$mysqli->query($query_Re_i);
$row_Re_i = mysqli_fetch_assoc($Re_i);
$totalRows_Re_i=$Re_i->num_rows;

if (!$Re_i) {printf("Error: %s\n", $mysqli->error);}

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
    <link rel="stylesheet" href="tools/datepicker/jquery-ui.css">
	<script src="tools/datepicker/jquery-ui.js"></script>
	<script src="tools/datepicker/jqueryui_datepicker_thai.js"></script>
    <script type="text/javascript">
		$( function () {
			$( "#i_date" ).datepicker( {
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true,
		        buttonImage: "images/icon/calendar.gif",
			} );
		} );
	</script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_interest_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_interest_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo " แก้ไข ".$title;?></div>
                <div class="box_form">
                    <form action="admin_interest_save.php?action=edit" method="POST" enctype="multipart/form-data" name="form_ir" id="form_ir" >
                    <table width="100%">
                            <tr>
                                <td width="160" valign="top">ประเภทไฟล์</td>
                                <td>
                                    <select name="i_type" id="i_type">
                                        <option value="">เลือก</option>
                                        <option value="PDF" <?php if($row_Re_i['i_type']=="PDF"){ echo "selected=\"selected\""; }?>>PDF</option>
                                        <option value="JPG" <?php if($row_Re_i['i_type']=="JPG"){ echo "selected=\"selected\""; }?>>รูปภาพ</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="120" valign="top">วันที่ประกาศและบังคับใช้</td>
                                <td><input type="text" id="i_date" name="i_date" readonly value="<?php echo dateTha($row_Re_i['i_date']);?>"></td>
                            </tr>
                            <tr><td colspan="2">&nbsp;</tr>
							<tr>
                                <td width="120" valign="top">หัวข้อ</td>
                                <td><input type="text" id="i_name" name="i_name" style="width:600px" value="<?php echo $row_Re_i['i_name'];?>"></td>
                            </tr>
                            <tr>
                                <td width="120" valign="top">Name</td>
                                <td><input type="text" id="e_i_name" name="e_i_name" style="width:600px" value="<?php echo $row_Re_i['e_i_name'];?>"></td>
                            </tr>
                            <tr><td colspan="2">&nbsp;</tr>
                            <tr>
                                <td width="120" valign="top">คำแนะนำ</td>
                                <td><input type="text" id="i_title" name="i_title" style="width:600px" value="<?php echo $row_Re_i['i_title'];?>"></td>
                            </tr>
                            <tr>
                                <td width="120" valign="top">Title</td>
                                <td><input type="text" id="e_i_title" name="e_i_title" style="width:600px" value="<?php echo $row_Re_i['e_i_title'];?>"></td>
                            </tr>
                            <tr><td colspan="2">&nbsp;</tr>
                            <tr>
                                <td width="120">ไฟล์แนบ</td>
                                <td>
                                    <?php 
                                     if($row_Re_i['i_file']!=""){
                                        echo "<br>";
                                        if(strchr($row_Re_i['i_file'],".")==".pdf"){
                                            echo "<i class=\"far fa-file-pdf\"></i>";
                                        }else if(strchr($row_Re_i['i_file'],".")==".jpg"){
                                            echo "<i class=\"far fa-file-image\"></i>";
                                        }
                                    ?>
									
									<a href="../images/interest/<?php echo $row_Re_i['i_file']; ?>"  target="_blank"><?php echo $row_Re_i['i_file']; ?></a><br>
                                    <br>
									<?php } ?>
                                    <input type="file" name="i_file_edit" id="i_file_edit" />
                                    <input type="hidden" name="h_i_file" id="h_i_file" value="<?php echo $row_Re_i['i_file']; ?>"/>
                                    เฉพาะไฟล์ PDF หรือ JPG ขนาดกว่างไม่เกิน 900px เท่านั้น
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr>
                                <input type="hidden" id="i_id" name="i_id" value="<?php echo $row_Re_i['i_id'];?>">
								<input name="button" type="submit" id="button" value="บันทึกแก้ไข" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>