<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="insert";
?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_search_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_search_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo " เพิ่ม ".$title;?></div>
                <div class="box_form">
                    <form action="admin_search_save.php?action=insert" method="POST" enctype="multipart/form-data" name="from_searchWeb" id="from_searchWeb" >
                        <table width="100%">
							<tr>
                                <td width="120" valign="top">หัวข้อ</td>
                                <td><input type="text" id="s_name" name="s_name" style="width:600px"></td>
                            </tr>
                            <tr>
                                <td width="120" valign="top">URL</td>
                                <td><input type="text" id="s_url" name="s_url" style="width:600px"></td>
							</tr>
							<tr>
                                <td width="120" valign="top">คำค้นหา</td>
                                <td><textarea name="s_tag" id="s_tag" style="width:600px; height:100px;"></textarea></td>
							</tr>
                            <tr>
                                <td colspan="2"><hr>
								<input name="button" type="submit" id="button" value="บันทึกรายการ" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>