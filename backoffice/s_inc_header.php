<meta charset="utf-8">
<title>Backoffice</title>
<script type="text/javascript">window.FontAwesomeConfig = { autoReplaceSvg: false }</script>
<link href="tools/fontawesome-free-5.0.6/web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/x-icon" href="../images/logo/favicon.ico">

<!--<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>-->

<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<!--<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>-->

<script type="text/javascript" src="js/check.js"></script>
<script type="text/javascript" src="tools/validation/js/jquery.validate.js"></script>
<script type="text/javascript" src="tools/validation/js/additional-methods.js"></script>
<script type="text/javascript" src="tools/validation/js/jquery.start.js"></script>
<script type="text/javascript" src="tools/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="tools/fancybox/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" type="text/css" href="tools/fancybox/jquery.fancybox.css" media="screen" />
<link href="tools/kindeditor/themes/default/default.css" rel="stylesheet" />
<script src="tools/kindeditor/kindeditor-all-min.js"></script>
<script  type="text/javascript" src="tools/nicedit/nicEdit.js"></script>