<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$maxRows_Re_a = 50;
$pageNum_Re_a = 0;
if (isset($_GET['pageNum_Re_a'])) {
  $pageNum_Re_a = $_GET['pageNum_Re_a'];
}
$startRow_Re_a = (($pageNum_Re_a-1) * $maxRows_Re_a);
if($startRow_Re_a<0){$startRow_Re_a=0;}

$query_Re_a = "SELECT * FROM tb_tff_article ORDER BY a_id DESC ";
$query_limit_Re_a = sprintf("%s LIMIT %d, %d", $query_Re_a, $startRow_Re_a, $maxRows_Re_a);
$Re_a=$mysqli->query($query_limit_Re_a);

if (isset($_GET['totalRows_Re_a'])) {
  $totalRows_Re_a = $_GET['totalRows_Re_a'];
} else {
  $all_Re_a=$mysqli->query($query_Re_a);
  $totalRows_Re_a=$all_Re_a->num_rows;
}
$totalPages_Re_a = ceil($totalRows_Re_a/$maxRows_Re_a);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_a==0){$page=1;}
else if($pageNum_Re_a>0){$page=$pageNum_Re_a;}

?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_tff_article_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_tff_article_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo" รายการ".$title; ?></div>

               <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_a=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_a=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_a>0){
                                echo "Page : ".$page."/".$totalPages_Re_a." Total : ".number_format($totalRows_Re_a)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_a=%d%s", $currentPage, $totalPages_Re_a, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_a){printf("%s?pageNum_Re_a=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" border="1" align="center" cellpadding="5">
                    <tr>
                        <th width="45"><div align="center">No.</div></th>
                        <th width="200"><div align="center">ภาพปก</div></th>
                        <th width=""><div align="center">ชื่อบทความ</div></th>
                        <th width="80">ผู้เข้าชม</th>
                        <th width="40">สถานะ</th>
                        <th width="40"><div align="center">แก้ไข</div></th>
                        <th width="40"><div align="center">ลบ</div></th>
                    </tr>
                    <?php 
                    if($totalRows_Re_a>0){
                        $number=$startRow_Re_a;
                        while($row_Re_a=$Re_a->fetch_assoc()){
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $number+=1; ?></div></td>
                        <td>
                            <div align="center">
                                <div class="img_a_list"><img src="../images/tff_article/<?php echo $row_Re_a['a_photo']; ?>"/></div>
                            </div>
                        </td>
                        <td>
                            <div align="left">
                                โพสต์ <?php echo datethai2($row_Re_a['a_date']); ?><br>
                                <?php echo $row_Re_a['a_name']; ?><br>
                            </div>
                        </td>
                        <td>
                            <div align="center">
								<?php 
								if($row_Re_a['a_counter']!=0){ echo $row_Re_a['a_counter']; }else{ echo "<font color='#FF000'>ไม่มีผุ้เข้าชม</font>";} ?>
                            </div>
                        </td>
                        <td><div align="center">
                                <?php if($row_Re_a['a_status']=="1"){ ?>
                                <a href="admin_tff_article_save.php?a_id=<?php echo $row_Re_a['a_id']; ?>&action=status&a_status=0"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
                                <?php }else{ ?>
                                <a href="admin_tff_article_save.php?a_id=<?php echo $row_Re_a['a_id']; ?>&action=status&a_status=1"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
                                <?php } ?>
                            </div></td>
                        <td><div align="center">
                                <div align="center"> <a href="admin_tff_article_edit.php?a_id=<?php echo $row_Re_a['a_id']; ?>"><img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a></div>
                            </div></td>
                        <td width="19" scope="col"><div align="center">
                                <div align="center"> <a href="admin_tff_article_save.php?action=dele&amp;a_id=<?php echo $row_Re_a['a_id']; ?>" onclick = "return confirm('คุณต้องการลบบทความหรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a></div>
                            </div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="7">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>   
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>

