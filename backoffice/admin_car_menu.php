<span class="btn"><a href="admin.php"><i class="fa fa-home fa-lg" style="color:#3097FF"></i> หน้าหลักผู้แลระบบ</a></span>
<span class="btn">
	<a href="admin_car_type.php">
	<i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i> ประเภทรถ</a>
</span>
<span class="btn">
	<a href="admin_car_brand.php">
	<i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i> ยี่ห้อรถ</a>
</span>
<span class="btn">
	<a href="admin_car_group.php">
	<i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i> กลุ่มรุ่นรถ</a>
</span>
<span class="btn">
	<a href="admin_car_model.php">
	<i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i> รุ่นรถ</a>
</span>
<span class="btn">
	<a href="<?php echo $filelink."_all.php"; ?>">
	<i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i> รายการ<?php echo $title?></a>
</span>
<span class="btn">
	<a href="admin_car_all_edit_group.php">
	<i class="fa fa-edit fa-lg" style="color:#3097FF"></i> แก้ไขราคาแบบกลุ่ม</a>
</span>