<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST['admin_id'])){$admin_id=mysqli_real_escape_string($mysqli, $_POST['admin_id']);}

if(isset($_POST['admin_name'])){$admin_name=mysqli_real_escape_string($mysqli, $_POST["admin_name"]);}
if(isset($_POST['admin_user'])){$admin_user=mysqli_real_escape_string($mysqli, $_POST["admin_user"]);}
if(isset($_POST['admin_pass'])){$admin_pass=mysqli_real_escape_string($mysqli, $_POST["admin_pass"]);}
if(isset($_POST['admin_status'])){$admin_status=mysqli_real_escape_string($mysqli, $_POST['admin_status']);}
if(isset($_POST['n_admin_pass'])){$n_admin_pass=mysqli_real_escape_string($mysqli, $_POST["n_admin_pass"]);}
if(isset($_POST['h_admin_pass'])){$h_admin_pass=mysqli_real_escape_string($mysqli, $_POST["h_admin_pass"]);}
if($_GET['action']=="status"){
	$admin_id = mysqli_real_escape_string($mysqli, $_GET['admin_id']);
	$admin_status = mysqli_real_escape_string($mysqli, $_GET['admin_status']);

	$sql="update tb_admin set admin_status=$admin_status where admin_id='$admin_id'";
	$Re_st=$mysqli->query($sql);
	
	$GoTo = "admin_user_list.php";
	  if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	  }
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="insert"){
	$admin_pass_md5=md5($admin_pass);
	$sql="INSERT INTO tb_admin(admin_name,admin_user,admin_pass,admin_status)
	values('$admin_name','$admin_user','$admin_pass_md5','$admin_status')";
	$Re_insert=$mysqli->query($sql);

	if($Re_insert){
		echo "<script type='text/javascript'>window.location='admin_user_list.php';</script>";
	}else{
		echo "<script type='text/javascript'>window.location='admin_user_insert.php';</script>";
	}
}

if($_GET['action']=="edit"){
	if($n_admin_pass==""){$admin_pass_md5=$h_admin_pass;}
	else{$admin_pass_md5=md5($n_admin_pass);}
		
	$sql= "UPDATE tb_admin SET admin_name='$admin_name', admin_user='$admin_user', admin_pass='$admin_pass_md5', admin_status='$admin_status' WHERE admin_id='$admin_id'";	
	$Re_ed=$mysqli->query($sql);

	$GoTo = "admin_user_edit.php?admin_id=".$admin_id;
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
		}
	echo "<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="dele"){
	$admin_id=mysqli_real_escape_string($mysqli, $_GET["admin_id"]);
	$admin_user=mysqli_real_escape_string($mysqli, $_GET["admin_user"]);
	if($admin_user == "admin"){
		echo "<script type='text/javascript'>alert('ท่านไม่สามารถลบ Username :".$admin_user." ได้');</script>";
	}else{
		$sql="DELETE FROM tb_admin WHERE admin_id='$admin_id'";
		$Re_dele=$mysqli->query($sql);
	}
	echo "<script type='text/javascript'>window.location='admin_user_list.php';</script>";
}

if($_GET['action']=="password"){
	if($n_admin_pass==""){$admin_pass_md5=$h_admin_pass;}
	else{$admin_pass_md5=md5($n_admin_pass);}
	
	$sql= "UPDATE tb_admin SET admin_pass='$admin_pass_md5' WHERE admin_id='$admin_id'";
	$Re_edit=$mysqli->query($sql);

	if($Re_edit){
		echo "<script type='text/javascript'>window.location='admin_user_edit_password_succeed.php';</script>";
	}else{
		echo "<script type='text/javascript'>window.location='admin_user_edit_password.php';</script>";
	}
}
$mysqli->close();
?>

