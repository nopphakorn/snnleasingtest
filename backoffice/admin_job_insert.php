<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="insert";
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
    <script type="text/javascript">
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('#j_detail', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
            editor = K.create('#en_j_detail', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
        });
        KindEditor.ready(function(K) {
            editor = K.create('#j_property', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
            editor = K.create('#en_j_property', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
        });
        KindEditor.ready(function(K) {
            editor = K.create('#j_contact', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
            editor = K.create('#en_j_contact', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
        });
    </script>
</head>

<body>
<div id="header"> <?php include("s_header.php"); ?></div>
<div id="nav"><?php include("admin_job_nav.php"); ?></div>
<div id="side"><?php include('s_menu_side.php'); ?></div>
<div id="containner">
    <div id="main">
      <?php include("admin_job_menu.php"); ?>
        <div id="main_content">
            <div class="main_content_title"><i class="fa fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;เพิ่มรายการ".$title;?></div>
            <div class="box_form">
                <form action="admin_job_save.php?action=insert" method="POST" enctype="multipart/form-data" name="form_job" id="form_job" >
                    <table width="100%" border="0" align="center">
                        <tr>
                            <td width="150px" valign="top" scope="col">เลือกสถานะ</td>
                            <td valign="top" scope="col"><select name="j_status" id="j_status">
                                <option value="1">แสดง</option>
                                <option value="0">ไม่แสดง</option>
                            </select></td>
                        </tr>
                        <tr>
                            <td width="150px" valign="top" scope="col">ความเร่งด่วน</td>
                            <td valign="top" scope="col"><select name="j_urgent" id="j_urgent">
                                <option value="ปกติ">ปกติ</option>
                                <option value="URGENT">URGENT</option>
                            </select></td>
                        </tr>
                        <tr>
                            <td valign="top" scope="col">JOB TITLE</td>
                            <td valign="top" scope="col"><input name="j_title" type="text" id="j_title" size="60"></td>
                        </tr>
                        <tr>
                            <td valign="top" scope="col">JOB FUNCTION</td>
                            <td valign="top" scope="col"><input name="j_function" type="text" id="j_function" size="60"></td>
                        </tr>
                        <tr>
                            <td valign="top" scope="col">สถานที่ทำงาน</td>
                            <td valign="top" scope="col"><input name="j_location" type="text" id="j_location" size="60"></td>
                        </tr>
                        <tr>
                            <td valign="top" scope="col">รายละเอียด</td>
                            <td valign="top" scope="col"><textarea name="j_detail" id="j_detail" style="height: 200px; width: 100%;" ></textarea></td>
                        </tr>
                        <tr>
                            <td valign="top" scope="col">คุณสมบัติ</td>
                            <td valign="top" scope="col"><textarea name="j_property" id="j_property" style="height: 200px; width: 100%;" ></textarea></td>
                        </tr>
                        <tr>
                            <td valign="top" scope="col">สอบถามเพิ่มเติม</td>
                            <td valign="top" scope="col"><textarea name="j_contact" id="j_contact" style="height: 100px; width: 100%;" ></textarea></td>
                        </tr>
                        <tr>
                            <td colspan="2"><hr /></td>
                        </tr>
                        <tr>
                            <td valign="top">JOB TITLE</td>
                            <td valign="top"><input name="en_j_title" type="text" id="en_j_title" size="60"></td>
                        </tr>
                        <tr>
                            <td valign="top">JOB FUNCTION</td>
                            <td valign="top"><input name="en_j_function" type="text" id="en_j_function" size="60"></td>
                        </tr>
                        <tr>
                            <td valign="top">Workplace</td>
                            <td valign="top"><input name="en_j_location" type="text" id="en_j_location" size="60"></td>
                        </tr>
                        <tr>
                            <td valign="top">Description</td>
                            <td valign="top"><textarea name="en_j_detail" id="en_j_detail" style="height: 200px; width: 100%;" ></textarea></td>
                        </tr>
                        <tr>
                            <td valign="top">Property</td>
                            <td valign="top"><textarea name="en_j_property" id="en_j_property" style="height: 200px; width: 100%;" ></textarea></td>
                        </tr>
                        <tr>
                            <td valign="top">Additional</td>
                            <td valign="top"><textarea name="en_j_contact" id="en_j_contact" style="height: 100px; width: 100%;" ></textarea></td>
                        </tr>
                        <tr>
                            <td colspan="2"><hr /></td>
                        </tr>
                        <tr>
                            <td colspan="2" ><input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/></td>
                        </tr>
                    </table>
                </form>
            </div>
            <!-- e-main_content --></div>
        <!-- e-main --></div>
    <!-- e-containner --></div>
    <div id="footer"><?php include("s_footer.php"); ?></div>
</body>
</html>
