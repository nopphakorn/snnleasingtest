<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST["pm_id"])){$pm_id=mysqli_real_escape_string($mysqli, $_POST["pm_id"]);}
if(isset($_POST["pm_status"])){$pm_status=mysqli_real_escape_string($mysqli, $_POST["pm_status"]);}
if(isset($_POST["pm_title"])){$pm_title=mysqli_real_escape_string($mysqli, $_POST["pm_title"]);}
if(isset($_POST["pm_detail"])){$pm_detail=mysqli_real_escape_string($mysqli, $_POST["pm_detail"]);}
if(isset($_POST["h_pm_photo"])){$h_pm_photo=mysqli_real_escape_string($mysqli, $_POST["h_pm_photo"]);}
	
if($action=="status"){
	$pm_id = mysqli_real_escape_string($mysqli, $_GET['pm_id']);
	$pm_status = mysqli_real_escape_string($mysqli, $_GET['pm_status']);
	
	$sql="update tb_promotion set pm_status=$pm_status where pm_id='$pm_id'";
	$Re_update=$mysqli->query($sql);
		
	$GoTo = "admin_promotion_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="insert"){
	$photo=$_FILES["pm_photo"];
	$photo=$_FILES['pm_photo']['tmp_name'];
	$photo_name=$_FILES['pm_photo']['name'];

	if($photo != "") {
		$new_name ="RV".date('His').rand(1000,9999); 
		$path = "../images/promotion"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>820){
			$width=820;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic="";}
	
	$sql="insert into tb_promotion(pm_title, pm_detail, pm_status, pm_photo)
	values('$pm_title','$pm_detail','$pm_status', '$pathPic')";
	$Re_insert=$mysqli->query($sql);
	
	$GoTo = "admin_promotion_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	//exit;
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="edit"){
	$photo=$_FILES["pm_photo_edit"];
	$photo=$_FILES['pm_photo_edit']['tmp_name'];
	$photo_name=$_FILES['pm_photo_edit']['name'];

	if($photo != "") {
		$sql_chk = "SELECT * FROM tb_promotion WHERE pm_id='$pm_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['pm_photo'];
		if($file!=""){
			if (file_exists ("../images/promotion/$file")){unlink("../images/promotion/$file");}
		}

		$new_name ="RV".date('His').rand(1000,9999); 
		$path = "../images/promotion"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>820){
			$width=820;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic=$h_pm_photo;}
	
	$sql="UPDATE tb_promotion SET pm_title='$pm_title', pm_detail='$pm_detail', pm_status='$pm_status', pm_photo='$pathPic' WHERE pm_id='$pm_id'";
	$Re_update=$mysqli->query($sql);
		
	$GoTo = "admin_promotion_edit.php?pm_id=$pm_id";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="dele"){
	if ((isset($_GET['pm_id'])) && ($_GET['pm_id'] != "")) {
	
		$pm_id_chk=$_GET['pm_id'];
		$sql_chk = "SELECT * FROM tb_promotion WHERE pm_id = '$pm_id_chk'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
		
		$file=$row_Re_chk['pm_photo'];
		if($file!=""){
			if (file_exists ("../images/promotion/$file")){
				unlink("../images/promotion/$file");
			}
		}
		$sql ="DELETE FROM tb_promotion WHERE pm_id='$pm_id_chk'";
		$Re_dele=$mysqli->query($sql);

		$GoTo = "admin_promotion_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}
?>