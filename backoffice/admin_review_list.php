<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$maxRows_Re_rv = 50;
$pageNum_Re_rv = 0;
if (isset($_GET['pageNum_Re_rv'])) {
  $pageNum_Re_rv = $_GET['pageNum_Re_rv'];
}
$startRow_Re_rv = (($pageNum_Re_rv-1) * $maxRows_Re_rv);
if($startRow_Re_rv<0){$startRow_Re_rv=0;}

$query_Re_rv = "SELECT * FROM tb_review ORDER BY rv_no ASC ";
$query_limit_Re_rv = sprintf("%s LIMIT %d, %d", $query_Re_rv, $startRow_Re_rv, $maxRows_Re_rv);
$Re_rv=$mysqli->query($query_limit_Re_rv);

if (isset($_GET['totalRows_Re_rv'])) {
  $totalRows_Re_rv = $_GET['totalRows_Re_rv'];
} else {
  $all_Re_rv=$mysqli->query($query_Re_rv);
  $totalRows_Re_rv=$all_Re_rv->num_rows;
}
$totalPages_Re_rv = ceil($totalRows_Re_rv/$maxRows_Re_rv);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_rv==0){$page=1;}
else if($pageNum_Re_rv>0){$page=$pageNum_Re_rv;}

$sql_mn = "SELECT max(rv_no) AS rv_max, min(rv_no) AS sv_min FROM tb_review ORDER BY rv_no ASC";
$Re_mn=$mysqli->query($sql_mn);
$row_Re_mn=$Re_mn->fetch_assoc();
$totalRows_Re_mn=$Re_mn->num_rows;
?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_review_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_review_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;".$title;?></div>

                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_rv=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_rv=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_rv>0){
                                echo "Page : ".$page."/".$totalPages_Re_rv." Total : ".number_format($totalRows_Re_rv)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                        

                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_rv=%d%s", $currentPage, $totalPages_Re_rv, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_rv){printf("%s?pageNum_Re_rv=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" border="1" align="center">
                    <tr>
						<th width="45">No.</th>
						<th width="60">ตำแหน่ง</th>
                        <th width="">รูปภาพ</th>
                        <th width="40"><div align="center">สถานะ</div></th>
                        <th width="40"><div align="center">แก้ไข</div></th>
                        <th width="40"><div align="center">ลบ</div></th>
                    </tr>
                    <?php 
                    if($totalRows_Re_rv>0){
                        $number=$startRow_Re_rv;
                        while($row_Re_rv=$Re_rv->fetch_assoc()){
                    ?>
                    <tr>
						<td><div align="center"><?php echo $row_Re_rv['rv_no']; ?></div></td>
						<td>
                            <div align="center">
                                <?php 
                                $list=$row_Re_rv['rv_no'];
                                $id=$row_Re_rv['rv_id'];
                                if($totalRows_Re_rv>=2){
                                ?>
                                <?php 
                                if($list==1){ 
                                ?>
                                    <div align="center">
                                        <a href="admin_review_save.php?action=no&amp;down=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_down.png" alt="down" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php 
                                } if($list!=1 and $list!=$row_Re_mn['rv_max']){
                                ?>
                                    <div align="center">
                                        <a href="admin_review_save.php?action=no&amp;down=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_down.png" alt="down" width="16" height="16" border="0" /></a>
                                        
                                        <a href="admin_review_save.php?action=no&amp;up=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_up.png" alt="up" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php 
                                } if($list==$row_Re_mn['rv_max'] AND $list>0){ 
                                ?>
                                    <div align="center">
                                        <a href="admin_review_save.php?action=no&amp;up=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_up.png" alt="up" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php } } ?>
                            </div>
                        </td>
                        <td valign="top">
                                <div class="img_rv_list">
                                    <img src="../images/review/<?php echo $row_Re_rv['rv_photo']; ?>" />
                                </div>
                        </td>
                        <td valign="top"><div align="center">
                                <?php if($row_Re_rv['rv_status']=="1"){ ?>
                                <a href="admin_review_save.php?rv_id=<?php echo $row_Re_rv['rv_id']; ?>&action=status&rv_status=0"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
                                <?php }else{ ?>
                                <a href="admin_review_save.php?rv_id=<?php echo $row_Re_rv['rv_id']; ?>&action=status&rv_status=1"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
                                <?php } ?>
                            </div></td>
                        <td valign="top"><div align="center">
                                <div align="center"> <a href="admin_review_edit.php?action=edit&rv_id=<?php echo $row_Re_rv['rv_id']; ?>"><img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a></div>
                            </div></td>
                        <td valign="top"><div align="center">
                                <div align="center"> <a href="admin_review_save.php?action=dele&rv_id=<?php echo $row_Re_rv['rv_id']; ?>" onclick = "return confirm('คุณต้องการลบ <?php echo $row_Re_rv['rv_name']; ?> หรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a></div>
                            </div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="8">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
           </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>