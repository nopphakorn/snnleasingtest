<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST["a_id"])){$a_id=mysqli_real_escape_string($mysqli, $_POST["a_id"]);}
if(isset($_POST["a_name"])){$a_name=mysqli_real_escape_string($mysqli, $_POST["a_name"]);}
if(isset($_POST["a_title"])){$a_title=mysqli_real_escape_string($mysqli, $_POST["a_title"]);}
if(isset($_POST["a_detail"])){$a_detail=mysqli_real_escape_string($mysqli, $_POST["a_detail"]);}
if(isset($_POST["a_date"])){$a_date=mysqli_real_escape_string($mysqli, $_POST["a_date"]);}
if(isset($_POST["a_status"])){$a_status=mysqli_real_escape_string($mysqli, $_POST["a_status"]);}
if(isset($_POST["h_a_photo"])){$h_a_photo=mysqli_real_escape_string($mysqli, $_POST["h_a_photo"]);}
	
if($action=="status"){
	$a_id = $_GET['a_id'];
	$a_status = $_GET['a_status'];
	
	$sql="UPDATE tb_tff_article SET a_status=$a_status WHERE a_id='$a_id'";
	$Re_sql=$mysqli->query($sql);
		
	$GoTo = "admin_tff_article_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="insert"){
	echo "insert";
	$photo=$_FILES["a_photo"];
	$photo=$_FILES['a_photo']['tmp_name'];
	$photo_name=$_FILES['a_photo']['name'];

	if($photo != "") {
		$new_name ="AT".date('His').rand(1000,9999); 
		$path = "../images/tff_article"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>900){
			$width=900;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic="";}
	
	$sql="INSERT INTO tb_tff_article(a_name, a_title, a_detail, a_date, a_status, a_photo)
	values('$a_name','$a_title','$a_detail','$a_date','$a_status','$pathPic')";
	$Re_sql=$mysqli->query($sql);
	if (!$Re_sql) {printf("Error: %s\n", $mysqli->error);}

	
	$GoTo = "admin_tff_article_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="edit"){
		
	$photo=$_FILES["a_photo_edit"];
	$photo=$_FILES['a_photo_edit']['tmp_name'];
	$photo_name=$_FILES['a_photo_edit']['name'];

	if($photo != "") {
		$sql_chk = "SELECT * FROM tb_tff_article WHERE a_id='$a_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['a_photo'];
		if($file!=""){
			if (file_exists ("../images/tff_article/$file")){unlink("../images/tff_article/$file");}
		}

		$new_name ="AT".date('His').rand(1000,9999); 
		$path = "../images/tff_article"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>900){
			$width=900;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic=$h_a_photo;}

	$sql="UPDATE tb_tff_article SET a_name='$a_name', a_title='$a_title', a_detail='$a_detail', a_photo='$pathPic', a_date='$a_date', a_status='$a_status' WHERE a_id='$a_id'";
	$Re_sql=$mysqli->query($sql);
	
	$GoTo = "admin_tff_article_edit.php?a_id=$a_id";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="dele"){
	if ((isset($_GET['a_id'])) && ($_GET['a_id'] != "")){
		$a_id=$_GET['a_id'];
			
		$sql_chk = "SELECT * FROM tb_tff_article WHERE a_id='$a_id'";
		$Re_chk = $mysqli->query($sql_chk);
		$row_Re_chk =$Re_chk->fetch_assoc();
		
		$file=$row_Re_chk['a_photo'];
		if($file!=""){
			if (file_exists ("../images/tff_article/$file")){
				unlink("../images/tff_article/$file");
				}
		}
		$sql1 ="DELETE FROM tb_tff_article WHERE a_id='$a_id'";
		$Re_sql1=$mysqli->query($sql1);

		$GoTo = "admin_tff_article_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}


?>
