<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="insert";
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
    <script>
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('#pro_detail', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
        });
    </script>
    <script type="text/javascript">
        function fncCreateElement1(){
            var mySpan1 = document.getElementById('mySpan1');
            var myElement = document.createElement('input');
            var myElement2 = document.createElement('br');
            myElement.setAttribute('type',"file");
            myElement.setAttribute('name',"pro_p_photo[]");
            mySpan1.appendChild(myElement);	
            mySpan1.appendChild(myElement2);  
        }
    </script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_product_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_product_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;เพิ่ม".$title;?></div>
                <div class="box_form">
                    <form action="admin_product_save.php?action=insert" method="POST" enctype="multipart/form-data" name="form_pd" id="form_pd" >
                        <table width="100%" border="0" align="center">
                            <tr>
                                <td width="150px" valign="top">เลือกสถานะ</td>
                                <td width="300px" valign="top">
                                    <select name="pro_status" id="pro_status">
                                        <option value="">เลือก</option>
                                        <option value="1">ลงขาย</option>
                                        <option value="0">ขายแล้ว</option>
                                    </select>
                                </td>
                                <td width="150px" valign="top">ประเภทรถ</td>
                                <td valign="top">
                                    <select name="pro_type" id="pro_type">
                                        <option value="">เลือก</option>
                                        <option value="0">รถใหม่</option>
                                        <option value="1">รถมือสอง</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">ยี่ห้อ</td>
                                <td valign="top"><input type="text" name="pro_brand" id="pro_brand"></td>
                                <td valign="top">รุ่นรถ</td>
                                <td valign="top"><input type="text" name="pro_model" id="pro_model" /></td>
                            </tr>
                            <tr>
                                <td valign="top">โฉมรถยนต์</td>
                                <td valign="top"><input type="text" name="pro_model_type" id="pro_model_type"></td>
                                <td valign="top">รายละเอียดรุ่น</td>
                                <td valign="top"><input type="text" name="pro_model_detail" id="pro_model_detail" /></td>
                            </tr>
                            <tr>
                                <td valign="top">ปี</td>
                                <td valign="top"><input type="text" name="pro_year" id="pro_year"></td>
                                <td valign="top">ขนาดเครื่องยนต์</td>
                                <td valign="top"><input type="text" name="pro_cc" id="pro_cc" /></td>
                            </tr>
                            <tr>
                                <td valign="top">ระบบเกียร์</td>
                                <td valign="top"><select name="pro_gear" id="pro_gear">
                                        <option value="">เลือก</option>
                                        <option value="MT">เกียร์ธรรมดา</option>
                                        <option value="AT">เกียร์อัตโนมัติ</option>
                                    </select>
                                </td>
                                <td valign="top">เลขไมล์(กม)</td>
                                <td valign="top"><input type="text" name="pro_mile" id="pro_mile" /></td>
                            </tr>
                            <tr>
                                <td valign="top">สี</td>
                                <td valign="top"><input type="text" name="pro_color" id="pro_color"></td>
                                <td valign="top">ราคาจำหน่าย</td>
                                <td valign="top"><input type="text" name="pro_price" id="pro_price" /></td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top">รายละเอียด</td>
                                <td colspan="3" valign="top"><textarea name="pro_detail" id="pro_detail" rows="" cols=""></textarea></td>
                            </tr>
                            <tr>
                                <td colspan="4"><hr /></td>
                            </tr>
                            <tr>
                                <td valign="top">รูปภาพรถ</td>
                                <td colspan="3" valign="top">
                                    <input type="file" name="pro_p_photo[]" id="pro_p_photo[]">
                                    <input type="button" name="button1" id="button1" value="+" onclick="JavaScript:fncCreateElement1();" />
                                    (กด + เพื่อเพิ่มรูปภาพ)<br/>
                                    <div id="mySpan1"></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4"><hr /></td>
                            </tr>
                            <tr>
                                <td colspan="4" >
                                    <input type="submit" name="Submit" id="Submit" value="บันทึกข้อมูล"/>
                                    <input type="reset" name="reset" id="reset" value="ยกเลิก"/>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>