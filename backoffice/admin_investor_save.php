<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST["iv_id"])){$iv_id=mysqli_real_escape_string($mysqli, $_POST["iv_id"]);}
if(isset($_POST["iv_type"])){$iv_type=mysqli_real_escape_string($mysqli, $_POST["iv_type"]);}
if(isset($_POST["iv_name"])){$iv_name=mysqli_real_escape_string($mysqli, $_POST["iv_name"]);}
if(isset($_POST["h_iv_file"])){$h_iv_file=mysqli_real_escape_string($mysqli, $_POST["h_iv_file"]);}

if(isset($_POST["en_iv_name"])){$en_iv_name=mysqli_real_escape_string($mysqli, $_POST["en_iv_name"]);}

if($_GET['action']=="insert"){
	$file=$_FILES["iv_file"];
	$file=$_FILES['iv_file']['tmp_name'];
	$file_name=$_FILES['iv_file']['name'];
	
	if($file != ""){
		$new_name ="IV".date('His').rand(1000,9999); 
		$path = "../file/investor"; 
										
		$RenameFile=$new_name.strchr($file_name,".");
		if (copy( $file , "$path/$RenameFile" )){		
			unlink($file);
			$NewFile="$RenameFile";	
		}
	}else{$NewFile="";}
	
	$sql_in="insert into tb_investor(iv_type, iv_name, en_iv_name, iv_file)values('$iv_type' ,'$iv_name' ,'$en_iv_name' ,'$NewFile')";
	$Re_in=$mysqli->query($sql_in);
	
	$GoTo = "admin_investor_list.php?type=".$iv_type;
	if (isset($_SERVER['QUERY_STRING'])) {
	$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
	$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="edit"){	
	$file=$_FILES["iv_file_edit"];
	$file=$_FILES['iv_file_edit']['tmp_name'];
	$file_name=$_FILES['iv_file_edit']['name'];

	if($file != ""){
		$sql_chk = "SELECT * FROM tb_investor WHERE iv_id='$iv_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file2=$row_Re_chk['iv_file'];
		if($file2!=""){
			if (file_exists ("../file/investor/$file2")){unlink("../file/investor/$file2");}
		}
		$new_name ="IV".date('His').rand(1000,9999); 
		$path = "../file/investor"; 

		$RenameFile=$new_name.strchr($file_name,".");
		if (copy( $file , "$path/$RenameFile" )){		
			unlink($file);
			$NewFile="$RenameFile";	
		}
	}else{$NewFile=$h_iv_file;}
	
	$sql_update="UPDATE tb_investor SET iv_name='$iv_name' , en_iv_name='$en_iv_name' , iv_file='$NewFile' WHERE iv_id='$iv_id'";
	$Re_update=$mysqli->query($sql_update);
	
	$GoTo = "admin_investor_edit.php?type=".$iv_type."&iv_id=".$iv_id;
	if (isset($_SERVER['QUERY_STRING'])) {
	$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
	$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="dele"){
	if ((isset($_GET['iv_id'])) && ($_GET['iv_id'] != "")) {
		$iv_id=mysqli_real_escape_string($mysqli, $_GET['iv_id']);
		$iv_type=mysqli_real_escape_string($mysqli, $_GET["type"]);
			
		$sql_chk= "SELECT * FROM tb_investor WHERE iv_id='$iv_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['iv_file'];
		if($file!=""){
			if (file_exists ("../file/investor/$file")){unlink("../file/investor/$file");}
		}

		$sql_dele ="DELETE FROM tb_investor WHERE iv_id='$iv_id'";
		$Re_dele=$mysqli->query($sql_dele);
			
		$GoTo = "admin_investor_list.php?type=".$iv_type;
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}
$mysqli->close();
?>
