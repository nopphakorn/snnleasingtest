<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

if (isset($_GET['em_id'])) {$em_id_chk = mysqli_real_escape_string($mysqli, $_GET['em_id']);}
$sql_admin="SELECT * FROM tb_employee WHERE em_id = '".$em_id_chk."'";
$Re_admin=$mysqli->query($sql_admin);
$row_Re_admin=$Re_admin->fetch_assoc();
?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_employee_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_employee_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-pencil fa-lg" style="color:#3097FF"></i><?php echo" แก้ไขรายการ".$title;?></div>
                <div class="box_form">
                    <form action="admin_employee_save.php?action=edit" method="post" enctype="multipart/form-data" name="form_em" id="form_em">
                        <table width="100%" border="0">
                            <tr>
                                <td width="160">สถานะ</td>
                                <td>
                                    <select name="em_status" id="em_status">
                                        <option value="1" <?php if (!(strcmp(1, $row_Re_admin['em_status']))) {echo "selected=\"selected\"";} ?>>อนุญาติ</option>
                                        <option value="0" <?php if (!(strcmp(0, $row_Re_admin['em_status']))) {echo "selected=\"selected\"";} ?>>ไม่อนุญาติ</option>
                                    </select>
                                </td>
                            </tr>
							<tr>
                                <td width="160"><div align="left">รหัสพนักงาน</div></td>
                                <td><input name="em_no" type="text" id="em_no"value="<?php echo $row_Re_admin['em_no']; ?>" size="30" /></td>
                            </tr>
                            <tr>
                                <td width="160"><div align="left">ชื่อ-นามสกุล</div></td>
                                <td><input name="em_name" type="text" id="em_name" value="<?php echo $row_Re_admin['em_name']; ?>" size="30"/></td>
                            </tr>
							<tr>
                                <td width="160"><div align="left">ตำแหน่ง</div></td>
                                <td><input name="em_position" type="text" id="em_position" value="<?php echo $row_Re_admin['em_position']; ?>" size="30" /></td>
                            </tr>
                            <tr>
                                <td width="160">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="160"><div align="left">Username</div></td>
                                <td>
                                    <input name="em_user" type="text" id="em_user" value="<?php echo $row_Re_admin['em_user']; ?>" onkeyup="check_user(form_em.em_user.value, form_em.h_em_user.value)"/>
                                    <span id="msg"></span>
                                    <input name="h_em_user" type="hidden" id="h_em_user" value="<?php echo  $row_Re_admin['em_user']; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td width="160"><div align="left" >เปลี่ยน Password ใหม่</div></td>
                                <td>
                                    <input name="em_pass_edit" type="password" id="em_pass_edit"/>
                                    <input name="h_em_pass" type="hidden" id="h_em_pass" value="<?php echo $row_Re_admin['em_pass']; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td width="160"><div align="left" >Confirm Password ใหม่</div></td>
                                <td><input name="c_em_pass_edit" type="password" id="c_em_pass_edit"/></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <hr>
                                    <input name="Submit" type="submit" id="Submit" value="แก้ไขรายชื่อพนักงาน" />
                                    <input name="em_id" type="hidden" id="em_id" value="<?php echo $row_Re_admin['em_id']; ?>" />
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>