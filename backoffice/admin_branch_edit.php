<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

$bch_id_chk=mysqli_real_escape_string($mysqli, $_GET['bch_id']);
$sql_bch="SELECT * FROM tb_branch WHERE bch_id = '$bch_id_chk'";
$Re_bch=$mysqli->query($sql_bch);
$row_Re_bch=$Re_bch->fetch_assoc();
?>
<!doctype html>
<html>
<head>
	<?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_branch_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_branch_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-pencil fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;แก้ไขรายการ".$title;?></div>
                <div class="box_form">
                    <form action="admin_branch_save.php?action=edit" method="POST" enctype="multipart/form-data" name="form_bch" id="form_bch" >
                        <table width="100%" border="0" align="center">
                            <tr>
                                <td width="120">ตั้งอยู่ที่จังหวัด</td>
                                <td><input name="bch_province" type="text" id="bch_province" value="<?php echo $row_Re_bch['bch_province']; ?>" size="40" /></td>
                            </tr>
                            <tr>
                                <td>ชื่อสาขา</td>
                                <td><input name="bch_name" type="text" id="bch_name" value="<?php echo $row_Re_bch['bch_name']; ?>" size="40" /></td>
                            </tr>
                            <tr>
                                <td valign="top">สถานที่ตั้ง</td>
                                <td><textarea name="bch_add" id="bch_add" cols="58" rows="3"><?php echo $row_Re_bch['bch_add']; ?></textarea></td>
                            </tr>
                            <tr>
                                <td>จุดสังเกตุ</td>
                                <td><input name="bch_landmark" type="text" id="bch_landmark" value="<?php echo $row_Re_bch['bch_landmark']; ?>" size="50"/></td>
                            </tr>
                            <tr>
                                <td>เบอร์โทรศัพท์</td>
                                <td><input name="bch_tel" type="text" id="bch_tel" value="<?php echo $row_Re_bch['bch_tel']; ?>" /></td>
                            </tr>
                            <tr>
                                <td>เบอร์โทรสาร</td>
                                <td><input name="bch_fax" type="text" id="bch_fax" value="<?php echo $row_Re_bch['bch_fax']; ?>" /></td>
                            </tr>
                            <tr>
                                <td colspan="2" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td>ตำแหน่งที่ตั้ง</td>
                                <td>
                                    lat: <input type="text" name="bch_lat" id="bch_lat" size="20" value="<?php echo $row_Re_bch['bch_lat']; ?>"/>&nbsp;&nbsp;
                                    lng: <input type="text" name="bch_lng" id="bch_lng" size="20" value="<?php echo $row_Re_bch['bch_lng']; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td>วันทำการ</td>
                                <td><input name="bch_open" type="text" id="bch_open" size="50" value="<?php echo $row_Re_bch['bch_open']; ?>"/></td>
                            </tr>
                            <tr>
                                <td>เวลาทำการ</td>
                                <td><input name="bch_time" type="text" id="bch_time" value="<?php echo $row_Re_bch['bch_time']; ?>"/></td>
                            </tr>
                            <tr>
                                <td valign="top">#Tags</td>
                                <td>
                                    <textarea name="bch_tag" id="bch_tag" cols="58" rows="2"><?php echo $row_Re_bch['bch_tag']; ?></textarea><br>
                                    <span class="tx_error">* การใส่ tags " จังหวัด,อำเภอ,ชื่อสาขา "</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" ><hr /></td>
                            </tr>
                            <tr>
                                <td colspan="2" >
                                    <input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/>
                                    <input name="bch_id" type="hidden" id="bch_id" value="<?php echo $row_Re_bch['bch_id']; ?>">
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>