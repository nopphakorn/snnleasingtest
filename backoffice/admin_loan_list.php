<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$maxRows_Re_l = 100;
$pageNum_Re_l = 0;
if (isset($_GET['pageNum_Re_l'])) {
  $pageNum_Re_l = $_GET['pageNum_Re_l'];
}
$startRow_Re_l = (($pageNum_Re_l-1) * $maxRows_Re_l);
if($startRow_Re_l<0){$startRow_Re_l=0;}

$query_Re_l = "SELECT * FROM tb_loan ORDER BY l_date_reg DESC ";
$query_limit_Re_l = sprintf("%s LIMIT %d, %d", $query_Re_l, $startRow_Re_l, $maxRows_Re_l);
$Re_l=$mysqli->query($query_limit_Re_l);

if (isset($_GET['totalRows_Re_l'])) {
  $totalRows_Re_l = $_GET['totalRows_Re_l'];
} else {
  $all_Re_l=$mysqli->query($query_Re_l);
  $totalRows_Re_l=$all_Re_l->num_rows;
}
$totalPages_Re_l = ceil($totalRows_Re_l/$maxRows_Re_l);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_l==0){$page=1;}
else if($pageNum_Re_l>0){$page=$pageNum_Re_l;}

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
	<script type="text/javascript">
        $(document).ready(function() {
            $('.btn_view').fancybox({
                    'type' : 'iframe',
                    'width' : '650',
                    'height' : '300',
                    'autoScale' : false,
                    'fitToView' : false,
                    'autoSize' : false,
            });
        });
	</script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_loan_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_loan_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo" รายการ".$title;?></div>

                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_l=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_l=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_l>0){
                                echo "Page : ".$page."/".$totalPages_Re_l." Total : ".number_format($totalRows_Re_l)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                        

                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_l=%d%s", $currentPage, $totalPages_Re_l, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_l){printf("%s?pageNum_Re_l=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" align="center">
                    <tr >
                        <th width="35"><div align="center">ลำดับ</div></th>
						 <th width="80"><div align="center">สถานะ</div></th>
						<th width="110"><div align="center">วันที่สมัคร</div></th>
                        <th width="250"><div align="center">ชื่อ-นามสกุล</div></th>
                        <th width="150"><div align="center">จังหวัดที่อยู่ปัจจุบัน</div></th>
						<th width=""><div align="center">ประเภทสมัครขอสินเชื่อ</div></th>
                        <th width="35"><div align="center">ดู</div></th>
                        <th width="35"><div align="center">ลบ</div></th>
                    </tr>
                    <?php 
                        if($totalRows_Re_l>0){
                            $number=$startRow_Re_l; 
                            while($row_Re_l=$Re_l->fetch_assoc()){
                    ?>
                    <tr >
                        <td><div align="center"><?php echo $number+=1; ?></div></td>
						<td>
                            <div align="center">
                                <?php if($row_Re_l['l_status']=="S"){ ?>
                                <a class="l_status_s" href="">ส่งเรื่องแล้ว</a>
                                <?php }else{ ?>
                                <a class="l_status_r"  href="admin_loan_save.php?l_id=<?php echo $row_Re_l['l_id']; ?>&action=status&l_status=S">รอดำเนินการ</a>
                                <?php } ?>
                            </div>
                        </td>
						<td><div align="left"><?php echo datethai_sm_time($row_Re_l['l_date_reg']); ?></div></td>
                        <td><div align="left">
                            <?php echo $row_Re_l['l_name']; ?>
                            <?php 
                                echo "<br> เบอร์ติดต่อ : ";
                                if(!empty($row_Re_l['l_tel'])){echo $row_Re_l['l_tel'];}else{echo "-";} ?>
                            </div></td>
                        <td><div align="left"><?php if(!empty($row_Re_l['l_province'])){echo $row_Re_l['l_province'];}else{echo "-";}?></div></td>
                        <td><div align="left"><?php echo $row_Re_l['l_type']; ?></div></td>
                        <td>
                            <div align="center">
                                <a class="btn_view" href="admin_loan_detail.php?l_id=<?php echo $row_Re_l['l_id']; ?>">
                                <img src="images/icon/icon_anw_view_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_loan_save.php?action=dele&l_id=<?php echo $row_Re_l['l_id']; ?>" onclick = "return confirm('ต้องการลบรายการสมัครขอสินเชื่อหรือไม่')">
                                <img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a></div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="10">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>   
    </div>
</body>
</html>
<?php $mysqli->close(); ?>