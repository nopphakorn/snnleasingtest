<?php require_once('Connections/con_db.php'); ?>
<?php 
include('function/f_admin.php');
$page_nav="detail";
?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$colname_Re_bch = "-1";
if (isset($_GET['bch_id'])) {
  $colname_Re_bch = $_GET['bch_id'];
}
mysql_select_db($database_con_db, $con_db);
$query_Re_bch = sprintf("SELECT tb_branch.bch_id, tb_branch.bch_province_id, tb_branch.bch_name, tb_branch.bch_add, tb_branch.bch_tel, tb_branch.bch_fax, tb_branch.bch_photo, tb_branch.bch_detail, tb_province_new.prov_id, tb_province_new.prov_name FROM tb_branch LEFT JOIN tb_province_new ON(tb_branch.bch_province_id=tb_province_new.prov_id) WHERE bch_id = %s", GetSQLValueString($colname_Re_bch, "int"));
$Re_bch = mysql_query($query_Re_bch, $con_db) or die(mysql_error());
$row_Re_bch = mysql_fetch_assoc($Re_bch);
$totalRows_Re_bch = mysql_num_rows($Re_bch);
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
<div id="header">
    <?php include("s_header.php"); ?>
</div>
<div id="containner">
    <div id="main_nav">
        <?php include("admin_branch_nav.php"); ?>
    </div>
    <div id="main">
        <div id="main_menu">
            <?php include("admin_branch_menu.php"); ?>
        </div>
        <div id="main_content">
            <div class="main_content_title"><i class="fa fa-search fa-lg" style="color:#3097FF"></i><?php echo"&nbsp;รายละเอียด".$title;?></div>
            <div class="box_form">
                <table width="100%" border="0" align="center">
                    <tr>
                        <td colspan="2"><div align="center"><div class="img_full"><img src="../images/branch/<?php echo $row_Re_bch['bch_photo']; ?>" /></div><br>
                            <hr>
                            </div></td>
                    </tr>
                    <tr>
                        <td scope="col">พื้นที่จังหวัด</td>
                        <td><?php echo $row_Re_bch['prov_name']; ?> </td>
                    </tr>
                    <tr>
                        <td width="120" scope="col"><div align="left">ชื่อสาขา :</div></td>
                        <td><?php echo $row_Re_bch['bch_name']; ?> </td>
                    </tr>
                    <tr>
                        <td width="120" valign="top" scope="col"><div align="left">สถานที่ตั้ง :</div></td>
                        <td scope="col"><?php echo $row_Re_bch['bch_add']; ?></td>
                    </tr>
                    <?php if($row_Re_bch['bch_tel']!=""){ ?>
                    <tr>
                        <td width="120" scope="col">เบอร์โทรศัพท์ :</td>
                        <td scope="col"><?php echo $row_Re_bch['bch_tel']; ?></td>
                    </tr>
                    <?php } if($row_Re_bch['bch_fax']!=""){ ?>
                    <tr>
                        <td width="120" scope="col"><div align="left">เบอร์โทรสาร :</div></td>
                        <td scope="col"><?php echo $row_Re_bch['bch_fax']; ?></td>
                    </tr>
                    <tr>
                        <td width="120" scope="col"><div align="left">วันเวลาทำการ :</div></td>
                        <td scope="col"><?php echo $row_Re_bch['bch_open']."  ".$row_Re_bch['bch_time']; ?></td>
                    </tr>
                    <?php } if($row_Re_bch['bch_detail']!=""){ ?>
                    <tr>
                        <td colspan="2"><hr>
                            <p><?php echo $row_Re_bch['bch_detail']; ?></p></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="2" ><hr>
                            <input type=button onClick="location.href='admin_branch_edit.php?bch_id=<?php echo $row_Re_bch['bch_id']; ?>'" value='แก้ไขข้อมูล'></td>
                    </tr>
                </table>
            </div>
            
            <!-- e-main_content --></div>
        <!-- e-main --></div>
    <!-- e-containner --></div>
<div id="footer">
    <?php include("s_footer.php"); ?>
</div>
</body>
</html>
<?php
mysql_free_result($Re_bch);
?>
