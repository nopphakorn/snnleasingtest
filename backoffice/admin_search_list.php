<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$maxRows_Re_s = 50;
$pageNum_Re_s = 0;
if (isset($_GET['pageNum_Re_s'])) {
  $pageNum_Re_s = $_GET['pageNum_Re_s'];
}
$startRow_Re_s = (($pageNum_Re_s-1) * $maxRows_Re_s);
if($startRow_Re_s<0){$startRow_Re_s=0;}

$query_Re_s = "SELECT * FROM tb_search ORDER BY s_id DESC ";
$query_limit_Re_s = sprintf("%s LIMIT %d, %d", $query_Re_s, $startRow_Re_s, $maxRows_Re_s);
$Re_s=$mysqli->query($query_limit_Re_s);
//$row_Re_s = mysqli_fetch_assoc($Re_s);

if (isset($_GET['totalRows_Re_s'])) {
  $totalRows_Re_s = $_GET['totalRows_Re_s'];
} else {
  $all_Re_s=$mysqli->query($query_Re_s);
  $totalRows_Re_s=$all_Re_s->num_rows;
}
$totalPages_Re_s = ceil($totalRows_Re_s/$maxRows_Re_s);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_s==0){$page=1;}
else if($pageNum_Re_s>0){$page=$pageNum_Re_s;}

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_search_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_search_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo" รายการ".$title;?></div>

				 <!-- papeging -->
                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_s=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_s=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_s>0){
                                echo "Page : ".$page."/".$totalPages_Re_s." Total : ".number_format($totalRows_Re_s)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                        

                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_s=%d%s", $currentPage, $totalPages_Re_s, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_s){printf("%s?pageNum_Re_s=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" align="center">
                    <tr>
                        <th width="35">ลำดับ</th>
                        <th width="200">หน้าเว็บ</th>
                        <th width="200">URL</th>
                        <th width="">คำค้นหา</th>
						<th width="35">แก้ไข</th>
						<th width="35">ลบ</th>
                    </tr>
                    <?php
                    if($totalRows_Re_s>0){
						$number=$startRow_Re_s; 
                        while($row_Re_s=$Re_s->fetch_assoc()){
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $number+=1; ?></div></td>
						<td><?php echo $row_Re_s['s_name']; ?> </td>
						<td>
                            <a href="<?php echo $row_Re_s['s_url']; ?>" target="_blank"><?php echo $row_Re_s['s_url']; ?></a>
                        </td>
						<td><?php echo $row_Re_s['s_tag']; ?> </td>
                        <td>
                            <div align="center">
                                <a href="admin_search_edit.php?s_id=<?php echo $row_Re_s['s_id']; ?>"><img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_search_save.php?action=dele&s_id=<?php echo $row_Re_s['s_id']; ?>" onclick = "return confirm('คุณต้องการลบหรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a>
                            </div>
                        </td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="6">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
   
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>