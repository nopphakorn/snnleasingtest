<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="insert";
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
    <script>
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('#a_detail', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
        });
    </script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_tff_article_nav.php"); ?></div>
    <div id="side"><?php include('s_menu_side.php'); ?></div>
    
    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_tff_article_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo" เพิ่ม".$title;?></div>
                <div class="box_form">
                    <form action="admin_tff_article_save.php?action=insert" method="post" enctype="multipart/form-data" name="form_tffa" id="form_tffa">
                            <table width="800" >
                                <tr>
                                    <td width="130">วันที่โพสต์</td>
                                    <td>
                                        <?php echo datethai(date('Y-m-d H:i:s')); ?>
                                        <input name="a_date" type="hidden" id="a_date" value="<?php echo date("Y-m-d H:i:s"); ?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>สถานะ</td>
                                    <td>
                                        <select name="a_status" id="a_status">
                                            <option value="1">แสดง</option>
                                            <option value="0">ไม่แสดง</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">ภาพปก</td>
                                    <td><input type="file" name="a_photo" id="a_photo"></td>
                                </tr>
								<tr>
                                    <td colspan="2">
                                        <br>ชื่อบทความ<br>
                                        <input type="text" name="a_name" id="a_name" style="width: 400px">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <br>หัวข้อบทความ<br>
                                        <textarea name="a_title" cols="70" rows="3" id="a_title"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <br>รายละเอียด<br>
                                        <textarea name="a_detail" id="a_detail" style="height: 300px; width: 99%;"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="submit" name="button" id="button" value="บันทึกข้อมูล" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>