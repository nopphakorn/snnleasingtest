<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

$sql_cn = "SELECT * FROM tb_company WHERE cn_id='".$_GET['cn_id']."'";
$Re_cn=$mysqli->query($sql_cn);
$row_Re_cn=$Re_cn->fetch_assoc();
$totalRows_Re_cn=$Re_cn->num_rows;
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_company_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_company_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;เพิ่มรายการ".$title;?></div>
                <div class="box_form">
                    <form action="admin_company_save.php?action=edit" method="POST" enctype="multipart/form-data" name="form_cn" id="form_cn" >
                        <table width="100%" border="0" align="center">
							<?php if($row_Re_cn['cn_logo']!=""){?>
							<tr>
                                <td colspan="2">
									<img src="../images/company/<?php echo $row_Re_cn['cn_logo'];?>" alt="">
									<hr>
                                </td>
							</tr>
							<?php } ?>
							<tr>
                                <td>Logo</td>
                                <td>
                                    <input type="file" name="cn_logo_edit" id="cn_logo_edit" />
                                </td>
                            </tr>
                            <tr>
                                <td width="120">ชื่อธุรกิจ</td>
                                <td><input type="text" name="cn_name" id="cn_name" size="40" value="<?php echo $row_Re_cn['cn_name'];?>"/></td>
							</tr>
							<tr>
                                <td>ประเภทธุรกิจ</td>
                                <td><input type="text" name="cn_type" id="cn_type" size="40" value="<?php echo $row_Re_cn['cn_type'];?>"/></td>
                            </tr>
                            <tr>
                                <td>บริษัท</td>
                                <td><input type="text" name="cn_company" id="cn_company" size="40" value="<?php echo $row_Re_cn['cn_company'];?>"/></td>
                            </tr>
                            <tr>
                                <td valign="top">สถานที่ตั้ง</td>
                                <td><textarea name="cn_address" id="cn_address" cols="58" rows="5"><?php echo $row_Re_cn['cn_address'];?></textarea></td>
                            </tr>
                            <tr>
                                <td>โทรศัพท์</td>
                                <td><input type="text" name="cn_tel" id="cn_tel" value="<?php echo $row_Re_cn['cn_tel'];?>"/></td>
                            </tr>
                            <tr>
                                <td>แฟ๊กซ์</td>
                                <td><input type="text" name="cn_fax" id="cn_fax" value="<?php echo $row_Re_cn['cn_fax'];?>"/></td>
                            </tr>
                            <tr>
                                <td>เว็บไซต์</td>
                                <td>
                                    <input type="text" name="cn_web" id="cn_web" size="40" value="<?php echo $row_Re_cn['cn_web'];?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Facebook</td>
                                <td>
                                    <input type="text" name="cn_fb" id="cn_fb" size="40" value="<?php echo $row_Re_cn['cn_fb'];?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Line@</td>
                                <td>
                                    <input type="text" name="cn_line" id="cn_line" size="40" value="<?php echo $row_Re_cn['cn_line'];?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Youtube</td>
                                <td>
                                    <input type="text" name="cn_youtube" id="cn_youtube" size="40" value="<?php echo $row_Re_cn['cn_youtube'];?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" ><hr /></td>
                            </tr>
                             <tr>
                                <td width="120">Business name</td>
                                <td><input type="text" name="en_cn_name" id="en_cn_name" size="40" value="<?php echo $row_Re_cn['en_cn_name'];?>"/></td>
							</tr>
							<tr>
                                <td>Business type</td>
                                <td><input type="text" name="en_cn_type" id="en_cn_type" size="40" value="<?php echo $row_Re_cn['en_cn_type'];?>"/></td>
                            </tr>
                            <tr>
                                <td>Company</td>
                                <td><input type="text" name="en_cn_company" id="en_cn_company" size="40" value="<?php echo $row_Re_cn['en_cn_company'];?>"/></td>
                            </tr>
                            <tr>
                                <td valign="top">Address</td>
                                <td><textarea name="en_cn_address" id="en_cn_address" cols="58" rows="5"><?php echo $row_Re_cn['en_cn_address'];?></textarea></td>
                            </tr>
                            <tr>
                                <td colspan="2" ><hr /></td>
                            </tr>
                            <tr>
                                <td colspan="2" >
                                    <input type="hidden" name="cn_id" id="cn_id" value="<?php echo $row_Re_cn['cn_id'];?>"/>
                                    <input type="hidden" name="h_cn_logo" id="h_cn_logo" value="<?php echo $row_Re_cn['cn_logo'];?>"/>
                                    <input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
