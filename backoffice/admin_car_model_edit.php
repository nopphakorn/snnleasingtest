<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');

$query_Re_cb = "SELECT * FROM tb_car_brand ORDER BY cb_name ASC ";
$Re_cb=$mysqli->query($query_Re_cb);


$cm_id_chk=$_GET['cm_id'];
$query_Re_cm = "SELECT * FROM tb_car_model WHERE cm_id='$cm_id_chk' ";
$Re_cm=$mysqli->query($query_Re_cm);
$row_Re_cm=$Re_cm->fetch_assoc();
$totalRows_Re_cm=$Re_cm->num_rows;

if((isset($row_Re_cm['cb_id']) AND $row_Re_cm['cb_id']!="") AND (isset($row_Re_cm['cg_id']) AND $row_Re_cm['cg_id']!="")){
$query_Re_cg_s = "SELECT * FROM tb_car_group WHERE cb_id='".$row_Re_cm['cb_id']."' ORDER BY cg_name ASC ";
$Re_cg_s=$mysqli->query($query_Re_cg_s);
}
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>
<body>
	<div id="fancy_title"><i class="fa fa-list-alt"></i>&nbsp;แก้ไขชื่อรุ่นรถ</div>
    <div id="fancy_box">
		<form action="admin_car_save.php?action=model-edit" method="post" enctype="multipart/form-data" name="form_cm" id="form_cm">
			
			<p>
				ประเภท รย.<br>
				 <select name="ty_car" id="" >
							<option value="" <?php if(@$row_Re_cm['ty_car']==''){echo 'selected';}?> >		เลือก</option>
							<option value="รย.1 เก๋ง" <?php if(@$row_Re_cm['ty_car']=='รย.1 เก๋ง'){echo 'selected';}?>>		รย.1 เก๋ง</option>
							<option value="รย.1 SUV/PPV" <?php if(@$row_Re_cm['ty_car']=='รย.1 SUV/PPV'){echo 'selected';}?>>	รย.1 SUV/PPV</option>
							<option value="รย.1 4ประตู" <?php if(@$row_Re_cm['ty_car']=='รย.1 4ประต'){echo 'selected';}?>>	รย.1 4ประตู</option>
							<option value="รย.2 รถตู้" <?php if(@$row_Re_cm['ty_car']=='รย.2 รถต'){echo 'selected';}?>>		รย.2 รถตู้</option>
							<option value="รย.3 กระบะ" <?php if(@$row_Re_cm['ty_car']=='รย.3 กระบะ'){echo 'selected';}?>>	รย.3 กระบะ</option>
                        </select>
			</p>
			<p>
				ยี่ห้อรถ<br>
				<select name="cb_id" id="cb_id">
					<option value="">เลือกยี่ห้อรถ</option>
					<?php while($row_Re_cb=$Re_cb->fetch_assoc()){?>
					<option value="<?php echo $row_Re_cb['cb_id'];?>" <?php if($row_Re_cm['cb_id']==$row_Re_cb['cb_id']){echo "selected=\"selected\"";} ?>><?php echo $row_Re_cb['cb_name'];?></option>
					<?php } ?>
				</select>
			</p>
			<p>
				กลุ่มรุ่นรถ<br>
				<select name="cg_id" id="cg_id" onchange="check_cm(form_cm.cb_id.value, form_cm.cg_id.value, form_cm.cm_name.value, form_cm.h_cm_name.value);">
					<option value="">กลุ่มรุ่นรถ</option>
					<?php 
					if((isset($row_Re_cm['cb_id']) AND $row_Re_cm['cb_id']!="") AND (isset($row_Re_cm['cg_id']) AND $row_Re_cm['cg_id']!="")){ 
						while($row_Re_cg_s=$Re_cg_s->fetch_assoc()){
					?>
					<option value="<?php echo $row_Re_cg_s['cg_id'];?>" <?php if(isset($row_Re_cm['cg_id'])){if($row_Re_cg_s['cg_id']==$row_Re_cm['cg_id']){echo "selected=\"selected\"";}} ?>><?php echo $row_Re_cg_s['cg_name'];?></option>
					<?php }} ?>
				</select>
			</p>
			<p>
				ชื่อกลุ่มรุ่นรถ<br>
				<input type="text" id="cm_name" name="cm_name" style="width:250px" value="<?php echo $row_Re_cm['cm_name']; ?>" 
				onblur="check_cm(form_cm.cb_id.value, form_cm.cg_id.value, form_cm.cm_name.value, form_cm.h_cm_name.value)">
				<span id="msg" class="tx_error"></span>
				<input type="hidden" id="h_cm_name" name="h_cm_name" value="<?php echo $row_Re_cm['cm_name']; ?>">
				<input type="hidden" id="cm_id" name="cm_id" value="<?php echo $row_Re_cm['cm_id']; ?>">
			</p>
			<p><hr><input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/></p>
		</form>
	</div>
</body>
</html>
<?php $mysqli->close(); ?>