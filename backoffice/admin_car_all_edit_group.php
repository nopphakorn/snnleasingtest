<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="all-list";

$query_Re_ct = "SELECT * FROM tb_car_type ORDER BY ct_name ASC ";
$Re_ct=$mysqli->query($query_Re_ct);

$query_Re_cb = "SELECT * FROM tb_car_brand ORDER BY cb_name ASC ";
$Re_cb=$mysqli->query($query_Re_cb);
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
	<link rel="stylesheet" href="tools/datepicker/jquery-ui.css">
	<script src="tools/datepicker/jquery-ui.js"></script>
	<script src="tools/datepicker/jqueryui_datepicker_thai.js"></script>
    <script type="text/javascript">
		$( function () {
			$( "#c_date" ).datepicker( {
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true,
				showOn: "button",
		        buttonImage: "images/icon/calendar.gif",
			} );
		} );
	</script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_car_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_car_menu.php"); ?></div>
            <div id="main_content">
				<div class="main_content_title"><i class="fa fa-edit fa-lg" style="color:#3097FF"></i>&nbsp;แก้ไขราคาแบบกลุ่ม</div>
				<div class="box_form">
                    <div id="success_bar" style="display:none;">
                        <i class="far fa-check-circle"></i> บันทึกแก้ไขราคาแบบกลุ่มเรียบร้อย
                    </div>
                    <div id="error_bar" style="display:none;">
                        <i class="fas fa-ban"></i> ไม่สามารถบันทึกเปลี่ยนราคาแบบกลุ่มได้
                    </div>

					<form action="admin_car_all_edit_group_save.php" method="post" enctype="multipart/form-data" name="form_edit_group" id="form_edit_group">
                        <table width="100%" cellspacing="5">
                            <tr>
                                <td width="150px">วันที่ Update ราคา</td>
                                <td>
                                    <input type="text" id="c_date" name="c_date" style="width:150px" readonly value="<?php echo datethai_num(date("Y-m-d")); ?>">
                                    <input type="time" id="c_date_t" name="c_date_t" style="width:70px" value="<?php echo date("H:i"); ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>ยี่ห้อ</td>
                                <td>
                                    <select name="cb_id" id="cb_id" class="g_search" onchange="list_ca('ct_id',form_edit_group.cb_id.value,'','')">
                                        <option value="">เลือกยี่ห้อรถ</option>
                                        <?php while($row_Re_cb=$Re_cb->fetch_assoc()){?>
                                        <option value="<?php echo $row_Re_cb['cb_id'];?>"><?php echo $row_Re_cb['cb_name'];?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>ประเภท</td>
                                <td>
                                    <select name="ct_id" id="ct_id" class="g_search" onchange="list_ca('cg_id', form_edit_group.cb_id.value, form_edit_group.ct_id.value,'')">
                                        <option value="">เลือกประเภท</option>
                                    </select>
                                </td>
                            </tr>
                            
                            <tr>
                                <td>กลุ่มรุ่นรถ</td>
                                <td>
                                    <select name="cg_id" id="cg_id" onchange="list_ca('cm_id', form_edit_group.cb_id.value, form_edit_group.ct_id.value, form_edit_group.cg_id.value)">
                                        <option value="">กลุ่มรุ่นรถ</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>ชื่อรุ่นรถ</td>
                                <td>
                                    <select name="cm_id" id="cm_id" class="g_search">
                                        <option value="">เลือกกรุ่นรถ</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>ระบบเกียร์</td>
                                <td>
                                    <select name="c_gear" id="c_gear" class="g_search">
                                        <option value="">เลือก</option>
                                        <option value="AT">ออโต้</option>
                                        <option value="MT">ธรรมดา</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>ช่วงปีรถที่ต้องการปรับ</td>
                                <td>
                                    <input type="text" id="c_year_s" name="c_year_s" class="g_search" placeholder="ค.ศ." maxlength="4" style="width:60px"> ถึง 
                                    <input type="text" id="c_year_e" name="c_year_e" class="g_search" placeholder="ค.ศ." maxlength="4" style="width:60px">
                                </td>
                            </tr>
                            <tr>
                                <td>จำนวนเงินที่ปรับราคา</td>
                                <td>
                                    <input type="text" id="c_price" name="c_price" class="g_search">
                                    <span class="tx_error_n">* เพิ่มหรือลดใส่จำนวนเงิน  ตัวอย่าง เพิ่ม 100 / ลด -100 </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <hr>
                                    <input name="Submit" type="submit" id="Submit" value="บันทึกปรับราคา"/>
                                    <input type="hidden" id="c_user" name="c_user" value="<?php echo $row_Re_admin_chk['admin_name'];?>">
                                    <input type="hidden" id="action" name="action" value="gedit-price">
                                </td>
                            </tr>
                        </table>
                    </form>
				</div>
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>

