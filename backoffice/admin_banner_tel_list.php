<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$sql_ban = "SELECT * FROM tb_banner_tel ORDER BY ban_id ASC";
$Re_ban=$mysqli->query($sql_ban);
$totalRows_Re_ban=$Re_ban->num_rows;

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_banner_tel_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_banner_tel_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo" รายการ".$title;?></div>

                <table class="tb1" width="100%" align="center">
                    <tr>
						<th width="200">รายการ</th>
                        <th>ภาพแบนเนอร์</th>
                        <th width="40">สถานะ</th>
                        <th width="40">แก้ไข</th>
                    </tr>
                    <?php
                    if($totalRows_Re_ban>0){
                        while($row_Re_ban=$Re_ban->fetch_assoc()){
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $row_Re_ban['ban_name']; ?></div></td>
                        <td>
                            <div class="img_ban_list">
							<?php if($row_Re_ban['ban_photo']!=""){?>
							<img src="../images/banner_tel/<?php echo $row_Re_ban['ban_photo']; ?>" border="0" />
							<?php }else{ ?>
							<img src="../images/banner_tel/nophoto.jpg" border="0" />
							<?php } ?>
							</div>
                            <?php echo "Link : ".$row_Re_ban['ban_link']; ?>
                        </td>
                        <td>
                            <div align="center">
                                <?php if($row_Re_ban['ban_status']=="1"){ ?>
                                <a href="admin_banner_tel_save.php?ban_id=<?php echo $row_Re_ban['ban_id']; ?>&action=status&ban_status=0"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
                                <?php }else{ ?>
                                <a href="admin_banner_tel_save.php?ban_id=<?php echo $row_Re_ban['ban_id']; ?>&action=status&ban_status=1"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
                                <?php } ?>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_banner_tel_edit.php?ban_id=<?php echo $row_Re_ban['ban_id']; ?>"><img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="5">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
   
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>