<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST["n_id"])){$n_id=mysqli_real_escape_string($mysqli, $_POST["n_id"]);}
if(isset($_POST["n_name"])){$n_name=mysqli_real_escape_string($mysqli, $_POST["n_name"]);}
if(isset($_POST["n_detail"])){$n_detail=mysqli_real_escape_string($mysqli, $_POST["n_detail"]);}
if(isset($_POST["en_n_name"])){$en_n_name=mysqli_real_escape_string($mysqli, $_POST["en_n_name"]);}
if(isset($_POST["en_n_detail"])){$en_n_detail=mysqli_real_escape_string($mysqli, $_POST["en_n_detail"]);}
if(isset($_POST["n_date"])){$n_date=mysqli_real_escape_string($mysqli, $_POST["n_date"]);}
if(isset($_POST["n_status"])){$n_status=mysqli_real_escape_string($mysqli, $_POST["n_status"]);}
if(isset($_POST["h_n_photo"])){$h_n_photo=mysqli_real_escape_string($mysqli, $_POST["h_n_photo"]);}
	
if($action=="status"){
	$n_id = $_GET['n_id'];
	$n_status = $_GET['n_status'];
	
	$sql="UPDATE tb_news SET n_status=$n_status WHERE n_id='$n_id'";
	$Re_sql=$mysqli->query($sql);
		
	$GoTo = "admin_news_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="insert"){
	$photo=$_FILES["n_photo"];
	$photo=$_FILES['n_photo']['tmp_name'];
	$photo_name=$_FILES['n_photo']['name'];

	if($photo != "") {
		$new_name ="N".date('His').rand(1000,9999); 
		$path = "../images/news"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>1024){
			$width=1024;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic="";}
	
	$sql="INSERT INTO tb_news(n_name, n_detail, en_n_name, en_n_detail, n_date, n_status, n_photo)
	values('$n_name','$n_detail','$en_n_name','$en_n_detail','$n_date','$n_status','$pathPic')";
	$Re_sql=$mysqli->query($sql);
	$id_update = $mysqli->insert_id;

	for($i=0;$i<count($_FILES["n_p_photo"]["name"]);$i++){
		if($_FILES["n_p_photo"]["name"][$i] != ""){
			$photo_name = $_FILES["n_p_photo"]["name"][$i];
			$n_p_photo_name = $_FILES["n_p_photo"]["tmp_name"][$i];
	
			$newfile = "N".date('His').rand(1000,9999); 
			$path2 = "../images/news/"; 
										
			$images_photo = $n_p_photo_name;
			$size=GetimageSize($images_photo);
			$size_w=$size[0];
			$size_h=$size[1];
			if($size_w>1024){
				$width=1024;					
				$height=round(($width/$size_w) * $size_h);
				
				if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
					$images_photo_orig = ImageCreateFromJPEG($images_photo);
				}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
					$images_photo_orig = ImageCreateFromJPEG($images_photo);
				}
			
				$images_photo_new = ImageCreateTrueColor($width, $height); 
				ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
			
				if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
					ImageJPEG($images_photo_new,$images_photo,100); 
				}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
					ImageJPEG($images_photo_new,$images_photo,100); 
				}			
				ImageDestroy($images_photo_orig);
				ImageDestroy($images_photo_new); 					
			}			
			$RenameFile=$newfile.strchr($photo_name,".");
			if (copy( $images_photo , "$path/$RenameFile" )){		
				unlink($images_photo);
				$pathPic2="$RenameFile";	
			}

			$sql2="INSERT INTO tb_news_photo (n_p_photo, n_id) VALUES ('$pathPic2','$id_update')";
			$Re_sql2=$mysqli->query($sql2);
		}
	}
	
	$GoTo = "admin_news_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="edit"){
		
	$photo=$_FILES["n_photo_edit"];
	$photo=$_FILES['n_photo_edit']['tmp_name'];
	$photo_name=$_FILES['n_photo_edit']['name'];

	if($photo != "") {
		$sql_chk = "SELECT * FROM tb_news WHERE n_id='$n_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['n_photo'];
		if($file!=""){
			if (file_exists ("../images/news/$file")){unlink("../images/news/$file");}
		}

		$new_name ="N".date('His').rand(1000,9999); 
		$path = "../images/news"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>1024){
			$width=1024;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic=$h_n_photo;}
	
	$sql="UPDATE tb_news SET n_detail='$n_detail',n_name='$n_name',en_n_detail='$en_n_detail',en_n_name='$en_n_name',n_date='$n_date',n_name='$n_name',n_status='$n_status',n_photo='$pathPic' WHERE n_id='$n_id'";
	$Re_sql=$mysqli->query($sql);

	for($i=0;$i<count($_FILES["n_p_photo"]["name"]);$i++){
		if($_FILES["n_p_photo"]["name"][$i] != ""){
			$photo_name = $_FILES["n_p_photo"]["name"][$i];
			$n_p_photo_name = $_FILES["n_p_photo"]["tmp_name"][$i];
	
			$newfile = "N".date('His').rand(1000,9999); 
			$path = "../images/news/"; 
										
			$images_photo = $n_p_photo_name;
			$size=GetimageSize($images_photo);
			$size_w=$size[0];
			$size_h=$size[1];
			if($size_w>1024){
				$width=1024;					
				$height=round(($width/$size_w) * $size_h);
				
				if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
					$images_photo_orig = ImageCreateFromJPEG($images_photo);
				}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
					$images_photo_orig = ImageCreateFromJPEG($images_photo);
				}
			
				$images_photo_new = ImageCreateTrueColor($width, $height); 
				ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
			
				if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
					ImageJPEG($images_photo_new,$images_photo,100); 
				}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
					ImageJPEG($images_photo_new,$images_photo,100); 
				}			
				ImageDestroy($images_photo_orig);
				ImageDestroy($images_photo_new); 					
			}			
			$RenameFile=$newfile.strchr($photo_name,".");
			if (copy( $images_photo , "$path/$RenameFile" )){		
				unlink($images_photo);
				$pathPic2="$RenameFile";	
			}

			$sql2="INSERT INTO tb_news_photo (n_p_photo, n_id) VALUES ('$pathPic2','$n_id')";
			$Re_sql2=$mysqli->query($sql2);
		}
	}
	
	$GoTo = "admin_news_edit.php?n_id=$n_id";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="dele"){
	if ((isset($_GET['n_id'])) && ($_GET['n_id'] != "")){
		$n_id=$_GET['n_id'];
			
		$sql_chk = "SELECT * FROM tb_news WHERE n_id='$n_id'";
		$Re_chk = $mysqli->query($sql_chk);
		$row_Re_chk =$Re_chk->fetch_assoc();
		
		$file=$row_Re_chk['n_photo'];
		if($file!=""){
			if (file_exists ("../images/news/$file")){
				unlink("../images/news/$file");
				}
		}

		$sql1 ="DELETE FROM tb_news WHERE n_id='$n_id'";
		$Re_sql1=$mysqli->query($sql1);
			
		$sql_chk1 = "SELECT * FROM tb_news_photo WHERE n_id='$n_id'";
		$Re_chk1=$mysqli->query($sql_chk1);
		$total_Re_chk1=$Re_chk1->num_rows;
			
		if($total_Re_chk1>0){
			while($row_Re_chk1 = $Re_chk1->fetch_assoc()){
				$file1=$row_Re_chk1['n_p_photo'];
				if(file_exists ("../images/news/$file1")){
					unlink("../images/news/$file1");
				}
			}
			$sql2 ="DELETE FROM tb_news_photo WHERE n_id='$n_id'";
			$Re_sql2=$mysqli->query($sql2);
		}
			
		$GoTo = "admin_news_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}

if($_GET['action']=="dele_p"){
	$n_p_id=$_GET['n_p_id'];
	$sql = "SELECT * FROM tb_news_photo WHERE n_p_id='$n_p_id'";
	$Re_chk=$mysqli->query($sql);
	$row_Re_chk=$Re_chk->fetch_assoc();
	
	$file=$row_Re_chk['n_p_photo'];
	if($file!=""){
		if (file_exists ("../images/news/$file")){
			unlink("../images/news/$file");
		}
	}
	$sql_dele ="DELETE FROM tb_news_photo WHERE n_p_id='$n_p_id'";
	$Re_dele=$mysqli->query($sql_dele);
	
	$GoTo = "admin_news_edit.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="dele_y"){
	$file=$_GET['n_y_id'];
	$sql_dele ="DELETE FROM tb_news_youtube WHERE n_y_id='$n_y_id'";
	$Re_dele=$mysqli->query($sql_dele);
			
	$GoTo = "admin_news_edit.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}
?>
