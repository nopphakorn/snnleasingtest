<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>
<body>
	<?php if(!isset($_GET['save']) OR $_GET['save']!='sucess'){ ?>
	<div id="fancy_title"><i class="fa fa-list-alt"></i>&nbsp;เพิ่มชื่อประเภทรถ</div>
    <div id="fancy_box">
		<form action="admin_car_save.php?action=type-insert" method="post" enctype="multipart/form-data" name="form_ct" id="form_ct">
			ชื่อประเภทรถ<br>
			<input type="text" id="ct_name" name="ct_name" style="width:250px" onkeyup="check_ct(form_ct.ct_name.value,form_ct.h_ct_name.value)">
			<span id="msg" class="tx_error"></span>
            <input type="hidden" name="h_ct_name" id="h_ct_name" />
			<br><br>
			<p><hr><input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/></p>
		</form>
	</div>
	<?php } ?>

	<?php if(isset($_GET['save']) AND $_GET['save']='sucess'){ ?>
	<div id="fancy_sucess">
		<br><br>
		<p><i class="fa fa-check-circle-o  fa-5x" aria-hidden="true"></i></p>
		<p>บันทึกรายการประเภทรถเรียบร้อย</p>
	</div>
	<?php } ?>
</body>
</html>


