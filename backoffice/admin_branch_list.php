<?php
require_once('Connections/con_db.php'); 
include('function/f_admin.php');
$page_nav="list";

$maxRows_Re_bch = 50;
$pageNum_Re_bch = 0;
if (isset($_GET['pageNum_Re_bch'])) {
  $pageNum_Re_bch = $_GET['pageNum_Re_bch'];
}
$startRow_Re_bch = (($pageNum_Re_bch-1) * $maxRows_Re_bch);
if($startRow_Re_bch<0){$startRow_Re_bch=0;}

$query_Re_bch = "SELECT * FROM tb_branch ORDER BY bch_no ASC ";
$query_limit_Re_bch = sprintf("%s LIMIT %d, %d", $query_Re_bch, $startRow_Re_bch, $maxRows_Re_bch);
$Re_bch=$mysqli->query($query_limit_Re_bch);

if (isset($_GET['totalRows_Re_bch'])) {
  $totalRows_Re_bch = $_GET['totalRows_Re_bch'];
} else {
  $all_Re_bch=$mysqli->query($query_Re_bch);
  $totalRows_Re_bch=$all_Re_bch->num_rows;
}
$totalPages_Re_bch = ceil($totalRows_Re_bch/$maxRows_Re_bch);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_bch==0){$page=1;}
else if($pageNum_Re_bch>0){$page=$pageNum_Re_bch;}

$sql_mn = "SELECT max(bch_no) AS bch_max, min(bch_no) AS bch_min FROM tb_branch ORDER BY bch_no ASC";
$Re_mn=$mysqli->query($sql_mn);
$row_Re_mn=$Re_mn->fetch_assoc();
$totalRows_Re_mn=$Re_mn->num_rows;

?>
<!doctype html>
<html>
<head>
	<?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_branch_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>
    
    <div id="containner">
        <div id="main">
            <div id="main_menu"> <?php include("admin_branch_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;รายการ".$title;?></div>

                <!-- papeging -->
                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_bch=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_bch=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_bch>0){
                                echo "Page : ".$page."/".$totalPages_Re_bch." Total : ".number_format($totalRows_Re_bch)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                        

                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_bch=%d%s", $currentPage, $totalPages_Re_bch, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_bch){printf("%s?pageNum_Re_bch=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" border="1" align="center">
                    <tr>
                        <th width="45">ลำดับ</th>
                        <th width="60">ตำแหน่ง</th>
                        <th width="100">จังหวัด</th>
                        <th width="120">สาขา</th>
                        <th width="">ที่ตั้ง</th>
                        <th width="120">ตำแหน่ง GPS</th>
                        <th width="40">แก้ไข</th>
                        <th width="40">ลบ</th>
                    </tr>
                    <?php 
                    if($totalRows_Re_bch>0){
                     while($row_Re_bch=$Re_bch->fetch_assoc()){
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $row_Re_bch['bch_no']; ?></div></td>
                        <td>
                            <div align="center">
                                <?php 
                                $list=$row_Re_bch['bch_no'];
                                $id=$row_Re_bch['bch_id'];
                                if($totalRows_Re_bch>=2){
                                ?>
                                <?php 
                                if($list==1){ 
                                ?>
                                    <div align="center">
                                        <a href="admin_branch_save.php?action=no&amp;down=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_down.png" alt="down" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php 
                                } if($list!=1 and $list!=$row_Re_mn['bch_max']){
                                ?>
                                    <div align="center">
                                        <a href="admin_branch_save.php?action=no&amp;down=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_down.png" alt="down" width="16" height="16" border="0" /></a>
                                        
                                        <a href="admin_branch_save.php?action=no&amp;up=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_up.png" alt="up" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php 
                                } if($list==$row_Re_mn['bch_max'] AND $list>0){ 
                                ?>
                                    <div align="center">
                                        <a href="admin_branch_save.php?action=no&amp;up=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_up.png" alt="up" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php } } ?>
                            </div>
                        </td>
                        <td><div align="center"><?php echo $row_Re_bch['bch_province']; ?></div></td>
                        <td><div align="center"><?php echo $row_Re_bch['bch_name']; ?></div></td>
                        <td>
                            <div align="left">
                                <?php 
                                echo $row_Re_bch['bch_add'];
                                if($row_Re_bch['bch_tel']!=""){ echo "<br>Tel : ".$row_Re_bch['bch_tel'];}
                                if($row_Re_bch['bch_fax']!=""){ echo "<br>Fax : ".$row_Re_bch['bch_fax'];}
                                ?>
                            </div>
                        </td>
                        <td>
                            <div align="left">
                                <?php echo "lat: ".$row_Re_bch['bch_lat']."<br>lng: ".$row_Re_bch['bch_lng']; ?>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_branch_edit.php?action=edit&bch_id=<?php echo $row_Re_bch['bch_id']; ?>">
                                <img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_branch_save.php?action=dele&bch_id=<?php echo $row_Re_bch['bch_id']; ?>" onclick = "return confirm('คุณต้องการลบสาขา <?php echo $row_Re_bch['bch_name']; ?> หรือไม่')">
                                <img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a>
                            </div>
                        </td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="9">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>