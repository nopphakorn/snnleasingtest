<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');

$query_Re_cb = "SELECT * FROM tb_car_brand ORDER BY cb_name ASC ";
$Re_cb=$mysqli->query($query_Re_cb);

?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>
<body>
	<?php if(!isset($_GET['save']) OR $_GET['save']!='sucess'){ ?>
	<div id="fancy_title"><i class="fa fa-list-alt"></i>&nbsp;เพิ่มชื่อกลุ่มรุ่นรถ</div>
    <div id="fancy_box">
		<form action="admin_car_save.php?action=group-insert" method="post" enctype="multipart/form-data" name="form_cg" id="form_cg">
			<p>
				ยี่ห้อรถ<br>
				<select name="cb_id" id="cb_id" onchange="list_g_bd('ct_id',form_cg.cb_id.value); check_cg(form_cg.cb_id.value, form_cg.ct_id.value, form_cg.cg_name.value, form_cg.h_cg_name.value);">
					<option value="">เลือกยี่ห้อรถ</option>
					<?php while($row_Re_cb=$Re_cb->fetch_assoc()){?>
					<option value="<?php echo $row_Re_cb['cb_id'];?>"><?php echo $row_Re_cb['cb_name'];?></option>
					<?php } ?>
				</select>
			</p>
			<p>
				ประเภทรถ<br>
				<select name="ct_id" id="ct_id" onchange="check_cg(form_cg.cb_id.value, form_cg.ct_id.value, form_cg.cg_name.value, form_cg.h_cg_name.value)">
					<option value="">เลือกประเภทรถ</option>
				</select>
			</p>
			<p>
				ชื่อกลุ่มรุ่นรถ<br>
				<input type="text" id="cg_name" name="cg_name" style="width:250px" 
				onblur="check_cg(form_cg.cb_id.value, form_cg.ct_id.value, form_cg.cg_name.value, form_cg.h_cg_name.value)">
				<span id="msg" class="tx_error"></span>
				<input type="hidden" name="h_cg_name" id="h_cg_name" />
			</p>
			<p><hr><input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/></p>
		</form>
	</div>
	<?php } ?>

	<?php if(isset($_GET['save']) AND $_GET['save']='sucess'){ ?>
	<div id="fancy_sucess">
		<br><br><br>
		<p><i class="fa fa-check-circle-o  fa-5x" aria-hidden="true"></i></p>
		<p>บันทึกรายการกลุ่มรุ่นรถเรียบร้อย</p>
	</div>
	<?php } ?>
</body>
</html>


