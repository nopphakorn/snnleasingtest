<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');

$c_name_chk = $_GET['c_name'];
$query_Re_c = "SELECT * FROM tb_content WHERE c_name = '$c_name_chk'";
$Re_c = $mysqli->query($query_Re_c);
$row_Re_c = $Re_c->fetch_assoc();
?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
    <script>
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('#c_detail', {
                langType : 'en',
                uploadJson : '../backoffice/tools/kindeditor/php/upload_json.php',
                fileManagerJson : '../backoffice/tools/kindeditor/php/file_manager_json.php',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                        'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                        '|', 'fullscreen', '/',
                        'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                        'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
            editor = K.create('#c_detail_en', {
                langType : 'en',
                uploadJson : '../backoffice/tools/kindeditor/php/upload_json.php',
                fileManagerJson : '../backoffice/tools/kindeditor/php/file_manager_json.php',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                        'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                        '|', 'fullscreen', '/',
                        'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                        'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
        });
    </script>
</head>

<body>
<div id="header"> <?php include("s_header.php"); ?></div>
<div id="nav"><a href="admin.php">Admin</a> &gt; แก้ไข<?php echo $row_Re_c['c_name_th']; ?></div>
<div id="side"><?php include('s_menu_side.php'); ?></div>
<div id="containner">
    <div id="main">
        <div id="main_content">
        	<div class="main_content_title"><i class="fa fa-pencil-square fa-lg" style="color:#3097FF"></i><?php echo " แก้ไข".$row_Re_c['c_name_th']; ?></div>
        	<div class="box_form">
                <form id="form1" name="form1" method="post" action="admin_content_save.php?action=edit&amp;c_name=<?php echo $row_Re_c['c_name']; ?>" enctype="multipart/form-data">
                    <div>
                        <input name="c_date" type="hidden" id="news_date" value="<?php echo date("Y-m-d H:i:s"); ?>" />
                        <input type="hidden" name="c_name" id="c_name" value="<?php echo $row_Re_c['c_name']; ?>">
                        หัวข้อ : <input type="text" name="c_name_th" id="c_name_th" style="width: 400px;" value="<?php echo $row_Re_c['c_name_th']; ?>">
                        <p><textarea name="c_detail" id="c_detail" style="height: 500px; width: 925px;">
                        <?php echo $row_Re_c['c_detail']; ?></textarea></p>
                        <hr>
                        Title : <input type="text" name="c_name_en" id="c_name_th" style="width: 400px;" value="<?php echo $row_Re_c['c_name_en']; ?>">
                        <p><textarea name="c_detail_en" id="c_detail_en" style="height: 500px; width: 925px;">
                        <?php echo $row_Re_c['c_detail_en']; ?></textarea></p>
                        <p><input type="submit" name="submit" id="submit" value="บันทึกข้อมูล"></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="footer">
    <?php include("s_footer.php"); ?>
</div>
</body>
</html>