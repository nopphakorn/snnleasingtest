<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

if (isset($_GET['v_id'])) {$v_id_chk = $_GET['v_id'];}
$sql_pro="SELECT * FROM tb_video WHERE v_id ='$v_id_chk'";
$Re_rv=$mysqli->query($sql_pro);
$row_Re_rv=$Re_rv->fetch_assoc();
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_video_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_video_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;เพิ่ม".$title;?></div>
                <div class="box_form">
                    <form action="admin_video_save.php?action=edit" method="POST" enctype="multipart/form-data" name="form_v" id="form_v" >
                        <table width="100%" border="0" align="center">
                            <tr>
                                <td width="120">แสดง/ไม่แสดง</td>
                                <td>
                                    <select name="v_status" id="v_status">
                                        <option value="" <?php if (!(strcmp("", $row_Re_rv['v_status']))) {echo "selected=\"selected\"";} ?>>เลือกสถานะ</option>
                                        <option value="1" <?php if (!(strcmp(1, $row_Re_rv['v_status']))) {echo "selected=\"selected\"";} ?>>แสดง</option>
                                        <option value="0" <?php if (!(strcmp(0, $row_Re_rv['v_status']))) {echo "selected=\"selected\"";} ?>>ไม่แสดง</option>
                                    </select>
                                </td>
							<tr>
                            <tr>
                                <td valign="top">ชื่อหัวข้อ</td>
                                <td valign="top"><input type="text" name="v_name" id="v_name" style="width: 400px;" value="<?php echo $row_Re_rv['v_name'];?>"></td>
                            </tr>
                            <tr>
                                <td valign="top">Youtube</td>
                                <td valign="top"><input type="text" name="v_url" id="v_url" style="width: 400px;" value="<?php echo $row_Re_rv['v_url'];?>"></td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr /></td>
                            </tr>
                            <tr>
                                <td colspan="2" >
									<input type="hidden" name="v_id" id="v_id" value="<?php echo $row_Re_rv['v_id'];?>">
									<input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>