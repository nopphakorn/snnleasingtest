<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST["i_id"])){$i_id=mysqli_real_escape_string($mysqli, $_POST["i_id"]);}
if(isset($_POST["i_type"])){$i_type=mysqli_real_escape_string($mysqli, $_POST["i_type"]);}
if(isset($_POST["i_date"])){$i_date=dateEng(mysqli_real_escape_string($mysqli, $_POST["i_date"]));}
if(isset($_POST["i_name"])){$i_name=mysqli_real_escape_string($mysqli, $_POST["i_name"]);}
if(isset($_POST["i_title"])){$i_title=mysqli_real_escape_string($mysqli, $_POST["i_title"]);}
if(isset($_POST["e_i_name"])){$e_i_name=mysqli_real_escape_string($mysqli, $_POST["e_i_name"]);}
if(isset($_POST["e_i_title"])){$e_i_title=mysqli_real_escape_string($mysqli, $_POST["e_i_title"]);}
if(isset($_POST["h_i_file"])){$h_i_file=mysqli_real_escape_string($mysqli, $_POST["h_i_file"]);}

if($_GET['action']=="insert"){

	$file=$_FILES["i_file"];
	$file=$_FILES['i_file']['tmp_name'];
	$file_name=$_FILES['i_file']['name'];
	
	if($file != ""){
        if(strchr($file_name,".")==".pdf" || strchr($file_name,".")==".PDF"){
            $new_name ="IR".date('His').rand(1000,9999); 
            $path = "../images/interest"; 
                                            
            $RenameFile=$new_name.strchr($file_name,".");
            if (copy( $file , "$path/$RenameFile" )){		
                unlink($file);
                $NewFile="$RenameFile";	
            }
        }else{
            $new_name ="IR".date('His').rand(1000,9999); 
            $path = "../images/interest"; 
                                            
            $images_photo = $file;
            $size=GetimageSize($images_photo);
            $size_w=$size[0];
            $size_h=$size[1];
            if($size_w>900){
                $width=900;					
                $height=round(($width/$size_w) * $size_h);
                
                if(strchr($file_name,".")==".jpg" || strchr($file_name,".")==".jpeg"){
                    $images_photo_orig = ImageCreateFromJPEG($images_photo);
                }else if(strchr($file_name,".")==".png"){
                    $images_photo_orig = ImageCreateFromPNG($images_photo);
                }else if(strchr($file_name,".")==".gif"){
                    $images_photo_orig = ImageCreateFromGIF($images_photo);
                }else if(strchr($file_name,".")==".JPG" || strchr($file_name,".")==".JPEG"){
                    $images_photo_orig = ImageCreateFromJPEG($images_photo);
                }else if(strchr($file_name,".")==".PNG"){
                    $images_photo_orig = ImageCreateFromPNG($images_photo);
                }else if(strchr($file_name,".")==".GIF"){
                    $images_photo_orig = ImageCreateFromGIF($images_photo);
                }
            
                $images_photo_new = ImageCreateTrueColor($width, $height); 
                ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
            
                if(strchr($file_name,".")==".jpg" || strchr($file_name,".")==".jpeg"){
                    ImageJPEG($images_photo_new,$images_photo, 100); 
                }else if(strchr($file_name,".")==".png"){
                    ImagePNG($images_photo_new,$images_photo, 100); 
                }else if(strchr($file_name,".")==".gif"){
                    ImageGIF($images_photo_new,$images_photo, 100); 
                }else if(strchr($file_name,".")==".JPG" || strchr($file_name,".")==".JPEG"){
                    ImageJPEG($images_photo_new,$images_photo, 100); 
                }else if(strchr($file_name,".")==".PNG"){
                    ImagePNG($images_photo_new,$images_photo, 100); 
                }else if(strchr($file_name,".")==".GIF"){
                    ImageGIF($images_photo_new,$images_photo, 100); 
                }
                                
                ImageDestroy($images_photo_orig);
                ImageDestroy($images_photo_new); 					
            }			
            $RenameFile=$new_name.strchr($file_name,".");
            if (copy( $file , "$path/$RenameFile" )){		
                unlink($file);
                $NewFile="$RenameFile";	
            }
        }
        
	}else{$NewFile="";}
	
	$sql_in="insert into tb_interest(i_type, i_date, i_name, i_title, e_i_name, e_i_title, i_file)values('$i_type','$i_date','$i_name','$i_title','$e_i_name','$e_i_title','$NewFile')";
    $Re_in=$mysqli->query($sql_in);
	exit;
	$GoTo = "admin_interest_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
	$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
	$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="edit"){	
	$file=$_FILES["i_file_edit"];
	$file=$_FILES['i_file_edit']['tmp_name'];
	$file_name=$_FILES['i_file_edit']['name'];

	if($file != ""){
		$sql_chk = "SELECT * FROM tb_interest WHERE i_id='$i_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file2=$row_Re_chk['i_file'];
		if($file2!=""){
			if (file_exists ("../file/interest/$file2")){unlink("../images/interest/$file2");}
        }
        
        if(strchr($file_name,".")==".pdf" || strchr($file_name,".")==".PDF"){
            $new_name ="IR".date('His').rand(1000,9999); 
            $path = "../images/interest"; 
                                            
            $RenameFile=$new_name.strchr($file_name,".");
            if (copy( $file , "$path/$RenameFile" )){		
                unlink($file);
                $NewFile="$RenameFile";	
            }
        }else{
            $new_name ="IR".date('His').rand(1000,9999); 
            $path = "../images/interest"; 
                                            
            $images_photo = $file;
            $size=GetimageSize($images_photo);
            $size_w=$size[0];
            $size_h=$size[1];
            if($size_w>900){
                $width=900;					
                $height=round(($width/$size_w) * $size_h);
                
                if(strchr($file_name,".")==".jpg" || strchr($file_name,".")==".jpeg"){
                    $images_photo_orig = ImageCreateFromJPEG($images_photo);
                }else if(strchr($file_name,".")==".png"){
                    $images_photo_orig = ImageCreateFromPNG($images_photo);
                }else if(strchr($file_name,".")==".gif"){
                    $images_photo_orig = ImageCreateFromGIF($images_photo);
                }else if(strchr($file_name,".")==".JPG" || strchr($file_name,".")==".JPEG"){
                    $images_photo_orig = ImageCreateFromJPEG($images_photo);
                }else if(strchr($file_name,".")==".PNG"){
                    $images_photo_orig = ImageCreateFromPNG($images_photo);
                }else if(strchr($file_name,".")==".GIF"){
                    $images_photo_orig = ImageCreateFromGIF($images_photo);
                }
            
                $images_photo_new = ImageCreateTrueColor($width, $height); 
                ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
            
                if(strchr($file_name,".")==".jpg" || strchr($file_name,".")==".jpeg"){
                    ImageJPEG($images_photo_new,$images_photo, 100); 
                }else if(strchr($file_name,".")==".png"){
                    ImagePNG($images_photo_new,$images_photo, 100); 
                }else if(strchr($file_name,".")==".gif"){
                    ImageGIF($images_photo_new,$images_photo, 100); 
                }else if(strchr($file_name,".")==".JPG" || strchr($file_name,".")==".JPEG"){
                    ImageJPEG($images_photo_new,$images_photo, 100); 
                }else if(strchr($file_name,".")==".PNG"){
                    ImagePNG($images_photo_new,$images_photo, 100); 
                }else if(strchr($file_name,".")==".GIF"){
                    ImageGIF($images_photo_new,$images_photo, 100); 
                }
                                
                ImageDestroy($images_photo_orig);
                ImageDestroy($images_photo_new); 					
            }			
            $RenameFile=$new_name.strchr($file_name,".");
            if (copy( $file , "$path/$RenameFile" )){		
                unlink($file);
                $NewFile="$RenameFile";	
            }
        }
	}else{$NewFile=$h_i_file;}
	
	$sql_update="UPDATE tb_interest SET i_type='$i_type', i_date='$i_date', i_name='$i_name', i_title='$i_title', e_i_name='$e_i_name', e_i_title='$e_i_title', i_file='$NewFile' WHERE i_id='$i_id'";
	$Re_update=$mysqli->query($sql_update);

	$GoTo = "admin_interest_edit.php?&i_id=".$i_id;
	if (isset($_SERVER['QUERY_STRING'])) {
	$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
	$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="dele"){
	if ((isset($_GET['i_id'])) && ($_GET['i_id'] != "")) {
		$i_id=mysqli_real_escape_string($mysqli, $_GET['i_id']);
		$i_type=mysqli_real_escape_string($mysqli, $_GET["type"]);
			
		$sql_chk= "SELECT * FROM tb_interest WHERE i_id='$i_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['i_file'];
		if($file!=""){
			if (file_exists ("../file/interest/$file")){unlink("../file/interest/$file");}
		}

		$sql_dele ="DELETE FROM tb_interest WHERE i_id='$i_id'";
		$Re_dele=$mysqli->query($sql_dele);
			
		$GoTo = "admin_interest_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}
$mysqli->close();
?>
