// http://jqueryvalidation.org/
$(document).ready(function() {
    $("#form_ad").validate({
        //admin
        rules: {
            admin_name: { required: true },
            admin_user: { required: true },
            admin_pass: { required: true },
            c_admin_pass: { required: true, equalTo: "#admin_pass" },
            o_admin_pass: { required: true },
            c_admin_pass_edit: { equalTo: "#n_admin_pass" },
        },
        messages: {
            admin_name: "กรุณาป้อน ชื่อ-นามสกุล",
            admin_user: "กรุณาป้อน Username",
            admin_pass: "กรุณาป้อน รหัสผ่าน",
            c_admin_pass: { required: "กรุณาป้อน รหัสผ่าน", equalTo: 'กรุณาป้อนให้เหมือนกับ Password ข้างบน' },
            o_admin_pass: "กรุณาป้อน Password เก่า",
            c_admin_pass_edit: { equalTo: 'กรุณาป้อนให้เหมือนกับ Password ใหม่' },
        },
    });

    $("#form_em").validate({
        //พนักงาน(ราคากลาง)
        rules: {
            em_name: { required: true },
            em_user: { required: true },
            em_pass: { required: true },
            c_em_pass: { required: true, equalTo: "#em_pass" },
            o_em_pass: { required: true },
            //em_pass_edit: { required: true },
            c_em_pass_edit: { equalTo: "#em_pass_edit" },
        },
        messages: {
            em_name: "กรุณาป้อน ชื่อ-นามสกุล",
            em_user: "กรุณาป้อน Username",
            em_pass: "กรุณาป้อน รหัสผ่าน",
            c_em_pass: { required: "กรุณาป้อน รหัสผ่าน", equalTo: 'กรุณาป้อนให้เหมือนกับ Password ข้างบน' },
            o_em_pass: "กรุณาป้อน Password เก่า",
            //em_pass_edit: "กรุณาป้อน Password ใหม่",
            c_em_pass_edit: { equalTo: 'กรุณาป้อนให้เหมือนกับ Password ใหม่' },
        },
    });

    $("#form_ban").validate({
        //banner
        rules: {
            ban_status: { required: true },
            ban_photo: { required: true, extension: "jpg|jpeg" },
            ban_photo_edit: { extension: "jpg|jpeg" },

        },
        messages: {
            ban_status: "กรุณาเลือก สถานะ",
            ban_photo: { required: "กรุณาเลือก รูปภาพ", extension: 'ไฟล์รูปภาพเฉพาะนามสกุล jpg หรือ jpeg เท่านั้น' },
            ban_photo_edit: { extension: 'ไฟล์รูปภาพเฉพาะนามสกุล jpg หรือ jpeg เท่านั้น' },
        },
    });

    $("#form_bch").validate({
        //สาขา
        rules: {
            bch_province: { required: true },
            bch_name: { required: true },
            bch_add: { required: true },
            bch_tel: { required: true },
            bch_photo: { extension: "jpg|jpeg" },
            bch_map: { extension: "jpg|jpeg" },
            bch_lat: { required: true, number: true },
            bch_lng: { required: true, number: true },
            bch_tag: { required: true },
        },
        messages: {
            bch_province: "กรุณาป้อน จังหวัดที่ตั้ง",
            bch_name: "กรุณาป้อน ชื่อสาขา",
            bch_add: "กรุณาป้อน สถานที่่ตั้ง",
            bch_tel: "กรุณาป้อน หมายเลขโทรศัพท์",
            bch_photo: { extension: 'ไฟล์รูปภาพเฉพาะนามสกุล jpg หรือ jpeg เท่านั้น' },
            bch_map: { extension: 'ไฟล์รูปภาพเฉพาะนามสกุล jpg หรือ jpeg เท่านั้น' },
            bch_lat: { required: "กรอก latitude", number: "กรุณาป้อน เป็นตัวเลขเท่านั้น" },
            bch_lng: { required: "กรอก longitude", number: "กรุณาป้อน เป็นตัวเลขเท่านั้น" },
            bch_tag: "กรุณาป้อน tags สำหรับค้นหา",
        },
    });

    $("#form_sv").validate({
        //service
        rules: {
            sv_status: { required: true },
            sv_name: { required: true },
            sv_photo: { required: true, extension: "jpg|jpeg" },
            sv_photo_edit: { extension: "jpg|jpeg" },
        },
        messages: {
            sv_status: "กรุณาเลือก สถานะ",
            sv_name: "กรุณาป้อน หัวข้อการบริการ",
            sv_photo: { required: "กรุณาเลือก รูปภาพการบริการ", extension: 'ไฟล์รูปภาพเฉพาะนามสกุล jpg หรือ jpeg เท่านั้น' },
            sv_photo_edit: { extension: 'ไฟล์รูปภาพเฉพาะนามสกุล jpg หรือ jpeg เท่านั้น' },
        },
    });

    $("#form_pd").validate({
        rules: {
            pro_status: { required: true },
            pro_type: { required: true },
            pro_brand: { required: true },
            pro_model: { required: true },
            pro_year: { required: true, number: true },
            pro_cc: { required: true, number: true },
            pro_gear: { required: true },
            pro_mile: { required: true, number: true },
            pro_color: { required: true },
            pro_price: { required: true },
        },
        messages: {
            pro_status: "เลือกสถานะ",
            pro_type: "เลือกประเภทรถ",
            pro_brand: "กรอกยี่ห้อรถ",
            pro_model: "กรอกรุ่นรถ",
            pro_year: { required: "กรอกปีรถ", number: "เป็นตัวเลขเท่านั้น" },
            pro_cc: { required: "กรอกขนาดเครื่องยนต์", number: "เป็นตัวเลขเท่านั้น" },
            pro_gear: "เลือกระบบเกียร์",
            pro_mile: { required: "กรอกเลขไมล์(กม)", number: "เป็นตัวเลขเท่านั้น" },
            pro_color: "กรอกสีรถ",
            pro_price: "กรอกราคาขาย",
        }
    });

    $("#form_gal").validate({
        rules: {
            gal_name: { required: true },
            gal_photo: { required: true, extension: "jpg|jpeg" },
            gal_photo_edit: { extension: "jpg|jpeg" },
        },
        messages: {
            gal_name: "กรุณาป้อน ชื่อแกลเลอรี่",
            gal_photo: { required: "กรุณาเลือก ภาพปกแกลเลอรี่", extension: 'ไฟล์รูปภาพเฉพาะนามสกุล jpg หรือ jpeg เท่านั้น' },
            gal_photo_edit: { extension: 'ไฟล์รูปภาพเฉพาะนามสกุล jpg หรือ jpeg เท่านั้น' },
        }
    });

    $("#form_ct").validate({
        rules: {
            ct_name: { required: true },
        },
        messages: {
            ct_name: "กรุณาป้อน ชื่อรายการประเภทรถ",
        }
    });

    $("#form_cb").validate({
        rules: {
            cb_name: { required: true },
            'ct_id_list[]': { required: true }
        },
        messages: {
            cb_name: "กรุณาป้อน ชื่อยี่ห้อรถ",
            'ct_id_list[]': "กรุณาระบุประเภทรถ"
        },
        // errorPlacement: function(error, element) {
        //     var name = $(element).attr("name");
        //     error.appendTo($("#" + name + "_validate"));
        // },
    });

    $("#form_cg").validate({
        rules: {
            cg_name: { required: true },
            cb_id: { required: true },
            ct_id: { required: true },
        },
        messages: {
            cg_name: "กรุณาป้อน ชื่อกลุ่มรุ่นรถ",
            cb_id: "กรุณาเลือก ยี่ห้อรถ",
            ct_id: "กรุณาเลือก ประเภทรถ",
        }
    });

    $("#form_cm").validate({
        rules: {
            cm_name: { required: true },
            cb_id: { required: true },
            cg_id: { required: true },
        },
        messages: {
            cm_name: "กรุณาป้อน ชื่อรุ่นรถ",
            cb_id: "กรุณาเลือก ยี่ห้อรถ",
            cg_id: "กรุณาเลือก กลุ่มรุ่นรถ",
        }
    });

    $("#form_ca").validate({
        rules: {
            ct_id: { required: true },
            cb_id: { required: true },
            cg_id: { required: true },
            cm_id: { required: true },
            c_year: { required: true },
            c_gear: { required: true },
            c_price: { required: true },
            c_photo1: { extension: "jpg|jpeg" },
            c_photo2: { extension: "jpg|jpeg" },
        },
        messages: {
            ct_id: "กรุณาเลือก ประเภท",
            cb_id: "กรุณาเลือก ยี่ห้อรถ",
            cg_id: "กรุณาเลือก กลุ่มรุ่นรถ",
            cm_id: "กรุณาเลือก รุ่นรถ",
            c_year: "กรุณาระบุ ปีรถ",
            c_gear: "กรุณาเลือก ระบบเกียร์",
            c_price: "กรุณาระบุ ราคากลาง",
            c_photo1: { extension: 'ไฟล์รูปภาพเฉพาะ jpg,jpeg เท่านั้น' },
            c_photo2: { extension: 'ไฟล์รูปภาพเฉพาะ jpg,jpeg เท่านั้น' },
        }
    });

    $("#form_rv").validate({
        rules: {
            rv_status: { required: true },
            rv_photo: { required: true, extension: "jpg|jpeg|png" },
            rv_photo_edit: { extension: "jpg|jpeg|png" },
        },
        messages: {
            rv_status: "กรุณาเลือก ประเภท",
            rv_photo: { required: "กรุณาเลือกรูปภาพ", extension: 'ไฟล์รูปภาพเฉพาะ jpg, jpeg หรือ png เท่านั้น' },
            rv_photo_edit: { extension: 'ไฟล์รูปภาพเฉพาะ jpg,jpeg หรือ png เท่านั้น' },
        }
    });

    $("#form_v").validate({

        rules: {
            v_status: { required: true },
            v_url: { required: true },
        },
        messages: {
            v_status: "กรุณาเลือก ประเภท",
            v_url: "กรุณากรอก URL Youtube",
        }
    });

    $("#form_cn").validate({

        rules: {
            cn_name: { required: true },
            cn_type: { required: true },
            cn_company: { required: true },
            cn_address: { required: true },
            cn_tel: { required: true },
            //cn_fax: { required: true },
            //cn_web: { required: true },
            cn_logo: { required: true, extension: "jpg|jpeg|png" },
            cn_logo_edit: { extension: "jpg|jpeg|png" },
        },
        messages: {
            cn_name: "กรุณากรอก ชื่อธุรกิจ",
            cn_type: "กรุณากรอก ประเภทธุรกิจ",
            cn_company: "กรุณากรอก ชื่อบริษัท",
            cn_address: "กรุณากรอก ที่อยู่",
            cn_tel: "กรุณากรอก เบอร์โทรศัพท์",
            //cn_fax: "กรุณากรอก เบอร์แฟ๊กซ์",
            //cn_web: "กรุณากรอก URL เว็บไซต์",
            cn_logo: { required: "กรุณาเลือกรูป Logo", extension: 'ไฟล์รูปภาพเฉพาะ jpg หรือ png เท่านั้น' },
            cn_logo_edit: { extension: 'ไฟล์รูปภาพเฉพาะ jpg หรือ png เท่านั้น' },
        }
    });

    $("#form_tffa").validate({
        rules: {
            a_status: { required: true },
            a_name: { required: true },
            a_title: { required: true },
            a_photo: { required: true, extension: "jpg|jpeg" },
            a_photo_edit: { extension: "jpg|jpeg" },
        },
        messages: {
            a_status: "กรุณาเลือก สถานะ",
            a_name: "กรุณาป้อน ชื่อบทความ",
            a_title: "กรุณาป้อน หัวข้อบทความ",
            a_photo: { required: "กรุณาเลือก รูปภาพการบริการ", extension: 'ไฟล์รูปภาพเฉพาะ jpg หรือ jpeg เท่านั้น' },
            a_photo_edit: { extension: 'ไฟล์รูปภาพเฉพาะ jpg หรือ jpeg เท่านั้น' },
        },
    });

    $("#form_job").validate({
        rules: {
            j_title: { required: true },
            j_function: { required: true },
            j_detail: { required: true },
            j_property: { required: true },
            j_location: { required: true },
        },
        messages: {
            j_title: "กรุณากรอก JOB TITLE",
            j_function: "กรุณากรอก JOB FUNCTION",
            j_detail: "กรุณากรอก รายละเอียด",
            j_property: "กรุณากรอก คุณสมบัติ",
            j_location: "กรุณากรอก สถานที่ทำงาน",
        },
    });

    $("#form_iv").validate({
        rules: {
            iv_name: { required: true },
            iv_file: { required: true, extension: "pdf" },
            iv_file_edit: { extension: "pdf" },
        },
        messages: {
            iv_name: "กรุณาป้อน หัวข้อ",
            iv_file: { required: "กรุณาเลือกไฟล์ข้อมูล", extension: 'เฉพาะไฟล์ PDF เท่านั้น' },
            iv_file_edit: { extension: 'เฉพาะไฟล์ PDF เท่านั้น' },
        },
    });

    $("#form_pu").validate({
        //ผู้เข้าใช้งาน(ขายรถยนต์)
        rules: {
            pro_u_name: { required: true },
            pro_u_user: { required: true },
            pro_u_pass: { required: true },
            c_pro_u_pass: { required: true, equalTo: "#pro_u_pass" },
            o_pro_u_pass: { required: true },
            c_pro_u_pass_edit: { equalTo: "#pro_u_pass_edit" },
        },
        messages: {
            pro_u_name: "กรุณาป้อน ชื่อ-นามสกุล",
            pro_u_user: "กรุณาป้อน Username",
            pro_u_pass: "กรุณาป้อน รหัสผ่าน",
            c_pro_u_pass: { required: "กรุณาป้อน รหัสผ่าน", equalTo: 'กรุณาป้อนให้เหมือนกับ Password ข้างบน' },
            o_pro_u_pass: "กรุณาป้อน Password เก่า",
            c_pro_u_pass_edit: { equalTo: 'กรุณาป้อนให้เหมือนกับ Password ใหม่' },
        },
    });

    $("#form_pm").validate({
        rules: {
            pm_status: { required: true },
            pm_title: { required: true },
            pm_photo: { required: true, extension: "jpg|jpeg" },
            pm_photo_edit: { extension: "jpg|jpeg" },
        },
        messages: {
            pm_status: "เลือกสถานะ",
            pm_title: "กรอกชื่อรายการโปรโมชั่น",
            pm_photo: { required: "กรุณาเลือก รูปภาพปก", extension: 'เฉพาะนามสกุล jpg หรือ jpeg เท่านั้น' },
            pm_photo_edit: { extension: 'เฉพาะนามสกุล jpg หรือ jpeg เท่านั้น' },
        },
    });

    $("#form_aw").validate({
        rules: {
            a_que: { required: true },
            a_anw: { required: true },
        },
        messages: {
            a_que: "กรอกคำถาม",
            a_anw: "กรอกคำตอบ",
        },
    });

    $("#from_searchWeb").validate({
        rules: {
            s_name: { required: true },
            s_url: { required: true },
            s_tag: { required: true },
        },
        messages: {
            s_name: "กรอกหัวข้อ",
            s_url: "กรอก URL",
            s_tag: "กรอก คำค้นหา",
        },
    });

    //---------------------------------------------
    //edit car group
    $("#form_edit_group").validate({
        rules: {
            ct_id: { require_from_group: [1, ".g_search"] },
            cb_id: { require_from_group: [1, ".g_search"] },
            cg_id: { require_from_group: [1, ".g_search"] },
            cm_id: { require_from_group: [1, ".g_search"] },
            c_year_s: {
                require_from_group: [1, ".g_search"],
                number: true,
                minlength: 4,
            },
            c_year_e: {
                require_from_group: [1, ".g_search"],
                number: true,
                minlength: 4,
            },
            c_gear: { require_from_group: [1, ".g_search"] },
            c_price: {
                required: true,
                number: true,

            },
        },
        messages: {
            ct_id: { require_from_group: "ระบุอย่างน้อย 1 รายการ" },
            cb_id: { require_from_group: "ระบุอย่างน้อย 1 รายการ" },
            cg_id: { require_from_group: "ระบุอย่างน้อย 1 รายการ" },
            cm_id: { require_from_group: "ระบุอย่างน้อย 1 รายการ" },
            c_year_s: {
                require_from_group: "ระบุอย่างน้อย 1 รายการ",
                number: "ระบุเป็นตัวเลข ค.ศ. เท่านั้น",
                minlength: "ระบุ 4 ตัวเลข(เช่น 2016)",
            },
            c_year_e: {
                require_from_group: "ระบุอย่างน้อย 1 รายการ",
                number: "ระบุเป็นตัวเลข ค.ศ. เท่านั้น",
                minlength: "ระบุ 4 ตัวเลข(เช่น 2016)",
            },
            c_gear: { require_from_group: "ระบุอย่างน้อย 1 รายการ" },
            c_price: {
                required: "ระบุจำนวนเงินที่ต้องการปรับ",
                number: "ระบุเป็นตัวเลขเท่านั้น",
            },
        },
        submitHandler: function(form) {
            $.ajax({
                url: "admin_car_all_edit_group_save.php",
                type: 'POST',
                data: $('#form_edit_group').serialize(),
                success: function(data) {
                    //alert(data);
                    if (data == 'ture') {
                        $('#form_edit_group')[0].reset();
                        $('#success_bar').show();
                        $('#success_bar').delay(4000).hide(0);
                        $('#error_bar').hide();
                    } else if (data == 'false') {
                        $('#success_bar').hide();
                        $('#error_bar').show();
                    }
                }
            });
        },
    });

    $("#form_ir").validate({
        rules: {
            i_type: { required: true },
            i_date: { required: true },
            i_name: { required: true },
            e_i_name: { required: true },
            i_file: { required: true, extension: "pdf|jpg|jpeg" },
            i_file_edit: { extension: "pdf|jpg|jpeg" },
        },
        messages: {
            i_type: "เลือกประเภทไฟล์",
            i_date: "เลือกวันที่ประกาศและบังคับใช้",
            i_name: "กรอกหัวข้อ",
            e_i_name: "กรอกหัวข้อ",
            i_file: { required: "เลือกไฟล์เนื้อหา", extension: "เฉพาะไฟล์ PDF หรือ JPG เท่านั้น" },
            i_file_edit: { extension: "" },
        },
    });


}); // JavaScript Document