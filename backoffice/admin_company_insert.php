<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="insert";
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_company_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_company_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;เพิ่มรายการ".$title;?></div>
                <div class="box_form">
                    <form action="admin_company_save.php?action=insert" method="POST" enctype="multipart/form-data" name="form_cn" id="form_cn" >
                        <table width="100%" border="0" align="center">
							<tr>
                                <td>Logo</td>
                                <td>
                                    <input type="file" name="cn_logo" id="cn_logo" />
                                </td>
                            </tr>
                            <tr>
                                <td width="120">ชื่อธุรกิจ</td>
                                <td><input type="text" name="cn_name" id="cn_name" size="40" /></td>
							</tr>
							<tr>
                                <td>ประเภทธุรกิจ</td>
                                <td><input type="text" name="cn_type" id="cn_type" size="40" /></td>
                            </tr>
                            <tr>
                                <td>บริษัท</td>
                                <td><input type="text" name="cn_company" id="cn_company" size="40" /></td>
                            </tr>
                            <tr>
                                <td valign="top">สถานที่ตั้ง</td>
                                <td><textarea name="cn_address" id="cn_address" cols="58" rows="5"></textarea></td>
                            </tr>
                            <tr>
                                <td>โทรศัพท์</td>
                                <td><input type="text" name="cn_tel" id="cn_tel"/></td>
                            </tr>
                            <tr>
                                <td>แฟ๊กซ์</td>
                                <td><input type="text" name="cn_fax" id="cn_fax"/></td>
                            </tr>
                            <tr>
                                <td>เว็บไซต์</td>
                                <td>
                                    <input type="text" name="cn_web" id="cn_web" size="40" />
                                </td>
                            </tr>
                            <tr>
                                <td>Facebook</td>
                                <td>
                                    <input type="text" name="cn_fb" id="cn_fb" size="40" />
                                </td>
                            </tr>
                            <tr>
                                <td>Line@</td>
                                <td>
                                    <input type="text" name="cn_line" id="cn_line" size="40" />
                                </td>
                            </tr>
                            <tr>
                                <td>Youtube</td>
                                <td>
                                    <input type="text" name="cn_youtube" id="cn_youtube" size="40" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" ><hr /></td>
                            </tr>
                            <tr>
                                <td width="120">Business name</td>
                                <td><input type="text" name="en_cn_name" id="en_cn_name" size="40" /></td>
							</tr>
							<tr>
                                <td>Business type</td>
                                <td><input type="text" name="en_cn_type" id="en_cn_type" size="40" /></td>
                            </tr>
                            <tr>
                                <td>Company</td>
                                <td><input type="text" name="en_cn_company" id="en_cn_company" size="40" /></td>
                            </tr>
                            <tr>
                                <td valign="top">Address</td>
                                <td><textarea name="en_cn_address" id="en_cn_address" cols="58" rows="5"></textarea></td>
                            </tr>
                            <tr>
                                <td colspan="2" ><hr /></td>
                            </tr>
                            <tr>
                                <td colspan="2" >
                                    <input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
