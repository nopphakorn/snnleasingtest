<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

$s_id=mysqli_real_escape_string($mysqli, $_GET['s_id']);
$query_Re_s = "SELECT * FROM tb_search WHERE s_id='$s_id' ";
$Re_s=$mysqli->query($query_Re_s);
$row_Re_s = mysqli_fetch_assoc($Re_s);
$totalRows_Re_s=$Re_s->num_rows;

if (!$Re_s) {printf("Error: %s\n", $mysqli->error);}

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_search_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_search_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo " แก้ไข ".$title;?></div>
                <div class="box_form">
                    <form action="admin_search_save.php?action=edit" method="POST" enctype="multipart/form-data" name="from_searchWeb" id="from_searchWeb" >
                        <table width="100%">
							<tr>
                                <td width="120" valign="top">หัวข้อ</td>
                                <td><input type="text" id="s_name" name="s_name" style="width:600px" value="<?php echo $row_Re_s['s_name'];?>"></td>
                            </tr>
                            <tr>
                                <td width="120" valign="top">URL</td>
                                <td><input type="text" id="s_url" name="s_url" style="width:600px" value="<?php echo $row_Re_s['s_url'];?>"></td>
							</tr>
							<tr>
                                <td width="120" valign="top">คำค้นหา</td>
                                <td><textarea name="s_tag" id="s_tag" style="width:600px; height:100px;"><?php echo $row_Re_s['s_tag'];?></textarea></td>
							</tr>
                            <tr>
                                <td colspan="2"><hr>
								<input type="hidden" id="s_id" name="s_id" value="<?php echo $row_Re_s['s_id'];?>">
								<input name="button" type="submit" id="button" value="บันทึกแก้ไข" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>