<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST["ban_id"])){$ban_id=mysqli_real_escape_string($mysqli, $_POST["ban_id"]);}
if(isset($_POST["ban_no"])){$ban_no=mysqli_real_escape_string($mysqli, $_POST["ban_no"]);}
if(isset($_POST["ban_url"])){$ban_url=mysqli_real_escape_string($mysqli, $_POST["ban_url"]);}
if(isset($_POST["ban_status"])){$ban_status=mysqli_real_escape_string($mysqli, $_POST["ban_status"]);}
if(isset($_POST["h_ban_photo"])){$h_photo=mysqli_real_escape_string($mysqli, $_POST["h_ban_photo"]);}
if(isset($_POST["h_ban_photo_md"])){$h_photo_md=mysqli_real_escape_string($mysqli, $_POST["h_ban_photo_md"]);}
if(isset($_POST["h_ban_photo_sm"])){$h_photo_sm=mysqli_real_escape_string($mysqli, $_POST["h_ban_photo_sm"]);}

if($_GET['action']=="no"){
	if(isset($_GET["up"])){$up=$_GET['up'];}else{$up=0;}
	if(isset($_GET["down"])){$down=$_GET['down'];}else{$down=0;}
	if(isset($_GET["id"])){$id=$_GET['id'];}
	if(isset($_GET["list"])){$list_new=$_GET['list'];}

	if($up==1){
		$list_old=$list_new-$up;
		$list_new=$list_new;
		$usql="update tb_banner set ban_no='$list_new' where ban_no='$list_old'";
		$Re_usql=$mysqli->query($usql);
		if (!$Re_usql) {printf("Error: %s\n", $mysqli->error);}
		
		$dsql="update tb_banner set ban_no='$list_old' where ban_id='$id'";
		$Re_dsql=$mysqli->query($dsql);
		if (!$Re_dsql) {printf("Error: %s\n", $mysqli->error);}
		
		$GoTo = "admin_banner_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>"; 
	}

	if($down==1){
		$list_old=$down+$list_new;
		$list_new=$list_new;
		$usql="update tb_banner set ban_no='$list_new' where ban_no='$list_old'";
		$Re_usql=$mysqli->query($usql);
		if (!$Re_usql) {printf("Error: %s\n", $mysqli->error);}
		
		$dsql="update tb_banner set ban_no='$list_old' where ban_id='$id'";
		$Re_dsql=$mysqli->query($dsql);
		if (!$Re_dsql) {printf("Error: %s\n", $mysqli->error);}
		
		$GoTo = "admin_banner_list.php";
		  if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		  }
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>"; 
	}
}

if($_GET['action']=="status"){
	$ban_id = $_GET['ban_id'];
	$ban_status = $_GET['ban_status'];

	$sql="UPDATE tb_banner SET ban_status='$ban_status' WHERE ban_id='$ban_id'";
	$Re_st=$mysqli->query($sql);
	
	$GoTo = "admin_banner_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
		}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="insert"){
	$photo=$_FILES["ban_photo"];
	$photo=$_FILES['ban_photo']['tmp_name'];
	$photo_name=$_FILES['ban_photo']['name'];

	$photo_md=$_FILES["ban_photo_md"];
	$photo_md=$_FILES['ban_photo_md']['tmp_name'];
	$photo_md_name=$_FILES['ban_photo_md']['name'];

	$photo_sm=$_FILES["ban_photo_sm"];
	$photo_sm=$_FILES['ban_photo_sm']['tmp_name'];
	$photo_sm_name=$_FILES['ban_photo_sm']['name'];
	
	$sql="SELECT max(ban_no) AS ban_max FROM tb_banner";
	$Re_sql=$mysqli->query($sql);
	$row_Re_sql=$Re_sql->fetch_assoc();
	if($row_Re_sql['ban_max']==0){$ban_no=1;}
	else{$ban_no=$row_Re_sql['ban_max']+1;}

	if($photo != ""){
		$new_name ="B".date('His').rand(1000,9999); 
		$path = "../images/banner"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>1300){
			$width=1300;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".png"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}else if(strchr($photo_name,".")==".gif"){
				$images_photo_orig = ImageCreateFromGIF($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".PNG"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}else if(strchr($photo_name,".")==".GIF"){
				$images_photo_orig = ImageCreateFromGIF($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".png"){
				ImagePNG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".gif"){
				ImageGIF($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".PNG"){
				ImagePNG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".GIF"){
				ImageGIF($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 					
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic="";}

	if($photo_md != ""){
		$new_name ="BMD".date('His').rand(1000,9999); 
		$path = "../images/banner"; 
										
		$images_md_photo = $photo_md;
		$size=GetimageSize($images_md_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>768){
			$width=768;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_md_name,".")==".jpg" || strchr($photo_md_name,".")==".jpeg"){
				$images_md_photo_orig = ImageCreateFromJPEG($images_md_photo);
			}else if(strchr($photo_md_name,".")==".png"){
				$images_md_photo_orig = ImageCreateFromPNG($images_md_photo);
			}else if(strchr($photo_md_name,".")==".gif"){
				$images_md_photo_orig = ImageCreateFromGIF($images_md_photo);
			}else if(strchr($photo_md_name,".")==".JPG" || strchr($photo_md_name,".")==".JPEG"){
				$images_md_photo_orig = ImageCreateFromJPEG($images_md_photo);
			}else if(strchr($photo_md_name,".")==".PNG"){
				$images_md_photo_orig = ImageCreateFromPNG($images_md_photo);
			}else if(strchr($photo_md_name,".")==".GIF"){
				$images_md_photo_orig = ImageCreateFromGIF($images_md_photo);
			}
		
			$images_md_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_md_photo_new, $images_md_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_md_name,".")==".jpg" || strchr($photo_md_name,".")==".jpeg"){
				ImageJPEG($images_md_photo_new,$images_md_photo, 100); 
			}else if(strchr($photo_md_name,".")==".png"){
				ImagePNG($images_md_photo_new,$images_md_photo, 100); 
			}else if(strchr($photo_md_name,".")==".gif"){
				ImageGIF($images_md_photo_new,$images_md_photo, 100); 
			}else if(strchr($photo_md_name,".")==".JPG" || strchr($photo_md_name,".")==".JPEG"){
				ImageJPEG($images_md_photo_new,$images_md_photo, 100); 
			}else if(strchr($photo_md_name,".")==".PNG"){
				ImagePNG($images_md_photo_new,$images_md_photo, 100); 
			}else if(strchr($photo_md_name,".")==".GIF"){
				ImageGIF($images_md_photo_new,$images_md_photo, 100); 
			}
							
			ImageDestroy($images_md_photo_orig);
			ImageDestroy($images_md_photo_new); 					
		}			
		$RenameFile=$new_name.strchr($photo_md_name,".");
		if (copy( $photo_md , "$path/$RenameFile" )){		
			unlink($photo_md);
			$pathPic_md="$RenameFile";	
		}
	}else{$pathPic_md="";}

	if($photo_sm != ""){
		$new_name ="BSM".date('His').rand(1000,9999); 
		$path = "../images/banner"; 
										
		$images_sm_photo = $photo_sm;
		$size=GetimageSize($images_sm_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>576){
			$width=576;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_sm_name,".")==".jpg" || strchr($photo_sm_name,".")==".jpeg"){
				$images_sm_photo_orig = ImageCreateFromJPEG($images_sm_photo);
			}else if(strchr($photo_sm_name,".")==".png"){
				$images_sm_photo_orig = ImageCreateFromPNG($images_sm_photo);
			}else if(strchr($photo_sm_name,".")==".gif"){
				$images_sm_photo_orig = ImageCreateFromGIF($images_sm_photo);
			}else if(strchr($photo_sm_name,".")==".JPG" || strchr($photo_sm_name,".")==".JPEG"){
				$images_sm_photo_orig = ImageCreateFromJPEG($images_sm_photo);
			}else if(strchr($photo_sm_name,".")==".PNG"){
				$images_sm_photo_orig = ImageCreateFromPNG($images_sm_photo);
			}else if(strchr($photo_sm_name,".")==".GIF"){
				$images_sm_photo_orig = ImageCreateFromGIF($images_sm_photo);
			}
		
			$images_sm_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_sm_photo_new, $images_sm_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_sm_name,".")==".jpg" || strchr($photo_sm_name,".")==".jpeg"){
				ImageJPEG($images_sm_photo_new,$images_sm_photo, 100); 
			}else if(strchr($photo_sm_name,".")==".png"){
				ImagePNG($images_sm_photo_new,$images_sm_photo, 100); 
			}else if(strchr($photo_sm_name,".")==".gif"){
				ImageGIF($images_sm_photo_new,$images_sm_photo, 100); 
			}else if(strchr($photo_sm_name,".")==".JPG" || strchr($photo_sm_name,".")==".JPEG"){
				ImageJPEG($images_sm_photo_new,$images_sm_photo, 100); 
			}else if(strchr($photo_sm_name,".")==".PNG"){
				ImagePNG($images_sm_photo_new,$images_sm_photo, 100); 
			}else if(strchr($photo_sm_name,".")==".GIF"){
				ImageGIF($images_sm_photo_new,$images_sm_photo, 100); 
			}
							
			ImageDestroy($images_sm_photo_orig);
			ImageDestroy($images_sm_photo_new); 					
		}			
		$RenameFile=$new_name.strchr($photo_sm_name,".");
		if (copy( $photo_sm , "$path/$RenameFile" )){		
			unlink($photo_sm);
			$pathPic_sm="$RenameFile";	
		}
	}else{$pathPic_sm="";}
	
	$sql_in="insert into tb_banner(ban_no, ban_status, ban_url, ban_photo, ban_photo_md, ban_photo_sm)
	values('$ban_no', '$ban_status', '$ban_url', '$pathPic', '$pathPic_md', '$pathPic_sm')";
	$Re_in=$mysqli->query($sql_in);
	
	$GoTo = "admin_banner_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
	$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
	$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="edit"){	
	$photo=$_FILES["ban_photo_edit"];
	$photo=$_FILES['ban_photo_edit']['tmp_name'];
	$photo_name=$_FILES['ban_photo_edit']['name'];

	$photo_md=$_FILES["ban_photo_md_edit"];
	$photo_md=$_FILES['ban_photo_md_edit']['tmp_name'];
	$photo_md_name=$_FILES['ban_photo_md_edit']['name'];

	$photo_sm=$_FILES["ban_photo_sm_edit"];
	$photo_sm=$_FILES['ban_photo_sm_edit']['tmp_name'];
	$photo_sm_name=$_FILES['ban_photo_sm_edit']['name'];

	if($photo != ""){
		$sql_chk = "SELECT * FROM tb_banner WHERE ban_id='$ban_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['ban_photo'];
		if($file!=""){
			if (file_exists ("../images/banner/$file")){unlink("../images/banner/$file");}
		}

		$new_name ="B".date('His').rand(1000,9999);
		$path = "../images/banner"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>1300){
			$width=1300;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".png"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}else if(strchr($photo_name,".")==".gif"){
				$images_photo_orig = ImageCreateFromGIF($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".PNG"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}else if(strchr($photo_name,".")==".GIF"){
				$images_photo_orig = ImageCreateFromGIF($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".png"){
				ImagePNG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".gif"){
				ImageGIF($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".PNG"){
				ImagePNG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".GIF"){
				ImageGIF($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 					
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic=$h_photo;}

	if($photo_md != ""){
		$new_name ="BMD".date('His').rand(1000,9999); 
		$path = "../images/banner"; 
										
		$images_md_photo = $photo_md;
		$size=GetimageSize($images_md_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>768){
			$width=768;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_md_name,".")==".jpg" || strchr($photo_md_name,".")==".jpeg"){
				$images_md_photo_orig = ImageCreateFromJPEG($images_md_photo);
			}else if(strchr($photo_md_name,".")==".png"){
				$images_md_photo_orig = ImageCreateFromPNG($images_md_photo);
			}else if(strchr($photo_md_name,".")==".gif"){
				$images_md_photo_orig = ImageCreateFromGIF($images_md_photo);
			}else if(strchr($photo_md_name,".")==".JPG" || strchr($photo_md_name,".")==".JPEG"){
				$images_md_photo_orig = ImageCreateFromJPEG($images_md_photo);
			}else if(strchr($photo_md_name,".")==".PNG"){
				$images_md_photo_orig = ImageCreateFromPNG($images_md_photo);
			}else if(strchr($photo_md_name,".")==".GIF"){
				$images_md_photo_orig = ImageCreateFromGIF($images_md_photo);
			}
		
			$images_md_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_md_photo_new, $images_md_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_md_name,".")==".jpg" || strchr($photo_md_name,".")==".jpeg"){
				ImageJPEG($images_md_photo_new,$images_md_photo, 100); 
			}else if(strchr($photo_md_name,".")==".png"){
				ImagePNG($images_md_photo_new,$images_md_photo, 100); 
			}else if(strchr($photo_md_name,".")==".gif"){
				ImageGIF($images_md_photo_new,$images_md_photo, 100); 
			}else if(strchr($photo_md_name,".")==".JPG" || strchr($photo_md_name,".")==".JPEG"){
				ImageJPEG($images_md_photo_new,$images_md_photo, 100); 
			}else if(strchr($photo_md_name,".")==".PNG"){
				ImagePNG($images_md_photo_new,$images_md_photo, 100); 
			}else if(strchr($photo_md_name,".")==".GIF"){
				ImageGIF($images_md_photo_new,$images_md_photo, 100); 
			}
							
			ImageDestroy($images_md_photo_orig);
			ImageDestroy($images_md_photo_new); 					
		}			
		$RenameFile=$new_name.strchr($photo_md_name,".");
		if (copy( $photo_md , "$path/$RenameFile" )){		
			unlink($photo_md);
			$pathPic_md="$RenameFile";	
		}
	}else{$pathPic_md=$h_photo_md;}

	if($photo_sm != ""){
		$new_name ="BSM".date('His').rand(1000,9999); 
		$path = "../images/banner"; 
										
		$images_sm_photo = $photo_sm;
		$size=GetimageSize($images_sm_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>576){
			$width=576;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_sm_name,".")==".jpg" || strchr($photo_sm_name,".")==".jpeg"){
				$images_sm_photo_orig = ImageCreateFromJPEG($images_sm_photo);
			}else if(strchr($photo_sm_name,".")==".png"){
				$images_sm_photo_orig = ImageCreateFromPNG($images_sm_photo);
			}else if(strchr($photo_sm_name,".")==".gif"){
				$images_sm_photo_orig = ImageCreateFromGIF($images_sm_photo);
			}else if(strchr($photo_sm_name,".")==".JPG" || strchr($photo_sm_name,".")==".JPEG"){
				$images_sm_photo_orig = ImageCreateFromJPEG($images_sm_photo);
			}else if(strchr($photo_sm_name,".")==".PNG"){
				$images_sm_photo_orig = ImageCreateFromPNG($images_sm_photo);
			}else if(strchr($photo_sm_name,".")==".GIF"){
				$images_sm_photo_orig = ImageCreateFromGIF($images_sm_photo);
			}
		
			$images_sm_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_sm_photo_new, $images_sm_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_sm_name,".")==".jpg" || strchr($photo_sm_name,".")==".jpeg"){
				ImageJPEG($images_sm_photo_new,$images_sm_photo, 100); 
			}else if(strchr($photo_sm_name,".")==".png"){
				ImagePNG($images_sm_photo_new,$images_sm_photo, 100); 
			}else if(strchr($photo_sm_name,".")==".gif"){
				ImageGIF($images_sm_photo_new,$images_sm_photo, 100); 
			}else if(strchr($photo_sm_name,".")==".JPG" || strchr($photo_sm_name,".")==".JPEG"){
				ImageJPEG($images_sm_photo_new,$images_sm_photo, 100); 
			}else if(strchr($photo_sm_name,".")==".PNG"){
				ImagePNG($images_sm_photo_new,$images_sm_photo, 100); 
			}else if(strchr($photo_sm_name,".")==".GIF"){
				ImageGIF($images_sm_photo_new,$images_sm_photo, 100); 
			}
							
			ImageDestroy($images_sm_photo_orig);
			ImageDestroy($images_sm_photo_new); 					
		}			
		$RenameFile=$new_name.strchr($photo_sm_name,".");
		if (copy( $photo_sm , "$path/$RenameFile" )){		
			unlink($photo_sm);
			$pathPic_sm="$RenameFile";	
		}
	}else{$pathPic_sm=$h_photo_sm;}
	
	$sql_update="UPDATE tb_banner SET ban_status='$ban_status', ban_url='$ban_url', ban_photo='$pathPic', ban_photo_md='$pathPic_md', ban_photo_sm='$pathPic_sm' WHERE ban_id='$ban_id'";
	$Re_update=$mysqli->query($sql_update);
	
	$GoTo = "admin_banner_edit.php?ban_id=".$ban_id;
	if (isset($_SERVER['QUERY_STRING'])) {
	$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
	$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="dele"){
	if ((isset($_GET['ban_id'])) && ($_GET['ban_id'] != "")) {
		$ban_id=$_GET['ban_id'];
			
		$sql_chk= "SELECT * FROM tb_banner WHERE ban_id='$ban_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['ban_photo'];
		if($file!=""){
			if (file_exists ("../images/banner/$file")){unlink("../images/banner/$file");}
		}
		$file2=$row_Re_chk['ban_photo_md'];
		if($file2!=""){
			if (file_exists ("../images/banner/$file2")){unlink("../images/banner/$file2");}
		}
		$file3=$row_Re_chk['ban_photo_sm'];
		if($file3!=""){
			if (file_exists ("../images/banner/$file3")){unlink("../images/banner/$file3");}
		}

		$sql_dele ="DELETE FROM tb_banner WHERE ban_id='$ban_id'";
		$Re_dele=$mysqli->query($sql_dele);
			
		$GoTo = "admin_banner_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}
$mysqli->close();
?>
