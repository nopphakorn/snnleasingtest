<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

$a_id_chk = mysqli_real_escape_string($mysqli, $_GET['a_id']);
$sql_a = "SELECT * FROM tb_tff_article WHERE tb_tff_article.a_id = '$a_id_chk'";
$Re_a=$mysqli->query($sql_a);
$row_Re_a =$Re_a->fetch_assoc();
$totalRows_Re_a = $Re_a->num_rows;

?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
    <script>
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('#a_detail', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
        });
    </script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_tff_article_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_tff_article_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-pencil fa-lg" style="color:#3097FF"></i><?php echo" แก้ไข".$title;?></div>
                <div class="box_form">
                    <form action="admin_tff_article_save.php?action=edit" method="post" enctype="multipart/form-data" name="form_tffa" id="form_tffa">
                        <table width="800" >
                            <?php if($row_Re_a['a_photo']!=""){ ?>
                            <tr>
                                <td colspan="2">
                                    <p class="img_a_full"><img src="../images/tff_article/<?php echo $row_Re_a['a_photo']; ?>" alt=""/></p>
                                     <hr>
                                </td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td width="130">วันที่โพสต์</td>
                                <td>
                                    <?php echo datethai($row_Re_a['a_date']); ?>
                                    <input name="a_date" type="hidden" id="a_date" value="<?php echo $row_Re_a['a_date']; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>สถานะ</td>
                                <td>
                                    <select name="a_status" id="a_status">
                                        <option value="1" <?php if($row_Re_a['a_status']=="1"){ echo "selected"; } ?> <?php if (!(strcmp(1, $row_Re_a['a_status']))) {echo "selected=\"selected\"";} ?>>แสดง</option>
                                        <option value="0" <?php if($row_Re_a['a_status']=="0"){ echo "selected"; } ?> <?php if (!(strcmp(0, $row_Re_a['a_status']))) {echo "selected=\"selected\"";} ?>>ไม่แสดง</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">เปลี่ยนภาพปก</td>
                                <td><input type="file" name="a_photo_edit" id="a_photo_edit"></td>
                            </tr>
							<tr>
                                <td colspan="2">
                                    <br>ชื่อบทความ<br>
                                    <input type="text" name="a_name" id="a_name" style="width: 400px" value="<?php echo $row_Re_a['a_name']; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br>หัวข้อบทความ<br>
                                    <textarea name="a_title" cols="60" rows="3" id="a_name"><?php echo $row_Re_a['a_title']; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br>รายละเอียด<br>
                                    <textarea name="a_detail" id="a_detail" style="height: 300px; width: 99%;"><?php echo $row_Re_a['a_detail']; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td><input type="submit" name="button" id="button" value="บันทึกข้อมูล" />
                                    <input name="a_id" type="hidden" id="a_id" value="<?php echo $row_Re_a['a_id']; ?>">
                                    <input name="h_a_photo" type="hidden" id="h_a_photo" value="<?php echo $row_Re_a['a_photo']; ?>"></td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript" src="tools/lightbox/js/lightbox.js"></script>
</body>
</html>
<?php $mysqli->close(); ?>