<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

if (isset($_GET['ban_id'])) {$ban_id_chk = mysqli_real_escape_string($mysqli, $_GET['ban_id']);}
$sql_ban="SELECT * FROM tb_banner_tel WHERE ban_id = '$ban_id_chk'";
$Re_ban=$mysqli->query($sql_ban);
$row_Re_ban=$Re_ban->fetch_assoc();
$totalRows_Re_ban=$Re_ban->num_rows;

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_banner_tel_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_banner_tel_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-pencil fa-lg" style="color:#3097FF"></i><?php echo" แก้ไข".$title;?></div>
                <div class="box_form">
                    <form action="admin_banner_tel_save.php?action=edit" method="POST" enctype="multipart/form-data" name="form1" id="form1">
                        <table width="800">
							<tr>
                                <td width="170" valign="top">รายการ</td>
                                <td><?php echo $row_Re_ban['ban_name']; ?></td>
                            </tr>
                            <tr>
                                <td valign="top">รูปภาพ</td>
                                <td>
									<div class="img_ban_list">
									<?php if($row_Re_ban['ban_photo']!=""){?>
									<img src="../images/banner_tel/<?php echo $row_Re_ban['ban_photo']; ?>" border="0" />
									<?php }else{ ?>
									<img src="../images/banner_tel/nophoto.jpg" border="0" />
									<?php } ?>
                                    </div>
                                    <br>ขนาดภาพกว้าง 1,300 x 350px.
								</td>
                            </tr>
                            <?php if(!empty($row_Re_ban['ban_photo_md'])){ ?>
                            <tr>
                                <td></td>
                                <td><div class="img_ban_list">
                                    <br><img src="../images/banner_tel/<?php echo $row_Re_ban['ban_photo_md']; ?>" />
                                    <br>ขนาดภาพกว้าง 768 x 350px.
                                    </div></td>
                            </tr>
                            <?php }if(!empty($row_Re_ban['ban_photo_sm'])){ ?>
                            <tr>
                                <td></td>
                                <td><div class="img_ban_list">
                                    <br><img src="../images/banner_tel/<?php echo $row_Re_ban['ban_photo_sm']; ?>" />
                                    <br>ขนาดภาพกว้าง 414 x 350px.
                                    </div></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="2"><hr></td>
                            </tr>
                            <tr>
                                <td width="120">แสดง/ไม่แสดง</td>
                                <td>
                                    <select name="ban_status" id="ban_status">
                                        <option value="" <?php if (!(strcmp("", $row_Re_ban['ban_status']))) {echo "selected=\"selected\"";} ?>>เลือกสถานะ</option>
                                        <option value="1" <?php if (!(strcmp(1, $row_Re_ban['ban_status']))) {echo "selected=\"selected\"";} ?>>แสดง</option>
                                        <option value="0" <?php if (!(strcmp(0, $row_Re_ban['ban_status']))) {echo "selected=\"selected\"";} ?>>ไม่แสดง</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="120">ลิ้ง(URL)</td>
                                <td>
                                    <input type="text" id="ban_link" name="ban_link" value="<?php echo $row_Re_ban['ban_link']; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td>เปลี่ยนรูปภาพขนาดใหญ่</td>
                                <td><input type="file" name="ban_photo_edit" id="ban_photo_edit" />กว้างไม่เกิน 900px.</td>
                            </tr>
                            <tr>
                                <td>เปลี่ยนรูปภาพขนาดกลาง</td>
                                <td><input type="file" name="ban_photo_md_edit" id="ban_photo_md_edit" />กว้างไม่เกิน 768px.</td>
                            </tr>
                            <tr>
                                <td>เปลี่ยนรูปภาพขนาดเล็ก</td>
                                <td><input type="file" name="ban_photo_sm_edit" id="ban_photo_sm_edit" />กว้างไม่เกิน 567px.</td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr>
                                    <input name="button" type="submit" id="button" value="บันทึกรายการ" />
                                    <input name="ban_id" type="hidden" id="ban_id" value="<?php echo $row_Re_ban['ban_id']; ?>">
                                    <input name="h_ban_photo" type="hidden" id="h_ban_photo" value="<?php echo $row_Re_ban['ban_photo']; ?>">
                                    <input name="h_ban_photo_md" type="hidden" id="h_ban_photo_md" value="<?php echo $row_Re_ban['ban_photo_md']; ?>">
                                    <input name="h_ban_photo_sm" type="hidden" id="h_ban_photo_sm" value="<?php echo $row_Re_ban['ban_photo_sm']; ?>">
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>