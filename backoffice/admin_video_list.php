<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$maxRows_Re_v = 50;
$pageNum_Re_v = 0;
if (isset($_GET['pageNum_Re_v'])) {
  $pageNum_Re_v = $_GET['pageNum_Re_v'];
}
$startRow_Re_v = (($pageNum_Re_v-1) * $maxRows_Re_v);
if($startRow_Re_v<0){$startRow_Re_v=0;}

$query_Re_v = "SELECT * FROM tb_video ORDER BY v_status DESC, v_id DESC ";
$query_limit_Re_v = sprintf("%s LIMIT %d, %d", $query_Re_v, $startRow_Re_v, $maxRows_Re_v);
$Re_v=$mysqli->query($query_limit_Re_v);

if (isset($_GET['totalRows_Re_v'])) {
  $totalRows_Re_v = $_GET['totalRows_Re_v'];
} else {
  $all_Re_v=$mysqli->query($query_Re_v);
  $totalRows_Re_v=$all_Re_v->num_rows;
}
$totalPages_Re_v = ceil($totalRows_Re_v/$maxRows_Re_v);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_v==0){$page=1;}
else if($pageNum_Re_v>0){$page=$pageNum_Re_v;}

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_video_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_video_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;".$title;?></div>

                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_v=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_v=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_v>0){
                                echo "Page : ".$page."/".$totalPages_Re_v." Total : ".number_format($totalRows_Re_v)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                        

                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_v=%d%s", $currentPage, $totalPages_Re_v, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_v){printf("%s?pageNum_Re_v=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" border="1" align="center">
                    <tr>
						<th width="45">No.</th>
                        <th width="426">Youtube</th>
                        <th width="">รายละเอียด</th>
                        <th width="40">สถานะ</th>
                        <th width="40">แก้ไข</th>
                        <th width="40">ลบ</th>
                    </tr>
                    <?php 
                    if($totalRows_Re_v>0){
                        $number=$startRow_Re_v;
                        while($row_Re_v=$Re_v->fetch_assoc()){
                    ?>
                    <tr>
						<td><div align="center"><?php echo $number+=1; ?></div></td>
                        <td valign="top">
                            <?php if($row_Re_v['v_url']!=""){ ?>
                                <div class="youtube_box">
                                    <?php
                                    $url = $row_Re_v['v_url'];
                                    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $matches);
                                    $id = $matches[1];
                                    ?>
                                    <object width="426px" height="240px">
                                        <param name="movie" value="http://www.youtube.com/v/<?php echo $id; ?>?version=3" />
                                        <param name="allowFullScreen" value="true" />
                                        <param name="allowScriptAccess" value="always" />
                                        <embed src="http://www.youtube.com/v/<?php echo $id; ?>?version=3" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="426px" height="240px"> </embed>
                                    </object>
                                </div>
                            <?php } ?>
                        </td>
                        <td valign="top"><?php echo $row_Re_v['v_name']."<br>".$row_Re_v['v_url'];?></td>
                        <td valign="top"><div align="center">
                                <?php if($row_Re_v['v_status']=="1"){ ?>
                                <a href="admin_video_save.php?v_id=<?php echo $row_Re_v['v_id']; ?>&action=status&v_status=0"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
                                <?php }else{ ?>
                                <a href="admin_video_save.php?v_id=<?php echo $row_Re_v['v_id']; ?>&action=status&v_status=1"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
                                <?php } ?>
                            </div></td>
                        <td valign="top"><div align="center">
                                <div align="center"> <a href="admin_video_edit.php?action=edit&v_id=<?php echo $row_Re_v['v_id']; ?>"><img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a></div>
                            </div></td>
                        <td valign="top"><div align="center">
                                <div align="center"> <a href="admin_video_save.php?action=dele&v_id=<?php echo $row_Re_v['v_id']; ?>" onclick = "return confirm('คุณต้องการลบ <?php echo $row_Re_v['v_url']; ?> หรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a></div>
                            </div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="5">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
           </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>