<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');

$query_Re_cb = "SELECT * FROM tb_car_brand ORDER BY cb_name ASC ";
$Re_cb=$mysqli->query($query_Re_cb);
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
	<link rel="stylesheet" href="tools/datepicker/jquery-ui.css">
	<script src="tools/datepicker/jquery-ui.js"></script>
	<script src="tools/datepicker/jqueryui_datepicker_thai.js"></script>
    <script type="text/javascript">
		$( function () {
			$( "#c_date" ).datepicker( {
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true,
				showOn: "button",
		        buttonImage: "images/icon/calendar.gif",
			} );
		} );
	</script>
</head>
<body>
	<?php if(!isset($_GET['save']) OR $_GET['save']!='sucess'){ ?>
	<div id="fancy_title"><i class="fa fa-list-alt"></i>&nbsp;เพิ่มราคากลางรถ</div>
    <div id="fancy_box">
		<form action="admin_car_save.php?action=all-insert" method="post" enctype="multipart/form-data" name="form_ca" id="form_ca">
            <table width="100%" cellspacing="5">
                <tr>
                    <td width="120px">วันที่ Update ราคา</td>
                    <td>
                        <input type="text" id="c_date" name="c_date" style="width:150px" readonly value="<?php echo datethai_num(date("Y-m-d")); ?>">
                        <input type="time" id="c_date_t" name="c_date_t" style="width:70px" value="<?php echo date("H:i"); ?>">
                    </td>
                </tr>
                <tr>
                    <td>ยี่ห้อ</td>
                    <td>
                        <select name="cb_id" id="cb_id" onchange="list_ca('ct_id',form_ca.cb_id.value,'','')">
                            <option value="">เลือก</option>
                            <?php while($row_Re_cb=$Re_cb->fetch_assoc()){?>
                            <option value="<?php echo $row_Re_cb['cb_id'];?>"><?php echo $row_Re_cb['cb_name'];?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>ประเภท</td>
                    <td>
                        <select name="ct_id" id="ct_id" onchange="list_ca('cg_id', form_ca.cb_id.value, form_ca.ct_id.value,'')">
                            <option value="">เลือก</option>
                        </select>
                    </td>
                </tr>
                
                <tr>
                    <td>กลุ่มรุ่นรถ</td>
                    <td>
                        <select name="cg_id" id="cg_id" onchange="list_ca('cm_id', form_ca.cb_id.value, form_ca.ct_id.value, form_ca.cg_id.value)">
					        <option value="">เลือก</option>
				        </select>
                    </td>
                </tr>
                <tr>
                    <td>ชื่อรุ่นรถ</td>
                    <td>
                        <select name="cm_id" id="cm_id">
					        <option value="">เลือก</option>
				        </select>
                    </td>
                </tr>
                <tr>
                    <td>ระบบเกียร์</td>
                    <td>
                        <select name="c_gear" id="c_gear">
					        <option value="">เลือก</option>
                            <option value="AT">ออโต้</option>
                            <option value="MT">ธรรมดา</option>
				        </select>
                    </td>
                </tr>
                <tr>
                    <td>ปีรถ</td>
                    <td><input type="text" id="c_year" name="c_year"></td>
                </tr>
                <tr>
                    <td>ราคากลาง</td>
                    <td><input type="text" id="c_price" name="c_price"></td>
                </tr>
                <tr>
                    <td>รูปภาพรถ 1</td>
                    <td> 
                        <div class="tx_red">ขนาดรูปภาพที่แนะนำ 640px X 480px</div>
                        <input type="file" id="c_photo1" name="c_photo1"></td>
                </tr>
                <tr>
                    <td>รูปภาพรถ 2</td>
                    <td>
                        <div class="tx_red">ขนาดรูปภาพที่แนะนำ 640px X 480px</div>
                        <input type="file" id="c_photo2" name="c_photo2"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr>
                        <input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/>
                        <input type="hidden" id="c_user" name="c_user" value="<?php echo $_SESSION['AD_Name'];?>">
                    </td>
                </tr>
            </table>
		</form>
	</div>
	<?php } ?>

	<?php if(isset($_GET['save']) AND $_GET['save']='sucess'){ ?>
	<div id="fancy_sucess">
		<br><br><br><br>
		<p><img src="images/icon/anw_success.png" alt=""></p>
		<p>บันทึกรายการ ราคากลาง เรียบร้อย</p>
	</div>
	<?php } ?>
</body>
</html>
<?php $mysqli->close(); ?>