<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');

$query_Re_ct = "SELECT * FROM tb_car_type ORDER BY ct_id ASC ";
$Re_ct=$mysqli->query($query_Re_ct);
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>
<body>
	<?php if(!isset($_GET['save']) OR $_GET['save']!='sucess'){ ?>
	<div id="fancy_title"><i class="fa fa-list-alt"></i>&nbsp;เพิ่มชื่อยี่ห้อรถ</div>
    <div id="fancy_box">
		<form action="admin_car_save.php?action=brand-insert" method="post" enctype="multipart/form-data" name="form_cb" id="form_cb">
			ชื่อยี่ห้อรถ<br>
			<input type="text" id="cb_name" name="cb_name" style="width:250px" onkeyup="check_cb(form_cb.cb_name.value,form_cb.h_cb_name.value)">
			<span id="msg" class="tx_error"></span>

			<p>
			<?php while($row_Re_ct = $Re_ct->fetch_assoc()){ ?>
				<label for="ct_id_list">
				<input type="checkbox" id="ct_id_list[]" name="ct_id_list[]" value="<?php echo $row_Re_ct['ct_id'];?>"> <?php echo $row_Re_ct['ct_name'];?>
				</label>
			<?php } ?>
			<div id="ct_id_list_validate"></div>
			</p>
			
            <input type="hidden" name="h_cb_name" id="h_cb_name" />
			<p><hr><input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/></p>
		</form>
	</div>
	<?php } ?>

	<?php if(isset($_GET['save']) AND $_GET['save']='sucess'){ ?>
	<div id="fancy_sucess">
		<br><br>
		<p><i class="fa fa-check-circle-o  fa-5x" aria-hidden="true"></i></p>
		<p>บันทึกรายการยี่ห้อรถเรียบร้อย</p>
	</div>
	<?php } ?>
</body>
</html>


