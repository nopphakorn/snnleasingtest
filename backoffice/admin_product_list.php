<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$maxRows_Re_pro = 50;
$pageNum_Re_pro = 0;
if (isset($_GET['pageNum_Re_pro'])) {
  $pageNum_Re_pro = $_GET['pageNum_Re_pro'];
}
$startRow_Re_pro = (($pageNum_Re_pro-1) * $maxRows_Re_pro);
if($startRow_Re_pro<0){$startRow_Re_pro=0;}

$query_Re_pro = "SELECT * FROM tb_product ORDER BY pro_id DESC ";
$query_limit_Re_pro = sprintf("%s LIMIT %d, %d", $query_Re_pro, $startRow_Re_pro, $maxRows_Re_pro);
$Re_pro=$mysqli->query($query_limit_Re_pro);

if (isset($_GET['totalRows_Re_pro'])) {
  $totalRows_Re_pro = $_GET['totalRows_Re_pro'];
} else {
  $all_Re_pro=$mysqli->query($query_Re_pro);
  $totalRows_Re_pro=$all_Re_pro->num_rows;
}
$totalPages_Re_pro = ceil($totalRows_Re_pro/$maxRows_Re_pro);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_pro==0){$page=1;}
else if($pageNum_Re_pro>0){$page=$pageNum_Re_pro;}

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_product_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_product_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;".$title;?></div>

                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_pro=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_pro=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_pro>0){
                                echo "Page : ".$page."/".$totalPages_Re_pro." Total : ".number_format($totalRows_Re_pro)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                        

                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_pro=%d%s", $currentPage, $totalPages_Re_pro, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_pro){printf("%s?pageNum_Re_pro=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" border="1" align="center">
                    <tr>
                        <th width="35"><div align="center">No.</div></th>
                        <th width="210">รูปภาพ</th>
                        <th width=60>สถานะ</th>
                        <th width=60>ประเภท</th>
                        <th width=""><div align="center">รายละเอียด</div></th>
                        <th width="93">ราคาขาย</th>
                        <th width="40"><div align="center">แก้ไข</div></th>
                        <th width="40"><div align="center">ลบ</div></th>
                    </tr>
                    <?php 
                    if($totalRows_Re_pro>0){
                        $number=$startRow_Re_pro;
                        while($row_Re_pro=$Re_pro->fetch_assoc()){
                            $sql="SELECT * FROM `tb_product_photo` WHERE pro_id='".$row_Re_pro['pro_id']."' LIMIT 1";
                            $Re_p=$mysqli->query($sql);
                            $row_Re_p=$Re_p->fetch_assoc();
                            $total_Re_p=$Re_p->num_rows;
                    ?>
                    <tr>
                        <td valign="top"><div align="center"><?php echo $number+=1; ?></div></td>
                        <td valign="top">
                            <div align="center">
                                <div class="img_pro_list">
                                    <?php if($total_Re_p>0){?>
                                    <img src="../images/product/<?php echo $row_Re_p['pro_p_photo'];?>" />
                                    <?php }else{?>
                                    <img src="../images/product/no_photo.jpg" />
                                    <?php } ?>
                                </div>
                            </div>
                        </td>
                        <td valign="top">
                            <div align="center">
                                <?php 
                                if($row_Re_pro['pro_status']==0){ echo "<font color='#FF0000'>ขายแล้ว</font>";} 
                                else if($row_Re_pro['pro_status']==1){ echo "<font color='#15B315'>ลงขาย</font>";} 
                                ?>
                            </div>
                        </td>
                        <td valign="top">
                            <div align="center">
                                <?php 
                                if($row_Re_pro['pro_type']==0){ echo "รถใหม่";} 
                                else if($row_Re_pro['pro_type']==1){ echo "<font color='#FF0000'>รถมือสอง</font>";} 
                                ?>
                            </div>
                        </td>
                        <td valign="top">
                            <?php
                            echo "อัพเดทวันที่ : ".datethai_sm_time($row_Re_pro['pro_brand'])."<br>";
                            echo "ยี่ห้อ: ".$row_Re_pro['pro_brand']."&nbsp;&nbsp;&nbsp;รุ่น: ".$row_Re_pro['pro_model']."<br>";
                            echo "โฉมรถยนต์: ".$row_Re_pro['pro_model_type']."&nbsp;&nbsp;&nbsp;รายละเอียดรุ่น: ".$row_Re_pro['pro_model_detail']."<br>";
                            echo "ปี: ".$row_Re_pro['pro_year']."&nbsp;&nbsp;&nbsp;สี: ".$row_Re_pro['pro_color']."&nbsp;&nbsp;&nbsp;ขนาดเครื่องยนต์: ".$row_Re_pro['pro_cc']." cc<br>";
                            echo "ระบบเกียร์: ".$row_Re_pro['pro_gear']."&nbsp;&nbsp;&nbsp;เลขไมล์(กม): ".$row_Re_pro['pro_mile']."<br>";
                            ?>
                        </td>
                        <td valign="top"><div align="right"><?php echo number_format($row_Re_pro['pro_price']); ?></div></td>
                        <td valign="top"><div align="center">
                                <div align="center"> <a href="admin_product_edit.php?action=edit&pro_id=<?php echo $row_Re_pro['pro_id']; ?>"><img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a></div>
                            </div></td>
                        <td valign="top"><div align="center">
                                <div align="center"> <a href="admin_product_save.php?action=dele&pro_id=<?php echo $row_Re_pro['pro_id']; ?>" onclick = "return confirm('คุณต้องการลบ <?php echo $row_Re_pro['pro_name']; ?> หรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a></div>
                            </div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="8">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
           </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>