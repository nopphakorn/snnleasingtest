<?php
require_once('Connections/con_db.php'); 
include('function/f_admin.php');
$page_nav="list";

$maxRows_Re_cn = 50;
$pageNum_Re_cn = 0;
if (isset($_GET['pageNum_Re_cn'])) {
  $pageNum_Re_cn = $_GET['pageNum_Re_cn'];
}
$startRow_Re_cn = (($pageNum_Re_cn-1) * $maxRows_Re_cn);
if($startRow_Re_cn<0){$startRow_Re_cn=0;}

$query_Re_cn = "SELECT * FROM tb_company ORDER BY cn_no ASC ";
$query_limit_Re_cn = sprintf("%s LIMIT %d, %d", $query_Re_cn, $startRow_Re_cn, $maxRows_Re_cn);
$Re_cn=$mysqli->query($query_limit_Re_cn);

if (isset($_GET['totalRows_Re_cn'])) {
  $totalRows_Re_cn = $_GET['totalRows_Re_cn'];
} else {
  $all_Re_cn=$mysqli->query($query_Re_cn);
  $totalRows_Re_cn=$all_Re_cn->num_rows;
}
$totalPages_Re_cn = ceil($totalRows_Re_cn/$maxRows_Re_cn);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_cn==0){$page=1;}
else if($pageNum_Re_cn>0){$page=$pageNum_Re_cn;}

//---------------
$sql_mn = "SELECT max(cn_no) AS cn_max, min(cn_no) AS cn_min FROM tb_company ORDER BY cn_no ASC";
$Re_mn=$mysqli->query($sql_mn);
$row_Re_mn=$Re_mn->fetch_assoc();
$totalRows_Re_mn=$Re_mn->num_rows;

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_company_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>
    
    <div id="containner">
        <div id="main">
            <div id="main_menu"> <?php include("admin_company_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;รายการ".$title;?></div>

                <!-- papeging -->
                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_cn=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_cn=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_cn>0){
                                echo "Page : ".$page."/".$totalPages_Re_cn." Total : ".number_format($totalRows_Re_cn)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                        

                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_cn=%d%s", $currentPage, $totalPages_Re_cn, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_cn){printf("%s?pageNum_Re_cn=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" border="1" align="center">
                    <tr>
                        <th width="35">ลำดับ</th>
                        <th width="50">ตำแหน่ง</th>
                        <th width="100">Logo</th>
                        <th width="140">ชื่อ</th>
                        <th width="">บริษัท</th>
                        <th width="200">การติดต่อ</th>
                        <th width="35">แก้ไข</th>
                        <th width="35">ลบ</th>
                    </tr>
                    <?php 
                    if($totalRows_Re_cn>0){
                     while($row_Re_cn=$Re_cn->fetch_assoc()){
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $row_Re_cn['cn_no']; ?></div></td>
                        <td>
                            <div align="center">
                                <?php 
                                $list=$row_Re_cn['cn_no'];
                                $id=$row_Re_cn['cn_id'];
                                if($totalRows_Re_cn>=2){
                                ?>
                                <?php 
                                if($list==1){ 
                                ?>
                                    <div align="center">
                                        <a href="admin_company_save.php?action=no&amp;down=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_down.png" alt="down" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php 
                                } if($list!=1 and $list!=$row_Re_mn['cn_max']){
                                ?>
                                    <div align="center">
                                        <a href="admin_company_save.php?action=no&amp;down=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_down.png" alt="down" width="16" height="16" border="0" /></a>
                                        
                                        <a href="admin_company_save.php?action=no&amp;up=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_up.png" alt="up" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php 
                                } if($list==$row_Re_mn['cn_max'] AND $list>0){ 
                                ?>
                                    <div align="center">
                                        <a href="admin_company_save.php?action=no&amp;up=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_up.png" alt="up" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php } } ?>
                            </div>
                        </td>
                        <td>
							<div align="center">
                            <div class="img_cn_list">
                                <?php if($row_Re_cn['cn_logo']!=""){ ?>
                                <img src="../images/company/<?php echo $row_Re_cn['cn_logo']; ?>" />
                                <?php }else{ ?>
                                <img src="../images/company/no_logo.png" />
                                <?php } ?>
                            </div>
							</div>
						</td>
                        <td>
                            <div align="center">
                                <?php echo $row_Re_cn['cn_name']."<br><br>".$row_Re_cn['en_cn_name']; ?>
                            </div>
                        </td>
                        <td>
							<div align="left">
								<?php 
								echo "ประเภทธุรกิจ : ".$row_Re_cn['cn_type']."<br>";
								echo "บริษัท : ".$row_Re_cn['cn_company']."<br>";
                                echo $row_Re_cn['cn_address']; 
                                echo "<br><br>";
                                echo "Type : ".$row_Re_cn['en_cn_type']."<br>";
								echo "Company : ".$row_Re_cn['en_cn_company']."<br>";
								echo $row_Re_cn['en_cn_address']; 
								?>
							</div>
						</td>
                        <td>
                            <div align="left">
								<?php 
								echo "TEL: ".$row_Re_cn['cn_tel']."<br>";
								echo "FAX: ".$row_Re_cn['cn_fax']."<br>"; 
                                echo "WEB: ".$row_Re_cn['cn_web']."<br>"; 
                                echo "Facebook: ".$row_Re_cn['cn_fb']."<br>"; 
                                echo "Line: ".$row_Re_cn['cn_line']; 
								?>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_company_edit.php?action=edit&cn_id=<?php echo $row_Re_cn['cn_id']; ?>">
                                <img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_company_save.php?action=dele&cn_id=<?php echo $row_Re_cn['cn_id']; ?>" onclick = "return confirm('คุณต้องการลบสาขา <?php echo $row_Re_cn['cn_name']; ?> หรือไม่')">
                                <img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a>
                            </div>
                        </td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="9">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>