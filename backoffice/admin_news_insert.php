<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="insert";
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
    <script>
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('#n_detail', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
            editor = K.create('#en_n_detail', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
        });
    </script>

    <script type="text/javascript">
        function fncCreateElement1(){
            var mySpan1 = document.getElementById('mySpan1');
            var myElement = document.createElement('input');
            var myElement2 = document.createElement('br');
            myElement.setAttribute('type',"file");
            myElement.setAttribute('name',"n_p_photo[]");
            mySpan1.appendChild(myElement);	
            mySpan1.appendChild(myElement2);  
        }
    </script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_news_nav.php"); ?></div>
    <div id="side"><?php include('s_menu_side.php'); ?></div>
    
    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_news_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo" เพิ่ม".$title;?></div>
                <div class="box_form">
                    <form action="admin_news_save.php?action=insert" method="post" enctype="multipart/form-data" name="form_gal" id="form_gal">
                            <table width="800" >
                                <tr>
                                    <td width="130">วันที่โพสต์</td>
                                    <td>
                                        <?php echo datethai(date('Y-m-d H:i:s')); ?>
                                        <input name="n_date" type="hidden" id="n_date" value="<?php echo date("Y-m-d H:i:s"); ?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>สถานะ</td>
                                    <td>
                                        <select name="n_status" id="n_status">
                                            <option value="1">แสดง</option>
                                            <option value="0">ไม่แสดง</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">ภาพปก</td>
                                    <td><input type="file" name="n_photo" id="n_photo"></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
										<br>หัวข้อเรื่อง<br>
										<textarea name="n_name" cols="50" rows="3" id="n_name"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <br>รายละเอียด<br>
                                        <textarea name="n_detail" id="n_detail" style="height: 300px; width: 99%;"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
										<br>Title (English)<br>
										<textarea name="en_n_name" cols="50" rows="3" id="en_n_name"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <br>Detail (English)<br>
                                        <textarea name="en_n_detail" id="en_n_detail" style="height: 300px; width: 99%;"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><hr size="3" noshade="noshade" /></td>
                                </tr>
                                <tr>
                                    <td valign="top"><i class="fa fa-picture-o fa-lg"></i>&nbsp;ภาพแกลเลอรี่</td>
                                    <td>
                                        <input type="file" name="n_p_photo[]" id="n_p_photo[]" />
                                        <input type="button" name="button1" id="button1" value="+" onclick="JavaScript:fncCreateElement1();" />
                                        (กด + เพื่อเพิ่มรูปภาพ)<br/>
                                        <div id="mySpan1"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><hr size="3" noshade="noshade" /></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="submit" name="button" id="button" value="บันทึกข้อมูล" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>