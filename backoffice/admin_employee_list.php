<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$sql_admin="SELECT * FROM tb_employee ORDER BY em_id ASC";
$Re_admin=$mysqli->query($sql_admin);
//$row_Re_admin=$Re_admin->fetch_assoc();
$totalRows_Re_admin=$Re_admin->num_rows;
?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_employee_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_employee_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo" รายการ".$title;?></div>

                <table class="tb1" width="100%" align="center">
                    <tr >
                        <th width="45" height="40"><div align="center">ลำดับ</div></th>
						<th width="80" height="40"><div align="center">รหัสพนักงาน</div></th>
                        <th width="" height="40"><div align="center">ชื่อ-นามสกุล</div></th>
						<th width="200" height="40"><div align="center">ตำแหน่ง</div></th>
						<th width="120" height="40"><div align="center">Username</div></th>
                        <th width="80" height="40"><div align="center">สถิติการเข้า</div></th>
                        <th width="180" height="40"><div align="center">เข้าล่าสุด</div></th>
                        <th width="40" height="40"><div align="center">สถานะ</div></th>
                        <th width="40" height="40"><div align="center">แก้ไข</div></th>
                        <th width="40" height="40"><div align="center">ลบ</div></th>
                    </tr>
                    <?php 
                        if($totalRows_Re_admin>0){
                            $number=0; 
                            while($row_Re_admin=$Re_admin->fetch_assoc()){
                    ?>
                    <tr >
                        <td><div align="center"><?php echo $number+=1; ?></div></td>
						<td><div align="center"><?php echo $row_Re_admin['em_no']; ?></div></td>
                        <td><div align="left"><?php echo $row_Re_admin['em_name']; ?></div></td>
						<td><div align="left"><?php echo $row_Re_admin['em_position']; ?></div></td>
						<td><div align="left"><?php echo $row_Re_admin['em_user']; ?></div></td>
                        <td>
                            <div align="center">
                                <?php 
                                if($row_Re_admin['em_count']!=""){
                                    echo $row_Re_admin['em_count'];
                                } else { echo "<font color='#FF0004'>-</font>";} 
                                ?>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <?php 
                                if($row_Re_admin['em_count']!=""){
                                    echo datethai($row_Re_admin['em_date']);
                                } else { echo "<font color='#FF0004'>ไม่เคยเข้าสู่ระบบ</font>";}
                                ?>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <?php if($row_Re_admin['em_status']=="1"){ ?>
                                <a href="admin_employee_save.php?em_id=<?php echo $row_Re_admin['em_id']; ?>&action=status&em_status=0"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
                                <?php }else{ ?>
                                <a href="admin_employee_save.php?em_id=<?php echo $row_Re_admin['em_id']; ?>&action=status&em_status=1"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
                                <?php } ?>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_employee_edit.php?em_id=<?php echo $row_Re_admin['em_id']; ?>">
                                <img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_employee_save.php?action=dele&em_id=<?php echo $row_Re_admin['em_id']; ?>" onclick = "return confirm('คุณต้องการลบรายชื่อพนักงานหรือไม่')">
                                <img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a></div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="10">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>   
    </div>
</body>
</html>
<?php $mysqli->close(); ?>