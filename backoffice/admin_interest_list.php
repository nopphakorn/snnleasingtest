<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$maxRows_Re_i = 50;
$pageNum_Re_i = 0;
if (isset($_GET['pageNum_Re_i'])) {
  $pageNum_Re_i = $_GET['pageNum_Re_i'];
}
$startRow_Re_i = (($pageNum_Re_i-1) * $maxRows_Re_i);
if($startRow_Re_i<0){$startRow_Re_i=0;}

$query_Re_i = "SELECT * FROM tb_interest ORDER BY i_id DESC ";
$query_limit_Re_i = sprintf("%s LIMIT %d, %d", $query_Re_i, $startRow_Re_i, $maxRows_Re_i);
$Re_i=$mysqli->query($query_limit_Re_i);

if (isset($_GET['totalRows_Re_i'])) {
  $totalRows_Re_i = $_GET['totalRows_Re_i'];
} else {
  $all_Re_i=$mysqli->query($query_Re_i);
  $totalRows_Re_i=$all_Re_i->num_rows;
}
$totalPages_Re_i = ceil($totalRows_Re_i/$maxRows_Re_i);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_i==0){$page=1;}
else if($pageNum_Re_i>0){$page=$pageNum_Re_i;}

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_interest_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_interest_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo" รายการ".$title;?></div>

                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_i=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_i=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_i>0){
                                echo "Page : ".$page."/".$totalPages_Re_i." Total : ".number_format($totalRows_Re_i)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                        

                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_i=%d%s", $currentPage, $totalPages_Re_i, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_i){printf("%s?pageNum_Re_i=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" align="center">
                    <tr>
                        <th width="35">ลำดับ</th>
                        <th width="60">ประเภท</th>
                        <th>รายละเอียด</th>
                        <th width="250">ไฟล์</th>
                        <th width="35">แก้ไข</th>
                        <th width="35">ลบ</th>
                    </tr>
                    <?php
                    if($totalRows_Re_i>0){
						$number=$startRow_Re_i; 
                        while($row_Re_i=$Re_i->fetch_assoc()){
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $number+=1; ?></div></td>
                        <td><div align="center">
                            <?php 
                            if($row_Re_i['i_type']=="PDF"){echo "<i class=\"far fa-file-pdf fa-lg\"></i>";} 
                            else if($row_Re_i['i_type']=="JPG"){echo "<i class=\"far fa-file-image fa-lg\"></i>";} 
                            ?>
                        </div></td>
                        <td>
                            <?php echo $row_Re_i['i_name']; ?>
                            <?php if($row_Re_i['i_title']!=""){echo "<br>".$row_Re_i['i_title'];} ?>
                        </td>
                        <td>
                            <div align="center">
								<?php if($row_Re_i['i_file']!=""){?>
                                    <?php 
                                        if(strchr($row_Re_i['i_file'],".")==".pdf"){
                                            echo "<i class=\"far fa-file-pdf\"></i>";
                                        }else if(strchr($row_Re_i['i_file'],".")==".jpg"){
                                            echo "<i class=\"far fa-file-image\"></i>";
                                        }
                                    ?>
								    <a href="../images/interest/<?php echo $row_Re_i['i_file']; ?>"  target="_blank"><?php echo $row_Re_i['i_file']; ?></a>
								<?php }else{echo "ไม่มีไฟล์เนื้อหา";} ?>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_interest_edit.php?i_id=<?php echo $row_Re_i['i_id']; ?>"><img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_interest_save.php?action=dele&i_id=<?php echo $row_Re_i['i_id']; ?>" onclick = "return confirm('คุณต้องการลบรูปภาพนี้หรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a>
                            </div>
                        </td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="6">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
   
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>