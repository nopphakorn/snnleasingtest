<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');

$query_Re_cb = "SELECT * FROM tb_car_brand ORDER BY cb_name ASC ";
$Re_cb=$mysqli->query($query_Re_cb);

$cg_id_chk=$_GET['cg_id'];
$query_Re_cg = "SELECT * FROM tb_car_group WHERE cg_id='$cg_id_chk' ";
$Re_cg=$mysqli->query($query_Re_cg);
$row_Re_cg=$Re_cg->fetch_assoc();
$totalRows_Re_cg=$Re_cg->num_rows;

$query_Re_cb2 = "SELECT * FROM tb_car_brand WHERE cb_id = ".$row_Re_cg['cb_id']." ";
$Re_cb2=$mysqli->query($query_Re_cb2);
$row_Re_cb2=$Re_cb2->fetch_assoc();

$query_Re_ct = "SELECT * FROM tb_car_type WHERE ct_id IN(".$row_Re_cb2['ct_id'].") ORDER BY ct_id ASC ";
$Re_ct=$mysqli->query($query_Re_ct);
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>
<body>
	<div id="fancy_title"><i class="fa fa-list-alt"></i>&nbsp;แก้ไขชื่อกลุ่มรุ่นรถ</div>
    <div id="fancy_box">
		<form action="admin_car_save.php?action=group-edit" method="post" enctype="multipart/form-data" name="form_cg" id="form_cg">
			<p>
				ยี่ห้อรถ<br>
				<select name="cb_id" id="cb_id">
					<option value="">เลือกยี่ห้อรถ</option>
					<?php while($row_Re_cb=$Re_cb->fetch_assoc()){?>
					<option value="<?php echo $row_Re_cb['cb_id'];?>" <?php if($row_Re_cg['cb_id']==$row_Re_cb['cb_id']){echo "selected=\"selected\"";} ?>><?php echo $row_Re_cb['cb_name'];?></option>
					<?php } ?>
				</select>
			</p>

			<p>
				ประเภทรถ<br>
				<select name="ct_id" id="ct_id" onchange="check_cg(form_cg.cb_id.value, form_cg.ct_id.value, form_cg.cg_name.value, form_cg.h_cg_name.value)">
					<option value="">เลือกประเภทรถ</option>
					<?php while($row_Re_ct=$Re_ct->fetch_assoc()){?>
					<option value="<?php echo $row_Re_ct['ct_id'];?>" <?php if($row_Re_ct['ct_id']==$row_Re_cg['ct_id']){echo "selected=\"selected\"";} ?>><?php echo $row_Re_ct['ct_name'];?></option>
					<?php } ?>
				</select>
			</p>

			<p>
				ชื่อกลุ่มรุ่นรถ<br>
				<input type="text" id="cg_name" name="cg_name" style="width:250px" value="<?php echo $row_Re_cg['cg_name']; ?>" 
				onkeyup="check_ct(form_cg.cg_id.value,form_cg.cg_name.value,form_cg.h_cg_name.value)">
				<span id="msg" class="tx_error"></span>
				<input type="hidden" id="h_cg_name" name="h_cg_name" value="<?php echo $row_Re_cg['cg_name']; ?>">
				<input type="hidden" id="cg_id" name="cg_id" value="<?php echo $row_Re_cg['cg_id']; ?>">
			</p>
			<p><hr><input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/></p>
		</form>
	</div>
</body>
</html>


