<div class="menu_side_admin">
  	<ul>
    	<li><a href="admin.php"><i class="fa fa-home" aria-hidden="true"></i>&nbsp;หน้าหลัก</a></li>
  	</ul>
</div>

<div class="menu_side">
	<ul>
		<li><a href="admin_user_list.php"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;ผู้ดูแลเว็บไซต์</a></li>
		<li><a href="admin_banner_list.php"><i class="fa fa-tv" aria-hidden="true"></i>&nbsp;ภาพสไลด์โชว์</a></li>
		<li><a href="admin_banner_page_list.php"><i class="fa fa-tv" aria-hidden="true"></i>&nbsp;ภาพแบนเนอร์</a></li>
		<li><a href="admin_banner_tel_list.php"><i class="fas fa-phone-square"></i>&nbsp;ภาพคลิกเลย</a></li>
		<li><a href="admin_branch_list.php"><i class="fa fa-university" aria-hidden="true"></i>&nbsp;สาขา</a></li>
		<li><a href="admin_service_list.php"><i class="far fa-money-bill-alt"></i>&nbsp;ผลิตภัณฑ์และบริการ</a></li>
		<li><a href="admin_news_list.php"><i class="fa fa-camera-retro" aria-hidden="true"></i>&nbsp;ข่าวสารและกิจกรรม</a></li>
		<li><a href="admin_promotion_list.php"><i class="fas fa-bullhorn"></i>&nbsp;โปรโมชั่น</a></li>
		<li><a href="admin_review_list.php"><i class="fas fa-comment"></i>&nbsp;Review ลูกค้า</a></li>
		<li><a href="admin_answer_list.php"><i class="fas fa-question"></i>&nbsp;คำถามที่พบบ่อย</a></li>
		<li><a href="admin_interest_list.php"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;อัตราดอกเบี้ยและค่าธรรมเนียม</a></li>
		<li><a href="admin_content_edit.php?c_name=Debtor"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;สิทธิลูกหนี้</a></li>
		<li><a href="admin_content_edit.php?c_name=Loan"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;เงื่อนไขสมัครสินเชื่อ</a></li>
		<li><a href="admin_content_edit.php?c_name=Contact"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;ติดต่อเรา</a></li>
		<li><a href="admin_content_edit.php?c_name=Policy"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Privacy Policy</a></li>
		<li><a href="admin_video_list.php"><i class="fab fa-youtube"></i>&nbsp;Youtube</a></li>
		<li><a href="admin_search_list.php"><i class="fas fa-search-plus"></i>&nbsp;ค้นหาในเว็บ</a></li>
	</ul>
	<hr>
	<ul>
		<li><a href="admin_loan_list.php"><i class="fas fa-credit-card"></i>&nbsp;สมัครขอสินเชื่อ</a></li>
		<li><a href="admin_tel_list.php"><i class="fas fa-phone"></i>&nbsp;สมัครรับ SMS</a></li>
	</ul>
	<hr>
	<ul>
		<li><a href="admin_content_edit.php?c_name=car"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;ข้อตกลง & เงื่อนไข ราคากลาง</a></li>
		<li><a href="admin_employee_list.php"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;พนักงาน(ราคากลาง)</a></li>
		<li><a href="admin_car_all.php"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;ราคากลางรถ</a></li>
	</ul>
	<hr>
	<ul>
		<li><a href="admin_content_edit.php?c_name=About"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;ประวัติความเป็นมา</a></li>
		<li><a href="admin_content_edit.php?c_name=Vision"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;วิสัยทัศน์/พันธกิจ/ค่านิยมองค์กร</a></li>
		<li><a href="admin_content_edit.php?c_name=Organize"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;โครงสร้างองค์กร</a></li>
		<li><a href="admin_content_edit.php?c_name=Board"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;โครงสร้างคณะกรรมการ</a></li>
		<li><a href="admin_content_edit.php?c_name=Complaint"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;การรับเรื่องร้องเรียน</a></li>
		<li><a href="admin_content_edit.php?c_name=Social"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;ความรับผิดชอบต่อสังคม</a></li>
		<li><a href="admin_company_list.php"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;บริษัทในเครือ</a></li>
		<li><a href="admin_content_edit.php?c_name=Payment"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;ช่องทางการชำระเงิน</a></li>
	</ul>
	<hr>
	<ul>
		<li><a href="admin_tff_list.php"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;สมาชิก TFF</a></li>
		<li><a href="admin_content_edit.php?c_name=TFF"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;ข้อตกลง & เงื่อนไข TFF</a></li>
		<li><a href="admin_content_edit.php?c_name=TFF_money"><i class="far fa-money-bill-alt"></i>&nbsp;วางแผนการเงิน TFF</a></li>
		<li><a href="admin_content_edit.php?c_name=TFF_phl"><i class="fa fa-university" aria-hidden="true"></i>&nbsp;อนาคตพิษณุโลก สู่สังคมเมือง TFF</a></li>
		<li><a href="admin_tff_article_list.php"><i class="fa fa-book" aria-hidden="true"></i>&nbsp;บทความดีๆ TFF</a></li>
	</ul>
	<hr>
	<ul>
		<li><a href="admin_content_edit.php?c_name=job_home"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;ร่วมงานกับเอส เอ็น เอ็น</a></li>
		<li><a href="admin_content_edit.php?c_name=job_trainee"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;Manager Trainee Program</a></li>
		<li><a href="admin_content_edit.php?c_name=job_personnel"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;การพัฒนาบุคลากรของเรา</a></li>
		<li><a href="admin_job_list.php"><i class="fas fa-user-plus"></i>&nbsp;ตำแหน่งงาน</a></li>
	</ul>
	<hr>
	<b>นักลงทุนสัมพันธ์</b>
	<ul>
		<li><a href="admin_investor_list.php?type=1"><i class="fas fa-paperclip"></i>&nbsp;Corporate Profile Presentation</a></li>
		<li><a href="admin_investor_list.php?type=2"><i class="fas fa-paperclip"></i>&nbsp;รายงานประจำปี</a></li>
		<li><a href="admin_investor_list.php?type=3"><i class="fas fa-paperclip"></i>&nbsp;ข้อมูลนำเสนอ</a></li>
	</ul>
	<hr>
	<ul>
		<li><a href="admin_product_user_list.php"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;ผู้เข้าใช้งาน(ทรัพย์สินรอขาย)</a></li>
		<li><a href="admin_product_list.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp;ทรัพย์สินรอขาย</a></li>
	</ul>
</div>
<br><br>
