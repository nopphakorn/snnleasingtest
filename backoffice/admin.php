<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

$strSQL = " SELECT COUNT(DATE) AS CounterToday FROM tb_counterc WHERE DATE = '".date("Y-m-d")."' ";
$Re_count = $mysqli->query($strSQL);
$row_Re_count = $Re_count->fetch_assoc();
$strToday = $row_Re_count["CounterToday"];

$strSQL = " SELECT NUM FROM tb_counterd WHERE DATE = '".date('Y-m-d',strtotime("-1 day"))."' ";
$Re_count = $mysqli->query($strSQL);
$row_Re_count = $Re_count->fetch_assoc();
$strYesterday = $row_Re_count["NUM"];

$strSQL = " SELECT SUM(NUM) AS CountMonth FROM tb_counterd WHERE DATE_FORMAT(DATE,'%Y-%m')  = '".date('Y-m')."' ";
$Re_count = $mysqli->query($strSQL);
$row_Re_count = $Re_count->fetch_assoc();
$strThisMonth = $row_Re_count["CountMonth"];

$strSQL = " SELECT SUM(NUM) AS CountMonth FROM tb_counterd WHERE DATE_FORMAT(DATE,'%Y-%m')  = '".date('Y-m',strtotime("-1 month"))."' ";
$Re_count = $mysqli->query($strSQL);
$row_Re_count = $Re_count->fetch_assoc();
$strLastMonth = $row_Re_count["CountMonth"];

$strSQL = " SELECT SUM(NUM) AS CountYear FROM tb_counterd WHERE DATE_FORMAT(DATE,'%Y')  = '".date('Y')."' ";
$Re_count = $mysqli->query($strSQL);
$row_Re_count = $Re_count->fetch_assoc();
$strThisYear = $row_Re_count["CountYear"];

$strSQL = " SELECT SUM(NUM) AS CountYear FROM tb_counterd WHERE DATE_FORMAT(DATE,'%Y')  = '".date('Y',strtotime("-1 year"))."' ";
$Re_count = $mysqli->query($strSQL);
$row_Re_count = $Re_count->fetch_assoc();
$strLastYear = $row_Re_count["CountYear"];

$strSQL = " SELECT SUM(NUM) as SumTotal FROM tb_counterd ";
$Re_count = $mysqli->query($strSQL);
$row_Re_count = $Re_count->fetch_assoc();
$strtotal = $row_Re_count["SumTotal"];
$ctotal=$strtotal+$strToday;
?>
<!doctype html>
<html>
<head>
	<?php include 's_inc_header.php';?>
</head>

<body>
	<div id="header"> <?php include("s_header.php"); ?></div>
	<div id="nav">หน้าหลักผู้ดูแลระบบ</div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>
	<div id="containner">
		<div id="main">
			<div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i>&nbsp;สถิติการเข้าชมเว็บไซต์</div>
			<table class="tb1" width="450" border="1" cellpadding="0" cellspacing="0">
				<tr>
					<th colspan="2" bgcolor="#DFDFDF"><div align="center">จำนวนผู้เข้าชมเว็บไซต์</div></th>
				</tr>
				<tr>
					<td width="381"><span class="text12">&nbsp;จำนวนผู้เข้าวันนี้</span></td>
					<td width="113"><div align="center"> <span class="text12"> <?php echo number_format($strToday,0);?> </span></div></td>
				</tr>
				<tr>
					<td><span class="text12">&nbsp;จำนวนผู้เข้าเมื่อวาน</span></td>
					<td><div align="center"> <span class="text12"> <?php echo number_format($strYesterday,0);?> </span></div></td>
				</tr>
				<tr>
					<td><span class="text12">&nbsp;จำนวนผู้เข้าเดือนนี้</span></td>
					<td><div align="center"> <span class="text12"> <?php echo number_format($strThisMonth,0);?> </span></div></td>
				</tr>
				<tr>
					<td><span class="text12">&nbsp;จำนวนผู้เข้าเดือนที่แล้ว</span></td>
					<td><div align="center"> <span class="text12"> <?php echo number_format($strLastMonth,0);?> </span></div></td>
				</tr>
				<tr>
					<td><span class="text12">&nbsp;จำนวนผู้เข้าปีนี้</span></td>
					<td><div align="center"> <span class="text12"> <?php echo number_format($strThisYear,0);?> </span></div></td>
				</tr>
				<tr>
					<td><span class="text12">&nbsp;จำนวนผู้เข้าปีที่แล้ว</span></td>
					<td><div align="center"> <span class="text12"> <?php echo number_format($strLastYear,0);?> </span></div></td>
				</tr>
				<tr>
					<td><span class="text12">&nbsp;จำนวนผู้เข้า</span>ทั้งหมด</td>
					<td><div align="center"> <span class="text12"> <?php echo number_format($ctotal,0);?> </span></div></td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>
