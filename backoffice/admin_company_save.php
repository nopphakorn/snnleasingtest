<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST["cn_id"])){$cn_id=mysqli_real_escape_string($mysqli, $_POST["cn_id"]);}
if(isset($_POST["cn_name"])){$cn_name=mysqli_real_escape_string($mysqli, $_POST["cn_name"]);}
if(isset($_POST["cn_type"])){$cn_type=mysqli_real_escape_string($mysqli, $_POST["cn_type"]);}
if(isset($_POST["cn_company"])){$cn_company=mysqli_real_escape_string($mysqli, $_POST["cn_company"]);}
if(isset($_POST["cn_address"])){$cn_address=mysqli_real_escape_string($mysqli, $_POST["cn_address"]);}
if(isset($_POST["cn_tel"])){$cn_tel=mysqli_real_escape_string($mysqli, $_POST["cn_tel"]);}
if(isset($_POST["cn_fax"])){$cn_fax=mysqli_real_escape_string($mysqli, $_POST["cn_fax"]);}
if(isset($_POST["cn_web"])){$cn_web=mysqli_real_escape_string($mysqli, $_POST["cn_web"]);}
if(isset($_POST["cn_fb"])){$cn_fb=mysqli_real_escape_string($mysqli, $_POST["cn_fb"]);}
if(isset($_POST["cn_line"])){$cn_line=mysqli_real_escape_string($mysqli, $_POST["cn_line"]);}
if(isset($_POST["cn_youtube"])){$cn_youtube=mysqli_real_escape_string($mysqli, $_POST["cn_youtube"]);}
if(isset($_POST["h_cn_logo"])){$h_cn_logo=mysqli_real_escape_string($mysqli, $_POST["h_cn_logo"]);}
if(isset($_POST["en_cn_name"])){$en_cn_name=mysqli_real_escape_string($mysqli, $_POST["en_cn_name"]);}
if(isset($_POST["en_cn_type"])){$en_cn_type=mysqli_real_escape_string($mysqli, $_POST["en_cn_type"]);}
if(isset($_POST["en_cn_company"])){$en_cn_company=mysqli_real_escape_string($mysqli, $_POST["en_cn_company"]);}
if(isset($_POST["en_cn_address"])){$en_cn_address=mysqli_real_escape_string($mysqli, $_POST["en_cn_address"]);}

if($_GET['action']=="no"){
	if(isset($_GET["up"])){$up=$_GET['up'];}else{$up=0;}
	if(isset($_GET["down"])){$down=$_GET['down'];}else{$down=0;}
	if(isset($_GET["id"])){$id=$_GET['id'];}
	if(isset($_GET["list"])){$list_new=$_GET['list'];}

	if($up==1){
		$list_old=$list_new-$up;
		$list_new=$list_new;
		$usql="update tb_company set cn_no='$list_new' where cn_no='$list_old'";
		$Re_usql=$mysqli->query($usql);
		if (!$Re_usql) {printf("Error: %s\n", $mysqli->error);}
		
		$dsql="update tb_company set cn_no='$list_old' where cn_id='$id'";
		$Re_dsql=$mysqli->query($dsql);
		if (!$Re_dsql) {printf("Error: %s\n", $mysqli->error);}
		
		$GoTo = "admin_company_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>"; 
	}

	if($down==1){
		$list_old=$down+$list_new;
		$list_new=$list_new;
		$usql="update tb_company set cn_no='$list_new' where cn_no='$list_old'";
		$Re_usql=$mysqli->query($usql);
		if (!$Re_usql) {printf("Error: %s\n", $mysqli->error);}
		
		$dsql="update tb_company set cn_no='$list_old' where cn_id='$id'";
		$Re_dsql=$mysqli->query($dsql);
		if (!$Re_dsql) {printf("Error: %s\n", $mysqli->error);}
		
		$GoTo = "admin_company_list.php";
		  if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		  }
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>"; 
	}
}
	
if($_GET['action']=="insert"){

	$sql="SELECT max(cn_no) AS cn_max FROM tb_company";
	$Re_sql=$mysqli->query($sql);
	$row_Re_sql=$Re_sql->fetch_assoc();
	if($row_Re_sql['cn_max']==0){$cn_no=1;}
	else{$cn_no=$row_Re_sql['cn_max']+1;}
	$photo=$_FILES["cn_logo"];
	$photo=$_FILES['cn_logo']['tmp_name'];
	$photo_name=$_FILES['cn_logo']['name'];

	if($photo != "") {
		$new_name ="CN".date('His').rand(1000,9999); 
		$path = "../images/company"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>320){
			$width=320;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".png" || strchr($photo_name,".")==".PNG"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".png" || strchr($photo_name,".")==".PNG"){
				ImagePNG($images_photo_new,$images_photo); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic="";}
	
	$sql="INSERT INTO tb_company(cn_no, cn_name, cn_type, cn_company, cn_address, en_cn_name, en_cn_type, en_cn_company, en_cn_address, cn_tel, cn_fax, cn_web, cn_fb, cn_line, cn_youtube, cn_logo)
	VALUES('$cn_no', '$cn_name', '$cn_type', '$cn_company', '$cn_address', '$en_cn_name', '$en_cn_type', '$en_cn_company', '$en_cn_address', '$cn_tel', '$cn_fax', '$cn_web',  '$cn_fb',  '$cn_line', '$cn_youtube', '$pathPic')";
	$Re_sql=$mysqli->query($sql);

	$GoTo = "admin_company_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}

	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="edit"){
	$photo=$_FILES["cn_logo_edit"];
	$photo=$_FILES['cn_logo_edit']['tmp_name'];
	$photo_name=$_FILES['cn_logo_edit']['name'];

	if($photo != "") {
		$sql_chk = "SELECT * FROM tb_company WHERE cn_id='$cn_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['cn_logo'];
		if($file!=""){
			if (file_exists ("../images/company/$file")){unlink("../images/company/$file");}
		}

		$new_name ="CN".date('His').rand(1000,9999); 
		$path = "../images/company"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>320){
			$width=320;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".png" || strchr($photo_name,".")==".PNG"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".png" || strchr($photo_name,".")==".PNG"){
				ImagePNG($images_photo_new,$images_photo); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic=$h_cn_logo;}
	
	$sql="UPDATE tb_company SET cn_name='$cn_name', cn_type='$cn_type', cn_company='$cn_company',  cn_address='$cn_address', 
	en_cn_name='$en_cn_name', en_cn_type='$en_cn_type', en_cn_company='$en_cn_company',  en_cn_address='$en_cn_address', 
	cn_tel='$cn_tel', cn_fax='$cn_fax', cn_web='$cn_web', cn_fb='$cn_fb', cn_line='$cn_line', cn_youtube='$cn_youtube', cn_logo='$pathPic' WHERE cn_id='$cn_id'";
	$Re_sql=$mysqli->query($sql);
	if (!$Re_sql) {printf("Error: %s\n", $mysqli->error);}
	
	$GoTo = "admin_company_edit.php?cn_id=$cn_id";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}

	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

	
if($_GET['action']=="dele"){
	if ((isset($_GET['cn_id'])) && ($_GET['cn_id'] != "")) {
		$cn_id_chk=$_GET['cn_id'];
		$sql_chk = "SELECT * FROM tb_company WHERE cn_id = '$cn_id_chk'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
		
		$file=$row_Re_chk['cn_logo'];
		if($file!=""){
			if (file_exists ("../images/company/$file")){
				unlink("../images/company/$file");
			}
		}

		$sql ="DELETE FROM tb_company WHERE cn_id='$cn_id'";
		$Re_sql=$mysqli->query($sql);

		$GoTo = "admin_company_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
	
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}
?>