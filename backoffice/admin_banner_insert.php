<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="insert";
?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_banner_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_banner_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo " เพิ่ม".$title;?></div>
                <div class="box_form">
                    <form action="admin_banner_save.php?action=insert" method="POST" enctype="multipart/form-data" name="form_ban" id="form_ban" >
                        <table width="100%">
                            <tr>
                                <td width="120">แสดง/ไม่แสดง</td>
                                <td>
                                    <select name="ban_status" id="ban_status">
                                        <option value="">เลือกสถานะ</option>
                                        <option value="1">แสดง</option>
                                        <option value="0">ไม่แสดง</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="120">รูปภาพขนาดใหญ่</td>
                                <td><input type="file" name="ban_photo" id="ban_photo" />ขนาดภาพกว้าง 1,300 x 350px.</td>
                            </tr>
                            <tr>
                                <td width="120">รูปภาพขนาดกลาง</td>
                                <td><input type="file" name="ban_photo" id="ban_photo" /> ขนาดภาพกว้าง 768 x 350px.</td>
                            </tr>
                            <tr>
                                <td width="120">รูปภาพขนาดเล็ก</td>
                                <td><input type="file" name="ban_photo" id="ban_photo" />ขนาดภาพกว้าง 414 x 350px.</td>
                            </tr>
                            <tr>
                                <td width="120" valign="top">URL</td>
                                <td><input type="text" id="ban_url" name="ban_url" style="width:600px"></td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr><input name="button" type="submit" id="button" value="บันทึกรายการ" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>