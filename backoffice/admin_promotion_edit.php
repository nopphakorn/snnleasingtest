<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

if (isset($_GET['pm_id'])) {$pm_id_chk = $_GET['pm_id'];}
$sql_pro="SELECT * FROM tb_promotion WHERE pm_id ='$pm_id_chk'";
$Re_pm=$mysqli->query($sql_pro);
$row_Re_pm=$Re_pm->fetch_assoc();
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
    <script>
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('#pm_detail', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
        });
    </script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_promotion_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_promotion_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;เพิ่ม".$title;?></div>
                <div class="box_form">
                    <form action="admin_promotion_save.php?action=edit" method="POST" enctype="multipart/form-data" name="form_pm" id="form_pm" >
                        <table width="100%" border="0" align="center">
                            <tr>
                                <td width="120" valign="top">รูปภาพ</td>
                                <td><div class="img_pm_full"><img src="../images/promotion/<?php echo $row_Re_pm['pm_photo']; ?>" /></div></td>
                            </tr>
                            <tr>
                                <td width="120">แสดง/ไม่แสดง</td>
                                <td>
                                    <select name="pm_status" id="pm_status">
                                        <option value="" <?php if (!(strcmp("", $row_Re_pm['pm_status']))) {echo "selected=\"selected\"";} ?>>เลือกสถานะ</option>
                                        <option value="1" <?php if (!(strcmp(1, $row_Re_pm['pm_status']))) {echo "selected=\"selected\"";} ?>>แสดง</option>
                                        <option value="0" <?php if (!(strcmp(0, $row_Re_pm['pm_status']))) {echo "selected=\"selected\"";} ?>>ไม่แสดง</option>
                                    </select>
                                </td>
                            </tr>
                                <td valign="top">รูปภาพปก</td>
                                <td valign="top"><input type="file" name="pm_photo_edit" id="pm_photo_edit" /></td>
							</tr>
                            <tr>
                                <td valign="top">โปรโมชั่น</td>
                                <td valign="top"><textarea name="pm_title" id="pm_title" style="height: 60px; width: 600px;" ><?php echo $row_Re_pm['pm_title'];?></textarea></td>
                            </tr>
                            <tr>
                                <td valign="top">รายละเอียด</td>
                                <td valign="top"><textarea name="pm_detail" id="pm_detail" style="height: 300px; width: 800px;" ><?php echo $row_Re_pm['pm_detail'];?></textarea></td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr /></td>
                            </tr>
                            <tr>
                                <td colspan="2" >
									<input type="hidden" name="pm_id" id="pm_id" value="<?php echo $row_Re_pm['pm_id'];?>">
									<input type="hidden" name="h_pm_photo" id="h_pm_photo" value="<?php echo $row_Re_pm['pm_photo'];?>">
									<input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>