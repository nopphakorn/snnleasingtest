<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

if (isset($_GET['a_id'])) {$a_id_chk = $_GET['a_id'];}
$sql_pro="SELECT * FROM tb_answer WHERE a_id ='$a_id_chk'";
$Re_pm=$mysqli->query($sql_pro);
$row_Re_pm=$Re_pm->fetch_assoc();
?>

<!doctype html>
<html>
<head>
	<?php include 's_inc_header.php';?>
	<script>
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('#a_que', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
			});
			editor = K.create('#a_anw', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
        });
    </script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_answer_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_answer_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;เพิ่ม".$title;?></div>
                <div class="box_form">
                    <form action="admin_answer_save.php?action=edit" method="POST" enctype="multipart/form-data" name="form_aw" id="form_aw" >
                        <table width="100%" border="0" align="center">
                            <tr>
                                <td valign="top">คำถาม</td>
                                <td valign="top"><textarea name="a_que" id="a_que" style="height: 60px; width: 600px;" ><?php echo $row_Re_pm['a_que'];?></textarea></td>
                            </tr>
                            <tr>
                                <td valign="top">คำตอบ</td>
                                <td valign="top"><textarea name="a_anw" id="a_anw" style="height: 200px; width: 800px;" ><?php echo $row_Re_pm['a_anw'];?></textarea></td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr /></td>
                            </tr>
                            <tr>
                                <td colspan="2" >
									<input type="hidden" name="a_id" id="a_id" value="<?php echo $row_Re_pm['a_id'];?>">
									<input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>