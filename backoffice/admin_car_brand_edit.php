<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');

$query_Re_ct = "SELECT * FROM tb_car_type ORDER BY ct_id ASC ";
$Re_ct=$mysqli->query($query_Re_ct);

$cb_id_chk=$_GET['cb_id'];
$query_Re_cb = "SELECT * FROM tb_car_brand WHERE cb_id='$cb_id_chk' ";
$Re_cb=$mysqli->query($query_Re_cb);
$row_Re_cb=$Re_cb->fetch_assoc();
$totalRows_Re_cb=$Re_cb->num_rows;
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>
<body>
	<div id="fancy_title"><i class="fa fa-list-alt"></i>&nbsp;แก้ไขชื่อยี่ห้อรถ</div>
    <div id="fancy_box">

		<form action="admin_car_save.php?action=brand-edit" method="post" enctype="multipart/form-data" name="form_cb" id="form_cb">
			ชื่อยี่ห้อรถ<br>
			<input type="text" id="cb_name" name="cb_name" style="width:250px" value="<?php echo $row_Re_cb['cb_name']; ?>" onkeyup="check_cb(form_cb.cb_name.value,form_cb.h_cb_name.value)">
			<span id="msg" class="tx_error"></span>

			<p>
			<?php 
			$a=explode(',',$row_Re_cb['ct_id']); 
			while($row_Re_ct = $Re_ct->fetch_assoc()){ 
			?>
				<label for="ct_id_list">
				<input type="checkbox" id="ct_id_list[]" name="ct_id_list[]" value="<?php echo $row_Re_ct['ct_id'];?>" <?php if(in_array($row_Re_ct['ct_id'],$a)){echo "checked";}?>> <?php echo $row_Re_ct['ct_name'];?>
				</label>
			<?php } ?>
			<div id="ct_id_list_validate"></div>
			</p>

			<input type="hidden" id="h_cb_name" name="h_cb_name" value="<?php echo $row_Re_cb['cb_name']; ?>">
			<input type="hidden" id="cb_id" name="cb_id" value="<?php echo $row_Re_cb['cb_id']; ?>">
			<p><hr><input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/></p>
		</form>
	</div>
</body>
</html>


