<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="insert";
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
    <script>
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('#sv_title', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
            editor = K.create('#sv_detail', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
            editor = K.create('#sv_title_en', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
            editor = K.create('#sv_detail_en', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
        });
    </script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_service_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_service_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;เพิ่มรายการ".$title;?></div>
                <div class="box_form">
                    <form action="admin_service_save.php?action=insert" method="POST" enctype="multipart/form-data" name="form_sv" id="form_sv" >
                        <table width="100%" border="0" align="center">
                            <tr>
                                <td width="160">เลือกสถานะ</td>
                                <td>
                                    <select name="sv_status" id="sv_status">
                                        <option value="">เลือกสถานะ</option>
                                        <option value="1">แสดง</option>
                                        <option value="0">ไม่แสดง</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>รูปภาพ</td>
                                <td><input type="file" name="sv_photo" id="sv_photo" /></td>
                            </tr>
                            <tr>
                                <td>ชื่อ</td>
                                <td><input name="sv_name" type="text" id="sv_name" style="width: 300px;"></td>
                            </tr>
                            <tr>
                                <td valign="top">รายละเอียด</td>
                                <td>
                                    <textarea name="sv_title" id="sv_title" style="height: 400px; width: 800px;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">การสมัคร</td>
                                <td>
                                    <textarea name="sv_detail" id="sv_detail" style="height: 400px; width: 800px;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr></td>
                            </tr>
                            <tr>
                                <td>ชื่อ (English)</td>
                                <td><input name="sv_name_en" type="text" id="sv_name_en" style="width: 300px;"></td>
                            </tr>
                            <tr>
                                <td valign="top">รายละเอียด (English)</td>
                                <td>
                                    <textarea name="sv_title_en" id="sv_title_en" style="height: 400px; width: 800px;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">การสมัคร (English)</td>
                                <td>
                                    <textarea name="sv_detail_en" id="sv_detail_en" style="height: 400px; width: 800px;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>