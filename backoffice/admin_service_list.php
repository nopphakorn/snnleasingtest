<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$maxRows_Re_sv = 50;
$pageNum_Re_sv = 0;
if (isset($_GET['pageNum_Re_sv'])) {
  $pageNum_Re_sv = $_GET['pageNum_Re_sv'];
}
$startRow_Re_sv = (($pageNum_Re_sv-1) * $maxRows_Re_sv);
if($startRow_Re_sv<0){$startRow_Re_sv=0;}

$query_Re_sv = "SELECT * FROM tb_service ORDER BY sv_no ASC ";
$query_limit_Re_sv = sprintf("%s LIMIT %d, %d", $query_Re_sv, $startRow_Re_sv, $maxRows_Re_sv);
$Re_sv=$mysqli->query($query_limit_Re_sv);

if (isset($_GET['totalRows_Re_sv'])) {
  $totalRows_Re_sv = $_GET['totalRows_Re_sv'];
} else {
  $all_Re_sv=$mysqli->query($query_Re_sv);
  $totalRows_Re_sv=$all_Re_sv->num_rows;
}
$totalPages_Re_sv = ceil($totalRows_Re_sv/$maxRows_Re_sv);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_sv==0){$page=1;}
else if($pageNum_Re_sv>0){$page=$pageNum_Re_sv;}

//---------------
$sql_mn = "SELECT max(sv_no) AS sv_max, min(sv_no) AS sv_min FROM tb_service ORDER BY sv_no ASC";
$Re_mn=$mysqli->query($sql_mn);
$row_Re_mn=$Re_mn->fetch_assoc();
$totalRows_Re_mn=$Re_mn->num_rows;

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_service_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_service_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;รายการ".$title;?></div>

                <!-- papeging -->
                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_sv=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_sv=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_sv>0){
                                echo "Page : ".$page."/".$totalPages_Re_sv." Total : ".number_format($totalRows_Re_sv)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                        

                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_sv=%d%s", $currentPage, $totalPages_Re_sv, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_sv){printf("%s?pageNum_Re_sv=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" border="1" align="center">
                    <tr>
                        <th width="45">ลำดับ</th>
                        <th width="60">ตำแหน่ง</th>
                        <th width="210">รูปภาพ</th>
                        <th width="">ชื่อเพิ่มรายการผลิตภัณฑ์และบริการ</th>
                        <th width="40">สถานะ</th>
                        <th width="40">แก้ไข</th>
                        <th width="40">ลบ</th>
                    </tr>
                    <?php 
                        if($totalPages_Re_sv>0){
                            while($row_Re_sv=$Re_sv->fetch_assoc()){
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $row_Re_sv['sv_no']; ?></div></td>
                        <td>
                            <div align="center">
                                <?php 
                                $list=$row_Re_sv['sv_no'];
                                $id=$row_Re_sv['sv_id'];
                                if($totalRows_Re_sv>=2){
                                ?>
                                <?php 
                                if($list==1){ 
                                ?>
                                    <div align="center">
                                        <a href="admin_service_save.php?action=no&amp;down=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_down.png" alt="down" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php 
                                } if($list!=1 and $list!=$row_Re_mn['sv_max']){
                                ?>
                                    <div align="center">
                                        <a href="admin_service_save.php?action=no&amp;down=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_down.png" alt="down" width="16" height="16" border="0" /></a>
                                        
                                        <a href="admin_service_save.php?action=no&amp;up=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_up.png" alt="up" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php 
                                } if($list==$row_Re_mn['sv_max'] AND $list>0){ 
                                ?>
                                    <div align="center">
                                        <a href="admin_service_save.php?action=no&amp;up=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_up.png" alt="up" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php } } ?>
                            </div>
                        </td>
                        <td><div align="center">
                                <div class="img_sv_list">
                                    <?php if($row_Re_sv['sv_photo']!=""){ ?>
                                    <img src="../images/service/<?php echo $row_Re_sv['sv_photo']; ?>" />
                                    <?php }else{ ?>
                                    <img src="../images/service/no_photo.jpg" />
                                    <?php } ?>
                                </div>
                            </div></td>
                        <td>
                            <div align="left">
                                <?php echo $row_Re_sv['sv_name'];?>
                                <?php if(!empty($row_Re_sv['sv_name_en'])){echo "<br>".$row_Re_sv['sv_name_en'];}?>
                            </div>
                        </td>
                        <td><div align="center">
                                <?php if($row_Re_sv['sv_status']=="1"){ ?>
                                <a href="admin_service_save.php?sv_id=<?php echo $row_Re_sv['sv_id']; ?>&action=status&sv_status=0"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
                                <?php }else{ ?>
                                <a href="admin_service_save.php?sv_id=<?php echo $row_Re_sv['sv_id']; ?>&action=status&sv_status=1"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
                                <?php } ?>
                            </div></td>
                        <td><div align="center">
                                <div align="center"> <a href="admin_service_edit.php?action=edit&sv_id=<?php echo $row_Re_sv['sv_id']; ?>"><img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a></div>
                            </div></td>
                        <td><div align="center">
                                <div align="center"> <a href="admin_service_save.php?action=dele&sv_id=<?php echo $row_Re_sv['sv_id']; ?>" onclick = "return confirm('คุณต้องการลบหน่วยงาน <?php echo $row_Re_sv['sv_name']; ?> หรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a></div>
                            </div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="7">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>

            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>