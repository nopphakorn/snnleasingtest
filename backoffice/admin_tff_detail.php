<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

if (isset($_GET['tff_id'])) {$tff_id_chk = mysqli_real_escape_string($mysqli, $_GET['tff_id']);}
$sql_admin="SELECT * FROM tb_tff WHERE tff_id = '".$tff_id_chk."'";
$Re_tff=$mysqli->query($sql_admin);
$row_Re_tff=$Re_tff->fetch_assoc();
?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="fancy_title"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;รายละเอียดสมาชิก TFF</div>
    <div id="fancy_box">
        <table width="100%" border="0">
            <tr>
                <td width="200">สถานะ</td>
                <td>
                    <?php
                    if($row_Re_tff['tff_status']=="1"){echo ": อนุญาติ";}
                    else if($row_Re_tff['tff_status']=="0"){echo ": ไม่อนุญาติ";}
                    ?>
                </td>
            </tr>
            <tr>
                <td><div align="left">บัตรประชาชน/หนังสือเดินทาง</div></td>
                <td><?php echo ": ".$row_Re_tff['tff_no']; ?></td>
            </tr>
            <tr>
                <td width="160"><div align="left">ชื่อ-นามสกุล</div></td>
                <td>
                    <?php 
                    if($row_Re_tff['tff_title']=='orther'){
                        echo ": ".$row_Re_tff['tff_title_n'];
                    }else{
                        echo ": ".$row_Re_tff['tff_title'];
                    }
                    echo ": ".$row_Re_tff['tff_name']." ".$row_Re_tff['tff_last']; 
                    ?>
                </td>
            </tr>
            <tr>
                <td><div align="left">วันเกิด</div></td>
                <td><?php echo ": ".datethai2($row_Re_tff['tff_bday']); ?></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><div align="left">Email</div></td>
                <td><?php echo ": ".$row_Re_tff['tff_email']; ?></td>
            </tr>
            <tr>
                <td><div align="left">เบอร์โทร</div></td>
                <td><?php echo ": ".$row_Re_tff['tff_tel']; ?></td>
            </tr>
            <tr>
                <td colspan="2"><hr></td>
            </tr>
            <tr>
                <td colspan="2">
                    ผลิตภัณฑ์และบริการของ เอส เอ็น เอ็น ที่คุณสนใจ (ตอบได้มากกว่า 1 ข้อ)<br>
                    <?php echo $row_Re_tff['tff_question']; ?>
                </td>
            </tr>
        </table>
    </div>
    

</body>
</html>
<?php $mysqli->close(); ?>