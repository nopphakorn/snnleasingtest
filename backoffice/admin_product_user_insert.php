<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="insert";
?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_product_user_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_product_user_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo" เพิ่ม".$title;?></div>
                <div class="box_form">
                    <form action="admin_product_user_save.php?action=insert" method="post" enctype="multipart/form-data" name="form_pu" id="form_pu">
                        <table width="100%" border="0">
                            <tr>
                                <td width="160">สถานะ</td>
                                <td>
                                    <select name="pro_u_status" id="pro_u_status">
                                        <option value="1">อนุญาติ</option>
                                        <option value="0">ไม่อนุญาติ</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="160"><div align="left">ชื่อ-นามสกุล</div></td>
                                <td><input name="pro_u_name" type="text" id="pro_u_name" size="30" /></td>
                            </tr>
                            <tr>
                                <td width="160">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="160"><div align="left">Username</div></td>
                                <td>
                                    <input type="text" name="pro_u_user" id="pro_u_user" onkeyup="check_pro_u_user(this.value)"/>
                                    <span id="msg" class="tx_error"></span>
                                    <input type="hidden" name="h_pro_u_user" id="h_pro_u_user" />
                                </td>
                            </tr>
                            <tr>
                                <td width="160"><div align="left">Password</div></td>
                                <td><input type="password" name="pro_u_pass" id="pro_u_pass" /></td>
                            </tr>
                            <tr>
                                <td width="160"><div align="left">Confirm Password</div></td>
                                <td><input type="password" name="c_pro_u_pass" id="c_pro_u_pass"/></td>
                            </tr>
                            <tr>
                                <td colspan="2" >
                                    <hr>
                                    <input name="Submit" type="submit" id="Submit" value="เพิ่มรายชื่อ" />
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>