<?php require_once('Connections/con_db.php'); ?>
<?php 
include('function/f_admin.php');
$page_nav="edit";
?>

<!doctype html>
<html>
<head>
	<?php include 's_inc_header.php';?>
</head>

<body>
	<div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_user_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

	<div id="containner"> 
    	<div id="main">
      		<div id="main_content">
				<div class="main_content_title"><i class="fa fa-pencil fa-lg" style="color:#3097FF"></i><?php echo" แก้ไข Password ".$title;?></div>
      			<div id="success">คุณ <?php echo $_SESSION['AD_Name']; ?> 
				  	เปลี่ยน Password เรียบร้อย<br>
      		  		หากต้องการไปหน้าหลัก <a href="admin.php">คลิกที่นี่</a><br>
   				</div>
      		</div>
		</div>
	</div>
</body>
</html>