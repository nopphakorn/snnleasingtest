<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="edit";

$n_id_chk = mysqli_real_escape_string($mysqli, $_GET['n_id']);
$sql_n = "SELECT * FROM tb_news WHERE tb_news.n_id = '$n_id_chk'";
$Re_n=$mysqli->query($sql_n);
$row_Re_n =$Re_n->fetch_assoc();
$totalRows_Re_n = $Re_n->num_rows;

$sql_n_p = "SELECT * FROM tb_news_photo WHERE n_id = '$n_id_chk' ORDER BY n_p_id ASC";
$Re_n_p = $mysqli->query($sql_n_p);
$totalRows_Re_n_p = $Re_n_p->num_rows;

?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
    <script>
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('#n_detail', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
            editor = K.create('#en_n_detail', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
        });
    </script>

    <script type="text/javascript">
        function fncCreateElement1(){
            var mySpan1 = document.getElementById('mySpan1');
            var myElement = document.createElement('input');
            var myElement2 = document.createElement('br');
            myElement.setAttribute('type',"file");
            myElement.setAttribute('name',"n_p_photo[]");
            mySpan1.appendChild(myElement);	
            mySpan1.appendChild(myElement2);  
        }
    </script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_news_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_news_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-pencil fa-lg" style="color:#3097FF"></i><?php echo" แก้ไข".$title;?></div>
                <div class="box_form">
                    <form action="admin_news_save.php?action=edit" method="post" enctype="multipart/form-data" name="form_news" id="form_news">
                        <table width="800" >
                            <?php if($row_Re_n['n_photo']!=""){ ?>
                            <tr>
                                <td colspan="2">
                                    <p class="img_n_full"><img src="../images/news/<?php echo $row_Re_n['n_photo']; ?>" alt=""/></p>
                                     <hr>
                                </td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td width="130">วันที่โพสต์</td>
                                <td>
                                    <?php echo datethai($row_Re_n['n_date']); ?>
                                    <input name="n_date" type="hidden" id="n_date" value="<?php echo $row_Re_n['n_date']; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>สถานะ</td>
                                <td>
                                    <select name="n_status" id="n_status">
                                        <option value="1" <?php if($row_Re_n['n_status']=="1"){ echo "selected"; } ?> <?php if (!(strcmp(1, $row_Re_n['n_status']))) {echo "selected=\"selected\"";} ?>>แสดง</option>
                                        <option value="0" <?php if($row_Re_n['n_status']=="0"){ echo "selected"; } ?> <?php if (!(strcmp(0, $row_Re_n['n_status']))) {echo "selected=\"selected\"";} ?>>ไม่แสดง</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">เปลี่ยนภาพปก</td>
                                <td><input type="file" name="n_photo_edit" id="n_photo_edit"></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br>หัวข้อเรื่อง<br>
                                    <textarea name="n_name" cols="50" rows="3" id="n_name"><?php echo $row_Re_n['n_name']; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br>รายละเอียด<br>
                                    <textarea name="n_detail" id="n_detail" style="height: 300px; width: 99%;"><?php echo $row_Re_n['n_detail']; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br>Title (English)<br>
                                    <textarea name="en_n_name" cols="50" rows="3" id="en_n_name"><?php echo $row_Re_n['en_n_name']; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br>Detail (English)<br>
                                    <textarea name="en_n_detail" id="en_n_detail" style="height: 300px; width: 99%;"><?php echo $row_Re_n['en_n_detail']; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr size="3" noshade="noshade" /></td>
                            </tr>
                            <tr>
                                <td valign="top"><i class="fa fa-picture-o fa-lg"></i>&nbsp;ภาพแกลเลอรี่</td>
                                <td>
                                    <input type="file" name="n_p_photo[]" id="n_p_photo[]" />
                                    <input type="button" name="button1" id="button1" value="+" onclick="JavaScript:fncCreateElement1();" />
                                    (กด + เพื่อเพิ่มรูปภาพ)<br/>
                                    <div id="mySpan1"></div>
                                </td>
                            </tr>
                            <?php if($totalRows_Re_n_p > 0){ ?>
                            <tr>
                                <td colspan="2" valign="top">
                                        <?php
                                        while($row_Re_n_p=$Re_n_p->fetch_assoc()) {
			                            ?>
                                        <div class="box_n">
                                            <div class="img_n_list">
                                                <a href="../images/news/<?php echo $row_Re_n_p['n_p_photo']; ?>"  rel="lightbox[]">
                                                <img src="../images/news/<?php echo $row_Re_n_p['n_p_photo']; ?>" width="150" border="0" /></a>
                                            </div>
                                            <div align="right">
                                                
                                                ลบรูปภาพ&nbsp; <a href="admin_news_save.php?action=dele_p&amp;n_id=<?php echo $row_Re_n_p['n_id']; ?>&amp;n_p_id=<?php echo $row_Re_n_p['n_p_id']; ?>&amp;n_p_photo=<?php echo $row_Re_n_p['n_p_photo']; ?>" onclick="return confirm('คุณต้องการลบภาพนี้หรืมไม่')" ><i class="fa fa-times tx_red" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                        <?php } ?>
                                </td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="2"><hr size="3" noshade="noshade" /></td>
                            </tr>
                            <tr>
                                <td><input type="submit" name="button" id="button" value="บันทึกข้อมูล" />
                                    <input name="n_id" type="hidden" id="n_id" value="<?php echo $row_Re_n['n_id']; ?>">
                                    <input name="h_n_photo" type="hidden" id="h_n_photo" value="<?php echo $row_Re_n['n_photo']; ?>"></td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript" src="tools/lightbox/js/lightbox.js"></script>
</body>
</html>
<?php $mysqli->close(); ?>