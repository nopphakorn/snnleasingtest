<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="type-list";

$query_Re_ct = "SELECT * FROM tb_car_type ORDER BY ct_id ASC ";
$Re_ct=$mysqli->query($query_Re_ct);
$totalRows_Re_ct=$Re_ct->num_rows;

?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
	
	<script type="text/javascript">
        $(document).ready(function() {
            $('.btn_insert').fancybox({
                    'type' : 'iframe',
                    'width' : '500',
                    'height' : '200',
                    'autoScale' : false,
                    'fitToView' : false,
                    'autoSize' : false,
                    //'onClosed' : function() {parent.location.reload(true);},
                    'afterClose' : function() {parent.location.reload(true); },
            });
        });
	</script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_car_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_car_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i>&nbsp;รายการประเภทรถ</div>

				<span class="btn_green">
					<a class="btn_insert" href="admin_car_type_insert.php">
					<i class="fa fa-plus fa-lg" style="color:#3097FF"></i> เพิ่มประเภทรถ</a>
				</span>

                <table class="tb1" width="100%" border="1" align="center" cellpadding="5">
                    <tr>
                        <th width="45"><div align="center">No.</div></th>
                        <th width=""><div align="center">รายการประเภท</div></th>
                        <th width="40">สถานะ</th>
                        <th width="40"><div align="center">แก้ไข</div></th>
                        <th width="40"><div align="center">ลบ</div></th>
                    </tr>
                    <?php 
                    if($totalRows_Re_ct>0){
                        $number=0;
                        while($row_Re_ct=$Re_ct->fetch_assoc()){
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $number+=1; ?></div></td>
                        <td>
                            <div align="left">
                                <?php echo $row_Re_ct['ct_name']; ?><br>
                            </div>
                        </td>
                        <td><div align="center">
                                <?php if($row_Re_ct['ct_status']=="1"){ ?>
                                <a href="admin_car_save.php?action=type-status&ct_id=<?php echo $row_Re_ct['ct_id']; ?>&ct_status=0"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
                                <?php }else{ ?>
                                <a href="admin_car_save.php?action=type-status&ct_id=<?php echo $row_Re_ct['ct_id']; ?>&ct_status=1"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
                                <?php } ?>
                            </div></td>
                        <td>
                            <div align="center">
                                <a class="btn_insert" href="admin_car_type_edit.php?ct_id=<?php echo $row_Re_ct['ct_id']; ?>">
                                <img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td width="19" scope="col"><div align="center">
                                <div align="center"> <a href="admin_car_save.php?action=type-dele&ct_id=<?php echo $row_Re_ct['ct_id']; ?>" onclick = "return confirm('คุณต้องการลบประเภทรถหรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a></div>
                            </div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="5">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>   
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>

