<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$sql_ban = "SELECT * FROM tb_banner ORDER BY ban_no ASC";
$Re_ban=$mysqli->query($sql_ban);
$totalRows_Re_ban=$Re_ban->num_rows;

$sql_mn = "SELECT max(ban_no) AS ban_max, min(ban_no) AS ban_min FROM tb_banner ORDER BY ban_no ASC";
$Re_mn=$mysqli->query($sql_mn);
$row_Re_mn=$Re_mn->fetch_assoc();
$totalRows_Re_mn=$Re_mn->num_rows;

?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_banner_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_banner_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo" รายการ".$title;?></div>

                <table class="tb1" width="100%" align="center">
                    <tr>
                        <th width="45">ลำดับ</th>
                        <th width="60">ตำแหน่ง</th>
                        <th>ป้ายประชาสัมพันธ์</th>
                        <th width="40">สถานะ</th>
                        <th width="40">แก้ไข</th>
                        <th width="40">ลบ</th>
                    </tr>
                    <?php
                    if($totalRows_Re_ban>0){
                        while($row_Re_ban=$Re_ban->fetch_assoc()){
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $row_Re_ban['ban_no']; ?></div></td>
                        <td>
                            <div align="center">
                                <?php 
                                $list=$row_Re_ban['ban_no'];
                                $id=$row_Re_ban['ban_id'];
                                if($totalRows_Re_ban>=2){
                                ?>
                                <?php 
                                if($list==1){ 
                                ?>
                                    <div align="center">
                                        <a href="admin_banner_save.php?action=no&amp;down=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_down.png" alt="down" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php 
                                } if($list!=1 and $list!=$row_Re_mn['ban_max']){
                                ?>
                                    <div align="center">
                                        <a href="admin_banner_save.php?action=no&amp;down=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_down.png" alt="down" width="16" height="16" border="0" /></a>
                                        
                                        <a href="admin_banner_save.php?action=no&amp;up=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_up.png" alt="up" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php 
                                } if($list==$row_Re_mn['ban_max'] AND $list>0){ 
                                ?>
                                    <div align="center">
                                        <a href="admin_banner_save.php?action=no&amp;up=1&amp;list=<?php echo $list; ?>&amp;id=<?php echo $id; ?>">
                                        <img src="images/icon/icon_anw_up.png" alt="up" width="16" height="16" border="0" /></a>
                                    </div>
                                <?php } } ?>
                            </div>
                        </td>
                        <td>
                            <div class="img_ban_list"><img src="../images/banner/<?php echo $row_Re_ban['ban_photo']; ?>" border="0" /></div>
                            <?php if($row_Re_ban['ban_url']!=""){ 
                                echo "<br>URL : <a href=\"".$row_Re_ban['ban_url']."\">".$row_Re_ban['ban_url']."</a>";
                            }?>
                        </td>
                        <td>
                            <div align="center">
                                <?php if($row_Re_ban['ban_status']=="1"){ ?>
                                <a href="admin_banner_save.php?ban_id=<?php echo $row_Re_ban['ban_id']; ?>&action=status&ban_status=0"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
                                <?php }else{ ?>
                                <a href="admin_banner_save.php?ban_id=<?php echo $row_Re_ban['ban_id']; ?>&action=status&ban_status=1"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
                                <?php } ?>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_banner_edit.php?ban_id=<?php echo $row_Re_ban['ban_id']; ?>"><img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td>
                            <div align="center">
                                <a href="admin_banner_save.php?action=dele&ban_id=<?php echo $row_Re_ban['ban_id']; ?>" onclick = "return confirm('คุณต้องการลบรูปภาพนี้หรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a>
                            </div>
                        </td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="6">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
   
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>