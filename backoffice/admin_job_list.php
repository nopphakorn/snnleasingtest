<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="list";

$maxRows_Re_job = 50;
$pageNum_Re_job = 0;
if (isset($_GET['pageNum_Re_job'])) {
  $pageNum_Re_job = $_GET['pageNum_Re_job'];
}
$startRow_Re_job = (($pageNum_Re_job-1) * $maxRows_Re_job);
if($startRow_Re_job<0){$startRow_Re_job=0;}

$query_Re_job = "SELECT * FROM tb_job ORDER BY j_id ASC ";
$query_limit_Re_job = sprintf("%s LIMIT %d, %d", $query_Re_job, $startRow_Re_job, $maxRows_Re_job);
$Re_job=$mysqli->query($query_limit_Re_job);

if (isset($_GET['totalRows_Re_job'])) {
  $totalRows_Re_job = $_GET['totalRows_Re_job'];
} else {
  $all_Re_job=$mysqli->query($query_Re_job);
  $totalRows_Re_job=$all_Re_job->num_rows;
}
$totalPages_Re_job = ceil($totalRows_Re_job/$maxRows_Re_job);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";

if($pageNum_Re_job==0){$page=1;}
else if($pageNum_Re_job>0){$page=$pageNum_Re_job;}

?>
<!doctype html>
<html>
<head>
  <?php include 's_inc_header.php';?>
</head>

<body>
  <div id="header"> <?php include("s_header.php"); ?></div>
  <div id="nav"><?php include("admin_job_nav.php"); ?></div>
  <div id="side"><?php include('s_menu_side.php'); ?></div>
  <div id="containner">
    <div id="main">
      <?php include("admin_job_menu.php"); ?>
      <div id="main_content">
        <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i><?php echo " รายการ".$title; ?></div>

        <div class="pi_box">
            <div class="pi_box_l">
                <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_job=%d%s", $currentPage, 0, $currentSent); ?>">
                <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                </a>

                <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_job=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                <i class="fa fa-angle-left" aria-hidden="true"></i>
              </a>
            </div>
            <div class="pi_box_m">
                <span class="page-link">
                    <?php 
                    if($totalPages_Re_job>0){
                        echo "Page : ".$page."/".$totalPages_Re_job." Total : ".number_format($totalRows_Re_job)." record";
                    }else{
                        echo "-";
                    }
                    ?>
              </span>
            </div>
            <div class="pi_box_r">
              <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_job=%d%s", $currentPage, $totalPages_Re_job, $currentSent); ?>">
                <i class="fa fa-angle-double-right" aria-hidden="true"></i>
              </a>

              <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_job){printf("%s?pageNum_Re_job=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                <i class="fa fa-angle-right" aria-hidden="true"></i>
              </a>
            </div>
        </div>

        <table class="tb1" width="100%" border="1" align="center">
          <tr>
            <th width="35">No.</th>
            <th width="60">เร่งด่วน</th>
            <th width="150">JOB TITLE</th>
            <th width="150">JOB FUNCTION</th>
            <th width="120">สถานที่ทำงาน</th>
            <th width="">รายละเอียด</th>
            <th width="35">สถานะ</th>
            <th width="35">แก้ไข</th>
            <th width="35">ลบ</th>
          </tr>
          <?php
          if($totalRows_Re_job>0){
          $number=$startRow_Re_job;
          while ($row_Re_job = mysqli_fetch_assoc($Re_job)){
          ?>
          <tr>
            <td valign="top"><div align="center"><?php echo $number+=1; ?></div></td>
            <td valign="top"><div align="center"><?php echo $row_Re_job['j_urgent']; ?></div></td>
            <td valign="top"><?php echo $row_Re_job['j_title']."<br><br>".$row_Re_job['en_j_title']; ?></td>
            <td valign="top"><?php echo $row_Re_job['j_function']."<br><br>".$row_Re_job['en_j_title']; ?></td>
            <td valign="top"><?php echo $row_Re_job['j_location']."<br><br>".$row_Re_job['en_j_title']; ?></td>
            <td valign="top"><?php echo $row_Re_job['j_detail']."<br><br>".$row_Re_job['en_j_title'];?></td>
            <td valign="top">
              <div align="center">
              <?php if($row_Re_job['j_status']=="1"){ ?>
              <a href="admin_job_save.php?j_id=<?php echo $row_Re_job['j_id']; ?>&action=status&j_status=0"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
              <?php }else{ ?>
              <a href="admin_job_save.php?j_id=<?php echo $row_Re_job['j_id']; ?>&action=status&j_status=1"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
              <?php } ?>
              </div>
            </td>
            <td valign="top">
              <div align="center">
              <a href="admin_job_edit.php?action=edit&j_id=<?php echo $row_Re_job['j_id']; ?>">
                <img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
              </div>
            </td>
            <td valign="top">
              <div align="center">
              <a href="admin_job_save.php?action=dele&j_id=<?php echo $row_Re_job['j_id']; ?>" onclick = "return confirm('คุณต้องการลบตำแหน่ง <?php echo $row_Re_job['j_title']; ?> หรือไม่')">
                <img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a>
              </div>
            </td>
          </tr>
          <?php }}else{ ?>
            <tr>
              <td colspan="10">
                <div class="alert_table">
                  <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้
                </div>
              </td>
            </tr>
          <?php } ?>
        </table>
      </div>
    </div>
  </div>
  <div id="footer">  <?php include("s_footer.php"); ?></div>
</body>
</html>
