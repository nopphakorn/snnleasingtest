<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="insert";
?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
    <link rel="stylesheet" href="tools/datepicker/jquery-ui.css">
	<script src="tools/datepicker/jquery-ui.js"></script>
	<script src="tools/datepicker/jqueryui_datepicker_thai.js"></script>
    <script type="text/javascript">
		$( function () {
			$( "#i_date" ).datepicker( {
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true,
		        buttonImage: "images/icon/calendar.gif",
			} );
		} );
	</script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_interest_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_interest_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo " เพิ่ม ".$title;?></div>
                <div class="box_form">
                    <form action="admin_interest_save.php?action=insert" method="POST" enctype="multipart/form-data" name="form_ir" id="form_ir" >
                        <table width="100%">
                            <tr>
                                <td width="160" valign="top">ประเภทไฟล์</td>
                                <td>
                                    <select name="i_type" id="i_type">
                                        <option value="">เลือก</option>
                                        <option value="PDF">PDF</option>
                                        <option value="JPG">รูปภาพ</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="120" valign="top">วันที่ประกาศและบังคับใช้</td>
                                <td><input type="text" id="i_date" name="i_date" readonly></td>
                            </tr>
                            <tr><td colspan="2">&nbsp;</tr>
							<tr>
                                <td width="120" valign="top">หัวข้อ</td>
                                <td><input type="text" id="i_name" name="i_name" style="width:600px"></td>
                            </tr>
                            <tr>
                                <td width="120" valign="top">Name</td>
                                <td><input type="text" id="e_i_name" name="e_i_name" style="width:600px"></td>
                            </tr>
                            <tr><td colspan="2">&nbsp;</tr>
                            <tr>
                                <td width="120" valign="top">คำแนะนำ</td>
                                <td><input type="text" id="i_title" name="i_title" style="width:600px"></td>
                            </tr>
                            <tr>
                                <td width="120" valign="top">Title</td>
                                <td><input type="text" id="e_i_title" name="e_i_title" style="width:600px"></td>
                            </tr>
                            <tr><td colspan="2">&nbsp;</tr>
                            <tr>
                                <td width="120">ไฟล์แนบ</td>
                                <td>
                                    <input type="file" name="i_file" id="i_file" />
                                    เฉพาะไฟล์ PDF หรือ JPG ขนาดกว่างไม่เกิน 900px เท่านั้น
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr>
								<input name="button" type="submit" id="button" value="บันทึกรายการ" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>