<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST["ct_id"])){$ct_id=mysqli_real_escape_string($mysqli, $_POST["ct_id"]);}
if(isset($_POST["ct_name"])){$ct_name=mysqli_real_escape_string($mysqli, $_POST["ct_name"]);}
if(isset($_POST["cb_id"])){$cb_id=mysqli_real_escape_string($mysqli, $_POST["cb_id"]);}
if(isset($_POST["cb_name"])){$cb_name=mysqli_real_escape_string($mysqli, $_POST["cb_name"]);}
if(isset($_POST["cg_id"])){$cg_id=mysqli_real_escape_string($mysqli, $_POST["cg_id"]);}
if(isset($_POST["cg_name"])){$cg_name=mysqli_real_escape_string($mysqli, $_POST["cg_name"]);}
if(isset($_POST["cm_id"])){$cm_id=mysqli_real_escape_string($mysqli, $_POST["cm_id"]);}
if(isset($_POST["cm_name"])){$cm_name=mysqli_real_escape_string($mysqli, $_POST["cm_name"]);}
if(isset($_POST["c_id"])){$c_id=mysqli_real_escape_string($mysqli, $_POST["c_id"]);}
if(isset($_POST["c_year"])){$c_year=mysqli_real_escape_string($mysqli, $_POST["c_year"]);}
if(isset($_POST["c_gear"])){$c_gear=mysqli_real_escape_string($mysqli, $_POST["c_gear"]);}
if(isset($_POST["c_price"])){$c_price=mysqli_real_escape_string($mysqli, $_POST["c_price"]);}
if(isset($_POST["c_date"])){$c_date=dateEng(mysqli_real_escape_string($mysqli, $_POST["c_date"]));}
if(isset($_POST["c_date_t"])){$c_date_t=mysqli_real_escape_string($mysqli, $_POST["c_date_t"]);}
if(isset($_POST["c_user"])){$c_user=mysqli_real_escape_string($mysqli, $_POST["c_user"]);}
if(isset($_POST["ty_car"])){$ty_car=mysqli_real_escape_string($mysqli, $_POST["ty_car"]);}

if($action=="type-status"){
	$ct_id = $_GET['ct_id'];
	$ct_status = $_GET['ct_status'];
	
	$sql="UPDATE tb_car_type SET ct_status=$ct_status WHERE ct_id='$ct_id'";
	$Re_sql=$mysqli->query($sql);
		
	$GoTo = "admin_car_type.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="type-insert"){
	$sql="INSERT INTO tb_car_type(ct_name, ct_status) values('$ct_name','1')";
	$Re_sql=$mysqli->query($sql);
	
	$GoTo = "admin_car_type_insert.php?save=sucess";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="type-edit"){
	$sql="UPDATE tb_car_type SET ct_name='$ct_name' WHERE ct_id='$ct_id'";
	$Re_sql=$mysqli->query($sql);
	
	$GoTo = "admin_car_type_edit.php?ct_id=$ct_id";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="type-dele"){
	if ((isset($_GET['ct_id'])) && ($_GET['ct_id'] != "")){
		$ct_id=$_GET['ct_id'];

		$sql1 ="DELETE FROM tb_car_type WHERE ct_id='$ct_id'";
		$Re_sql1=$mysqli->query($sql1);

		$GoTo = "admin_car_type.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}

if($action=="brand-status"){
	$cb_id = $_GET['cb_id'];
	$cb_status = $_GET['cb_status'];
	
	$sql="UPDATE tb_car_brand SET cb_status=$cb_status WHERE cb_id='$cb_id'";
	$Re_sql=$mysqli->query($sql);
		
	$GoTo = "admin_car_brand.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="brand-insert"){
	$ct_id_list = implode(',', $_POST['ct_id_list']);
	$Re_chk = $mysqli -> query("SELECT * FROM tb_car_type WHERE ct_id IN ($ct_id_list)");
    $total_Re_chk=$Re_chk->num_rows;

    $ct_name="";
    $x = 0; 
    while($row = $Re_chk -> fetch_assoc()){
      $x++;
      if($total_Re_chk!=$x){$ct_name.=$row['ct_name'].",";}
      else {$ct_name.= $row['ct_name'];}
    }

	$sql="INSERT INTO tb_car_brand(cb_name, cb_status, ct_id, ct_name) values('$cb_name','1','$ct_id_list', '$ct_name')";
	$Re_sql=$mysqli->query($sql);
	
	$GoTo = "admin_car_brand_insert.php?save=sucess";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="brand-edit"){

	$ct_id_list = implode(',', $_POST['ct_id_list']);
	$Re_chk = $mysqli -> query("SELECT * FROM tb_car_type WHERE ct_id IN ($ct_id_list)");
    $total_Re_chk=$Re_chk->num_rows;

    $ct_name="";
    $x = 0; 
    while($row = $Re_chk -> fetch_assoc()){
      $x++;
      if($total_Re_chk!=$x){$ct_name.=$row['ct_name'].",";}
      else {$ct_name.= $row['ct_name'];}
    }

	$sql="UPDATE tb_car_brand SET cb_name='$cb_name', ct_id='$ct_id_list', ct_name='$ct_name' WHERE cb_id='$cb_id'";
	$Re_sql=$mysqli->query($sql);
	
	$GoTo = "admin_car_brand_edit.php?cb_id=$cb_id";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="brand-dele"){
	if ((isset($_GET['cb_id'])) && ($_GET['cb_id'] != "")){
		$cb_id=$_GET['cb_id'];

		$sql1 ="DELETE FROM tb_car_brand WHERE cb_id='$cb_id'";
		$Re_sql1=$mysqli->query($sql1);

		$GoTo = "admin_car_brand.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}

if($action=="group-status"){
	$cg_id = $_GET['cg_id'];
	$cg_status = $_GET['cg_status'];
	
	$sql="UPDATE tb_car_group SET cg_status=$cg_status WHERE cg_id='$cg_id'";
	$Re_sql=$mysqli->query($sql);
		
	$GoTo = "admin_car_group.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="group-insert"){
	$sql="INSERT INTO tb_car_group(cb_id, ct_id, cg_name, cg_status) values('$cb_id', '$ct_id', '$cg_name','1')";
	$Re_sql=$mysqli->query($sql);
	
	$GoTo = "admin_car_group_insert.php?save=sucess";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="group-edit"){
	$sql="UPDATE tb_car_group SET cb_id='$cb_id', ct_id='$ct_id', cg_name='$cg_name' WHERE cg_id='$cg_id'";
	$Re_sql=$mysqli->query($sql);
	
	$GoTo = "admin_car_group_edit.php?cg_id=$cg_id";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="group-dele"){
	if ((isset($_GET['cg_id'])) && ($_GET['cg_id'] != "")){
		$cg_id=$_GET['cg_id'];

		$sql1 ="DELETE FROM tb_car_group WHERE cg_id='$cg_id'";
		$Re_sql1=$mysqli->query($sql1);

		$GoTo = "admin_car_group.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}

if($action=="model-status"){
	$cm_id = $_GET['cm_id'];
	$cm_status = $_GET['cm_status'];
	
	$sql="UPDATE tb_car_model SET cm_status=$cm_status WHERE cm_id='$cm_id'";
	$Re_sql=$mysqli->query($sql);
		
	$GoTo = "admin_car_model.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="model-insert"){
	$sql="INSERT INTO tb_car_model(cb_id,cg_id,cm_name,cm_status,ty_car) values('$cb_id','$cg_id','$cm_name','1','$ty_car')";
	$Re_sql=$mysqli->query($sql);
	
	$GoTo = "admin_car_model_insert.php?save=sucess";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="model-edit"){
	$sql="UPDATE tb_car_model SET cb_id='$cb_id',cg_id='$cg_id',cm_name='$cm_name' ,ty_car='$ty_car' WHERE cm_id='$cm_id'";
	
	$Re_sql=$mysqli->query($sql);
	
	$GoTo = "admin_car_model_edit.php?cm_id=$cm_id";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="model-dele"){
	if ((isset($_GET['cm_id'])) && ($_GET['cm_id'] != "")){
		$cm_id=$_GET['cm_id'];

		$sql1 ="DELETE FROM tb_car_model WHERE cm_id='$cm_id'";
		$Re_sql1=$mysqli->query($sql1);

		$GoTo = "admin_car_model.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}

if($action=="all-status"){
	$c_id = $_GET['c_id'];
	$c_status = $_GET['c_status'];
	
	$sql="UPDATE tb_car SET c_status=$c_status WHERE c_id='$c_id'";
	$Re_sql=$mysqli->query($sql);
		
	$GoTo = "admin_car_all.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="all-insert"){

	$photo=$_FILES["c_photo1"];
	$photo=$_FILES['c_photo1']['tmp_name'];
	$photo_name=$_FILES['c_photo1']['name'];

	$photo2=$_FILES["c_photo2"];
	$photo2=$_FILES['c_photo2']['tmp_name'];
	$photo2_name=$_FILES['c_photo2']['name'];

	if($photo != "") {
		$new_name ="C".date('His').rand(1000,9999); 
		$path = "../images/model"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>640){
			$width=640;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$newphoto1="$RenameFile";	
		}
	}else{$newphoto1="";}

	if($photo2 != "") {
		$new_name ="C".date('His').rand(1000,9999); 
		$path = "../images/model"; 
										
		$images_photo = $photo2;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>640){
			$width=640;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo2_name,".")==".jpg" || strchr($photo2_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo2_name,".")==".JPG" || strchr($photo2_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo2_name,".")==".jpg" || strchr($photo2_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo2_name,".")==".JPG" || strchr($photo2_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo2_name,".");
		if (copy( $photo2 , "$path/$RenameFile" )){		
			unlink($photo2);
			$newphoto2="$RenameFile";	
		}
	}else{$newphoto2="";}

	$c_date=$c_date." ".$c_date_t;
	$sql="INSERT INTO tb_car(ct_id, cb_id, cg_id, cm_id, c_year, c_gear, c_price, c_status, c_date, c_user, c_photo1, c_photo2) 
	VALUES ('$ct_id','$cb_id','$cg_id','$cm_id','$c_year','$c_gear','$c_price','1','$c_date','$c_user' ,'$newphoto1' ,'$newphoto2')";
	$Re_sql=$mysqli->query($sql);
	exit;
	$GoTo = "admin_car_all_insert.php?save=sucess";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?"; 
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="all-edit"){
	if(isset($_POST["h_c_photo1"])){$h_c_photo1=mysqli_real_escape_string($mysqli, $_POST["h_c_photo1"]);}
	if(isset($_POST["h_c_photo2"])){$h_c_photo2=mysqli_real_escape_string($mysqli, $_POST["h_c_photo2"]);}
	
	$photo=$_FILES["c_photo1"];
	$photo=$_FILES['c_photo1']['tmp_name'];
	$photo_name=$_FILES['c_photo1']['name'];

	$photo2=$_FILES["c_photo2"];
	$photo2=$_FILES['c_photo2']['tmp_name'];
	$photo2_name=$_FILES['c_photo2']['name'];

	if($photo != "") {

		$sql_chk = "SELECT * FROM tb_car WHERE c_id='$c_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['c_photo1'];
		if($file!=""){
			if (file_exists ("../images/model/$file")){unlink("../images/model/$file");}
		}

		$new_name ="C".date('His').rand(1000,9999); 
		$path = "../images/model"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>640){
			$width=640;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$newphoto1="$RenameFile";	
		}
	}else{$newphoto1=$h_c_photo1;}

	if($photo2 != "") {

		$sql_chk = "SELECT * FROM tb_car WHERE c_id='$c_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['c_photo2'];
		if($file!=""){
			if (file_exists ("../images/model/$file")){unlink("../images/model/$file");}
		}

		$new_name ="C".date('His').rand(1000,9999); 
		$path = "../images/model"; 
										
		$images_photo = $photo2;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>640){
			$width=640;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo2_name,".")==".jpg" || strchr($photo2_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo2_name,".")==".JPG" || strchr($photo2_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo2_name,".")==".jpg" || strchr($photo2_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo2_name,".")==".JPG" || strchr($photo2_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo2_name,".");
		if (copy( $photo2 , "$path/$RenameFile" )){		
			unlink($photo2);
			$newphoto2="$RenameFile";	
		}
	}else{$newphoto2=$h_c_photo2;}


	$c_date=$c_date." ".$c_date_t;
	$sql="UPDATE tb_car SET ct_id='$ct_id', cb_id='$cb_id', cg_id='$cg_id', cm_id='$cm_id', c_year='$c_year', c_gear='$c_gear', c_price='$c_price',
	c_date='$c_date', c_user='$c_user', c_photo1='$newphoto1', c_photo2='$newphoto2'  WHERE c_id='$c_id'";
	$Re_sql=$mysqli->query($sql);
	/*echo $sql;*/
	$GoTo = "admin_car_all_edit.php?c_id=".$c_id."&save=sucess";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}

	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="all-dele"){
	if ((isset($_GET['c_id'])) && ($_GET['c_id'] != "")){
		$cm_id=$_GET['c_id'];

		$sql = "SELECT * FROM tb_car WHERE c_id='$c_id'";
		$Re_chk=$mysqli->query($sql);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file1=$row_Re_chk['c_photo1'];
		$file2=$row_Re_chk['c_photo2'];
		if($file1!=""){
			if (file_exists ("../images/model/$file1")){
				unlink("../images/model/$file1");
			}
		}
		if($file2!=""){
			if (file_exists ("../images/model/$file2")){
				unlink("../images/model/$file2");
			}
		}

		$sql1 ="DELETE FROM tb_car WHERE c_id='$cm_id'";
		$Re_sql1=$mysqli->query($sql1);

		$GoTo = "admin_car_all.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}

if($_GET['action']=="dele_p_carprice"){
	$c_id=$_GET['c_id'];
	$photo=$_GET['photo'];

	$sql = "SELECT * FROM tb_car WHERE c_id='$c_id'";
	$Re_chk=$mysqli->query($sql);
	$row_Re_chk=$Re_chk->fetch_assoc();
	
	if($photo=='1'){
		$file=$row_Re_chk['c_photo1'];
		if($file!=""){
			if (file_exists ("../images/model/$file")){
				unlink("../images/model/$file");
			}
			$sql="UPDATE tb_car SET c_photo1=NULL WHERE c_id='$c_id' ";
			$Re_sql=$mysqli->query($sql);
		}
	}else if($photo=='2'){
		$file=$row_Re_chk['c_photo2'];
		if($file!=""){
			if (file_exists ("../images/model/$file")){
				unlink("../images/model/$file");
			}
			$sql="UPDATE tb_car SET c_photo2=NULL WHERE c_id='$c_id' ";
			$Re_sql=$mysqli->query($sql);
		}
	}
	
	$GoTo = "admin_car_all_edit.php?c_id=".$c_id;
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="edit"){
		
	$photo=$_FILES["gal_photo_edit"];
	$photo=$_FILES['gal_photo_edit']['tmp_name'];
	$photo_name=$_FILES['gal_photo_edit']['name'];
	if($photo != "") {

		$sql_chk = "SELECT * FROM tb_gal WHERE gal_id='$gal_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['gal_photo'];
		if($file!=""){
			if (file_exists ("../images/gallery/$file")){unlink("../images/gallery/$file");}
		}

		$new_name ="GL".date('His').rand(1000,9999); 
		$path = "../images/gallery"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>640){
			$width=640;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$newphoto="$RenameFile";	
		}
	}else{$newphoto=$h_gal_photo;}
	
	$sql="UPDATE tb_gal SET gal_detail='$gal_detail',gal_name='$gal_name',gal_date='$gal_date',gal_name='$gal_name',gal_status='$gal_status',gal_photo='$newphoto' WHERE gal_id='$gal_id'";
	$Re_sql=$mysqli->query($sql);

	for($i=0;$i<count($_FILES["gal_p_photo"]["name"]);$i++){
		if($_FILES["gal_p_photo"]["name"][$i] != ""){
			$photo_name = $_FILES["gal_p_photo"]["name"][$i];
			$gal_p_photo_name = $_FILES["gal_p_photo"]["tmp_name"][$i];
	
			$newfile = "GL".date('His').rand(1000,9999); 
			$path = "../images/gallery/"; 
										
			$images_photo = $gal_p_photo_name;
			$size=GetimageSize($images_photo);
			$size_w=$size[0];
			$size_h=$size[1];
			if($size_w>640){
				$width=640;					
				$height=round(($width/$size_w) * $size_h);
				
				if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
					$images_photo_orig = ImageCreateFromJPEG($images_photo);
				}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
					$images_photo_orig = ImageCreateFromJPEG($images_photo);
				}
			
				$images_photo_new = ImageCreateTrueColor($width, $height); 
				ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
			
				if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
					ImageJPEG($images_photo_new,$images_photo,100); 
				}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
					ImageJPEG($images_photo_new,$images_photo,100); 
				}			
				ImageDestroy($images_photo_orig);
				ImageDestroy($images_photo_new); 					
			}			
			$RenameFile=$newfile.strchr($photo_name,".");
			if (copy( $images_photo , "$path/$RenameFile" )){		
				unlink($images_photo);
				$newphoto2="$RenameFile";	
			}

			$sql2="INSERT INTO tb_gal_photo (gal_p_photo, gal_id) VALUES ('$newphoto2','$gal_id')";
			$Re_sql2=$mysqli->query($sql2);
		}
	}

	for($i=0;$i<count($_POST["gal_y_url"]);$i++){
		if($_POST["gal_y_url"][$i] != ""){
			$sql3 = "INSERT INTO tb_gal_youtube (gal_id, gal_y_url) VALUES ('$gal_id','".$_POST["gal_y_url"][$i]."')";
			$Re_sql3=$mysqli->query($sql3);
		}
	}
	
	
	$GoTo = "admin_gallery_edit.php?gal_id=$gal_id";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}

	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="dele"){
	if ((isset($_GET['gal_id'])) && ($_GET['gal_id'] != "")){
		$gal_id=$_GET['gal_id'];
			
		$sql_chk = "SELECT * FROM tb_gal WHERE gal_id='$gal_id'";
		$Re_chk = $mysqli->query($sql_chk);
		$row_Re_chk =$Re_chk->fetch_assoc();
		
		$file=$row_Re_chk['gal_photo'];
		if($file!=""){
			if (file_exists ("../images/gallery/$file")){
				unlink("../images/gallery/$file");
				}
		}

		$sql1 ="DELETE FROM tb_gal WHERE gal_id='$gal_id'";
		$Re_sql1=$mysqli->query($sql1);
			
		$sql_chk1 = "SELECT * FROM tb_gal_photo WHERE gal_id='$gal_id'";
		$Re_chk1=$mysqli->query($sql_chk1);
		$total_Re_chk1=$Re_chk1->num_rows;
			
		if($total_Re_chk1>0){
			while($row_Re_chk1 = $Re_chk1->fetch_assoc()){
				$file1=$row_Re_chk1['gal_p_photo'];
				if(file_exists ("../photo/gallery/$file1")){
					unlink("../photo/gallery/$file1");
				}
			}
			$sql2 ="DELETE FROM tb_gal_photo WHERE gal_id='$gal_id'";
			$Re_sql2=$mysqli->query($sql2);
		}

		$sql3 ="DELETE FROM tb_gal_youtube WHERE gal_id='$gal_id'";
		$Re_sql3=$mysqli->query($sql3);
			
		$GoTo = "admin_gallery_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
	
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}

if($_GET['action']=="dele_p"){
	$gal_p_id=$_GET['gal_p_id'];
	$sql = "SELECT * FROM tb_gal_photo WHERE gal_p_id='$gal_p_id'";
	$Re_chk=$mysqli->query($sql);
	$row_Re_chk=$Re_chk->fetch_assoc();
	
	$file=$row_Re_chk['gal_p_photo'];
	if($file!=""){
		if (file_exists ("../images/gallery/$file")){
			unlink("../images/gallery/$file");
		}
	}
	$sql_dele ="DELETE FROM tb_gal_photo WHERE gal_p_id='$gal_p_id'";
	$Re_dele=$mysqli->query($sql_dele);
	
	$GoTo = "admin_gallery_edit.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="dele_y"){
	$file=$_GET['gal_y_id'];
	$sql_dele ="DELETE FROM tb_gal_youtube WHERE gal_y_id='$gal_y_id'";
	$Re_dele=$mysqli->query($sql_dele);
			
	$GoTo = "admin_gallery_edit.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}
?>
