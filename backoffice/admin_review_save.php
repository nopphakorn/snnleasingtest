<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST["rv_id"])){$rv_id=mysqli_real_escape_string($mysqli, $_POST["rv_id"]);}
if(isset($_POST["rv_status"])){$rv_status=mysqli_real_escape_string($mysqli, $_POST["rv_status"]);}
if(isset($_POST["h_rv_photo"])){$h_rv_photo=mysqli_real_escape_string($mysqli, $_POST["h_rv_photo"]);}
	
if($action=="status"){
	$rv_id = mysqli_real_escape_string($mysqli, $_GET['rv_id']);
	$rv_status = mysqli_real_escape_string($mysqli, $_GET['rv_status']);
	
	$sql="update tb_review set rv_status=$rv_status where rv_id='$rv_id'";
	$Re_update=$mysqli->query($sql);
		
	$GoTo = "admin_review_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="insert"){
	$photo=$_FILES["rv_photo"];
	$photo=$_FILES['rv_photo']['tmp_name'];
	$photo_name=$_FILES['rv_photo']['name'];

	$sql="SELECT max(rv_no) AS rv_max FROM tb_review";
	$Re_sql=$mysqli->query($sql);
	$row_Re_sql=$Re_sql->fetch_assoc();
	if($row_Re_sql['rv_max']==0){$rv_no=1;}
	else{$rv_no=$row_Re_sql['rv_max']+1;}

	//----
	if($photo != "") {
		$new_name ="RV".date('His').rand(1000,9999); 
		$path = "../images/review"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>800){
			$width=800;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".png"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".png"){
				ImagePNG($images_photo_new,$images_photo); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic="";}
	
	$sql="insert into tb_review(rv_no, rv_status, rv_photo)
	values('$rv_no', '$rv_status', '$pathPic')";
	$Re_insert=$mysqli->query($sql);
	
	$GoTo = "admin_review_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="edit"){
	$photo=$_FILES["rv_photo_edit"];
	$photo=$_FILES['rv_photo_edit']['tmp_name'];
	$photo_name=$_FILES['rv_photo_edit']['name'];

	if($photo != "") {
		$sql_chk = "SELECT * FROM tb_review WHERE rv_id='$rv_id'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
	
		$file=$row_Re_chk['rv_photo'];
		if($file!=""){
			if (file_exists ("../images/review/$file")){unlink("../images/review/$file");}
		}

		$new_name ="RV".date('His').rand(1000,9999); 
		$path = "../images/review"; 
										
		$images_photo = $photo;
		$size=GetimageSize($images_photo);
		$size_w=$size[0];
		$size_h=$size[1];
		if($size_w>800){
			$width=800;					
			$height=round(($width/$size_w) * $size_h);
			
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				$images_photo_orig = ImageCreateFromJPEG($images_photo);
			}else if(strchr($photo_name,".")==".png"){
				$images_photo_orig = ImageCreateFromPNG($images_photo);
			}
		
			$images_photo_new = ImageCreateTrueColor($width, $height); 
			ImageCopyResized($images_photo_new, $images_photo_orig, 0, 0, 0, 0, $width, $height, $size_w, $size_h); 
		
			if(strchr($photo_name,".")==".jpg" || strchr($photo_name,".")==".jpeg"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".JPG" || strchr($photo_name,".")==".JPEG"){
				ImageJPEG($images_photo_new,$images_photo, 100); 
			}else if(strchr($photo_name,".")==".png"){
				ImagePNG($images_photo_new,$images_photo); 
			}
							
			ImageDestroy($images_photo_orig);
			ImageDestroy($images_photo_new); 						
		}			
		$RenameFile=$new_name.strchr($photo_name,".");
		if (copy( $photo , "$path/$RenameFile" )){		
			unlink($photo);
			$pathPic="$RenameFile";	
		}
	}else{$pathPic=$h_rv_photo;}
	
	$sql="UPDATE tb_review SET rv_status='$rv_status', rv_photo='$pathPic' WHERE rv_id='$rv_id'";
	$Re_update=$mysqli->query($sql);
		
	$GoTo = "admin_review_edit.php?rv_id=$rv_id";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="dele"){
	if ((isset($_GET['rv_id'])) && ($_GET['rv_id'] != "")) {
	
		$rv_id_chk=$_GET['rv_id'];
		$sql_chk = "SELECT * FROM tb_review WHERE rv_id = '$rv_id_chk'";
		$Re_chk=$mysqli->query($sql_chk);
		$row_Re_chk=$Re_chk->fetch_assoc();
		
		$file=$row_Re_chk['rv_photo'];
		if($file!=""){
			if (file_exists ("../images/review/$file")){
				unlink("../images/review/$file");
			}
		}
		$sql ="DELETE FROM tb_review WHERE rv_id='$rv_id_chk'";
		$Re_dele=$mysqli->query($sql);

		$GoTo = "admin_review_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}
?>