<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="insert";
?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
    <script>
        var editor;
        KindEditor.ready(function(K) {
            editor = K.create('#pm_detail', {
                langType : 'en',
                items: ['source', '|', 'undo', 'redo', '|','cut', 'copy', 'paste','plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 
                    'justifyright', 'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript','superscript', 
                    '|', 'fullscreen', '/',
                    'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold','italic', 'underline', 'strikethrough', 
                    'lineheight', 'removeformat', '|', 'image', 'multiimage', 'insertfile', 'table', 'hr',  'pagebreak', 'link', 'unlink'],
            });
        });
    </script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_promotion_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_promotion_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo "&nbsp;เพิ่ม".$title;?></div>
                <div class="box_form">
                    <form action="admin_promotion_save.php?action=insert" method="POST" enctype="multipart/form-data" name="form_pm" id="form_pm" >
                        <table width="100%" border="0" align="center">
                            <tr>
                                <td width="150px" valign="top">เลือกสถานะ</td>
                                <td width="" valign="top">
                                    <select name="pm_status" id="pm_status">
                                        <option value="">เลือกสถานะ</option>
                                        <option value="1">แสดง</option>
                                        <option value="0">ไม่แสดง</option>
                                    </select>
                                </td>
                            </tr>
                                <td valign="top">รูปภาพปก</td>
                                <td valign="top"><input type="file" name="pm_photo" id="pm_photo" /></td>
							</tr>
                            <tr>
                                <td valign="top">โปรโมชั่น</td>
                                <td valign="top"><textarea name="pm_title" id="pm_title" style="height: 60px; width: 600px;" ></textarea></td>
                            </tr>
                            <tr>
                                <td valign="top">รายละเอียด</td>
                                <td valign="top"><textarea name="pm_detail" id="pm_detail" style="height: 300px; width: 800px;" ></textarea></td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr /></td>
                            </tr>
                            <tr>
                                <td colspan="2" ><input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>