<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="model-list";

$query_Re_cb_s = "SELECT * FROM tb_car_brand ORDER BY cb_name ASC ";
$Re_cb_s=$mysqli->query($query_Re_cb_s);

if((isset($_GET['cb_id'])) AND (isset($_GET['cg_id']))){
$query_Re_cg_s = "SELECT * FROM tb_car_group WHERE cb_id='".$_GET['cb_id']."' ORDER BY cg_name ASC ";
$Re_cg_s=$mysqli->query($query_Re_cg_s);
}

/*-----------*/
$maxRows_Re_cm = 50;
$pageNum_Re_cm = 0;
if (isset($_GET['pageNum_Re_cm'])) {
  $pageNum_Re_cm = $_GET['pageNum_Re_cm'];
}
$startRow_Re_cm = (($pageNum_Re_cm-1) * $maxRows_Re_cm);
if($startRow_Re_cm<0){$startRow_Re_cm=0;}

$query_Re_cm = "SELECT a.*, b.cb_name, c.cg_name FROM tb_car_model a ";
$query_Re_cm.= "LEFT JOIN tb_car_brand b ON(a.cb_id=b.cb_id) ";
$query_Re_cm.= "LEFT JOIN tb_car_group c ON(a.cg_id=c.cg_id) ";
$query_Re_cm.= "WHERE cm_id!=0 ";
if(isset($_GET['cb_id']) AND $_GET['cb_id']!=""){
    $cb_id_chk=mysqli_real_escape_string($mysqli, $_GET["cb_id"]);
    $query_Re_cm.= "AND a.cb_id='$cb_id_chk' ";
}
if(isset($_GET['cg_id']) AND $_GET['cg_id']!=""){
    $cg_id_chk=mysqli_real_escape_string($mysqli, $_GET["cg_id"]);
    $query_Re_cm.= "AND a.cg_id='$cg_id_chk' ";
}
$query_Re_cm.= "ORDER BY b.cb_name ASC, c.cg_name ASC, a.cm_name ASC ";
$query_limit_Re_cm = sprintf("%s LIMIT %d, %d", $query_Re_cm, $startRow_Re_cm, $maxRows_Re_cm);
$Re_cm=$mysqli->query($query_limit_Re_cm);

if (isset($_GET['totalRows_Re_cm'])) {
  $totalRows_Re_cm = $_GET['totalRows_Re_cm'];
} else {
  $all_Re_cm=$mysqli->query($query_Re_cm);
  $totalRows_Re_cm=$all_Re_cm->num_rows;
}
$totalPages_Re_cm = ceil($totalRows_Re_cm/$maxRows_Re_cm);

$currentPage = "http://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
$currentSent = "";
if(isset($_GET['cb_id'])){$currentSent.= "&cb_id=".$_GET['cb_id'];}
if(isset($_GET['cg_id'])){$currentSent.= "&cg_id=".$_GET['cg_id'];}

if($pageNum_Re_cm==0){$page=1;}
else if($pageNum_Re_cm>0){$page=$pageNum_Re_cm;}

?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
	
	<script type="text/javascript">
        $(document).ready(function() {
            $('.btn_insert').fancybox({
                    'type' : 'iframe',
                    'width' : '500',
                    'height' : '300',
                    'autoScale' : false,
                    'fitToView' : false,
                    'autoSize' : false,
                    //'onClosed' : function() {parent.location.reload(true);},
                    'afterClose' : function() {parent.location.reload(true); },
            });
        });
	</script>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_car_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_car_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-list-alt fa-lg" style="color:#3097FF"></i>&nbsp;รายการรุ่นรถ</div>

                <table width="100%">
                    <tr>
                        <td width="130px">
                            <span class="btn_green">
                                <a class="btn_insert" href="admin_car_model_insert.php">
                                <i class="fa fa-plus fa-lg" style="color:#3097FF"></i> เพิ่มรุ่นรถ</a>
                            </span>
                        </td>
                        <td>
                            <form action="admin_car_model.php" method="GET" enctype="multipart/form-data" name="form1" id="form1">
                                เลือกยี่ห้อรถ 
                                <select name="cb_id" id="cb_id" onchange="list_cg(this.value,'search')">
                                    <option value="">เลือกยี่ห้อรถ</option>
                                    <option value="all">ทุกยี่ห้อ</option>
                                    <?php while($row_Re_cb_s=$Re_cb_s->fetch_assoc()){?>
                                    <option value="<?php echo $row_Re_cb_s['cb_id'];?>" <?php if(isset($_GET['cb_id'])){if($row_Re_cb_s['cb_id']==$_GET['cb_id']){echo "selected=\"selected\"";}} ?>><?php echo $row_Re_cb_s['cb_name'];?></option>
                                    <?php } ?>
                                </select>
                                &nbsp;
                                <select name="cg_id" id="cg_id">
                                    <option value="">กลุ่มรุ่นรถ</option>
                                    <?php 
                                    if((isset($_GET['cb_id'])) AND (isset($_GET['cg_id']))){ 
                                        while($row_Re_cg_s=$Re_cg_s->fetch_assoc()){
                                    ?>
                                    <option value="<?php echo $row_Re_cg_s['cg_id'];?>" <?php if(isset($_GET['cg_id'])){if($row_Re_cg_s['cg_id']==$_GET['cg_id']){echo "selected=\"selected\"";}} ?>><?php echo $row_Re_cg_s['cg_name'];?></option>
                                    <?php }} ?>
                                </select>
                                <input type="submit" value="ค้นหา">
                            </form>
                        </td>
                    </tr>
                </table>
				
                <!-- papeging -->
                <div class="pi_box">
                    <div class="pi_box_l">
                        <a class="pi_btn pi_l" href="<?php printf("%s?pageNum_Re_cm=%d%s", $currentPage, 0, $currentSent); ?>">
                        <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a>

                        <a class="pi_btn pi_l" href="<?php if($page>0){printf("%s?pageNum_Re_cm=%d%s", $currentPage, $page - 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="pi_box_m">
                        <span class="page-link">
                            <?php 
                            if($totalPages_Re_cm>0){
                                echo "Page : ".$page."/".$totalPages_Re_cm." Total : ".number_format($totalRows_Re_cm)." record";
                            }else{
                                echo "-";
                            }
                            ?>
                      </span>
                    </div>
                    <div class="pi_box_r">
                      <a class="pi_btn pi_r" href="<?php printf("%s?pageNum_Re_cm=%d%s", $currentPage, $totalPages_Re_cm, $currentSent); ?>">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                      </a>

                      <a class="pi_btn pi_r" href="<?php if($page<$totalPages_Re_cm){printf("%s?pageNum_Re_cm=%d%s", $currentPage, $page + 1, $currentSent);} ?>" >
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                    </div>
                </div>

                <table class="tb1" width="100%" border="1" align="center" cellpadding="5">
                    <tr>
                        <th width="45">No.</th>
						<th width="200">ยี่ห้อรถ</th>
                        <th width="200">กลุ่มรุ่นรถ</th>
                        <th width="">รายการรุ่นรถ</th>
						<th width="">ประเภท รย.</th>
                        <th width="40">สถานะ</th>
                        <th width="40">แก้ไข</th>
                        <th width="40">ลบ</th>
                    </tr>
                    <?php 
                    if($totalRows_Re_cm>0){
                        $number=0;
                        while($row_Re_cm=$Re_cm->fetch_assoc()){
                    ?>
                    <tr>
                        <td><div align="center"><?php echo $number+=1; ?></div></td> 
                        <td><div align="left"><?php echo $row_Re_cm['cb_name']; ?></div></td>
						<td><div align="left"><?php echo $row_Re_cm['cg_name']; ?></div></td>
                        <td><div align="left"><?php echo $row_Re_cm['cm_name']; ?></div></td>
						<td><div align="left"><?php echo $row_Re_cm['ty_car']; ?></div></td>
                        <td><div align="center">
                                <?php if($row_Re_cm['cm_status']=="1"){ ?>
                                <a href="admin_car_save.php?action=model-status&cm_id=<?php echo $row_Re_cm['cm_id']; ?>&cm_status=0"><img src="images/icon/icon_anw_online_18.png" width="18" height="18" border="0" /></a>
                                <?php }else{ ?>
                                <a href="admin_car_save.php?action=model-status&cm_id=<?php echo $row_Re_cm['cm_id']; ?>&cm_status=1"><img src="images/icon/icon_anw_offline_18.png" width="18" height="18" border="0" /></a>
                                <?php } ?>
                            </div></td>
                        <td>
                            <div align="center">
                                <a class="btn_insert" href="admin_car_model_edit.php?cm_id=<?php echo $row_Re_cm['cm_id']; ?>">
                                <img src="images/icon/icon_anw_edit_18.png" width="18" height="21" border="0" /></a>
                            </div>
                        </td>
                        <td width="19" scope="col"><div align="center">
                                <div align="center"> <a href="admin_car_save.php?action=model-dele&cm_id=<?php echo $row_Re_cm['cm_id']; ?>" onclick = "return confirm('คุณต้องการลบ รุ่นรถ หรือไม่')"><img src="images/icon/icon_anw_dele_18.png" width="18" height="19" border="0"></a></div>
                            </div></td>
                    </tr>
                    <?php }}else{  ?>
                    <tr>
                        <td colspan="7">
                            <div class="alert_table"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>&nbsp;ไม่มีรายการในขณะนี้</div>
                        </td>
                    </tr>
                    <?php } ?>
                </table>   
            </div>
        </div>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>

