<?php
require_once('Connections/con_db.php');
include('function/f_admin.php');

if(isset($_POST["bch_id"])){$bch_id=mysqli_real_escape_string($mysqli, $_POST["bch_id"]);}
if(isset($_POST["bch_province"])){$bch_province=mysqli_real_escape_string($mysqli, $_POST["bch_province"]);}
if(isset($_POST["bch_name"])){$bch_name=mysqli_real_escape_string($mysqli, $_POST["bch_name"]);}
if(isset($_POST["bch_add"])){$bch_add=mysqli_real_escape_string($mysqli, $_POST["bch_add"]);}
if(isset($_POST["bch_landmark"])){$bch_landmark=mysqli_real_escape_string($mysqli, $_POST["bch_landmark"]);}
if(isset($_POST["bch_tel"])){$bch_tel=mysqli_real_escape_string($mysqli, $_POST["bch_tel"]);}
if(isset($_POST["bch_fax"])){$bch_fax=mysqli_real_escape_string($mysqli, $_POST["bch_fax"]);}
if(isset($_POST["bch_lat"])){$bch_lat=mysqli_real_escape_string($mysqli, $_POST["bch_lat"]);}
if(isset($_POST["bch_lng"])){$bch_lng=mysqli_real_escape_string($mysqli, $_POST["bch_lng"]);}
if(isset($_POST["bch_tag"])){$bch_tag=mysqli_real_escape_string($mysqli, $_POST["bch_tag"]);}
if(isset($_POST["bch_open"])){$bch_open=mysqli_real_escape_string($mysqli, $_POST["bch_open"]);}
if(isset($_POST["bch_time"])){$bch_time=mysqli_real_escape_string($mysqli, $_POST["bch_time"]);}

if($_GET['action']=="no"){
	if(isset($_GET["up"])){$up=$_GET['up'];}else{$up=0;}
	if(isset($_GET["down"])){$down=$_GET['down'];}else{$down=0;}
	if(isset($_GET["id"])){$id=$_GET['id'];}
	if(isset($_GET["list"])){$list_new=$_GET['list'];}

	if($up==1){
		$list_old=$list_new-$up;
		$list_new=$list_new;
		$usql="update tb_branch set bch_no='$list_new' where bch_no='$list_old'";
		$Re_usql=$mysqli->query($usql);
		if (!$Re_usql) {printf("Error: %s\n", $mysqli->error);}
		
		$dsql="update tb_branch set bch_no='$list_old' where bch_id='$id'";
		$Re_dsql=$mysqli->query($dsql);
		if (!$Re_dsql) {printf("Error: %s\n", $mysqli->error);}
		
		$GoTo = "admin_branch_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>"; 
	}

	if($down==1){
		$list_old=$down+$list_new;
		$list_new=$list_new;
		$usql="update tb_branch set bch_no='$list_new' where bch_no='$list_old'";
		$Re_usql=$mysqli->query($usql);
		if (!$Re_usql) {printf("Error: %s\n", $mysqli->error);}
		
		$dsql="update tb_branch set bch_no='$list_old' where bch_id='$id'";
		$Re_dsql=$mysqli->query($dsql);
		if (!$Re_dsql) {printf("Error: %s\n", $mysqli->error);}
		
		$GoTo = "admin_branch_list.php";
		  if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		  }
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>"; 
	}
}
	
if($_GET['action']=="insert"){

	$sql="SELECT max(bch_no) AS bch_max FROM tb_branch";
	$Re_sql=$mysqli->query($sql);
	$row_Re_sql=$Re_sql->fetch_assoc();
	if($row_Re_sql['bch_max']==0){$bch_no=1;}
	else{$bch_no=$row_Re_sql['bch_max']+1;}

	$sql="INSERT INTO tb_branch(bch_no, bch_province, bch_name, bch_add, bch_landmark, bch_tel, bch_fax, bch_lat, bch_lng, bch_tag, bch_open, bch_time)
	VALUES('$bch_no', '$bch_province', '$bch_name', '$bch_add', '$bch_landmark', '$bch_tel', '$bch_fax', '$bch_lat', '$bch_lng', '$bch_tag', '$bch_open' , '$bch_time')";
	$Re_sql=$mysqli->query($sql);

	$GoTo = "admin_branch_list.php";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}

if($_GET['action']=="edit"){
	
	$sql="UPDATE tb_branch SET bch_province='$bch_province', bch_name='$bch_name', bch_add='$bch_add', bch_landmark='$bch_landmark', bch_tel='$bch_tel', bch_fax='$bch_fax', bch_lat='$bch_lat', bch_lng='$bch_lng', bch_tag='$bch_tag', bch_open='$bch_open' , bch_time='$bch_time'  WHERE bch_id='$bch_id'";
	$Re_sql=$mysqli->query($sql);
	if (!$Re_sql) {printf("Error: %s\n", $mysqli->error);}
	
	$GoTo = "admin_branch_edit.php?bch_id=$bch_id";
	if (isset($_SERVER['QUERY_STRING'])) {
		$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
		$GoTo .= $_SERVER['QUERY_STRING'];
	}
	echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
}


if($_GET['action']=="dele"){
	if ((isset($_GET['bch_id'])) && ($_GET['bch_id'] != "")) {
		$bch_id_chk=$_GET['bch_id'];

		$sql ="DELETE FROM tb_branch WHERE bch_id='$bch_id'";
		$Re_sql=$mysqli->query($sql);

		$GoTo = "admin_branch_list.php";
		if (isset($_SERVER['QUERY_STRING'])) {
			$GoTo .= (strpos($GoTo, '?')) ? "&" : "?";
			$GoTo .= $_SERVER['QUERY_STRING'];
		}
		echo"<script type='text/javascript'>window.location='".$GoTo."';</script>";
	}
}
?>
