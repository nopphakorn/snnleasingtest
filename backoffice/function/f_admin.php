<?php
if (!isset($_SESSION)) {session_start();}
if (!isset($_SESSION['AD_Name']) 
  OR !isset($_SESSION['AD_Username']) 
  OR !isset($_SESSION['AD_Password']) 
  OR !isset($_SESSION['AD_Access']) 
  OR ($_SESSION['AD_Access']!='true')) {
    $_SESSION['AD_Name']= NULL;
	  $_SESSION['AD_Username']= NULL;
    $_SESSION["AD_Password"]= NULL;
    $_SESSION["AD_Access"]= NULL;
	
	  unset($_SESSION['AD_Name']);
	  unset($_SESSION['AD_Username']);
    unset($_SESSION["AD_Password"]);
    unset($_SESSION["AD_Access"]);
    header("Location: ../login.php"); 
    exit;
}
?>
<?php
while(list($xVarName, $xVarvalue) = each($_GET)) {
     ${$xVarName} = $xVarvalue;
}
 
while(list($xVarName, $xVarvalue) = each($_POST)) {
     ${$xVarName} = $xVarvalue;
}
 
while(list($xVarName, $xVarvalue) = each($_FILES)) {
     ${$xVarName."_name"} = $xVarvalue['name'];
     ${$xVarName."_type"} = $xVarvalue['type'];
     ${$xVarName."_size"} = $xVarvalue['size'];
     ${$xVarName."_error"} = $xVarvalue['error'];
     ${$xVarName} = $xVarvalue['tmp_name'];
}

function datethai($date){
	if($date>0){
		$_month_name = array(
		"01"=>"มกราคม",
		"02"=>"กุมภาพันธ์",
		"03"=>"มีนาคม",
		"04"=>"เมษายน",
		"05"=>"พฤษภาคม",
		"06"=>"มิถุนายน",
		"07"=>"กรกฏาคม",
		"08"=>"สิงหาคม",
		"09"=>"กันยายน",
		"10"=>"ตุลาคม",
		"11"=>"พฤศจิกายน",
		"12"=>"ธันวาคม");
		$yy=substr($date,0,4);
		$mm=substr($date,5,2);
		$dd=substr($date,8,2);
		$time=substr($date,11,8);
		$yy+=543;
		$datethai=intval($dd)." ".$_month_name[$mm]." ".$yy." ".$time;
		return $datethai;
	}else{
		$datethai="-";
		return $datethai;
	}
}

function datethai2($date){
	if($date>0){
		$_month_name = array(
		"01"=>"มกราคม",
		"02"=>"กุมภาพันธ์",
		"03"=>"มีนาคม",
		"04"=>"เมษายน",
		"05"=>"พฤษภาคม",
		"06"=>"มิถุนายน",
		"07"=>"กรกฏาคม",
		"08"=>"สิงหาคม",
		"09"=>"กันยายน",
		"10"=>"ตุลาคม",
		"11"=>"พฤศจิกายน",
		"12"=>"ธันวาคม");
		$yy=substr($date,0,4);
		$mm=substr($date,5,2);
		$dd=substr($date,8,2);
		$time=substr($date,11,8);
		$yy+=543;
		$datethai2=intval($dd)." ".$_month_name[$mm]." ".$yy;
		return $datethai2;
	}else{
		$datethai2="-";
		return $datethai2;
	}
}

function datethai_num($date){
	if($date>0){
		$yy=substr($date,0,4);
		$mm=substr($date,5,2);
		$dd=substr($date,8,2);
		$time=substr($date,11,8);
		$yy+=543;

		$dd2=str_pad($dd, 2, "0", STR_PAD_LEFT);
		$datethai_num = $dd2."/".$mm."/".$yy;
		return $datethai_num;
	}else{
		$datethai_num="-";
		return $datethai_num;
	}
}
function dateEng($date){
	if($date>0){
		$d=explode("/",$date);
		$dd=$d[0];
		$mm=$d[1];
		$yy=$d[2];
		$yy-=543;
		$dateEng = $yy."-".$mm."-".$dd;
		return $dateEng;
	}else{
		$dateEng="-";
		return $dateEng;
	}
}

function dateTha($date){
	if($date>0){
		$yy=substr($date,0,4);
		$mm=substr($date,5,2);
		$dd=substr($date,8,2);
		$yy+=543;
		$dateTha = intval($dd)."/".$mm."/".$yy;
		return $dateTha;
	}else{
		$dateTha="-";
		return $dateTha;
	}
}

function dateTime($date){
	if($date>0){
		$time=substr($date,11,5);
		$dateTime = $time;
		return $dateTime;
	}else{
		$dateTime="-";
		return $dateTime;
	}
}

function datethai_sm_time($date){
	if($date>0){
		$_month_name = array(
		"01"=>"ม.ค.",
		"02"=>"ก.พ.",
		"03"=>"มี.ค.",
		"04"=>"เม.ย.",
		"05"=>"พ.ค.",
		"06"=>"มิ.ย.",
		"07"=>"ก.ค.",
		"08"=>"ส.ค.",
		"09"=>"ก.ย.",
		"10"=>"ต.ค.",
		"11"=>"พ.ย.",
		"12"=>"ธ.ค.");
		$yy=substr($date,0,4);
		$mm=substr($date,5,2);
		$dd=substr($date,8,2);
		$time=substr($date,11,5);
		$yy+=543;
		$yy2=substr($yy,2,2);
		$datethai_sm_time = intval($dd)." ".$_month_name[$mm]." ".$yy2." ".$time;
		return $datethai_sm_time;
	}else{
		$datethai_sm_time="-";
		return $datethai_sm_time;
	}
}
?>