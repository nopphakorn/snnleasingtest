<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');
$page_nav="insert";
?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
</head>

<body>
    <div id="header"><?php include("s_header.php"); ?></div>
	<div id="nav"><?php include("admin_investor_nav.php"); ?></div>
	<div id="side"><?php include('s_menu_side.php'); ?></div>

    <div id="containner">
        <div id="main">
            <div id="main_menu"><?php include("admin_investor_menu.php"); ?></div>
            <div id="main_content">
                <div class="main_content_title"><i class="fa fa-plus fa-lg" style="color:#3097FF"></i><?php echo " เพิ่ม ".$title;?></div>
                <div class="box_form">
                    <form action="admin_investor_save.php?action=insert" method="POST" enctype="multipart/form-data" name="form_iv" id="form_iv" >
                        <table width="100%">
							<tr>
                                <td width="120" valign="top">หัวข้อ</td>
                                <td><input type="text" id="iv_name" name="iv_name" style="width:600px"></td>
                            </tr>
                            <tr>
                                <td width="120" valign="top">Name</td>
                                <td><input type="text" id="en_iv_name" name="en_iv_name" style="width:600px"></td>
                            </tr>
                            <tr>
                                <td width="120">ไฟล์ PDF</td>
                                <td>
                                    <input type="file" name="iv_file" id="iv_file" />
                                    เฉพาะไฟล์ PDF เท่านั้น
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><hr>
								<input type="hidden" id="iv_type" name="iv_type" value="<?php echo $_GET['type'];?>">
								<input name="button" type="submit" id="button" value="บันทึกรายการ" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>