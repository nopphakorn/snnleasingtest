function getHttpRequest() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        try {
            return new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
            return new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
}

function check_user(admin_user, h_admin_user) {
    var receiveReq = false;
    var url = 'admin_user_verify.php';
    var pmeters = "admin_user=" + encodeURI(admin_user);

    if (admin_user != "") {
        if (admin_user != h_admin_user) {
            receiveReq = new getHttpRequest();
            receiveReq.open("POST", url, true);

            receiveReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            receiveReq.setRequestHeader("Content-length", pmeters.length);
            receiveReq.setRequestHeader("Connection", "close");
            receiveReq.send(pmeters);

            receiveReq.onreadystatechange = function() {
                if (receiveReq.readyState == 3) {
                    document.getElementById("msg").innerHTML = "...";
                }
                if (receiveReq.readyState == 4 && receiveReq.status == 200) {

                    var data = receiveReq.responseText;
                    var Strip = data.replace(/(<([^>]+)>)/ig, "");
                    if (Strip.indexOf('Y') != -1) {
                        document.getElementById('msg').innerHTML = "";
                        document.getElementById('Submit').disabled = false;
                    } else if (Strip.indexOf('N') != -1) {
                        document.getElementById('msg').innerHTML = "<font color=red>ไม่สามารถใช้ Username นี้ได้</font>";
                        document.getElementById('Submit').disabled = true;
                    }
                }
            }
        } else {
            document.getElementById("msg").innerHTML = "";
            document.getElementById('Submit').disabled = false;
        }
    } else {
        document.getElementById("msg").innerHTML = "";
    }
}

function check_em_user(em_user, h_em_user) {
    var receiveReq = false;
    var url = 'admin_employee_verify.php';
    var pmeters = "em_user=" + encodeURI(em_user);

    if (em_user != "") {
        if (em_user != h_em_user) {
            receiveReq = new getHttpRequest();
            receiveReq.open("POST", url, true);
            receiveReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            receiveReq.setRequestHeader("Content-length", pmeters.length);
            receiveReq.setRequestHeader("Connection", "close");
            receiveReq.send(pmeters);

            receiveReq.onreadystatechange = function() {
                if (receiveReq.readyState == 3) {
                    document.getElementById("msg").innerHTML = "...";
                }
                if (receiveReq.readyState == 4 && receiveReq.status == 200) {

                    var data = receiveReq.responseText;
                    var Strip = data.replace(/(<([^>]+)>)/ig, "");
                    if (Strip.indexOf('Y') != -1) {
                        document.getElementById('msg').innerHTML = "";
                        document.getElementById('Submit').disabled = false;
                    } else if (Strip.indexOf('N') != -1) {
                        document.getElementById('msg').innerHTML = "<font color=red>ไม่สามารถใช้  Username นี้ได้</font>";
                        document.getElementById('Submit').disabled = true;
                    }
                }
            }
        } else {
            document.getElementById("msg").innerHTML = "";
            document.getElementById('Submit').disabled = false;
        }
    } else {
        document.getElementById("msg").innerHTML = "";
    }
}

function check_edit_password(o_admin_pass, h_admin_pass) {
    var receiveReq = false;
    var url = 'admin_user_edit_password_verify.php';
    var pmeters = "o_admin_pass=" + encodeURI(o_admin_pass);

    if (o_admin_pass != "") {
        receiveReq = new getHttpRequest();
        receiveReq.open("POST", url, true);

        receiveReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        receiveReq.setRequestHeader("Content-length", pmeters.length);
        receiveReq.setRequestHeader("Connection", "close");
        receiveReq.send(pmeters);

        receiveReq.onreadystatechange = function() {
            if (receiveReq.readyState == 3) {
                document.getElementById("msg").innerHTML = "...";
            }
            if (receiveReq.readyState == 4 && receiveReq.status == 200) {
                var data = receiveReq.responseText;
                var Strip = data.replace(/(<([^>]+)>)/ig, "");
                if (Strip.indexOf('Y') != -1) {
                    document.getElementById('msg').innerHTML = "<font color=green>Password ถูกต้อง</font>";
                    document.getElementById('Submit').disabled = false;
                } else if (Strip.indexOf('N') != -1) {
                    document.getElementById('msg').innerHTML = "<font color=red>Password เดิมไม่ถูกต้อง</font>";
                    document.getElementById('Submit').disabled = true;
                }
            }
        }
    } else {
        document.getElementById("msg").innerHTML = "";
        document.getElementById('Submit').disabled = true;
    }
}

function check_ct(ct_name, h_ct_name) {
    var receiveReq = false;
    var url = 'admin_car_type_verify.php';
    var pmeters = "ct_name=" + encodeURI(ct_name);

    if (ct_name != "") {
        if (ct_name != h_ct_name) {
            receiveReq = new getHttpRequest();
            receiveReq.open("POST", url, true);

            receiveReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            receiveReq.setRequestHeader("Content-length", pmeters.length);
            receiveReq.setRequestHeader("Connection", "close");
            receiveReq.send(pmeters);

            receiveReq.onreadystatechange = function() {
                if (receiveReq.readyState == 3) {
                    document.getElementById("msg").innerHTML = "...";
                }
                if (receiveReq.readyState == 4 && receiveReq.status == 200) {

                    var data = receiveReq.responseText;
                    var Strip = data.replace(/(<([^>]+)>)/ig, "");
                    if (Strip.indexOf('Y') != -1) {
                        document.getElementById('msg').innerHTML = "";
                        document.getElementById('Submit').disabled = false;
                    } else if (Strip.indexOf('N') != -1) {
                        document.getElementById('msg').innerHTML = "<font color=red>มีรายการประเภทรถอยู่ในระบบแล้ว</font>";
                        document.getElementById('Submit').disabled = true;
                    }
                }
            }
        } else {
            document.getElementById("msg").innerHTML = "";
            document.getElementById('Submit').disabled = false;
        }
    } else {
        document.getElementById("msg").innerHTML = "";
    }
}

function check_cb(cb_name, h_cb_name) {
    var receiveReq = false;
    var url = 'admin_car_brand_verify.php';
    var pmeters = "cb_name=" + encodeURI(cb_name);

    if (cb_name != "") {
        if (cb_name != h_cb_name) {
            receiveReq = new getHttpRequest();
            receiveReq.open("POST", url, true);

            receiveReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            receiveReq.setRequestHeader("Content-length", pmeters.length);
            receiveReq.setRequestHeader("Connection", "close");
            receiveReq.send(pmeters);

            receiveReq.onreadystatechange = function() {
                if (receiveReq.readyState == 3) {
                    document.getElementById("msg").innerHTML = "...";
                }
                if (receiveReq.readyState == 4 && receiveReq.status == 200) {

                    var data = receiveReq.responseText;
                    var Strip = data.replace(/(<([^>]+)>)/ig, "");
                    if (Strip.indexOf('Y') != -1) {
                        document.getElementById('msg').innerHTML = "";
                        document.getElementById('Submit').disabled = false;
                    } else if (Strip.indexOf('N') != -1) {
                        document.getElementById('msg').innerHTML = "<font color=red>มีรายการยี่ห้อรถถอยู่ในระบบแล้ว</font>";
                        document.getElementById('Submit').disabled = true;
                    }
                }
            }
        } else {
            document.getElementById("msg").innerHTML = "";
            document.getElementById('Submit').disabled = false;
        }
    } else {
        document.getElementById("msg").innerHTML = "";
    }
}

function list_g_bd(action, cb_id, ct_id) {
    var url = 'admin_car_group_list.php?action=' + action + '&cb_id=' + cb_id;
    xmlhttp = uzXmlHttp();
    xmlhttp.open("GET", url, false);
    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8"); // set Header
    xmlhttp.send(null);
    document.getElementById(action).innerHTML = xmlhttp.responseText;
}


function check_cg(cb_id, ct_id, cg_name, h_cg_name) {
    var receiveReq = false;
    var url = 'admin_car_group_verify.php';
    var pmeters = "cb_id=" + encodeURI(cb_id) + "&ct_id=" + encodeURI(ct_id) + "&cg_name=" + encodeURI(cg_name);
    if (cg_name != "") {
        if (cg_name != h_cg_name) {
            receiveReq = new getHttpRequest();
            receiveReq.open("POST", url, true);
            receiveReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            receiveReq.setRequestHeader("Content-length", pmeters.length);
            receiveReq.setRequestHeader("Connection", "close");
            receiveReq.send(pmeters);
            receiveReq.onreadystatechange = function() {
                if (receiveReq.readyState == 3) {
                    document.getElementById("msg").innerHTML = "...";
                }
                if (receiveReq.readyState == 4 && receiveReq.status == 200) {
                    var data = receiveReq.responseText;
                    var Strip = data.replace(/(<([^>]+)>)/ig, "");
                    if (Strip.indexOf('Y') != -1) {
                        document.getElementById('msg').innerHTML = "";
                        document.getElementById('Submit').disabled = false;
                    } else if (Strip.indexOf('N') != -1) {
                        document.getElementById('msg').innerHTML = "<font color=red>มีรายการกลุ่มรุ่นรถอยู่ในระบบแล้ว</font>";
                        document.getElementById('Submit').disabled = true;
                    }
                }
            }
        } else {
            document.getElementById("msg").innerHTML = "";
            document.getElementById('Submit').disabled = false;
        }
    } else {
        document.getElementById("msg").innerHTML = "";
    }
}

function check_cm(cb_id, cg_id, cm_name, h_cm_name) {

    var receiveReq = false;
    var url = 'admin_car_model_verify.php';
    var pmeters = "cb_id=" + encodeURI(cb_id) + "&cg_id=" + encodeURI(cg_id) + "&cm_name=" + encodeURI(cm_name);

    if (cm_name != "") {
        if (cm_name != h_cm_name) {
            receiveReq = new getHttpRequest();
            receiveReq.open("POST", url, true);

            receiveReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            receiveReq.setRequestHeader("Content-length", pmeters.length);
            receiveReq.setRequestHeader("Connection", "close");
            receiveReq.send(pmeters);
            receiveReq.onreadystatechange = function() {
                if (receiveReq.readyState == 3) {
                    document.getElementById("msg").innerHTML = "...";
                }
                if (receiveReq.readyState == 4 && receiveReq.status == 200) {
                    var data = receiveReq.responseText;
                    var Strip = data.replace(/(<([^>]+)>)/ig, "");
                    if (Strip.indexOf('Y') != -1) {
                        document.getElementById('msg').innerHTML = "";
                        document.getElementById('Submit').disabled = false;
                    } else if (Strip.indexOf('N') != -1) {
                        document.getElementById('msg').innerHTML = "<font color=red>มีรายการ รุ่นรถ อยู่ในระบบแล้ว</font>";
                        document.getElementById('Submit').disabled = true;
                    }
                }
            }
        } else {
            document.getElementById("msg").innerHTML = "";
            document.getElementById('Submit').disabled = false;
        }
    } else {
        document.getElementById("msg").innerHTML = "";
    }
}

function uzXmlHttp() {
    var xmlhttp = false;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
            xmlhttp = false;
        }
    }

    if (!xmlhttp && document.createElement) {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}
function list_cg(cb_id, action, cm_name, h_cm_name) {
    var url = 'admin_car_model_list.php?action=' + action + '&cb_id=' + cb_id;
    xmlhttp = uzXmlHttp();
    xmlhttp.open("GET", url, false);
    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8"); // set Header
    xmlhttp.send(null);
    document.getElementById('cg_id').innerHTML = xmlhttp.responseText;
}

function list_ca(action, cb_id, ct_id, cg_id) {
    var url = 'admin_car_all_list.php?action=' + action + '&cb_id=' + cb_id + '&ct_id=' + ct_id + '&cg_id=' + cg_id;
    xmlhttp = uzXmlHttp();
    xmlhttp.open("GET", url, false);
    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8"); // set Header
    xmlhttp.send(null);
    document.getElementById(action).innerHTML = xmlhttp.responseText;
}