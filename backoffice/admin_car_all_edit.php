<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');

$query_Re_c = "SELECT * FROM tb_car WHERE c_id='".$_GET['c_id']."'";
$Re_c=$mysqli->query($query_Re_c);
$row_Re_c=$Re_c->fetch_assoc();

$query_Re_cb = "SELECT * FROM tb_car_brand ORDER BY cb_name ASC ";
$Re_cb=$mysqli->query($query_Re_cb);

$cb_id_chk=$row_Re_c['cb_id'];
$ct_id_chk=$row_Re_c['ct_id'];
$cg_id_chk=$row_Re_c['cg_id'];
$query_Re_cb2 = "SELECT * FROM tb_car_brand WHERE cb_id = ".$cb_id_chk." ";
$Re_cb2=$mysqli->query($query_Re_cb2);
$row_Re_cb2=$Re_cb2->fetch_assoc();

$query_Re_ct = "SELECT * FROM tb_car_type WHERE ct_id IN(".$row_Re_cb2['ct_id'].") ORDER BY ct_id ASC ";
$Re_ct=$mysqli->query($query_Re_ct);

$query_Re_cg = "SELECT * FROM tb_car_group WHERE cb_id='".$cb_id_chk."' AND ct_id='".$ct_id_chk."'  ORDER BY cg_name ASC ";
$Re_cg=$mysqli->query($query_Re_cg);


$query_Re_cm = "SELECT * FROM tb_car_model WHERE cb_id='".$cb_id_chk."' AND cg_id='".$cg_id_chk."' ORDER BY cm_name ASC ";
$Re_cm=$mysqli->query($query_Re_cm);

?>

<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
    <link rel="stylesheet" href="tools/datepicker/jquery-ui.css">
	<script src="tools/datepicker/jquery-ui.js"></script>
	<script src="tools/datepicker/jqueryui_datepicker_thai.js"></script>
    <script type="text/javascript">
		$( function () {
			$( "#c_date" ).datepicker( {
				dateFormat: 'dd-mm-yy',
				changeMonth: true,
				changeYear: true,
				showOn: "button",
		        buttonImage: "images/icon/calendar.gif",
			} );
		} );
	</script>
</head>
<body>
	<?php if(!isset($_GET['save']) OR $_GET['save']!='sucess'){ ?>
	<div id="fancy_title"><i class="fa fa-list-alt"></i>&nbsp;แก้ไขราคากลางรถ</div>
    <div id="fancy_box">
		<form action="admin_car_save.php?action=all-edit" method="post" enctype="multipart/form-data" name="form_ca" id="form_ca">
            <table width="100%" cellspacing="5">
                <tr>
                    <td width="120px">ID</td>
                    <td>
                       <?php echo $row_Re_c['c_id']; ?>
                    </td>
                </tr>
                <tr>
                    <td width="120px">วันที่ Update ราคา</td>
                    <td>
                        <input type="text" id="c_date" name="c_date" style="width:150px" readonly value="<?php echo datethai_num(date("Y-m-d")); ?>">
                        <input type="time" id="c_date_t" name="c_date_t" style="width:70px" value="<?php echo date("H:i"); ?>">
                    </td>
                </tr>
				
                    <td>ยี่ห้อ</td>
                    <td>
                        <select name="cb_id" id="cb_id" onchange="list_ca('ct_id',form_ca.cb_id.value,'','')">
                            <option value="">เลือกยี่ห้อรถ</option>
                            <?php while($row_Re_cb=$Re_cb->fetch_assoc()){?>
                            <option value="<?php echo $row_Re_cb['cb_id'];?>" <?php if($row_Re_c['cb_id']==$row_Re_cb['cb_id']){echo "selected=\"selected\"";} ?>><?php echo $row_Re_cb['cb_name'];?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="130px">ประเภท</td>
                    <td>
                        <select name="ct_id" id="ct_id" onchange="list_ca('cg_id', form_ca.cb_id.value, form_ca.ct_id.value,'')">
                            <option value="">เลือก</option>
                            <?php while($row_Re_ct=$Re_ct->fetch_assoc()){?>
                            <option value="<?php echo $row_Re_ct['ct_id'];?>" <?php if($row_Re_c['ct_id']==$row_Re_ct['ct_id']){echo "selected=\"selected\"";} ?>><?php echo $row_Re_ct['ct_name'];?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>กลุ่มรุ่นรถ</td>
                    <td>
                        <select name="cg_id" id="cg_id" onchange="list_ca('cm_id', form_ca.cb_id.value, form_ca.ct_id.value, form_ca.cg_id.value)">
					        <option value="">เลือก</option>
                            <?php while($row_Re_cg=$Re_cg->fetch_assoc()){?>
                            <option value="<?php echo $row_Re_cg['cg_id'];?>" <?php if($row_Re_c['cg_id']==$row_Re_cg['cg_id']){echo "selected=\"selected\"";} ?>><?php echo $row_Re_cg['cg_name'];?></option>
                            <?php } ?>
				        </select>
                    </td>
                </tr>
                <tr>
                    <td>ชื่อรุ่นรถ</td>
                    <td>
                        <select name="cm_id" id="cm_id">
					        <option value="">เลือก</option>
                            <?php while($row_Re_cm=$Re_cm->fetch_assoc()){?>
                            <option value="<?php echo $row_Re_cm['cm_id'];?>" <?php if($row_Re_c['cm_id']==$row_Re_cm['cm_id']){echo "selected=\"selected\"";} ?>><?php echo $row_Re_cm['cm_name'];?></option>
                            <?php } ?>
				        </select>
                    </td>
                </tr>
                <tr>
                    <td>ระบบเกียร์</td>
                    <td>
                        <select name="c_gear" id="c_gear">
					        <option value="">เลือก</option>
                            <option value="AT" <?php if($row_Re_c['c_gear']=='AT'){echo "selected=\"selected\"";} ?>>ออโต้</option>
                            <option value="MT" <?php if($row_Re_c['c_gear']=='MT'){echo "selected=\"selected\"";} ?>>ธรรมดา</option>
				        </select>
                    </td>
                </tr>
                <tr>
                    <td>ปีรถ</td>
                    <td><input type="text" id="c_year" name="c_year" value="<?php echo $row_Re_c['c_year'];?>"></td>
                </tr>
                <tr>
                    <td>ราคากลาง</td>
                    <td><input type="text" id="c_price" name="c_price" value="<?php echo $row_Re_c['c_price'];?>"></td>
                </tr>
                <tr>
                    <td>รูปภาพรถ 1</td>
                    <td>
                        <div class="tx_red">ขนาดรูปภาพที่แนะนำ 640px X 480px</div>
                        <input type="file" id="c_photo1" name="c_photo1">
                        <input type="hidden" id="h_c_photo1" name="h_c_photo1" value="<?php echo $row_Re_c['c_photo1'];?>">
                    </td>
                </tr>
                <tr>
                    <td>รูปภาพรถ 2</td>
                    <td>
                        <div class="tx_red">ขนาดรูปภาพที่แนะนำ 640px X 480px</div>
                        <input type="file" id="c_photo2" name="c_photo2">
                        <input type="hidden" id="h_c_photo2" name="h_c_photo2" value="<?php echo $row_Re_c['c_photo2'];?>">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr>
                        <input name="Submit" type="submit" id="Submit" value="บันทึกข้อมูล"/>
                        <input type="hidden" id="c_user" name="c_user" value="<?php echo $_SESSION['AD_Name'];?>">
                        <input type="hidden" id="c_id" name="c_id" value="<?php echo $row_Re_c['c_id'];?>">
                    </td>
                </tr>
                <?php if($row_Re_c['c_photo1']!="" OR $row_Re_c['c_photo2']!=""){ ?>
                <tr>
                    <td colspan="2">
                        <hr>
                        <?php if($row_Re_c['c_photo1']!=""){ ?>
                        <div class="carprice_box">
                            <a href="../images/model/<?php echo $row_Re_c['c_photo1']; ?>"  rel="lightbox[]">
                                <div class="carprice_img_view"><img src="../images/model/<?php echo $row_Re_c['c_photo1']; ?>"/></div>
                            </a>
                            <div align="right">
                                ลบรูปภาพที่ 1&nbsp; <a href="admin_car_save.php?action=dele_p_carprice&c_id=<?php echo $row_Re_c['c_id']; ?>&photo=1" onclick="return confirm('ต้องการลบรูปภาพที่ 1 หรือไม่')" ><i class="fas fa-trash-alt tx_red"></i></a>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if($row_Re_c['c_photo2']!=""){ ?>
                        <div class="carprice_box">
                            <a href="../images/model/<?php echo $row_Re_c['c_photo2']; ?>"  rel="lightbox[]">
                                <div class="carprice_img_view"><img src="../images/model/<?php echo $row_Re_c['c_photo2']; ?>"/></div>
                            </a>
                            <div align="right">
                                ลบรูปภาพที่ 2&nbsp; <a href="admin_car_save.php?action=dele_p_carprice&c_id=<?php echo $row_Re_c['c_id']; ?>&photo=2" onclick="return confirm('ต้องการลบภาพที่ 2 หรือไม่')" ><i class="fas fa-trash-alt tx_red"></i></a>
                            </div>
                        </div>
                        <?php } ?>
                    </td>
                </tr>
                <?php } ?>
            </table>
		</form>
	</div>
	<?php } ?>

	<?php if(isset($_GET['save']) AND $_GET['save']='sucess'){ ?>
	<div id="fancy_sucess">
		<br><br><br><br>
		<p><img src="images/icon/anw_success.png" alt=""></p>
		<p>บันทึกรายการ ราคากลาง เรียบร้อย</p>
	</div>
	<?php } ?>
</body>
</html>
<?php $mysqli->close(); ?>