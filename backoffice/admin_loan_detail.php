<?php 
require_once('Connections/con_db.php');
include('function/f_admin.php');

if (isset($_GET['l_id'])) {$l_id_chk = mysqli_real_escape_string($mysqli, $_GET['l_id']);}
$sql_l="SELECT * FROM tb_loan WHERE l_id = '".$l_id_chk."'";
$Re_l=$mysqli->query($sql_l);
$row_Re_l=$Re_l->fetch_assoc();
?>
<!doctype html>
<html>
<head>
    <?php include 's_inc_header.php';?>
	<style>
	body,
	td,
	th {
		background-color: #ffffff;
	}

	</style>
	<script language="javascript" type="text/javascript">
        function printDiv(print_div) {
            var page_print = document.getElementById(print_div).innerHTML;
            var page_old = document.body.innerHTML;
            document.body.innerHTML = "<html><head><title></title></head><body>"+page_print+"</body>";
            window.print();
            document.body.innerHTML = page_old;
        }
    </script>
</head>

<body>
    <div id="fancy_title"><i class="fas fa-credit-card"></i>&nbsp;สมัครขอสินเชื่อ</div>
    <div id="fancy_box">
		<div id="print_div">
			<b>สมัครขอสินเชื่อ</b><br><br>
			<table class="tb1" width="100%" border="1">
				<tr>
					<td width="160"><div align="left">วันที่สมัคร</div></td>
					<td><?php echo datethai($row_Re_l['l_date_reg']); ?></td>
				</tr>
				<tr>
					<td width="160"><div align="left">ชื่อ-นามสกุล</div></td>
					<td><?php echo $row_Re_l['l_name'];?></td>
				</tr>
				<tr>
					<td width="160"><div align="left">จังหวัดที่อยู่ปัจจุบัน</div></td>
					<td><?php if(!empty($row_Re_l['l_province'])){echo $row_Re_l['l_province'];}else{echo "-";}?></td>
				</tr>
				<tr>
					<td width="160"><div align="left">เบอร์โทรศัพท์</div></td>
					<td><?php echo $row_Re_l['l_tel']; ?></td>
				</tr>
				<tr>
					<td><div align="left">ประเภทสมัครขอสินเชื่อ</div></td>
					<td><?php echo $row_Re_l['l_type']; ?></td>
				</tr>
			</table>
		</div>
		<br>
		<button onclick="javascript:printDiv('print_div')"><i class="fa fa-print" aria-hidden="true" style="color:#434343"></i>&nbsp;พิมพ์</button>
    </div>
</body>
</html>
<?php $mysqli->close(); ?>