<?php
if (!isset($_SESSION)) {session_start();}
while(list($xVarName, $xVarvalue) = each($_GET)) {
     ${$xVarName} = $xVarvalue;
}

while(list($xVarName, $xVarvalue) = each($_POST)) {
     ${$xVarName} = $xVarvalue;
}

while(list($xVarName, $xVarvalue) = each($_FILES)) {
     ${$xVarName."_name"} = $xVarvalue['name'];
     ${$xVarName."_type"} = $xVarvalue['type'];
     ${$xVarName."_size"} = $xVarvalue['size'];
     ${$xVarName."_error"} = $xVarvalue['error'];
     ${$xVarName} = $xVarvalue['tmp_name'];
}

function datethai_time($date){
    if($date>0){
        $_month_name = array(
        "01"=>"มกราคม",
        "02"=>"กุมภาพันธ์",
        "03"=>"มีนาคม",
        "04"=>"เมษายน",
        "05"=>"พฤษภาคม",
        "06"=>"มิถุนายน",
        "07"=>"กรกฏาคม",
        "08"=>"สิงหาคม",
        "09"=>"กันยายน",
        "10"=>"ตุลาคม",
        "11"=>"พฤศจิกายน",
        "12"=>"ธันวาคม");
        $yy=substr($date,0,4);
        $mm=substr($date,5,2);
        $dd=substr($date,8,2);
        $time=substr($date,11,8);
        $yy+=543;
        $datethai_time=intval($dd)." ".$_month_name[$mm]." ".$yy." ".$time;
        return $datethai_time;
    }else{
        $datethai_time="-";
        return $datethai_time;
    }
}

function datethai($date){
    if($date>0){
        $_month_name = array(
        "01"=>"มกราคม",
        "02"=>"กุมภาพันธ์",
        "03"=>"มีนาคม",
        "04"=>"เมษายน",
        "05"=>"พฤษภาคม",
        "06"=>"มิถุนายน",
        "07"=>"กรกฏาคม",
        "08"=>"สิงหาคม",
        "09"=>"กันยายน",
        "10"=>"ตุลาคม",
        "11"=>"พฤศจิกายน",
        "12"=>"ธันวาคม");
        $yy=substr($date,0,4);
        $mm=substr($date,5,2);
        $dd=substr($date,8,2);
        $time=substr($date,11,8);
        $yy+=543;
        $datethai=intval($dd)." ".$_month_name[$mm]." ".$yy;
        return $datethai;
    }else{
        $datethai="-";
        return $datethai;
    }
}

function datethai_time_sm($date){
    if($date>0){
        $_month_name = array(
        "01"=>"ม.ค.",
        "02"=>"ก.พ.",
        "03"=>"มี.ค.",
        "04"=>"เม.ย.",
        "05"=>"พ.ค.",
        "06"=>"มิ.ย.",
        "07"=>"ก.ค.",
        "08"=>"ส.ค.",
        "09"=>"ก.ย.",
        "10"=>"ต.ค.",
        "11"=>"พ.ย.",
        "12"=>"ธ.ค.");
        $yy=substr($date,0,4);
        $mm=substr($date,5,2);
        $dd=substr($date,8,2);
        $time=substr($date,11,8);
        $yy+=543;
        $datethai_time_sm=intval($dd)." ".$_month_name[$mm]." ".$yy." ".$time;
        return $datethai_time_sm;
    }else{
        $datethai_time_sm="-";
        return $datethai_time_sm;
    }
}

function datethai_sm($date){
    if($date>0){
        $_month_name = array(
        "01"=>"ม.ค.",
        "02"=>"ก.พ.",
        "03"=>"มี.ค.",
        "04"=>"เม.ย.",
        "05"=>"พ.ค.",
        "06"=>"มิ.ย.",
        "07"=>"ก.ค.",
        "08"=>"ส.ค.",
        "09"=>"ก.ย.",
        "10"=>"ต.ค.",
        "11"=>"พ.ย.",
        "12"=>"ธ.ค.");
        $yy=substr($date,0,4);
        $mm=substr($date,5,2);
        $dd=substr($date,8,2);
        $time=substr($date,11,8);
        $yy+=543;
        $datethai_sm=intval($dd)." ".$_month_name[$mm]." ".$yy;
        return $datethai_sm;
    }else{
        $datethai_sm="-";
        return $datethai_sm;
    }
}

function dateEng($date){
    if($date>0){
        $yy=substr($date,6,4);
        $mm=substr($date,3,2);
        $dd=substr($date,0,2);
        $yy-=543;
        $dateEng=$yy."-".$mm."-".$dd;
        return $dateEng;
    }else{
        $dateEng="-";
        return $dateEng;
    }
}

function dateEng_m($date){
    if($date>0){
        $yy=substr($date,2,2);
        $mm=substr($date,5,2);
        $dd=substr($date,8,2);
        $dateEng_m=$dd."/".$mm."/".$yy;
        return $dateEng_m;
    }else{
        $dateEng_m="-";
        return $dateEng_m;
    }
}

function dateTH($date){
    if($date>0){
        $yy=substr($date,0,4);
        $mm=substr($date,5,2);
        $dd=substr($date,8,2);
        $yy+=543;
        $dateTH=$dd."/".$mm."/".$yy;
        return $dateTH;
    }else{
        $dateTH="-";
        return $dateTH;
    }
}

function dateTH_m($date){
    if($date>0){
        $yy=substr($date,0,4);
        $mm=substr($date,5,2);
        $dd=substr($date,8,2);
        $yy+=543;
        $yym=substr($yy,2,2);
        $dateTH_m=$dd."/".$mm."/".$yym;
        return $dateTH_m;
    }else{
		$dateTH_m="-";
		return $dateTH_m;
	}
}
?>
